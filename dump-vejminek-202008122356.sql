-- MySQL dump 10.17  Distrib 10.3.22-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: vejminek
-- ------------------------------------------------------
-- Server version	10.3.22-MariaDB-0ubuntu0.19.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `device_port_values`
--

DROP TABLE IF EXISTS `device_port_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_port_values` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `device_id` int(10) unsigned NOT NULL DEFAULT 0,
  `port_identifier` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_value` float NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_port_values`
--

LOCK TABLES `device_port_values` WRITE;
/*!40000 ALTER TABLE `device_port_values` DISABLE KEYS */;
INSERT INTO `device_port_values` VALUES (1,14,'light',0),(2,15,'light',0);
/*!40000 ALTER TABLE `device_port_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL DEFAULT 0,
  `label` text COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `template` varchar(100) COLLATE utf8_bin NOT NULL,
  `data` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devices`
--

LOCK TABLES `devices` WRITE;
/*!40000 ALTER TABLE `devices` DISABLE KEYS */;
INSERT INTO `devices` VALUES (14,19,'Osvětlení','','cz.allcomp:Light',''),(15,19,'Regulované světlo','','cz.allcomp:ExpRegulatedLight',''),(18,19,'Analog - ukázka','','cz.allcomp:DiscreteWatcher',''),(19,20,'Spínané světlo','Test','cz.allcomp:LogicalWatcher',''),(21,20,'Rolety','','cz.allcomp:blinds','{\n	\"attributes\": {\n		\"unsafe_wind\": \"return false\"\n	}\n}');
/*!40000 ALTER TABLE `devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one_time_tasks`
--

DROP TABLE IF EXISTS `one_time_tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one_time_tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `execution_time` datetime NOT NULL,
  `executed` tinyint(1) NOT NULL DEFAULT 0,
  `type` int(10) unsigned NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one_time_tasks`
--

LOCK TABLES `one_time_tasks` WRITE;
/*!40000 ALTER TABLE `one_time_tasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `one_time_tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ports_links`
--

DROP TABLE IF EXISTS `ports_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ports_links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `driver` varchar(500) COLLATE utf8_bin NOT NULL,
  `driver_port` varchar(100) COLLATE utf8_bin NOT NULL,
  `device_id` int(10) unsigned NOT NULL,
  `device_port` varchar(100) COLLATE utf8_bin NOT NULL,
  `preferred_cfg` varchar(100) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ports_links`
--

LOCK TABLES `ports_links` WRITE;
/*!40000 ALTER TABLE `ports_links` DISABLE KEYS */;
INSERT INTO `ports_links` VALUES (34,'cz.din_u10:DIN_U10_1','in4',19,'input',''),(35,'cz.din_u10:DIN_U10_1','out9',19,'output',''),(36,'cz.din_u10:DIN_U10_1','out0',14,'light',''),(37,'cz.din_u10:DIN_U10_1','in0',14,'button',''),(40,'cz.din_u10:DIN_U10_1','in5',18,'input',''),(41,'cz.din_u10:DIN_U10_1','out8',18,'output','pwm'),(47,'cz.din_u10:DIN_U10_1','out2',21,'motor_up',''),(48,'cz.din_u10:DIN_U10_1','out3',21,'motor_down',''),(49,'cz.din_u10:DIN_U10_1','out1',15,'light',''),(50,'cz.din_u10:DIN_U10_1','in1',15,'button','');
/*!40000 ALTER TABLE `ports_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repeated_tasks`
--

DROP TABLE IF EXISTS `repeated_tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repeated_tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `days` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `execution_time` time NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_time_executed` datetime DEFAULT NULL,
  `disabled` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repeated_tasks`
--

LOCK TABLES `repeated_tasks` WRITE;
/*!40000 ALTER TABLE `repeated_tasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `repeated_tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` text COLLATE utf8_bin NOT NULL,
  `floor` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms`
--

LOCK TABLES `rooms` WRITE;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` VALUES (1,'Obývák',0),(19,'Kuchyně',0),(20,'Ložnice',0);
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `signals_links`
--

DROP TABLE IF EXISTS `signals_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `signals_links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `driver` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `driver_signal` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_id` int(10) unsigned NOT NULL,
  `device_signal` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `signals_links`
--

LOCK TABLES `signals_links` WRITE;
/*!40000 ALTER TABLE `signals_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `signals_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slots_links`
--

DROP TABLE IF EXISTS `slots_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slots_links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `driver` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `driver_slot` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_id` int(10) unsigned NOT NULL,
  `device_slot` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slots_links`
--

LOCK TABLES `slots_links` WRITE;
/*!40000 ALTER TABLE `slots_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `slots_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmp_config`
--

DROP TABLE IF EXISTS `tmp_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `property` text COLLATE utf8_bin NOT NULL,
  `value` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmp_config`
--

LOCK TABLES `tmp_config` WRITE;
/*!40000 ALTER TABLE `tmp_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `tmp_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmp_device_action_arguments`
--

DROP TABLE IF EXISTS `tmp_device_action_arguments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_device_action_arguments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `action_id` int(10) unsigned NOT NULL,
  `identifier` varchar(100) COLLATE utf8_bin NOT NULL,
  `type` int(11) NOT NULL,
  `label` text COLLATE utf8_bin NOT NULL,
  `default_value` text COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmp_device_action_arguments`
--

LOCK TABLES `tmp_device_action_arguments` WRITE;
/*!40000 ALTER TABLE `tmp_device_action_arguments` DISABLE KEYS */;
INSERT INTO `tmp_device_action_arguments` VALUES (1,3,'duration',0,'Trvání (sec)','3.000000');
/*!40000 ALTER TABLE `tmp_device_action_arguments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmp_device_actions`
--

DROP TABLE IF EXISTS `tmp_device_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_device_actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `device_id` int(10) unsigned NOT NULL,
  `identifier` varchar(100) COLLATE utf8_bin NOT NULL,
  `label` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmp_device_actions`
--

LOCK TABLES `tmp_device_actions` WRITE;
/*!40000 ALTER TABLE `tmp_device_actions` DISABLE KEYS */;
INSERT INTO `tmp_device_actions` VALUES (1,1,'turn_on','Rozsvítit'),(2,1,'turn_off','Zhasnout'),(3,1,'impulse','Rozvítit na ...'),(4,2,'turn_on','Rozsvítit'),(5,2,'turn_off','Zhasnout'),(6,3,'turn_on','Rozsvítit'),(7,3,'turn_off','Zhasnout'),(8,4,'turn_on','Rozsvítit'),(9,4,'turn_off','Zhasnout'),(10,8,'move_up','Posuv nahoru'),(11,8,'move_down','Posuv dolů'),(12,8,'stop','Stop');
/*!40000 ALTER TABLE `tmp_device_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmp_device_attributes`
--

DROP TABLE IF EXISTS `tmp_device_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_device_attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `device_id` int(10) unsigned NOT NULL,
  `identifier` varchar(100) COLLATE utf8_bin NOT NULL,
  `type` int(11) NOT NULL,
  `label` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmp_device_attributes`
--

LOCK TABLES `tmp_device_attributes` WRITE;
/*!40000 ALTER TABLE `tmp_device_attributes` DISABLE KEYS */;
INSERT INTO `tmp_device_attributes` VALUES (1,8,'motors_speed',0,'Rychlost motorů'),(2,8,'unsafe_wind',4,'Fouká silný vítr?');
/*!40000 ALTER TABLE `tmp_device_attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmp_device_emitted_events`
--

DROP TABLE IF EXISTS `tmp_device_emitted_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_device_emitted_events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `device_id` int(10) unsigned NOT NULL,
  `identifier` varchar(100) COLLATE utf8_bin NOT NULL,
  `label` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmp_device_emitted_events`
--

LOCK TABLES `tmp_device_emitted_events` WRITE;
/*!40000 ALTER TABLE `tmp_device_emitted_events` DISABLE KEYS */;
INSERT INTO `tmp_device_emitted_events` VALUES (1,8,'down','Zatáhly se'),(2,8,'up','Vytáhly se'),(3,8,'stopped','Zastavily'),(4,8,'movement_down_inhibited','Pohyb dolů zablokován');
/*!40000 ALTER TABLE `tmp_device_emitted_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmp_device_ports`
--

DROP TABLE IF EXISTS `tmp_device_ports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_device_ports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `device_id` int(10) unsigned NOT NULL,
  `identifier` varchar(100) COLLATE utf8_bin NOT NULL,
  `direction` int(11) NOT NULL,
  `mode` int(11) NOT NULL,
  `optional` tinyint(1) NOT NULL,
  `default_value` float NOT NULL DEFAULT 0,
  `value_stored` tinyint(1) NOT NULL DEFAULT 0,
  `label` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmp_device_ports`
--

LOCK TABLES `tmp_device_ports` WRITE;
/*!40000 ALTER TABLE `tmp_device_ports` DISABLE KEYS */;
INSERT INTO `tmp_device_ports` VALUES (1,1,'light',0,0,0,0,1,'Světlo'),(2,1,'button',1,0,0,0,0,'Spínač'),(3,2,'light',0,1,0,0,1,'Světlo'),(4,2,'button',1,0,0,0,0,'Spínač'),(5,3,'light',0,1,0,0,1,'Světlo'),(6,3,'button',1,0,0,0,0,'Spínač'),(7,4,'light',0,1,0,0,1,'Světlo'),(8,4,'button',1,0,0,0,0,'Spínač'),(9,5,'input',1,1,0,0,0,'Vstup'),(10,5,'output',0,1,0,0,0,'Výstup'),(11,6,'input',1,0,0,0,0,'Vstup'),(12,6,'output',0,0,0,0,0,'Výstup'),(13,7,'trigger',1,0,0,0,0,'Spoušť'),(14,8,'motor_up',0,0,0,0,0,'Motor - nahoru'),(15,8,'motor_down',0,0,0,0,0,'Motor - dolů'),(16,8,'push_button_up',1,0,0,0,0,'Tlačítko - nahoru'),(17,8,'push_button_down',1,0,0,0,0,'Tlačítko - dolů'),(18,8,'push_button_stop',1,0,0,0,0,'Tlačítko - stop'),(19,8,'limit_switch_upper',1,0,0,0,0,'Koncový spínač - horní'),(20,8,'limit_switch_lower',1,0,0,0,0,'Koncový spínač - dolní');
/*!40000 ALTER TABLE `tmp_device_ports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmp_device_signals`
--

DROP TABLE IF EXISTS `tmp_device_signals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_device_signals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `device_id` int(10) unsigned NOT NULL,
  `identifier` varchar(100) COLLATE utf8_bin NOT NULL,
  `label` text COLLATE utf8_bin NOT NULL,
  `direction` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmp_device_signals`
--

LOCK TABLES `tmp_device_signals` WRITE;
/*!40000 ALTER TABLE `tmp_device_signals` DISABLE KEYS */;
INSERT INTO `tmp_device_signals` VALUES (1,7,'trigger_sig','Spoušť',1);
/*!40000 ALTER TABLE `tmp_device_signals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmp_device_slots`
--

DROP TABLE IF EXISTS `tmp_device_slots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_device_slots` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `device_id` int(10) unsigned NOT NULL,
  `identifier` varchar(100) COLLATE utf8_bin NOT NULL,
  `type` int(11) NOT NULL,
  `label` text COLLATE utf8_bin NOT NULL,
  `direction` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmp_device_slots`
--

LOCK TABLES `tmp_device_slots` WRITE;
/*!40000 ALTER TABLE `tmp_device_slots` DISABLE KEYS */;
INSERT INTO `tmp_device_slots` VALUES (1,7,'number',0,'Number',1),(2,7,'result',1,'Result',0),(3,8,'speed',0,'Rychlost',1);
/*!40000 ALTER TABLE `tmp_device_slots` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmp_devices`
--

DROP TABLE IF EXISTS `tmp_devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_devices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plugin_identifier` varchar(100) COLLATE utf8_bin NOT NULL,
  `identifier` varchar(100) COLLATE utf8_bin NOT NULL,
  `label` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmp_devices`
--

LOCK TABLES `tmp_devices` WRITE;
/*!40000 ALTER TABLE `tmp_devices` DISABLE KEYS */;
INSERT INTO `tmp_devices` VALUES (1,'cz.allcomp','Light','Světlo'),(2,'cz.allcomp','RegulatedLight','Regulované světlo'),(3,'cz.allcomp','ExpRegulatedLight','Světlo regulované exponenciálním průběhem'),(4,'cz.allcomp','Sin2RegulatedLight','Světlo regulované průběhem sin^2'),(5,'cz.allcomp','DiscreteWatcher','Sledovač diskrétní hodnoty'),(6,'cz.allcomp','LogicalWatcher','Sledovač logické hodnoty'),(7,'cz.allcomp','TestDevice','Testovací zařízení'),(8,'cz.allcomp','blinds','Rolety');
/*!40000 ALTER TABLE `tmp_devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmp_driver_port_configurations`
--

DROP TABLE IF EXISTS `tmp_driver_port_configurations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_driver_port_configurations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `port_id` bigint(20) unsigned NOT NULL,
  `direction` int(10) unsigned NOT NULL,
  `mode` int(10) unsigned NOT NULL,
  `identifier` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmp_driver_port_configurations`
--

LOCK TABLES `tmp_driver_port_configurations` WRITE;
/*!40000 ALTER TABLE `tmp_driver_port_configurations` DISABLE KEYS */;
INSERT INTO `tmp_driver_port_configurations` VALUES (1,1,0,0,'digital','digital'),(2,1,0,1,'pwm','pwm'),(3,2,0,0,'digital','digital'),(4,2,0,1,'pwm','pwm'),(5,3,0,0,'digital','digital'),(6,3,0,1,'pwm','pwm'),(7,4,0,0,'digital','digital'),(8,4,0,1,'pwm','pwm'),(9,5,0,0,'digital','digital'),(10,5,0,1,'pwm','pwm'),(11,6,0,0,'digital','digital'),(12,6,0,1,'pwm','pwm'),(13,7,0,0,'digital','digital'),(14,7,0,1,'pwm','pwm'),(15,8,0,0,'digital','digital'),(16,8,0,1,'pwm','pwm'),(17,9,0,0,'digital','digital'),(18,9,0,1,'pwm','pwm'),(19,10,0,0,'digital','digital'),(20,10,0,1,'pwm','pwm'),(21,11,1,0,'digital','digital'),(22,11,1,1,'analog','analog'),(23,11,1,1,'counter_rising_edge','counter_rising_edge'),(24,11,1,1,'counter_falling_edge','counter_falling_edge'),(25,12,1,0,'digital','digital'),(26,12,1,1,'analog','analog'),(27,12,1,1,'counter_rising_edge','counter_rising_edge'),(28,12,1,1,'counter_falling_edge','counter_falling_edge'),(29,13,1,0,'digital','digital'),(30,13,1,1,'analog','analog'),(31,13,1,1,'counter_rising_edge','counter_rising_edge'),(32,13,1,1,'counter_falling_edge','counter_falling_edge'),(33,14,1,0,'digital','digital'),(34,14,1,1,'analog','analog'),(35,14,1,1,'counter_rising_edge','counter_rising_edge'),(36,14,1,1,'counter_falling_edge','counter_falling_edge'),(37,15,1,0,'digital','digital'),(38,15,1,1,'analog','analog'),(39,15,1,1,'counter_rising_edge','counter_rising_edge'),(40,15,1,1,'counter_falling_edge','counter_falling_edge'),(41,16,1,0,'digital','digital'),(42,16,1,1,'analog','analog'),(43,16,1,1,'counter_rising_edge','counter_rising_edge'),(44,16,1,1,'counter_falling_edge','counter_falling_edge'),(45,17,1,0,'digital','digital'),(46,17,1,1,'analog','analog'),(47,17,1,1,'counter_rising_edge','counter_rising_edge'),(48,17,1,1,'counter_falling_edge','counter_falling_edge'),(49,18,1,0,'digital','digital'),(50,18,1,1,'analog','analog'),(51,18,1,1,'counter_rising_edge','counter_rising_edge'),(52,18,1,1,'counter_falling_edge','counter_falling_edge'),(53,19,1,0,'digital','digital'),(54,19,1,1,'analog','analog'),(55,19,1,1,'counter_rising_edge','counter_rising_edge'),(56,19,1,1,'counter_falling_edge','counter_falling_edge'),(57,20,1,0,'digital','digital'),(58,20,1,1,'analog','analog'),(59,20,1,1,'counter_rising_edge','counter_rising_edge'),(60,20,1,1,'counter_falling_edge','counter_falling_edge');
/*!40000 ALTER TABLE `tmp_driver_port_configurations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmp_driver_ports`
--

DROP TABLE IF EXISTS `tmp_driver_ports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_driver_ports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `driver_id` int(10) unsigned NOT NULL,
  `identifier` varchar(100) COLLATE utf8_bin NOT NULL,
  `label` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmp_driver_ports`
--

LOCK TABLES `tmp_driver_ports` WRITE;
/*!40000 ALTER TABLE `tmp_driver_ports` DISABLE KEYS */;
INSERT INTO `tmp_driver_ports` VALUES (1,1,'out0','Výstup 0'),(2,1,'out1','Výstup 1'),(3,1,'out2','Výstup 2'),(4,1,'out3','Výstup 3'),(5,1,'out4','Výstup 4'),(6,1,'out5','Výstup 5'),(7,1,'out6','Výstup 6'),(8,1,'out7','Výstup 7'),(9,1,'out8','Výstup 8'),(10,1,'out9','Výstup 9'),(11,1,'in0','Vstup 0'),(12,1,'in1','Vstup 1'),(13,1,'in2','Vstup 2'),(14,1,'in3','Vstup 3'),(15,1,'in4','Vstup 4'),(16,1,'in5','Vstup 5'),(17,1,'in6','Vstup 6'),(18,1,'in7','Vstup 7'),(19,1,'in8','Vstup 8'),(20,1,'in9','Vstup 9');
/*!40000 ALTER TABLE `tmp_driver_ports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmp_driver_signals`
--

DROP TABLE IF EXISTS `tmp_driver_signals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_driver_signals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `driver_id` int(10) unsigned NOT NULL,
  `identifier` varchar(100) COLLATE utf8_bin NOT NULL,
  `label` text COLLATE utf8_bin NOT NULL,
  `direction` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmp_driver_signals`
--

LOCK TABLES `tmp_driver_signals` WRITE;
/*!40000 ALTER TABLE `tmp_driver_signals` DISABLE KEYS */;
/*!40000 ALTER TABLE `tmp_driver_signals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmp_driver_slots`
--

DROP TABLE IF EXISTS `tmp_driver_slots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_driver_slots` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `driver_id` int(10) unsigned NOT NULL,
  `identifier` varchar(100) COLLATE utf8_bin NOT NULL,
  `type` int(11) NOT NULL,
  `label` text COLLATE utf8_bin NOT NULL,
  `direction` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmp_driver_slots`
--

LOCK TABLES `tmp_driver_slots` WRITE;
/*!40000 ALTER TABLE `tmp_driver_slots` DISABLE KEYS */;
/*!40000 ALTER TABLE `tmp_driver_slots` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmp_drivers`
--

DROP TABLE IF EXISTS `tmp_drivers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_drivers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plugin_identifier` varchar(100) COLLATE utf8_bin NOT NULL,
  `identifier` varchar(100) COLLATE utf8_bin NOT NULL,
  `label` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmp_drivers`
--

LOCK TABLES `tmp_drivers` WRITE;
/*!40000 ALTER TABLE `tmp_drivers` DISABLE KEYS */;
INSERT INTO `tmp_drivers` VALUES (1,'cz.din_u10','DIN_U10_1','DIN-U10 #1');
/*!40000 ALTER TABLE `tmp_drivers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmp_plugins`
--

DROP TABLE IF EXISTS `tmp_plugins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_plugins` (
  `identifier` varchar(100) COLLATE utf8_bin NOT NULL,
  `label` text COLLATE utf8_bin NOT NULL,
  `version` text COLLATE utf8_bin NOT NULL,
  `author` text COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `entry_file` text COLLATE utf8_bin NOT NULL,
  `directory` text COLLATE utf8_bin NOT NULL,
  `valid` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmp_plugins`
--

LOCK TABLES `tmp_plugins` WRITE;
/*!40000 ALTER TABLE `tmp_plugins` DISABLE KEYS */;
INSERT INTO `tmp_plugins` VALUES ('cz.allcomp','Allcomp','1.0','Samuel Trávníček','Standard library provided by Allcomp a.s.','main.lua','./build/plugins/allcomp',1),('cz.din_u10','DIN U10','1.0','Samuel Trávníček','Driver for DIN-U10 device','main.lua','./build/plugins/din-u10',1);
/*!40000 ALTER TABLE `tmp_plugins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(100) COLLATE utf8_bin NOT NULL,
  `permissions` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'travnicek','$argon2id$v=19$m=65536,t=2,p=1$fl00HgALfvWS9FNM0Y0rGQ$Fz7h4Y+UyZ2U94aiI29VjYKQdgPbelpRgGk+kR1MDK4',100);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'vejminek'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-12 23:56:28
