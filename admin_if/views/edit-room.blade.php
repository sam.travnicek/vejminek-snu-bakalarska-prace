@extends("layouts.admin")
@section("title", "Upravit místnost")

@section('head')
    <link rel="stylesheet" href="{{ BASE_PATH }}/css/edit-room.section.css" />
    <script src="{{ BASE_PATH }}/js/modules/url-utils.js"></script>
@endsection

@section("content")
    <header>Místnosti &#x203A; Upravit místnost</header>
    <section class="edit-room">
        @isset($_POST["submit"])
            @php $table->updateByPrimaryKey($roomId, $_POST["room-name"], $_POST["room-floor"]); @endphp
            <div class="info">Změny byly uloženy.</div>
        @endisset
        @forelse($table->selectByPrimaryKey($roomId) as $row)
            <form action="#" method="POST" class="w100">
                <input name="room-floor" type="hidden" value="{{ $row->floor }}">
                <div class="edit-room-form w100">
                    <div class="text-input">
                        <div class="label">Název místnosti</div>
                        <input name="room-name" type="text" placeholder="Název" value="{{ $row->label }}">
                    </div>
                    <div class="controls">
                        <button name="submit" class="btn-add" value="">Uložit změny</button>
                    </div>
                </div>
            </form>
            @break
        @empty
            <div class="error">Místnost nebyla nalezena.</div>
        @endforelse
    </section>
    <footer class="footer">
        <p>Autorem této aplikace je <strong>Samuel Trávníček.</strong></p>
    </footer>
@endsection