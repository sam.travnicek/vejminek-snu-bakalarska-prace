<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>VejminekAdmin | Error @yield('error-number')</title>
    <link rel="stylesheet" href="{{ BASE_PATH }}/vendor/google-font-Montserrat/Montserrat.css">
    <link rel="stylesheet" href="{{ BASE_PATH }}/vendor/google-font-Dancing-Script/Dancing-Script.css">
    <link rel="stylesheet" href="{{ BASE_PATH }}/css/error.css">
    <link rel="stylesheet" href="{{ BASE_PATH }}/vendor/font-awesome/css/font-awesome.min.css">
</head>
<body>
    <div class="wrapper">
        <h1>@yield('error-number')</h1>
        <p>@yield('message')</p>
    </div>
</body>
</html>
