@yield('file-head')

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>VejminekAdmin | Script</title>

    <script src="{{ BASE_PATH }}/vendor/jquery/js/jquery-3.4.1.min.js"></script>

    @yield('head')
</head>
<body>
    @yield('body')

    @if(redirectEnabled())
        <script>
        {!! generateRedirectScript() !!}
        </script>
    @endif
</body>
</html>