<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>VejminekAdmin | @yield('title')</title>

    <link rel="stylesheet" href="{{ BASE_PATH }}/vendor/google-font-Montserrat/Montserrat.css">
    <link rel="stylesheet" href="{{ BASE_PATH }}/vendor/google-font-Dancing-Script/Dancing-Script.css">
    <link rel="stylesheet" href="{{ BASE_PATH }}/css/main.css">
    <link rel="stylesheet" href="{{ BASE_PATH }}/vendor/font-awesome/css/all.min.css">

    <script src="{{ BASE_PATH }}/vendor/jquery/js/jquery-3.4.1.min.js"></script>
    <script src="{{ BASE_PATH }}/vendor/pushjs/serviceWorker.min.js"></script>
    <script src="{{ BASE_PATH }}/vendor/pushjs/push.min.js"></script>

    <script src="{{ BASE_PATH }}/js/modules/jquery-utils.js"></script>
    <script src="{{ BASE_PATH }}/js/general.js"></script>

    @yield('head')
</head>
<body>
<noscript class="noscript-error">
    <p>Your browser does not support JavaScript!</p>
    <p>Unfortunatelly, this application needs JavaScript working.</p>
</noscript>
<div class="site hidden">
    <header class="logo">
        <i class="icon fa fa-home"></i>
        <span>Vejminek&nbsp;II</span>
        <i class="menu-btn menu-closed"></i>
    </header>

    <header class="site-header"></header>

    <aside class="sidebar scrollable-y menu-closed">
        {{-- <div class="scrollbar"></div> --}}
        <nav>
            <ul>
                <li>
                    <a href="{{ BASE_PATH }}/">
                        <i class="fa fa-laptop"></i>
                        <span>Přehled</span>
                    </a>
                </li>
                <li>
                    <a href="{{ BASE_PATH }}/logout">
                        <i class="fas fa-sign-out-alt"></i>
                        <span>Odhlásit se</span>
                    </a>
                </li>
            </ul>
        </nav>
        <h2>Server</h2>
        <nav>
            <ul>
                <li>
                    <a href="{{ BASE_PATH }}/server/plugins">
                        <i class="fa fa-cubes"></i>
                        <span>Zásuvné moduly</span>
                    </a>
                </li>
                <li>
                    <a href="{{ BASE_PATH }}/server/devices">
                        <i class="fa fa-cogs"></i>
                        <span>Zařízení</span>
                    </a>
                </li>
                {{-- <li>
                    <a href="{{ BASE_PATH }}#">
                        <i class="fa fa-hdd"></i>
                        <span>Řadiče</span>
                    </a>
                </li>
                <li>
                    <a href="{{ BASE_PATH }}#">
                        <i class="fa fa-scroll"></i>
                        <span>Skripty</span>
                    </a>
                </li> --}}
            </ul>
        </nav>
        <h2>Ostatní</h2>
        <nav>
            <ul>
                <li>
                    <a href="{{ BASE_PATH }}/rooms">
                        <i class="fa fa-door-open"></i>
                        <span>Místnosti</span>
                    </a>
                </li>
            </ul>
        </nav>
    </aside>

    <main class="main-content scrollable">
        @yield('content')
    </main>
</div>
{{-- <div class="prompt">
    <div class="window">
        <div class="header">
            <i class="fas fa-times" data-close-btn="true"></i>
        </div>
        <div class="message">
            <p>Opravdu chcete smazat místnost Obývák?</p>
        </div>
        <div class="accept">
            Potvrdit
        </div>
        <div class="cancel">
            Zrušit
        </div>
    </div>
</div> --}}
</body>
</html>