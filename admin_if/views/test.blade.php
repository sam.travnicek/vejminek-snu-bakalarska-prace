@php
    setRedirect("/script/test", ["x" => 123, "y" => "aha"], "GET", 4000);
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="{{ BASE_PATH }}/vendor/jquery/js/jquery-3.4.1.min.js"></script>
    <title>Responsive Table</title>
    <script>
        location.href = '{{ BASE_PATH }}/script/test';
    </script>
</head>
<body>
</body>
</html>