@extends("layouts.admin")
@section("title", "Server | Přidat zařízení")

@section('head')
    <link rel="stylesheet" href="{{ BASE_PATH }}/css/server-new-device.section.css" />
    <script src="{{ BASE_PATH }}/js/modules/url-utils.js"></script>
    <script src="{{ BASE_PATH }}/js/modules/input-utils.js"></script>

    <script>
        $(function() {
            interactiveSelect(".select-container");
        });
    </script>
@endsection

@section("content")
    <header>Server &#x203A; Přidat zařízení</header>
    <section class="server-new-device">
        @isset($_POST["submit"])
            @if(strlen($_POST["device-name"]) > 0)
                {{-- @php $table->insert(NULL, $_GET["room-name"], $_GET["room-floor"]); @endphp --}}
                @if($tableDevices->safeInsert($_POST["device-name"], $_POST["room-id"]))
                    {{-- <div class="info">Zařízení '{{ $_POST["device-name"] }}' R#{{ $_POST["room-id"] }} bylo přidáno.</div> --}}
                    <script>location.href = "{{ BASE_PATH }}/server/edit-device/{{ DB::lastInsertId() }}";</script>
                @else
                    <div class="error">Zařízení nebylo přidáno - neplatná místnost.</div>
                @endif
            @else
                <div class="error">Zařízení nebylo přidáno - prázdný název.</div>
            @endif
        @endisset
        <form action="#" method="POST" class="w100">
            <div class="new-device-form w100">
                <div class="text-input">
                    <div class="label">Název zařízení</div>
                    <input name="device-name" type="text" placeholder="Název">
                </div>
                <div class="select-input">
                    <div class="label">Místnost</div>
                    <div class="select-container">
                        <select name="room-id">
                            @foreach($tableRooms->selectAll() as $room)
                                <option value="{{ $room->id }}">{{ $room->label }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="controls">
                    <button name="submit" class="btn-add" value="">Přidat zařízení</button>
                </div>
            </div>
        </form>
    </section>
    <footer class="footer">
        <p>Autorem této aplikace je <strong>Samuel Trávníček.</strong></p>
    </footer>
@endsection