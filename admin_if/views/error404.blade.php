@extends("layouts.error")
@section("title", "Not found")
@section("error-number", "404")

@section("message", "Požadovaný obsah nenalezen.")