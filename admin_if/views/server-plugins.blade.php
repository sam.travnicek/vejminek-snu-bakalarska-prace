@extends("layouts.admin")
@section("title", "Server | Zásuvné moduly")

@section('head')
    <link rel="stylesheet" href="{{ BASE_PATH }}/css/server-devices.section.css" />
@endsection

@section("content")
    <header>Server &#x203A; Zásuvné moduly</header>
    <section class="server-devices">
        <div class="table-wrapper w100">
            <table class="server-devices-table" id="server-devices-table">
                <thead>
                    <tr>
                        <th>Identifikátor</th>
                        <th>Název</th>
                        <th>Verze</th>
                        <th>Autor</th>
                        <th>Stav</th>
                    </tr>
                </thead>
                <tbody class="table-hover">
                    @forelse($table->selectAll() as $row)
                    <tr>
                        <td>{{ $row->identifier }}</td>
                        <td>{{ $row->label }}</td>
                        <td>{{ $row->version }}</td>
                        <td>{{ $row->author }}</td>
                        <td>{{ $row->valid ? "V pořádku" : "Poškozen" }}</td>
                    </tr>
                    @empty

                    @endforelse
                </tbody>
            </table>
        </div>
    </section>
    <footer class="footer">
            <p>Autorem této aplikace je <strong>Samuel Trávníček.</strong></p>
    </footer>
@endsection