@extends("layouts.admin")
@section("title", "Server | Upravit zařízení")

@section('head')
    <link rel="stylesheet" href="{{ BASE_PATH }}/css/server-edit-device.section.css" />
    <script src="{{ BASE_PATH }}/js/modules/input-utils.js"></script>
    <script src="{{ BASE_PATH }}/js/modules/Table.js"></script>
    <script>
        $(function() {
            interactiveSelect(".select-container");
            initSelectTextInput("#device-type-container", "--- Vlastní hodnota ---");

            let tablePorts = new Table("#ports-table");

            tablePorts.setup({
                clickableRows: true
            });

            tablePorts.on('click', function() {
                let id = $(this).data("port-id");
                location.href = "{{ BASE_PATH }}/server/edit-device/{{ $deviceId }}/port/" + id;
            });

            let tableSlots = new Table("#slots-table");

            tableSlots.setup({
                clickableRows: true
            });

            tableSlots.on('click', function() {
                let id = $(this).data("slot-id");
                location.href = "{{ BASE_PATH }}/server/edit-device/{{ $deviceId }}/slot/" + id;
            });

            let tableSignals = new Table("#signals-table");

            tableSignals.setup({
                clickableRows: true
            });

            tableSignals.on('click', function() {
                let id = $(this).data("signal-id");
                location.href = "{{ BASE_PATH }}/server/edit-device/{{ $deviceId }}/signal/" + id;
            });

            let tableAttributes = new Table("#attributes-table");

            tableAttributes.setup({
                clickableRows: true
            });

            tableAttributes.on('click', function() {
                // let id = $(this).data("attribute-id");
                // location.href = "{{ BASE_PATH }}/server/edit-device/{{ $deviceId }}/attribute/" + id;
            });
        });
    </script>
@endsection

@section("content")
    <header>Server &#x203A; Upravit zařízení</header>
    <section class="server-edit-device">
        @isset($_POST["submit-basic"])
            @php $tableDevices->safeUpdate(
                $deviceId, $_POST["room-id"], $_POST["device-label"], $_POST["device-description"],
                $_POST["device-template"], ""); @endphp
            <div class="info">Změny byly uloženy.</div>
        @endisset
        @forelse($tableDevices->selectByPrimaryKey($deviceId) as $row)
            <form action="#" method="POST" class="w100">
                <div class="edit-device-form w100">
                    <div class="text-input">
                        <div class="label">Název zařízení</div>
                        <input name="device-label" type="text" placeholder="Název" value="{{ $row->label }}">
                    </div>
                    <div class="select-input">
                        <div class="label">Místnost</div>
                        <div class="select-container">
                            <select name="room-id">
                                @foreach($tableRooms->selectAll() as $room)
                                    <option value="{{ $room->id }}"{{ $row->room_id == $room->id ? " selected" : "" }}>{{ $room->label }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="textarea-input">
                        <div class="label">Popis zařízení</div>
                        <textarea name="device-description" style="height: 6rem">{{ $row->description }}</textarea>
                    </div>
                    <div class="select-text-input" id="device-type-container">
                        <div class="label">Typ zařízení</div>
                        <div class="select-container">
                            <select>
                                @foreach($tableDeviceTemplates->selectAll() as $dtempl)
                                    <option value="{{ $dtempl->plugin_identifier }}:{{ $dtempl->identifier }}">{{ $dtempl->label }}</option>
                                @endforeach
                            </select>
                        </div>
                        <input name="device-template" type="text" placeholder="Typ" value="{{ $row->template }}" spellcheck="false">
                    </div>
                    <div class="controls">
                        <button name="submit-basic" class="btn-add" value="">Uložit změny</button>
                    </div>
                </div>
            </form>
            @break
        @empty
            <div class="error">Zařízení nebylo nalezeno.</div>
        @endforelse

        @php
            $deviceTemplate = "";

            foreach($tableDevices->selectByPrimaryKey($deviceId) as $row) {
                $deviceTemplate = $row->template;
                break;
            }

            $deviceTemplateId = 0;

            if($deviceTemplate != "") {
                $templParts = explode(":", $deviceTemplate);
                $pluginIdentifier = $templParts[0];
                $deviceIdentifier = $templParts[1];
                $res = $tableDeviceTemplates->selectByIdentifiers(
                    $pluginIdentifier, $deviceIdentifier);

                foreach($res as $row) {
                    $deviceTemplateId = $row->id;
                    break;
                }
            }
        @endphp

        <div class="edit-attributes-box w100">
            <h2 class="title">Atributy zařízení</h2>
            <div class="table-wrapper content">
                <table class="attributes-table interactive no-select" id="attributes-table">
                    <thead>
                        <tr>
                            <th>Název</th>
                            <th>Typ</th>
                        </tr>
                    </thead>
                    <tbody class="table-hover">
                        @php
                            $attributeTypes = ["Číslo", "Logická hodnota", "Text", "Čas", "Podmínka"]
                        @endphp
                        @forelse($tableDeviceAttributes->selectByDeviceId($deviceTemplateId) as $row)
                        <tr data-attribute-id="{{ $row->identifier }}">
                            <td>{{ $row->label }}</td>
                            <td>{{ $attributeTypes[$row->type] }}</td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="4">Nebyly nalezeny žádné atributy. </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>

        <div class="edit-ports-box w100">
            <h2 class="title">Porty zařízení</h2>
            <div class="table-wrapper content">
                <table class="ports-table interactive no-select" id="ports-table">
                    <thead>
                        <tr>
                            <th>Název</th>
                            <th>Typ</th>
                            <th>Režim</th>
                        </tr>
                    </thead>
                    <tbody class="table-hover">
                        @forelse($tableDevicePorts->selectByDeviceId($deviceTemplateId) as $row)
                        <tr data-port-id="{{ $row->identifier }}">
                            <td>{{ $row->label }}</td>
                            <td>{{ $row->direction == 0 ? "Výstup" : "Vstup "}}</td>
                            <td>{{ $row->mode == 0 ? "Logický" : "Diskrétní "}}</td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="4">Nebyly nalezeny žádné porty.</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>

        <div class="edit-slots-box w100">
            <h2 class="title">Sloty zařízení</h2>
            <div class="table-wrapper content">
                <table class="slots-table interactive no-select" id="slots-table">
                    <thead>
                        <tr>
                            <th>Název</th>
                            <th>Směr</th>
                            <th>Hodnota</th>
                        </tr>
                    </thead>
                    <tbody class="table-hover">
                        @forelse($tableDeviceSlots->selectByDeviceId($deviceTemplateId) as $row)
                        <tr data-slot-id="{{ $row->identifier }}">
                            <td>{{ $row->label }}</td>
                            <td>{{ $row->direction == 0 ? "Z řadiče" : "Do řadiče" }}</td>
                            <td>{{ $row->type == 0 ? "Číselná" : ($row->type == 1 ? "Logická" : ($row->type == 2 ? "Textová" : "Neznámá")) }}</td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="4">Nebyly nalezeny žádné sloty. </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>

        <div class="edit-signals-box w100">
            <h2 class="title">Signály zařízení</h2>
            <div class="table-wrapper content">
                <table class="signals-table interactive no-select" id="signals-table">
                    <thead>
                        <tr>
                            <th>Název</th>
                            <th>Směr</th>
                        </tr>
                    </thead>
                    <tbody class="table-hover">
                        @forelse($tableDeviceSignals->selectByDeviceId($deviceTemplateId) as $row)
                        <tr data-signal-id="{{ $row->identifier }}">
                            <td>{{ $row->label }}</td>
                            <td>{{ $row->direction == 0 ? "Z řadiče" : "Do řadiče" }}</td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="4">Nebyly nalezeny žádné signály. </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <footer class="footer">
        <p>Autorem této aplikace je <strong>Samuel Trávníček.</strong></p>
    </footer>
@endsection