@extends("layouts.admin")
@section("title", "Server | Zařízení")

@section('head')
    <link rel="stylesheet" href="{{ BASE_PATH }}/css/server-devices.section.css" />
    <script src="{{ BASE_PATH }}/js/modules/Table.js"></script>
    <script src="{{ BASE_PATH }}/js/modules/Prompt.js"></script>
    <script src="{{ BASE_PATH }}/js/modules/url-utils.js"></script>
    <script>
        $(document).ready(function() {
            let table = new Table("#server-devices-table");

            table.setup({
                editableRows: true,
                clickableRows: true
            });

            let updateButtons = function()
            {
                let cnt = table.selectedRows().length;

                if(cnt == 1)
                    $("#btn-edit").prop("disabled", false);
                else
                    $("#btn-edit").prop("disabled", true);

                if(cnt > 0)
                    $("#btn-delete").prop("disabled", false);
                else
                    $("#btn-delete").prop("disabled", true);
            };

            table.on('select', updateButtons);
            table.on('unselect', updateButtons);

            let $btnDelete = $("#btn-delete");
            let $btnEdit = $("#btn-edit");

            $btnDelete.on("click", function() {

                let prompt = new Prompt("Opravdu chcete smazat vybraná zařízení? ");

                prompt.on('accept', function() {
                    $selectedRows = table.selectedRows();
                    let ids = [];
                    $selectedRows.each(function(i) {
                        ids.push($(this).data("device-id"));
                    });

                    URLRedirectPost(location.href, {
                        delete: URLEncodeObject(ids)
                    });
                });
            });

            $btnEdit.on('click', function() {
                location.href = '{{ BASE_PATH }}/server/edit-device/' + table.selectedRows().first().data("device-id");
            });
        });
    </script>
@endsection

@section("content")
    <header>Server &#x203A; Zařízení</header>
    <section class="server-devices">
        @isset($_POST["delete"])
            @php $ids = URLDecodeObject($_POST["delete"]); @endphp
            @foreach($ids as $id)
                @php $table->safeDelete($id); @endphp
            @endforeach
            <div class="info">Vybraná zařízení byla smazána.</div>
        @endisset
        <div class="control-panel w100">
            <a href="{{ BASE_PATH }}/server/new-device"><button id="btn-new"><i class="fas fa-plus-square"></i><span>Přidat zařízení</span></button></a>
            <button id="btn-delete" disabled><i class="fas fa-trash-alt"></i><span>Smazat</span></button>
            <button id="btn-edit" disabled><i class="fas fa-pencil-alt"></i><span>Upravit</span></button>
        </div>
        <div class="table-wrapper w100">
            <table class="server-devices-table interactive" id="server-devices-table">
                <thead>
                    <tr>
                        <th><input type="checkbox"></th>
                        <th>ID</th>
                        <th>Název</th>
                        <th>Místnost</th>
                    </tr>
                </thead>
                <tbody class="table-hover">
                    @forelse($table->devices(0, 100) as $row)
                    <tr>
                        <td><input type="checkbox" data-device-id="{{ $row->id }}"></td>
                        <td>{{ $row->id }}</td>
                        <td>{{ $row->label }}</td>
                        <td>{{ $row->room }}</td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="4">Nebyla nalezena žádná zařízení.</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </section>
    <footer class="footer">
            <p>Autorem této aplikace je <strong>Samuel Trávníček.</strong></p>
    </footer>
@endsection