@extends("layouts.admin")
@section("title", "Test")

@section('head')
    <link rel="stylesheet" href="{{ BASE_PATH }}/css/test.section.css" />
@endsection

@section("content")
    <header>Přehled</header>
    <section class="test">
    </section>
    <footer class="footer">
            <p>Autorem této aplikace je <strong>Samuel Trávníček.</strong></p>
    </footer>
@endsection