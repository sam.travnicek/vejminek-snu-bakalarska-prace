@extends("layouts.admin")
@section("title", "Místnosti")

@section('head')
    <link rel="stylesheet" href="{{ BASE_PATH }}/css/rooms.section.css" />
    <script src="{{ BASE_PATH }}/js/modules/Table.js"></script>
    <script src="{{ BASE_PATH }}/js/modules/Prompt.js"></script>
    <script src="{{ BASE_PATH }}/js/modules/url-utils.js"></script>
    <script>
        $(document).ready(function() {
            let table = new Table("#rooms-table");

            table.setup({
                editableRows: true,
                clickableRows: true
            });

            let updateButtons = function()
            {
                let cnt = table.selectedRows().length;

                if(cnt == 1)
                    $("#btn-edit").prop("disabled", false);
                else
                    $("#btn-edit").prop("disabled", true);

                if(cnt > 0)
                    $("#btn-delete").prop("disabled", false);
                else
                    $("#btn-delete").prop("disabled", true);
            };

            table.on('select', updateButtons);
            table.on('unselect', updateButtons);

            let $btnDelete = $("#btn-delete");
            let $btnEdit = $("#btn-edit");

            $btnDelete.on("click", function() {

                let prompt = new Prompt("Opravdu chcete smazat vybrané místnosti? ");

                prompt.on('accept', function() {
                    $selectedRows = table.selectedRows();
                    let ids = [];
                    $selectedRows.each(function(i) {
                        ids.push($(this).data("room-id"));
                    });

                    URLRedirectPost(location.href, {
                        delete: URLEncodeObject(ids)
                    });
                });
            });

            $btnEdit.on('click', function() {
                location.href = '{{ BASE_PATH }}/edit-room/' + table.selectedRows().first().data("room-id");
            });
        });
    </script>
@endsection

@section("content")
    <header>Místnosti</header>
    <section class="rooms">
        @isset($_POST["delete"])
            @php $ids = URLDecodeObject($_POST["delete"]); @endphp
            @foreach($ids as $id)
                @php $res = $table->safeDelete($id); @endphp
                    @if($res["label"])
                        @if($res["deleted"])
                            <div class="info">Místnost '{{ $res["label"] }}' byla smazána.</div>
                        @else
                            <div class="error">Místnost '{{ $res["label"] }}' nemohla být smazána, protože obsahuje nenulový počet zařízení.</div>
                        @endif
                    @endif
            @endforeach
        @endisset
        <div class="control-panel w100">
            <a href="{{ BASE_PATH }}/new-room"><button id="btn-new"><i class="fas fa-plus-square"></i><span>Přidat místnost</span></button></a>
            <button id="btn-delete" disabled><i class="fas fa-trash-alt"></i><span>Smazat</span></button>
            <button id="btn-edit" disabled><i class="fas fa-pencil-alt"></i><span>Upravit</span></button>
        </div>
        <div class="table-wrapper w100">
            <table class="rooms-table interactive no-select" id="rooms-table">
                <thead>
                    <tr>
                        <th><input type="checkbox" name="" id=""></th>
                        <th>Název</th>
                    </tr>
                </thead>
                <tbody class="table-hover">
                    @forelse($table->selectAll() as $row)
                    <tr>
                        <td><input type="checkbox" data-room-id="{{ $row->id }}"></td>
                        <td>{{ $row->label }}</td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="2">Nebyly nalezeny žádné místnosti.</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </section>
    <footer class="footer">
            <p>Autorem této aplikace je <strong>Samuel Trávníček.</strong></p>
    </footer>
@endsection