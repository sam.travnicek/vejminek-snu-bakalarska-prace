@extends("layouts.admin")
@section("title", "Server | Upravit signál")

@php
    $allDriverSignals = [];
    if($signalDescriptor != null)
        $allDriverSignals = $tableDriverSignals
            ->selectByDirection($signalDescriptor->direction);
@endphp

@section('head')
    <link rel="stylesheet" href="{{ BASE_PATH }}/css/server-edit-device-signal.section.css" />
    <script src="{{ BASE_PATH }}/js/modules/input-utils.js"></script>
    <script>
        $(function() {
            interactiveSelect(".select-container");
            initSelectTextInput("#driver-link-container", "--- Vlastní hodnota ---");
        });
    </script>
@endsection

@section("content")
    <header>Server &#x203A; Upravit signál</header>
    <section class="server-edit-device-signal">
        @isset($_POST["submit"])
            @php
                if($_POST['driver-linkage'] != "") {
                    $submitDriverAndSignal = explode(":", $_POST['driver-linkage']);
                    $submitDriver = $submitDriverAndSignal[0] . ":" . $submitDriverAndSignal[1];
                    $submitSignal = $submitDriverAndSignal[2];
                    $tableSignalsLinks->insertOrUpdate(
                        $submitDriver, $submitSignal, $deviceId, $signalDescriptor->identifier);
                } else {
                    $tableSignalsLinks->deleteByDeviceSignal($deviceId, $signalDescriptor->identifier);
                }
            @endphp
            <div class="info">Změny byly uloženy.</div>
        @endisset
        @php
            $linkRows = $tableSignalsLinks->selectByDeviceSignal($deviceId, $signalDescriptor->identifier);
            $driverLinkValue = "";

            if(count($linkRows) != 0)
                $driverLinkValue = $linkRows[0]->driver . ":" . $linkRows[0]->driver_signal;
        @endphp
        @if($signalDescriptor != null)
            <div class="table-wrapper signal-info w100">
                <table>
                    <tr>
                        <td>Název signálu:</td>
                        <td>{{ $signalDescriptor->label }}</td>
                    </tr>
                    <tr>
                        <td>Identifikátor:</td>
                        <td>{{ $signalDescriptor->identifier }}</td>
                    </tr>
                    <tr>
                        <td>Směr:</td>
                        <td>{{ $signalDescriptor->direction == 0 ? "Z řadiče" : "Do řadiče" }}</td>
                    </tr>
                </table>
                <div class="controls">
                    <a href="{{ BASE_PATH }}/server/edit-device/{{$deviceId}}/"><button name="submit" value="">Zpět na zařízení</button></a>
                </div>
            </div>
            <form action="#" method="POST" class="w100">
                <div class="edit-device-signal-form w100">
                    <div class="select-text-input" id="driver-link-container">
                        <div class="label">Připojení na řadič</div>
                        <div class="select-container">
                            <select>
                                <option value="">--- Nepřipojeno ---</option>
                                @foreach($allDriverSignals as $signal)
                                    <option value="{{ $signal->plugin_identifier }}:{{ $signal->driver_identifier }}:{{ $signal->signal_identifier }}">{{ $signal->driver_label }} &gt; {{ $signal->signal_label }}</option>
                                @endforeach
                            </select>
                        </div>
                        <input name="driver-linkage" type="text" placeholder="Identifikátor řadiče" value="{{ $driverLinkValue }}" spellcheck="false">
                    </div>
                    <div class="controls">
                        <button name="submit" value="">Uložit změny</button>
                    </div>
                </div>
            </form>
        @else
            <div class="error">Požadovaný signál nebyl nalezen.</div>
        @endif
    </section>
    <footer class="footer">
        <p>Autorem této aplikace je <strong>Samuel Trávníček.</strong></p>
    </footer>
@endsection