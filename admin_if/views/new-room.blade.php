@extends("layouts.admin")
@section("title", "Přidat místnost")

@section('head')
    <link rel="stylesheet" href="{{ BASE_PATH }}/css/new-room.section.css" />
    <script src="{{ BASE_PATH }}/js/modules/url-utils.js"></script>
@endsection

@section("content")
    <header>Místnosti &#x203A; Přidat místnost</header>
    <section class="new-room">
        @isset($_POST["submit"])
            @if(strlen($_POST["room-name"]) > 0)
                @php $table->insert(NULL, $_POST["room-name"], $_POST["room-floor"]); @endphp
                <script>location.href = "{{ BASE_PATH }}/edit-room/{{ DB::lastInsertId() }}";</script>
            @else
                <div class="error">Místnost nebyla přidána - prázdný název.</div>
            @endif
        @endisset
        <form action="#" method="POST" class="w100">
            <input name="room-floor" type="hidden" value="0">
            <div class="new-room-form w100">
                <div class="text-input">
                    <div class="label">Název místnosti</div>
                    <input name="room-name" type="text" placeholder="Název">
                </div>
                <div class="controls">
                    <button name="submit" class="btn-add" value="">Přidat místnost</button>
                </div>
            </div>
        </form>
    </section>
    <footer class="footer">
        <p>Autorem této aplikace je <strong>Samuel Trávníček.</strong></p>
    </footer>
@endsection