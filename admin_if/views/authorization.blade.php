<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>VejminekAdmin | Authorization</title>
    <link rel="stylesheet" href="{{ BASE_PATH }}/vendor/google-font-Montserrat/Montserrat.css">
    <link rel="stylesheet" href="{{ BASE_PATH }}/css/authorization.css">
    <link rel="stylesheet" href="{{ BASE_PATH }}/vendor/font-awesome/css/all.min.css">
</head>
<body>
    <div class="wrapper">
        <h2>Autorizace</h2>
        <form method="POST">
            <input type="password" name="passphrase" placeholder="Autorizační klíč" />
            <button name="submit"><i class="fas fa-sign-in-alt"></i></button>
        </form>
        @isset($_POST["submit"])
            @if(APP_PASSPHRASE == $_POST["passphrase"])
                @php($_SESSION["authorized"] = true)
                <script>location.href="{{ BASE_PATH }}";</script>
            @else
                <p class="error">Neplatný klíč.</p>
            @endif
        @endisset
    </div>
</body>
</html>
