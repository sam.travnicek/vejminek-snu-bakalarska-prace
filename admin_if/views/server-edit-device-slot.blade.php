@extends("layouts.admin")
@section("title", "Server | Upravit slot")

@php
    $allDriverSlots = [];
    if($slotDescriptor != null)
        $allDriverSlots = $tableDriverSlots
            ->selectByDirectionAndType($slotDescriptor->direction, $slotDescriptor->type);
@endphp

@section('head')
    <link rel="stylesheet" href="{{ BASE_PATH }}/css/server-edit-device-slot.section.css" />
    <script src="{{ BASE_PATH }}/js/modules/input-utils.js"></script>
    <script>
        $(function() {
            interactiveSelect(".select-container");
            initSelectTextInput("#driver-link-container", "--- Vlastní hodnota ---");
        });
    </script>
@endsection

@section("content")
    <header>Server &#x203A; Upravit slot</header>
    <section class="server-edit-device-slot">
        @isset($_POST["submit"])
            @php
                if($_POST['driver-linkage'] != "") {
                    $submitDriverAndSlot = explode(":", $_POST['driver-linkage']);
                    $submitDriver = $submitDriverAndSlot[0] . ":" . $submitDriverAndSlot[1];
                    $submitSlot = $submitDriverAndSlot[2];
                    $tableSlotsLinks->insertOrUpdate(
                        $submitDriver, $submitSlot, $deviceId, $slotDescriptor->identifier);
                } else {
                    $tableSlotsLinks->deleteByDeviceSlot($deviceId, $slotDescriptor->identifier);
                }
            @endphp
            <div class="info">Změny byly uloženy.</div>
        @endisset
        @php
            $linkRows = $tableSlotsLinks->selectByDeviceSlot($deviceId, $slotDescriptor->identifier);
            $driverLinkValue = "";

            if(count($linkRows) != 0)
                $driverLinkValue = $linkRows[0]->driver . ":" . $linkRows[0]->driver_slot;
        @endphp
        @if($slotDescriptor != null)
            <div class="table-wrapper slot-info w100">
                <table>
                    <tr>
                        <td>Název slotu:</td>
                        <td>{{ $slotDescriptor->label }}</td>
                    </tr>
                    <tr>
                        <td>Identifikátor:</td>
                        <td>{{ $slotDescriptor->identifier }}</td>
                    </tr>
                    <tr>
                        <td>Směr:</td>
                        <td>{{ $slotDescriptor->direction == 0 ? "Z řadiče" : "Do řadiče" }}</td>
                    </tr>
                    <tr>
                        <td>Hodnota:</td>
                        <td>{{ $slotDescriptor->type == 0 ? "Číselná" : ($slotDescriptor->type == 1 ? "Logická" : ($slotDescriptor->type == 2 ? "Textová" : "Neznámá")) }}</td>
                    </tr>
                </table>
                <div class="controls">
                    <a href="{{ BASE_PATH }}/server/edit-device/{{$deviceId}}/"><button name="submit" value="">Zpět na zařízení</button></a>
                </div>
            </div>
            <form action="#" method="POST" class="w100">
                <div class="edit-device-slot-form w100">
                    <div class="select-text-input" id="driver-link-container">
                        <div class="label">Připojení na řadič</div>
                        <div class="select-container">
                            <select>
                                <option value="">--- Nepřipojeno ---</option>
                                @foreach($allDriverSlots as $slot)
                                    <option value="{{ $slot->plugin_identifier }}:{{ $slot->driver_identifier }}:{{ $slot->slot_identifier }}">{{ $slot->driver_label }} &gt; {{ $slot->slot_label }}</option>
                                @endforeach
                            </select>
                        </div>
                        <input name="driver-linkage" type="text" placeholder="Identifikátor řadiče" value="{{ $driverLinkValue }}" spellcheck="false">
                    </div>
                    <div class="controls">
                        <button name="submit" value="">Uložit změny</button>
                    </div>
                </div>
            </form>
        @else
            <div class="error">Požadovaný slot nebyl nalezen.</div>
        @endif
    </section>
    <footer class="footer">
        <p>Autorem této aplikace je <strong>Samuel Trávníček.</strong></p>
    </footer>
@endsection