@extends("layouts.error")
@section("error-number", 500)

@section("message")
    <h2>The database is not available.</h2>
@endsection