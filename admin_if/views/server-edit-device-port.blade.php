@extends("layouts.admin")
@section("title", "Server | Upravit port")

@php
    $allDriverPorts = [];
    if($portDescriptor != null)
        $allDriverPorts = $tableDriverPorts
            ->selectByAvailableDirectionAndMode($portDescriptor->direction, $portDescriptor->mode);
@endphp

@section('head')
    <link rel="stylesheet" href="{{ BASE_PATH }}/css/server-edit-device-port.section.css" />
    <script src="{{ BASE_PATH }}/js/modules/input-utils.js"></script>
    <script>
        let configurations = {};
        @if($portDescriptor != null)
            @php
                $alreadyVisitedPorts = [];
                $allPortConfigs = $tableDriverPortConfigurations
                    ->selectByDirectionAndMode($portDescriptor->direction, $portDescriptor->mode);
            @endphp
            @foreach($allPortConfigs as $row)
                @php
                    $pid = $row->plugin_identifier . ":" . $row->driver_identifier . ":" . $row->port_identifier;
                    if(!in_array($pid, $alreadyVisitedPorts)) {
                        echo("configurations[\"" . $pid . "\"] = [];\r\n");
                        $alreadyVisitedPorts[] = $pid;
                    }
                @endphp
                configurations["{{ $pid }}"].push({
                    label: "{{ $row->config_label }}",
                    identifier: "{{ $row->config_identifier }}"
                });
            @endforeach
        @endif

        $(function() {
            interactiveSelect(".select-container");
            let firstChange = true;

            initSelectTextInput("#driver-link-container", "--- Vlastní hodnota ---", function() {
                $linkSel = $("#driver-link-container select");
                $sel = $("#pref-conf-container select");
                $sel.find("option").remove();
                $sel.append('<option value="" selected>--- Bez preference ---</option>');
                if(!firstChange)
                    $("#pref-conf-container input[type='text']").val("");
                firstChange = false;

                if(configurations[$linkSel.val()] !== undefined) {
                    let cfg = configurations[$linkSel.val()];
                    for(let i in cfg) {
                        $sel.append('<option value="' + cfg[i].identifier + '">' + cfg[i].label + '</option>');
                    }
                }

                initSelectTextInput("#pref-conf-container", "--- Vlastní hodnota ---");
            });

            // let tablePorts = new Table("#ports-table");

            // tablePorts.setup({
            //     clickableRows: true
            // });

            // tablePorts.on('click', function() {
            //     let id = $(this).data("port-identifier");
            //     location.href = "{{ BASE_PATH }}/server/edit-device/{{ $deviceId }}/port/" + id;
            // });
        });
    </script>
@endsection

@section("content")
    <header>Server &#x203A; Upravit port</header>
    <section class="server-edit-device-port">
        @isset($_POST["submit"])
            @php
                if($_POST['driver-linkage'] != "") {
                    $submitDriverAndPort = explode(":", $_POST['driver-linkage']);
                    $submitDriver = $submitDriverAndPort[0] . ":" . $submitDriverAndPort[1];
                    $submitPort = $submitDriverAndPort[2];
                    $tablePortsLinks->insertOrUpdate(
                        $submitDriver, $submitPort, $deviceId, $portDescriptor->identifier,
                        $_POST['preferred-conf']);
                } else {
                    $tablePortsLinks->deleteByDevicePort($deviceId, $portDescriptor->identifier);
                }
            @endphp
            <div class="info">Změny byly uloženy.</div>
        @endisset
        @php
            $linkRows = $tablePortsLinks->selectByDevicePort($deviceId, $portDescriptor->identifier);

            $driverLinkValue = "";
            $preferredLinkConfigValue = "";

            if(count($linkRows) != 0) {
                $driverLinkValue = $linkRows[0]->driver . ":" . $linkRows[0]->driver_port;
                $preferredLinkConfigValue = $linkRows[0]->preferred_cfg;
            }
        @endphp
        @if($portDescriptor != null)
            <div class="table-wrapper port-info w100">
                <table>
                    <tr>
                        <td>Název portu:</td>
                        <td>{{ $portDescriptor->label }}</td>
                    </tr>
                    <tr>
                        <td>Identifikátor:</td>
                        <td>{{ $portDescriptor->identifier }}</td>
                    </tr>
                    <tr>
                        <td>Typ:</td>
                        <td>{{ $portDescriptor->direction == 0 ? "Výstup" : "Vstup" }}</td>
                    </tr>
                    <tr>
                        <td>Režim:</td>
                        <td>{{ $portDescriptor->mode == 0 ? "Logický" : "Diskrétní" }}</td>
                    </tr>
                </table>
                <div class="controls">
                    <a href="{{ BASE_PATH }}/server/edit-device/{{$deviceId}}/"><button name="submit" value="">Zpět na zařízení</button></a>
                </div>
            </div>
            <form action="#" method="POST" class="w100">
                <div class="edit-device-port-form w100">
                    <div class="select-text-input" id="driver-link-container">
                        <div class="label">Připojení na řadič</div>
                        <div class="select-container">
                            <select>
                                <option value="">--- Nepřipojeno ---</option>
                                @foreach($allDriverPorts as $port)
                                    <option value="{{ $port->plugin_identifier }}:{{ $port->driver_identifier }}:{{ $port->port_identifier }}">{{ $port->driver_label }} &gt; {{ $port->port_label }}</option>
                                @endforeach
                            </select>
                        </div>
                        <input name="driver-linkage" type="text" placeholder="Identifikátor řadiče" value="{{ $driverLinkValue }}" spellcheck="false">
                    </div>
                    <div class="select-text-input" id="pref-conf-container">
                        <div class="label">Preferovaná konfigurace</div>
                        <div class="select-container">
                            <select>
                                <option value="">--- Bez preference ---</option>
                                {{-- @foreach($tableDeviceTemplates->selectAll() as $dtempl)
                                    <option value="{{ $dtempl->plugin_identifier }}:{{ $dtempl->identifier }}">{{ $dtempl->label }}</option>
                                @endforeach --}}
                            </select>
                        </div>
                        <input name="preferred-conf" type="text" placeholder="Identifikátor konfigurace" value="{{ $preferredLinkConfigValue }}" spellcheck="false">
                    </div>
                    <div class="controls">
                        <button name="submit" value="">Uložit změny</button>
                    </div>
                </div>
            </form>
        @else
            <div class="error">Požadovaný port nebyl nalezen.</div>
        @endif
    </section>
    <footer class="footer">
        <p>Autorem této aplikace je <strong>Samuel Trávníček.</strong></p>
    </footer>
@endsection