$(document).ready(function(params) {
    const SCROLLBAR_PADDING = rem2px(0.2);

    function onScrollX($scrollable, delta)
    {
        if(delta > 0)
            console.log("LEFT");
        else
            console.log("RIGHT");
    }

    function onScrollY($scrollable, delta)
    {
        if(delta > 0)
            console.log("UP");
        else
            console.log("DOWN");
    }

    function onElementInserted(containerSelector, elementSelector, callback)
    {
        var onMutationsObserved = function(mutations) {
            mutations.forEach(function(mutation) {
                if (mutation.addedNodes.length) {
                    var elements = $(mutation.addedNodes).find(elementSelector);
                    for (var i = 0, len = elements.length; i < len; i++) {
                        callback(elements[i]);
                    }
                }
            });
        };

        var target = $(containerSelector)[0];
        var config = { childList: true, subtree: true };
        var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
        var observer = new MutationObserver(onMutationsObserved);
        observer.observe(target, config);

    }

    onElementInserted('body', '*', function(element) {
        console.log(element);
    });

    //TODO: make invisible clickable container for scrollbar (to make it easier to hit it with mouse)
    //TODO; make two variants: overlapping or by-side scrollbar
    //TODO: init element also when added after page loads
    // https://stackoverflow.com/questions/10415400/jquery-detecting-div-of-certain-class-has-been-added-to-dom

    function initAll()
    {
        $(".scrollable").each(function(idx) {
            $scrollable = $(this);

            $scrollable.prepend($("<div class=\"scrollbar-x\"><div class=\"thumb-container\"><div class=\"thumb\"></div></div></div>"));
            $scrollable.prepend($("<div class=\"scrollbar-y\"><div class=\"thumb-container\"><div class=\"thumb\"></div></div></div>"));

            $scrollable[0].addEventListener("wheel", function(e) {
                if(e.deltaX != 0)
                    onScrollX($scrollable, Math.sign(e.deltaX));

                if(e.deltaY != 0)
                    onScrollY($scrollable, Math.sign(e.deltaY));
            });

            $scrollbarX = $scrollable.find(".scrollbar-x");

            $scrollbarX.mousedown(function() {
                $(this).addClass("scrolling");
            });

            $scrollbarY = $scrollable.find(".scrollbar-y");

            $scrollbarY.mousedown(function() {
                $(this).addClass("scrolling");
            });

            $(window).mouseup(function() {
                $scrollbarX.removeClass("scrolling");
                $scrollbarY.removeClass("scrolling");
            });
        });

        $(".scrollable-x").each(function(idx) {
            $scrollable = $(this);
            $scrollable.prepend($("<div class=\"scrollbar-x\"><div class=\"thumb-container\"><div class=\"thumb\"></div></div></div>"));

            $scrollable[0].addEventListener("wheel", function(e) {
                if(e.deltaX != 0)
                    onScrollX($scrollable, Math.sign(e.deltaX));
            });

            $scrollbarX = $scrollable.find(".scrollbar-x");

            $scrollbarX.mousedown(function() {
                $(this).addClass("scrolling");
            });

            $(window).mouseup(function() {
                $scrollbarX.removeClass("scrolling");
            });
        });

        $(".scrollable-y").each(function(idx) {
            $scrollable = $(this);
            $scrollable.prepend($("<div class=\"scrollbar-y\"><div class=\"thumb-container\"><div class=\"thumb\"></div></div></div>"));

            $scrollable[0].addEventListener("wheel", function(e) {
                if(e.deltaY != 0)
                    onScrollY($scrollable, Math.sign(e.deltaY));
            });

            $scrollbarY = $scrollable.find(".scrollbar-y");

            $scrollbarY.mousedown(function() {
                $(this).addClass("scrolling");
            });

            $(window).mouseup(function() {
                $scrollbarY.removeClass("scrolling");
            });
        });
    }

    function xcoef($scrollable) {
        var e = $scrollable[0];
        return e.clientWidth / e.scrollWidth;
    }

    function ycoef($scrollable) {
        var e = $scrollable[0];
        return e.clientHeight / e.scrollHeight;
    }

    function updateThumbX($scrollable, $thumbContainer)
    {
        var maxWidth = $scrollable[0].clientWidth - 2*SCROLLBAR_PADDING;
        var w = xcoef($scrollable) * maxWidth / $scrollable[0].clientWidth;
        var width = Math.floor(w * 100);
        console.log("Cx: " + xcoef($scrollable));
        console.log("W: " + width);
        $thumbContainer.css("width", width + "%");
        $thumbContainer.css("left", SCROLLBAR_PADDING + "px");
    }

    function updateThumbY($scrollable, $thumbContainer)
    {
        var maxHeight = $scrollable[0].clientHeight - 2*SCROLLBAR_PADDING;
        var h = ycoef($scrollable) * maxHeight / $scrollable[0].clientHeight;
        var height = Math.floor(h * 100);
        console.log("Cy: " + ycoef($scrollable));
        console.log("H: " + height);
        $thumbContainer.css("height", height + "%");
        $thumbContainer.css("top", SCROLLBAR_PADDING + "px");
    }

    function checkX($scrollable)
    {
        $scrollbarX = $scrollable.find(".scrollbar-x");
        $thumbContainer = $scrollbarX.find(".thumb-container");
        if(xcoef($scrollable) < 1) {
            updateThumbX($scrollable, $thumbContainer);
            $scrollbarX.show();
        } else
            $scrollbarX.hide();
    }

    function checkY($scrollable)
    {
        $scrollbarY = $scrollable.find(".scrollbar-y");
        $thumbContainer = $scrollbarY.find(".thumb-container");
        if(ycoef($scrollable) < 1) {
            updateThumbY($scrollable, $thumbContainer);
            $scrollbarY.show();
        } else
            $scrollbarY.hide();
    }

    function checkAll()
    {
        $(".scrollable").each(function(idx) {
            checkX($(this));
            checkY($(this));
        });

        $(".scrollable-x").each(function(idx) {
            checkX($(this));
        });

        $(".scrollable-y").each(function(idx) {
            checkY($(this));
        });
    }

    initAll();
    checkAll();

    $(window).on("resize", checkAll);
});