<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define("APP_DIR", dirname(__DIR__));

require APP_DIR . "/app/settings.php";

if(!isset($_GET["fancy_url_get"]))
    $_GET["fancy_url_get"] = "";

require APP_DIR . "/lib/BladeOne.php";
require APP_DIR . "/core/Kernel.php";
require APP_DIR . "/core/Route.php";
require APP_DIR . "/core/RouteValidator.php";

define("ENV_VIEWS_PATH", APP_DIR . "/views");
define("ENV_CACHE_PATH", APP_DIR . "/cache");

$kernel = new Kernel(ENV_VIEWS_PATH, ENV_CACHE_PATH);

require APP_DIR . "/core/global_functions.php";
require APP_DIR . "/core/DB.php";
require APP_DIR . "/core/Table.php";

DB::connect(DATABASE_HOST, DATABASE_USER, DATABASE_PASS, DATABASE_NAME);
if(DB::connectionError()) {
    echo view("dberror");
    exit;
}

$tables_dir = new DirectoryIterator(APP_DIR . "/app/tables");
foreach ($tables_dir as $fileinfo) {
    if(!$fileinfo->isDot())
        include APP_DIR . "/app/tables/" . $fileinfo->getFilename();
}

require APP_DIR . "/app/routes.php";

if(!isset($_SESSION["authorized"]) || !$_SESSION["authorized"]) {
    echo view("authorization");
    exit;
}

Route::on_GET($_GET["fancy_url_get"]);