function URLRedirectPost(url, data)
{
    let html = '<form method="post" action="' + url + '">';
    for(let i in data)
        html += '<input type="hidden" name="' + i + '" value="' + data[i] + '">';
    html += '</form>';
    let $form = $(html);
    $(document.body).append($form);
    $form.submit();
}

function URLRedirectGet(url, data)
{
    let urlObj = new URL(url);
    for(let i in data)
        urlObj.searchParams.set(i, data[i]);
    location.href = urlObj.href;
}

function URLEncodeObject(obj)
{
    return btoa(JSON.stringify(obj));
}

function URLDecodeObject(str)
{
    return JSON.parse(atob(str));
}