class Prompt
{
    static _ID_CNTR = 0;

    constructor(msg, acceptTxt = "Potvrdit", denyTxt = "Zrušit")
    {
        this.id = Prompt._ID_CNTR++;
        this.acceptCallbacks = [];
        this.denyCallbacks = [];

        let html = '<div class="prompt" id="prompt-' + this.id + '">';
        html += '<div class="window">';
        html += '<div class="header"><i class="fas fa-times" data-close-btn="true"></i></div>';
        html += '<div class="message">' + msg + '</div>';
        html += '<div class="accept" data-accept-btn="true">' + acceptTxt + '</div>';
        html += '<div class="cancel" data-cancel-btn="true">' + denyTxt + '</div>';
        html += '</div>';
        html += '</div>';

        this.$elem = $(html);
        $("body").append(this.$elem);
        // $("body").append(html);
        // this.$elem = $("#prompt-" + this.id);
        let self = this;

        let denyRoutine = function(e) {
            e.stopPropagation();
            for(let i = 0; i < self.denyCallbacks.length; ++i)
                self.denyCallbacks[i]();
            self.close();
        };

        let acceptRoutine = function(e) {
            e.stopPropagation();
            for(let i = 0; i < self.acceptCallbacks.length; ++i)
                self.acceptCallbacks[i]();
            self.close();
        };

        this.$elem.click(denyRoutine);
        this.$elem.find("*[data-close-btn]").click(denyRoutine);
        this.$elem.find("*[data-cancel-btn]").click(denyRoutine);
        this.$elem.find("*[data-accept-btn]").click(acceptRoutine);

        this.$elem.find(".header, .message").click(function(e) {
            e.stopPropagation();
        });
    }

    on(action, callback)
    {
        if(action == "deny")
            this.denyCallbacks.push(callback);
        else if(action == "accept")
            this.acceptCallbacks.push(callback);
    }

    close()
    {
        this.$elem.remove();
    }
}