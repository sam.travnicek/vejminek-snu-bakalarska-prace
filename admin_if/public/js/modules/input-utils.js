function interactiveSelect(elementSelector)
{
    let $select = $(elementSelector).find("select");

    $select.focus(function() {
        $(this).parent().addClass("focus");
    });

    $select.focusout(function() {
        $(this).parent().removeClass("focus");
    });
}

function initSelectTextInput(elementSelector, customValueText, changeCallback)
{
    let $elem = $(elementSelector);

    let $select = $elem.find("select");
    let $text = $elem.find("input[type='text']");

    $select.prepend("<option value='__CustomValue__'>" + customValueText + "</option>");
    let $options = $select.find("option");

    function selectValue(value) {
        $options.prop("selected", false);
        let found = false;

        $options.each(function(idx) {
            if($(this).val() == value) {
                found = true;
                $(this).prop("selected", true);
            }
        });
        return found;
    }

    if(!selectValue($text.val()))
        $options.first().prop("selected", true);
    if(changeCallback !== undefined)
        changeCallback();

    $text.change(function(e) {
        if(!selectValue($text.val()))
            $options.first().prop("selected", true);
        if(changeCallback !== undefined)
            changeCallback();
    });

    let previousSelectValue = $select.val();

    $select.change(function(e) {
        let val = $(this).val();

        if(val == "__CustomValue__")
            selectValue(previousSelectValue);
        else {
            $text.val(val);
            previousSelectValue = val;
        }

        if(changeCallback !== undefined)
            changeCallback();
    });
}