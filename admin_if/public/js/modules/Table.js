class Table
{
    constructor(selector)
    {
        this.$selector = $(selector).first();
        this.cbClicked = [];
        this.cbSelected = [];
        this.cbUnselected = [];
    }

    setup(config)
    {
        if(!config)
            return;

        let table = this;
        let $table = this.$selector;

        if(config.editableRows) { // rows can be checked/unchecked
            let $rowCheckboxes = $table.find("tbody > tr > td:first-child > input[type='checkbox']");
            let $firstCheckbox = $table.find("thead > tr > th:first-child > input[type='checkbox']");

            $firstCheckbox.on('change', function() {
                let checked = $(this).prop("checked");

                $rowCheckboxes.each(function() {
                    $(this).prop("checked", checked);

                    if(checked) {
                        for(let i = 0; i < table.cbSelected.length; ++i)
                            table.cbSelected[i].call(this);
                    } else {
                        for(let i = 0; i < table.cbSelected.length; ++i)
                            table.cbUnselected[i].call(this);
                    }
                });
            });

            $rowCheckboxes.on('click', function(e) {
                e.stopPropagation();
            })

            $rowCheckboxes.on('change', function() {
                let checked = $(this).prop("checked");

                if(checked) {
                    for(let i = 0; i < table.cbSelected.length; ++i)
                        table.cbSelected[i].call(this);
                } else {
                    for(let i = 0; i < table.cbSelected.length; ++i)
                        table.cbUnselected[i].call(this);
                }
            });
        }

        if(config.clickableRows) {
            let $bodyRows = $table.find("tbody > tr");
            $bodyRows.css("cursor", "pointer");

            $bodyRows.on('click', function() {
                if(config.editableRows) {
                    let $rowCheckboxes = $table.find("tbody > tr > td:first-child > input[type='checkbox']");
                    let $firstCheckbox = $table.find("thead > tr > th:first-child > input[type='checkbox']");
                    let $checkbox = $(this).find("td:first-child > input[type='checkbox']");
                    let checked = $checkbox.prop("checked");
                    $checkbox.prop("checked", !checked);

                    let allChecked = true;
                    $rowCheckboxes.each(function(index) {
                        if(!$(this).prop("checked"))
                            allChecked = false;
                    });
                    $firstCheckbox.prop("checked", allChecked);

                    if(!checked) {
                        for(let i = 0; i < table.cbSelected.length; ++i)
                            table.cbSelected[i].call(this);
                    } else {
                        for(let i = 0; i < table.cbSelected.length; ++i)
                            table.cbUnselected[i].call(this);
                    }
                }

                for(let i = 0; i < table.cbClicked.length; ++i)
                    table.cbClicked[i].call(this);
            });
        }
    }

    selectedRows()
    {
        return this.$selector.find("tbody > tr > td:first-child > input[type='checkbox']:checked");
    }

    on(event, callback)
    {
        if(event == 'click')
            this.cbClicked.push(callback);
        else if(event == 'select')
            this.cbSelected.push(callback);
        else if(event == 'unselect')
            this.cbUnselected.push(callback);
    }
}