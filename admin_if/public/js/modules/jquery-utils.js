$.fn.overflown = function()
{
    var e = this[0];
    return e.scrollHeight > e.clientHeight || e.scrollWidth > e.clientWidth;
}

$.fn.verticalOverflown = function()
{
    var e = this[0];
    return e.scrollHeight > e.clientHeight;
}

$.fn.horizontalOverflown = function()
{
    var e = this[0];
    return e.scrollWidth > e.clientWidth;
}

$.fn.isMouseOver = function()
{
    return $(this).filter(":hover").length > 0;
}