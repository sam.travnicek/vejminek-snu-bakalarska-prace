function rem2px(rem)
{
    return Math.floor(rem * parseFloat(getComputedStyle(document.documentElement).fontSize));
}

class App
{
    constructor()
    {
        this.notifEnabled = false;

        var self = this;

        // if(Push.Permission.get() !== "granted") {
        //     Push.Permission.request(function() {
        //         self.notifEnabled = true;
        //     });
        // }

        Push.Permission.request(function() {
            self.notifEnabled = true;
        });
    }

    openMenu()
    {
        $(".site > .sidebar").removeClass("menu-closed");
        $(".site > .logo > .menu-btn").removeClass("menu-closed");
    }

    closeMenu()
    {
        $(".site > .sidebar").addClass("menu-closed");
        $(".site > .logo > .menu-btn").addClass("menu-closed");
    }

    notification(title, params)
    {
        if(!this.notifEnabled)
            return null;
        return Push.create(title, params);
    }
}

let app = new App();

$(document).ready(function() {

    var last_menu_open = false;
    if($(window).innerWidth() >= 576) {
        last_menu_open = true;
        app.openMenu();
    } else {
        // $(".site").addClass("sticky-header");
        // alert("HA");
    }

    $(window).on("resize", function(){
        var win = $(this);

        if(win.innerWidth() >= 576) {
            app.openMenu();
            // $(".site").removeClass("sticky-header");
        } else {
            if(last_menu_open)
                app.openMenu();
            else
                app.closeMenu();
            // $(".site").addClass("sticky-header");
        }

        if($(".sidebar").overflown())
            console.log("OVERFLOWN");
    });

    // $(window).on("scroll", function(e) {
    //     console.log("scroll");
    //     console.log(e);
    // });

    $(".site > .logo > .menu-btn").click(function() {
        if($(this).hasClass("menu-closed")) {
            app.openMenu();
            last_menu_open = true;
        } else {
            app.closeMenu();
            last_menu_open = false;
        }
    });

    $(".site").removeClass("hidden");

    // app.notification("Vejminek Snů", {
    //     body: "Testovací upozornění",
    //     icon: '/apple-icon.png',
    //     timeout: 2000,
    //     // requireInteraction: true,
    //     onClick: function () {
    //         window.focus();
    //         this.close();
    //     }
    // });
});

// $(window).on("load resize", function() {
//     if($("table").overflown())
//     console.log("overflown");
// });