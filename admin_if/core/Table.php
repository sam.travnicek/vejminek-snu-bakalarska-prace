<?php

class Table
{
    protected $tableName;
    protected $fields;
    protected $orderBy;

    public function __construct(string $tableName, array $fields)
    {
        $this->tableName = $tableName;
        $this->fields = $fields;
        $this->orderBy = "";
    }

    public function countAll()
    {
        $query = "SELECT COUNT(*) as 'cnt' FROM `" . $this->tableName . "`;";
        $stmt = DB::prepare($query);
        $stmt->execute();

        if($row = $stmt->fetch(PDO::FETCH_ASSOC))
            return $row["cnt"];
        return 0;
    }

    public function selectAll()
    {
        $query = "SELECT ";

        for($i = 0; $i < count($this->fields); ++$i) {
            if($i != 0)
                $query .= ", ";
            $query .= "`" . $this->fields[$i] . "`";
        }

        $query .= " FROM `" . $this->tableName . "`";

        if($this->orderBy != "")
            $query .= " ORDER BY `" . $this->orderBy . "` ASC";

        $query .= ";";

        $stmt = DB::prepare($query);
        $stmt->execute();

        $results = [];

        while($row = $stmt->fetch(PDO::FETCH_ASSOC))
            $results[] = (object) $row;

        return $results;
    }

    function insert(...$values)
    {
        if(count($values) != count($this->fields))
            throw new Exception("Invalid number of values.");

        $query = "INSERT INTO `" . $this->tableName . "` (";

        for($i = 0; $i < count($this->fields); ++$i) {
            if($i != 0)
                $query .= ", ";
            $query .= "`" . $this->fields[$i] . "`";
        }

        $query .= ") VALUES (";

        for($i = 0; $i < count($values); ++$i) {
            if($i != 0)
                $query .= ", ";
            if($values[$i] === NULL)
                $query .= "NULL";
            else
                $query .= ":param" . $i;
        }

        $query .= ");";

        $stmt = DB::prepare($query);
        for($i = 0; $i < count($values); ++$i) {
            if($values[$i] !== NULL)
                $stmt->bindParam(':param' . $i, $values[$i]);
        }
        $stmt->execute();
    }

    function deleteByPrimaryKey($primaryKeyValue)
    {
        if(count($this->fields) == 0)
            return;

        $query = "DELETE FROM `" . $this->tableName . "` WHERE `"
            . $this->fields[0] . "`=:primaryKeyValue;";

        $stmt = DB::prepare($query);
        $stmt->bindParam(':primaryKeyValue', $primaryKeyValue);
        $stmt->execute();
    }

    function selectByPrimaryKey($primaryKeyValue)
    {
        if(count($this->fields) == 0)
            return [];

        $query = "SELECT ";

        for($i = 0; $i < count($this->fields); ++$i) {
            if($i != 0)
                $query .= ", ";
            $query .= "`" . $this->fields[$i] . "`";
        }

        $query .= " FROM `" . $this->tableName . "`";
        $query .= " WHERE `" . $this->fields[0] . "`=:primaryKeyValue";

        if($this->orderBy != "")
            $query .= " ORDER BY `" . $this->orderBy . "` ASC";

        $query .= ";";

        $stmt = DB::prepare($query);
        $stmt->bindParam(':primaryKeyValue', $primaryKeyValue);
        $stmt->execute();

        $results = [];

        while($row = $stmt->fetch(PDO::FETCH_ASSOC))
            $results[] = (object) $row;

        return $results;
    }

    function updateByPrimaryKey($primaryKeyValue, ...$values)
    {
        if(count($this->fields) == 0)
            return [];
        if(count($values) != count($this->fields)-1)
            throw new Exception("Invalid number of values.");

        $query = "UPDATE `" . $this->tableName . "` SET ";

        for($i = 1; $i < count($this->fields); ++$i) {
            if($i != 1)
                $query .= ", ";
            $query .= "`" . $this->fields[$i] . "`=:param" . $i;
        }

        $query .= " WHERE `" . $this->fields[0] . "`=:primaryKeyValue;";

        $stmt = DB::prepare($query);
        for($i = 1; $i < count($this->fields); ++$i)
            if($values[$i-1] !== NULL)
                $stmt->bindParam(':param' . $i, $values[$i-1]);
        $stmt->bindParam(':primaryKeyValue', $primaryKeyValue);
        $stmt->execute();
    }
}