<?php

use eftec\bladeone\BladeOne;

class Kernel
{
    private $blade;

    public function __construct($views_path, $chache_path)
    {
        session_start();

        if(!isset($_SESSION["redirect"])) {
            $_SESSION["redirect"] = [];
            $_SESSION["redirect"]["URL"] = "/";
            $_SESSION["redirect"]["data"] = [];
            $_SESSION["redirect"]["method"] = "GET";
            $_SESSION["redirect"]["delay"] = 0;
            $_SESSION["redirect"]["enabled"] = false;
        }

        $this->blade = new BladeOne(
            $views_path, $chache_path, BladeOne::MODE_AUTO);
    }

    function view($name, $globals = [])
    {
        return $this->blade->run($name, $globals);
    }

    function setRedirect(
        $redirectURL, $redirectData, $redirectMethod = "GET",
        $redirectDelay = 0)
    {
        $_SESSION["redirect"]["URL"] = $redirectURL;
        $_SESSION["redirect"]["data"] = $redirectData;
        $_SESSION["redirect"]["method"] = $redirectMethod == "POST" ? "POST" : "GET";
        $_SESSION["redirect"]["delay"] = $redirectDelay > 0 ? $redirectDelay : 0;
        $_SESSION["redirect"]["enabled"] = true;
    }

    function disableRedirect()
    {
        $_SESSION["redirect"]["enabled"] = false;
    }

    function redirectEnabled()
    {
        return $_SESSION["redirect"]["enabled"];
    }

    function generateRedirectScript()
    {
        $script = "setTimeout(function() {\r\n";
        $script .= "let html = '<form method=\""
            . $_SESSION["redirect"]["method"] . "\" action=\""
            . $_SESSION["redirect"]["URL"] . "\">';\r\n";
        foreach($_SESSION["redirect"]["data"] as $key => $value)
            $script .= "html += '<input type=\"hidden\" name=\"$key\" value=\"$value\">';\r\n";
        $script .= "html += '</form>'\r\n";
        $script .= "let \$form = \$(html);\r\n";
        $script .= "\$(document.body).append(\$form);\r\n";
        $script .= "\$form.submit();\r\n";
        $script .= "}, " . $_SESSION["redirect"]["delay"] . ")";

        $this->disableRedirect();
        return $script;
    }
}