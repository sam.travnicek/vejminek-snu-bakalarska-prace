<?php

class Route
{
    private static $GET_routes = [];

    public static function get($url, $callback)
    {
        self::$GET_routes[] = new Route($url, $callback);
    }

    public static function on_GET($url)
    {
        $sample = new RouteValidator($url, RouteTokenizer::SAMPLE);

        foreach(self::$GET_routes as $route) {
            $res = $route->_pattern->match($sample);
            if($res["matching"]) {
                if(is_callable($route->_callback)) {
                    $cb = $route->_callback;
                    echo $cb($res["parameters"]);
                }
                return;
            }
        }

        echo view("error404");
    }

    private $_pattern;
    private $_callback;

    public function __construct($url, $callback)
    {
        $this->_pattern = new RouteValidator($url, RouteTokenizer::PATTERN);
        $this->_callback = $callback;
    }
}