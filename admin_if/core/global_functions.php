<?php

function view($name, $globals = [])
{
    global $kernel;
    return $kernel->view($name, $globals);
}

function setRedirect(
    $redirectURL, $redirectData, $redirectMethod = "GET", $redirectDelay = 0)
{
    global $kernel;

    $kernel->setRedirect(
        $redirectURL, $redirectData, $redirectMethod, $redirectDelay);
}

function redirectEnabled()
{
    global $kernel;
    return $kernel->redirectEnabled();
}

function generateRedirectScript()
{
    global $kernel;
    return $kernel->generateRedirectScript();
}

function URLEncodeObject($obj)
{
    return base64_encode(json_encode($obj));
}

function URLDecodeObject($obj)
{
    return json_decode(base64_decode($obj));
}