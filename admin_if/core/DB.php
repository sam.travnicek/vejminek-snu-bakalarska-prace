<?php

class DB
{
    private static $db = null;
    private static $connectionError = null;
    private static $errorMessage = "";

    public static function connect($host, $user, $password, $name)
    {
        self::$connectionError = null;
        self::$db = null;

        try {
            self::$db = new PDO(
                "mysql:host=".$host.";dbname=".$name.";charset=utf8", $user, $password);
            self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch(PDOException $e) {
            self::$connectionError = $e->getMessage();
            return false;
        }

        return true;
    }

    public static function connectionError()
        { return self::$connectionError; }

    public static function errorMessage()
        { return self::$errorMessage; }

    public static function prepare($query)
    {
        if(self::$connectionError) {
            self::$errorMessage = "connection error";
            return null;
        }

        try {
            return self::$db->prepare($query);
        } catch(PDOException $e) {
            self::$errorMessage = $e->getMessage();
            return null;
        }
    }

    public static function lastInsertId()
    {
        return self::$db->lastInsertId();
    }
}