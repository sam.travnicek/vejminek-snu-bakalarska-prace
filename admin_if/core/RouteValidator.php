<?php

class RouteToken
{
    const SLASH     = 0;
    const WORD      = 1; // a part of SAMPLE without special meaning
    const LCBRA     = 2;
    const RCBRA     = 3;
    const QMARK     = 4;
    const INVALID   = 5;
    const EOF       = 6;

    public $pos;
    public $type;
    public $value;

    public function __construct($pos, $type = self::EOF, $value = NULL)
    {
        $this->pos = $pos;
        $this->type = $type;
        $this->value = $value;
    }
}

class RouteTokenizer
{
    const PATTERN = 0;
    const SAMPLE = 1;

    private $_input;
    private $_len;
    private $_pos;
    private $_cchar;
    private $_type; // Pattern or SAMPLE

    public function __construct($input, $type)
    {
        $this->_input = $input;
        $this->_len = strlen($input);
        $this->_type = $type;
        if($this->_type != self::PATTERN && $this->_type != self::SAMPLE)
            $this->_type = self::PATTERN;
        $this->reset();
    }

    public function reset()
    {
        $this->_pos = 0;

        if($this->_len > 0)
            $this->_cchar = $this->_input[0];
        else
            $this->_cchar = NULL;
    }

    public function type()
    {
        return $this->_type;
    }

    private function _advance()
    {
        if(++$this->_pos >= $this->_len)
            $this->_cchar = NULL;
        else
            $this->_cchar = $this->_input[$this->_pos];
    }

    private function _nextWord()
    {
        $word = "";

        while($this->_cchar == '-' || $this->_cchar == '_' || ctype_alnum($this->_cchar)) {
            $word .= $this->_cchar;
            $this->_advance();
        }

        return $word;
    }

    public function nextToken()
    {
        $token = new RouteToken($this->_pos);

        while($this->_pos < $this->_len) {
            $c = $this->_cchar;

            // SAMPLE specific characters
            if($this->_type == self::SAMPLE) {
                if($c == '?' || $c == '#') // for SAMPLE '?', '#' are EOF
                    return $token;
            }

            // Pattern specific characters
            if($this->_type == self::PATTERN) {
                if($c == '{') {
                    $token->type = RouteToken::LCBRA;
                    $this->_advance();
                    return $token;
                }

                if($c == '}') {
                    $token->type = RouteToken::RCBRA;
                    $this->_advance();
                    return $token;
                }

                if($c == '?') { // for pattern '?' means optional parameter
                    $token->type = RouteToken::QMARK;
                    $this->_advance();
                    return $token;
                }
            }

            // General characters
            if($c == '/') {
                $token->type = RouteToken::SLASH;
                $this->_advance();
                return $token;
            }

            if($c == '-' || $c == '_' || ctype_alnum($c)) {
                $token->type = RouteToken::WORD;
                $token->value = $this->_nextWord();
                return $token;
            }

            $token->type = RouteToken::INVALID;
            $token->value = $c;
            return $token;
        }

        return $token;
    }
}

class RouteValidator
{
    //-GRAMMAR:--------------------------------------------------------
    // word:    *('[a-zA-Z0-9_\-]')
    // param:   '{' word ['?'] '}'
    // pattern: ['/'] [(word | param) *('/' (word | param)) ['/']] EOF
    // sample:  ['/'] [word *('/' word) ['/']] EOF
    //-----------------------------------------------------------------

    private $_tokenizer;
    private $_ctoken;

    public function __construct($input, $type)
    {
        $this->_tokenizer = new RouteTokenizer($input, $type);
        $this->_ctoken = $this->_nextToken();
    }

    public function type()
    {
        return $this->_tokenizer->type();
    }

    private function _nextToken()
    {
        $this->_ctoken = $this->_tokenizer->nextToken();
    }

    private function _parseWord()
    {
        if($this->_ctoken->type == RouteToken::WORD) {
            $val = $this->_ctoken->value;
            $this->_nextToken();

            return [
                "valid" => true,
                "word" => $val
            ];
        }

        return [
            "valid" => false,
            "error" => "expected WORD"
        ];
    }

    private function _parseParam()
    {
        if($this->_ctoken->type != RouteToken::LCBRA)
            return [
                "valid" => false,
                "error" => "expected '{'"
            ];
        $this->_nextToken();

        $word = $this->_parseWord();
        if(!$word["valid"])
            return $word;

        $word["optional"] = false;

        if($this->_ctoken->type == RouteToken::QMARK) {
            $word["optional"] = true;
            $this->_nextToken();
        }

        if($this->_ctoken->type != RouteToken::RCBRA)
            return [
                "valid" => false,
                "error" => "expected '}'"
            ];
        $this->_nextToken();

        return $word;
    }

    private function _parseWordOrParam()
    {
        $wp = $this->_parseWord();
        if($wp["valid"]) {
            $wp["type"] = "word";
            return $wp;
        }

        $wp = $this->_parseParam();
        if($wp["valid"]) {
            $wp["type"] = "param";
            return $wp;
        }

        return [
            "valid" => false,
            "error" => "expected WORD or PARAM: " . $wp["error"] // TODO: better debug message
        ];
    }

    private function _parsePattern()
    {
        if($this->_ctoken->type == RouteToken::SLASH)
            $this->_nextToken();

        $pattern = [];

        if($this->_ctoken->type == RouteToken::EOF)
            return [
                "valid" => true,
                "pattern" => $pattern
            ];

        $first = $this->_parseWordOrParam();
        if(!$first["valid"])
            return $first;

        $firstElem = [
            "type" => $first["type"],
            "word" => $first["word"]
        ];
        if($firstElem["type"] == "param")
            $firstElem["optional"] = $first["optional"];
        array_push($pattern, $firstElem);

        while($this->_ctoken->type == RouteToken::SLASH) {
            $this->_nextToken();

            if($this->_ctoken->type == RouteToken::EOF)
                break;

            $next = $this->_parseWordOrParam();
            if(!$next["valid"])
                return $next;

            $nextElem = [
                "type" => $next["type"],
                "word" => $next["word"]
            ];
            if($nextElem["type"] == "param")
                $nextElem["optional"] = $next["optional"];
            array_push($pattern, $nextElem);
        }

        if($this->_ctoken->type == RouteToken::SLASH)
            $this->_nextToken();

        if($this->_ctoken->type != RouteToken::EOF)
            return [
                "valid" => false,
                "error" => "expected EOF"
            ];

        return [
            "valid" => true,
            "pattern" => $pattern
        ];
    }

    private function _parseSample()
    {
        if($this->_ctoken->type == RouteToken::SLASH)
            $this->_nextToken();

        $sample = [];

        if($this->_ctoken->type == RouteToken::EOF)
            return [
                "valid" => true,
                "sample" => $sample
            ];

        $first = $this->_parseWord();
        if(!$first["valid"])
            return $first;

        array_push($sample, $first["word"]);

        while($this->_ctoken->type == RouteToken::SLASH) {
            $this->_nextToken();

            if($this->_ctoken->type == RouteToken::EOF)
                break;

            $next = $this->_parseWord();
            if(!$next["valid"])
                return $next;

            array_push($sample, $next["word"]);
        }

        if($this->_ctoken->type == RouteToken::SLASH)
            $this->_nextToken();

        if($this->_ctoken->type != RouteToken::EOF)
            return [
                "valid" => false,
                "error" => "expected EOF"
            ];

        return [
            "valid" => true,
            "sample" => $sample
        ];
    }

    public function parse()
    {
        $this->_tokenizer->reset();
        $this->_nextToken();

        if($this->type() == RouteTokenizer::PATTERN)
            return $this->_parsePattern();
        return $this->_parseSample();
    }

    private static function _match(RouteValidator $patternRoute, RouteValidator $sampleRoute)
    {
        $patternParts = $patternRoute->parse();
        if(!$patternParts["valid"])
            return [
                "matching" => false,
                "error" => "Pattern error: " . $patternParts["error"]
            ];

        $sampleParts = $sampleRoute->parse();
        if(!$sampleParts["valid"])
            return [
                "matching" => false,
                "error" => "Sample error: " . $sampleParts["error"]
            ];

        if(count($patternParts["pattern"]) < count($sampleParts["sample"]))
            return [
                "matching" => false,
                "error" => "Sample error: unexpected word count"
            ];

        $parameters = [];

        // TODO: implement parameter name collisions

        foreach($patternParts["pattern"] as $idx => $patternPart) {
            if($patternPart["type"] == "word") { // WORD
                if(!array_key_exists($idx, $sampleParts["sample"]) || $patternPart["word"] != $sampleParts["sample"][$idx])
                    return [
                        "matching" => false,
                        "error" => "words not matching"
                    ];
            } else { // PARAMETER
                if(!array_key_exists($idx, $sampleParts["sample"])) {
                    if(!$patternPart["optional"]) {
                        return [
                            "matching" => false,
                            "error" => "expected required parameter '" . $patternPart["word"] . "'"
                        ];
                    } else {
                        $parameters[$patternPart["word"]] = "";
                    }
                } else {
                    $parameters[$patternPart["word"]] = $sampleParts["sample"][$idx];
                }
            }
        }

        return [
            "matching" => true,
            "parameters" => $parameters
        ];
    }

    public function match(RouteValidator $other)
    {
        if($this->type() == RouteTokenizer::PATTERN && $other->type() == RouteTokenizer::SAMPLE)
            return self::_match($this, $other);
        if($this->type() == RouteTokenizer::SAMPLE && $other->type() == RouteTokenizer::PATTERN)
            return self::_match($other, $this);
        return [
            "matching" => false,
            "error" => "incompatible routes"
        ];
    }
}