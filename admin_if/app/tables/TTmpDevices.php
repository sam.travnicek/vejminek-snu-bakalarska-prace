<?php

class TTmpDevices extends Table
{
    public function __construct()
    {
        parent::__construct("tmp_devices", [
            "id", "plugin_identifier", "identifier", "label"
        ]);

        $this->orderBy = "label";
    }

    public function selectByIdentifiers($pluginIdentifier, $deviceIdentifier)
    {
        $query = "SELECT ";

        for($i = 0; $i < count($this->fields); ++$i) {
            if($i != 0)
                $query .= ", ";
            $query .= "`" . $this->fields[$i] . "`";
        }

        $query .= " FROM `" . $this->tableName . "`";
        $query .= " WHERE `plugin_identifier`=:pluginIdentifier";
        $query .= " AND `identifier`=:deviceIdentifier;";

        $stmt = DB::prepare($query);
        $stmt->bindParam(':pluginIdentifier', $pluginIdentifier);
        $stmt->bindParam(':deviceIdentifier', $deviceIdentifier);
        $stmt->execute();

        $results = [];

        while($row = $stmt->fetch(PDO::FETCH_ASSOC))
            $results[] = (object) $row;

        return $results;
    }
}