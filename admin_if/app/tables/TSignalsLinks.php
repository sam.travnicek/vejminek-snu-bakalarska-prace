<?php

class TSignalsLinks extends Table
{
    public function __construct()
    {
        parent::__construct("signals_links", [
            "id", "driver", "driver_signal", "device_id", "device_signal"
        ]);

        $this->orderBy = "id";
    }

    public function selectByDeviceSignal($deviceId, $deviceSignal)
    {
        $query = <<<SQL
            SELECT
            `id`, `driver`, `driver_signal`, `device_id`, `device_signal`
            FROM `signals_links`
            WHERE `device_id`=:deviceId AND `device_signal`=:deviceSignal;
        SQL;

        $stmt = DB::prepare($query);
        $stmt->bindParam(':deviceId', $deviceId);
        $stmt->bindParam(':deviceSignal', $deviceSignal);
        $stmt->execute();

        $results = [];
        while($row = $stmt->fetch(PDO::FETCH_ASSOC))
            $results[] = (object) $row;
        return $results;
    }

    public function deleteByDeviceSignal($deviceId, $deviceSignal)
    {
        $query = <<<SQL
            DELETE FROM `signals_links`
            WHERE `device_id`=:deviceId AND `device_signal`=:deviceSignal;
        SQL;

        $stmt = DB::prepare($query);
        $stmt->bindParam(':deviceId', $deviceId);
        $stmt->bindParam(':deviceSignal', $deviceSignal);
        $stmt->execute();
    }

    public function countByDeviceSignal($deviceId, $deviceSignal)
    {
        $query = <<<SQL
            SELECT COUNT(*) as 'cnt'
            FROM `signals_links`
            WHERE `device_id`=:deviceId AND `device_signal`=:deviceSignal;
        SQL;

        $stmt = DB::prepare($query);
        $stmt->bindParam(':deviceId', $deviceId);
        $stmt->bindParam(':deviceSignal', $deviceSignal);
        $stmt->execute();

        $cnt = 0;
        if($row = $stmt->fetch(PDO::FETCH_ASSOC))
            $cnt = $row["cnt"];
        return $cnt;
    }

    public function insertOrUpdate(
        $driver, $driverSignal, $deviceId, $deviceSignal)
    {
        $rows = $this->selectByDeviceSignal($deviceId, $deviceSignal);

        if(count($rows) == 0) {
            if($driverSignal != "")
                $this->insert(
                    null, $driver, $driverSignal, $deviceId, $deviceSignal);
        } else {
            $this->updateByPrimaryKey(
                $rows[0]->id, $driver, $driverSignal, $deviceId, $deviceSignal);
        }
    }
}