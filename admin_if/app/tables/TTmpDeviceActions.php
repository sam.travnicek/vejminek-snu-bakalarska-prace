<?php

class TTmpDeviceActions extends Table
{
    public function __construct()
    {
        parent::__construct("tmp_device_actions", [
            "id", "device_id", "identifier", "label"
        ]);

        $this->orderBy = "label";
    }
}