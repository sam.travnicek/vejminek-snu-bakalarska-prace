<?php

class TTmpDrivers extends Table
{
    public function __construct()
    {
        parent::__construct("tmp_drivers", [
            "id", "plugin_identifier", "identifier", "label"
        ]);

        $this->orderBy = "label";
    }
}