<?php

class TTmpConfig extends Table
{
    public function __construct()
    {
        parent::__construct("tmp_config", ["id", "property", "value"]);

        $this->orderBy = "property";
    }
}