<?php

class TPortsLinks extends Table
{
    public function __construct()
    {
        parent::__construct("ports_links", [
            "id", "driver", "driver_port", "device_id", "device_port",
            "preferred_cfg"
        ]);

        $this->orderBy = "id";
    }

    public function selectByDevicePort($deviceId, $devicePort)
    {
        $query = <<<SQL
            SELECT
            `id`, `driver`, `driver_port`, `device_id`, `device_port`,
            `preferred_cfg`
            FROM `ports_links`
            WHERE `device_id`=:deviceId AND `device_port`=:devicePort;
        SQL;

        $stmt = DB::prepare($query);
        $stmt->bindParam(':deviceId', $deviceId);
        $stmt->bindParam(':devicePort', $devicePort);
        $stmt->execute();

        $results = [];
        while($row = $stmt->fetch(PDO::FETCH_ASSOC))
            $results[] = (object) $row;
        return $results;
    }

    public function deleteByDevicePort($deviceId, $devicePort)
    {
        $query = <<<SQL
            DELETE FROM `ports_links`
            WHERE `device_id`=:deviceId AND `device_port`=:devicePort;
        SQL;

        $stmt = DB::prepare($query);
        $stmt->bindParam(':deviceId', $deviceId);
        $stmt->bindParam(':devicePort', $devicePort);
        $stmt->execute();
    }

    public function countByDevicePort($deviceId, $devicePort)
    {
        $query = <<<SQL
            SELECT COUNT(*) as 'cnt'
            FROM `ports_links`
            WHERE `device_id`=:deviceId AND `device_port`=:devicePort;
        SQL;

        $stmt = DB::prepare($query);
        $stmt->bindParam(':deviceId', $deviceId);
        $stmt->bindParam(':devicePort', $devicePort);
        $stmt->execute();

        $cnt = 0;
        if($row = $stmt->fetch(PDO::FETCH_ASSOC))
            $cnt = $row["cnt"];
        return $cnt;
    }

    public function insertOrUpdate(
        $driver, $driverPort, $deviceId, $devicePort, $preferredCfg)
    {
        $rows = $this->selectByDevicePort($deviceId, $devicePort);

        if(count($rows) == 0) {
            if($driverPort != "")
                $this->insert(null, $driver, $driverPort, $deviceId,
                    $devicePort, $preferredCfg);
        } else {
            $this->updateByPrimaryKey($rows[0]->id, $driver, $driverPort,
                $deviceId, $devicePort, $preferredCfg);
        }
    }
}