<?php

class TSlotsLinks extends Table
{
    public function __construct()
    {
        parent::__construct("slots_links", [
            "id", "driver", "driver_slot", "device_id", "device_slot"
        ]);

        $this->orderBy = "id";
    }

    public function selectByDeviceSlot($deviceId, $deviceSlot)
    {
        $query = <<<SQL
            SELECT
            `id`, `driver`, `driver_slot`, `device_id`, `device_slot`
            FROM `slots_links`
            WHERE `device_id`=:deviceId AND `device_slot`=:deviceSlot;
        SQL;

        $stmt = DB::prepare($query);
        $stmt->bindParam(':deviceId', $deviceId);
        $stmt->bindParam(':deviceSlot', $deviceSlot);
        $stmt->execute();

        $results = [];
        while($row = $stmt->fetch(PDO::FETCH_ASSOC))
            $results[] = (object) $row;
        return $results;
    }

    public function deleteByDeviceSlot($deviceId, $deviceSlot)
    {
        $query = <<<SQL
            DELETE FROM `slots_links`
            WHERE `device_id`=:deviceId AND `device_slot`=:deviceSlot;
        SQL;

        $stmt = DB::prepare($query);
        $stmt->bindParam(':deviceId', $deviceId);
        $stmt->bindParam(':deviceSlot', $deviceSlot);
        $stmt->execute();
    }

    public function countByDeviceSlot($deviceId, $deviceSlot)
    {
        $query = <<<SQL
            SELECT COUNT(*) as 'cnt'
            FROM `slots_links`
            WHERE `device_id`=:deviceId AND `device_slot`=:deviceSlot;
        SQL;

        $stmt = DB::prepare($query);
        $stmt->bindParam(':deviceId', $deviceId);
        $stmt->bindParam(':deviceSlot', $deviceSlot);
        $stmt->execute();

        $cnt = 0;
        if($row = $stmt->fetch(PDO::FETCH_ASSOC))
            $cnt = $row["cnt"];
        return $cnt;
    }

    public function insertOrUpdate($driver, $driverSlot, $deviceId, $deviceSlot)
    {
        $rows = $this->selectByDeviceSlot($deviceId, $deviceSlot);

        if(count($rows) == 0) {
            if($driverSlot != "")
                $this->insert(
                    null, $driver, $driverSlot, $deviceId, $deviceSlot);
        } else {
            $this->updateByPrimaryKey(
                $rows[0]->id, $driver, $driverSlot, $deviceId, $deviceSlot);
        }
    }
}