<?php

class TTmpPlugins extends Table
{
    public function __construct()
    {
        parent::__construct("tmp_plugins", [
            "identifier", "label", "version", "author", "description",
            "entry_file", "directory", "valid"
        ]);

        $this->orderBy = "label";
    }
}