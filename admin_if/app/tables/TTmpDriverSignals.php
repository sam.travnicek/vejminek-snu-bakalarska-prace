<?php

class TTmpDriverSignals extends Table
{
    public function __construct()
    {
        parent::__construct("tmp_driver_signals", [
            "id", "driver_id", "identifier", "label", "direction"
        ]);

        $this->orderBy = "label";
    }

    public function selectByDirection($direction)
    {
        $query = <<<SQL
            SELECT
            `tmp_drivers`.`label` as 'driver_label',
            `tmp_drivers`.`identifier` as 'driver_identifier',
            `tmp_drivers`.`plugin_identifier` as 'plugin_identifier',
            `tmp_driver_signals`.`identifier` as 'signal_identifier',
            `tmp_driver_signals`.`label` as 'signal_label',
            `tmp_driver_signals`.`direction` as 'signal_direction'
            FROM
            `tmp_drivers`,
            `tmp_driver_signals`
            WHERE `tmp_drivers`.`id` = `tmp_driver_signals`.`driver_id`
            AND `tmp_driver_signals`.`direction` = :direction
        SQL;

        $stmt = DB::prepare($query);
        $stmt->bindParam(':direction', $direction);
        $stmt->execute();

        $results = [];

        while($row = $stmt->fetch(PDO::FETCH_ASSOC))
            $results[] = (object) $row;

        return $results;
    }
}