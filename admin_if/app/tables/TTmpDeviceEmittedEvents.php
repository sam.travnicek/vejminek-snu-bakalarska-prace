<?php

class TTmpDeviceEmittedEvents extends Table
{
    public function __construct()
    {
        parent::__construct("tmp_device_emitted_events", [
            "id", "device_id", "identifier", "label"
        ]);

        $this->orderBy = "label";
    }
}