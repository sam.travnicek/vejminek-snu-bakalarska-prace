<?php

class TTmpDriverPorts extends Table
{
    public function __construct()
    {
        parent::__construct("tmp_driver_ports", [
            "id", "driver_id", "identifier", "label"
        ]);

        $this->orderBy = "label";
    }

    public function selectByAvailableDirectionAndMode($direction, $mode)
    {
        $confs = (new TTmpDriverPortConfigurations())
            ->selectByDirectionAndMode($direction, $mode);

        $ports = [];

        foreach($confs as $conf) {
            $alreadyContained = false;

            foreach($ports as $port) {
                $alreadyContained =
                    $port->driver_identifier == $conf->driver_identifier
                    && $port->port_identifier == $conf->port_identifier;
                if($alreadyContained)
                    break;
            }

            if($alreadyContained)
                continue;
            $ports[] = $conf;
        }

        return $ports;
    }
}