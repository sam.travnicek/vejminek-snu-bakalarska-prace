<?php

class TTmpDriverSlots extends Table
{
    public function __construct()
    {
        parent::__construct("tmp_driver_slots", [
            "id", "driver_id", "identifier", "type", "label", "direction"
        ]);

        $this->orderBy = "label";
    }

    public function selectByDirectionAndType($direction, $type)
    {
        $query = <<<SQL
            SELECT
            `tmp_drivers`.`label` as 'driver_label',
            `tmp_drivers`.`identifier` as 'driver_identifier',
            `tmp_drivers`.`plugin_identifier` as 'plugin_identifier',
            `tmp_driver_slots`.`identifier` as 'slot_identifier',
            `tmp_driver_slots`.`label` as 'slot_label',
            `tmp_driver_slots`.`direction` as 'slot_direction',
            `tmp_driver_slots`.`type` as 'slot_type'
            FROM
            `tmp_drivers`,
            `tmp_driver_slots`
            WHERE `tmp_drivers`.`id` = `tmp_driver_slots`.`driver_id`
            AND `tmp_driver_slots`.`direction` = :direction
            AND `tmp_driver_slots`.`type` = :stype;
        SQL;

        $stmt = DB::prepare($query);
        $stmt->bindParam(':direction', $direction);
        $stmt->bindParam(':stype', $type);
        $stmt->execute();

        $results = [];

        while($row = $stmt->fetch(PDO::FETCH_ASSOC))
            $results[] = (object) $row;

        return $results;
    }
}