<?php

class TTmpDeviceActionArguments extends Table
{
    public function __construct()
    {
        parent::__construct("tmp_device_action_arguments", [
            "id", "action_id", "identifier", "type", "label"
        ]);

        $this->orderBy = "label";
    }
}