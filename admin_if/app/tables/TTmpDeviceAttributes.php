<?php

class TTmpDeviceAttributes extends Table
{
    public function __construct()
    {
        parent::__construct("tmp_device_attributes", [
            "id", "device_id", "identifier", "type", "label"
        ]);

        $this->orderBy = "label";
    }

    public function selectByDeviceId($deviceId)
    {
        $query = "SELECT ";

        for($i = 0; $i < count($this->fields); ++$i) {
            if($i != 0)
                $query .= ", ";
            $query .= "`" . $this->fields[$i] . "`";
        }

        $query .= " FROM `" . $this->tableName . "`";
        $query .= " WHERE `device_id`=:deviceId;";

        $stmt = DB::prepare($query);
        $stmt->bindParam(':deviceId', $deviceId);
        $stmt->execute();

        $results = [];

        while($row = $stmt->fetch(PDO::FETCH_ASSOC))
            $results[] = (object) $row;

        return $results;
    }

    public function selectByIdentifiers($deviceId, $identifier)
    {
        $query = "SELECT ";

        for($i = 0; $i < count($this->fields); ++$i) {
            if($i != 0)
                $query .= ", ";
            $query .= "`" . $this->fields[$i] . "`";
        }

        $query .= " FROM `" . $this->tableName . "`";
        $query .= " WHERE `device_id`=:deviceId";
        $query .= " AND `identifier`=:identifier;";

        $stmt = DB::prepare($query);
        $stmt->bindParam(':deviceId', $deviceId);
        $stmt->bindParam(':identifier', $identifier);
        $stmt->execute();

        $results = [];

        while($row = $stmt->fetch(PDO::FETCH_ASSOC))
            $results[] = (object) $row;

        return $results;
    }
}