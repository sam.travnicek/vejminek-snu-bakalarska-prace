<?php

class TUsers extends Table
{
    public function __construct()
    {
        parent::__construct("users", [
            "id", "username", "password", "permissions"
        ]);

        $this->orderBy = "username";
    }
}