<?php

class TRooms extends Table
{
    public function __construct()
    {
        parent::__construct("rooms", [
            "id", "label", "floor"
        ]);

        $this->orderBy = "label";
    }

    public function safeDelete($id)
    {
        $data = [];
        $data["label"] = NULL;
        $data["deleted"] = false;

        $rooms = $this->selectByPrimaryKey($id);
        if(count($rooms) != 1)
            return $data;

        $data["label"] = $rooms[0]->label;

        $dependentDevicesCnt = (new TDevices())->countByRoomId($id);
        if($dependentDevicesCnt > 0)
            return $data;

        $this->deleteByPrimaryKey($id);
        $data["deleted"] = true;

        return $data;
    }
}