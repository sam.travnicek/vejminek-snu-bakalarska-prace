<?php

class TTmpDriverPortConfigurations extends Table
{
    public function __construct()
    {
        parent::__construct("tmp_driver_port_configurations", [
            "id", "port_id", "direction", "mode", "identifier", "label"
        ]);

        $this->orderBy = "label";
    }

    function selectByDirectionAndMode($direction, $mode)
    {
        $query = <<<SQL
            SELECT
            `tmp_drivers`.`label` as 'driver_label',
            `tmp_drivers`.`identifier` as 'driver_identifier',
            `tmp_drivers`.`plugin_identifier` as 'plugin_identifier',
            `tmp_driver_ports`.`identifier` as 'port_identifier',
            `tmp_driver_ports`.`label` as 'port_label',
            `tmp_driver_port_configurations`.`identifier` as 'config_identifier',
            `tmp_driver_port_configurations`.`label` as 'config_label'
            FROM
            `tmp_drivers`,
            `tmp_driver_ports`,
            `tmp_driver_port_configurations`
            WHERE `tmp_drivers`.`id` = `tmp_driver_ports`.`driver_id`
            AND `tmp_driver_ports`.`id` = `tmp_driver_port_configurations`.`port_id`
            AND `tmp_driver_port_configurations`.`direction` = :direction
            AND `tmp_driver_port_configurations`.`mode` = :mode;
        SQL;

        $stmt = DB::prepare($query);
        $stmt->bindParam(':direction', $direction);
        $stmt->bindParam(':mode', $mode);
        $stmt->execute();

        $results = [];
        while($row = $stmt->fetch(PDO::FETCH_ASSOC))
            $results[] = (object) $row;
        return $results;
    }

    function selectByPortId($portId)
    {
        $query = "SELECT ";

        for($i = 0; $i < count($this->fields); ++$i) {
            if($i != 0)
                $query .= ", ";
            $query .= "`" . $this->fields[$i] . "`";
        }

        $query .= " FROM `" . $this->tableName . "`";
        $query .= " WHERE `port_id`=:portId";

        if($this->orderBy != "")
            $query .= " ORDER BY `" . $this->orderBy . "` ASC";

        $query .= ";";

        $stmt = DB::prepare($query);
        $stmt->bindParam(':portId', $portId);
        $stmt->execute();

        $results = [];

        while($row = $stmt->fetch(PDO::FETCH_ASSOC))
            $results[] = (object) $row;

        return $results;
    }
}