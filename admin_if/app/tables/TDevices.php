<?php

class TDevices extends Table
{
    public function __construct()
    {
        parent::__construct("devices", [
            "id", "room_id", "label", "description", "template", "data"
        ]);

        $this->orderBy = "label";
    }

    public function countByRoomId($roomId)
    {
        $query = <<<SQL
            SELECT COUNT(*) as 'cnt' FROM `devices`
            WHERE `room_id` = :roomId;
        SQL;

        $stmt = DB::prepare($query);
        $stmt->bindParam(':roomId', $roomId);
        $stmt->execute();

        if($row = $stmt->fetch(PDO::FETCH_ASSOC))
            return $row["cnt"];
        return 0;
    }

    public function safeInsert($label, $roomId)
    {
        $rooms = (new TRooms())->selectByPrimaryKey($roomId);
        if(count($rooms) != 1)
            return false;

        $this->insert(NULL, $roomId, $label, "", "", "");
        return true;
    }

    public function safeDelete($id)
    {
        $query1 = <<<SQL
            DELETE FROM `ports_links` WHERE `device_id`=:deviceId;
        SQL;

        $query2 = <<<SQL
            DELETE FROM `slots_links` WHERE `device_id`=:deviceId;
        SQL;

        $query3 = <<<SQL
            DELETE FROM `signals_links` WHERE `device_id`=:deviceId;
        SQL;

        $stmt = DB::prepare($query1);
        $stmt->bindParam(':deviceId', $id);
        $stmt->execute();

        $stmt = DB::prepare($query2);
        $stmt->bindParam(':deviceId', $id);
        $stmt->execute();

        $stmt = DB::prepare($query3);
        $stmt->bindParam(':deviceId', $id);
        $stmt->execute();

        $this->deleteByPrimaryKey($id);
    }

    public function safeUpdate(
        $id, $roomId, $label, $description, $template, $data)
    {
        $rows = $this->selectByPrimaryKey($id);

        if(count($rows) == 0)
            return;

        if($rows[0]->template != $template) {
            $query1 = <<<SQL
                DELETE FROM `ports_links` WHERE `device_id`=:deviceId;
            SQL;

            $query2 = <<<SQL
                DELETE FROM `slots_links` WHERE `device_id`=:deviceId;
            SQL;

            $query3 = <<<SQL
                DELETE FROM `signals_links` WHERE `device_id`=:deviceId;
            SQL;

            $stmt = DB::prepare($query1);
            $stmt->bindParam(':deviceId', $id);
            $stmt->execute();

            $stmt = DB::prepare($query2);
            $stmt->bindParam(':deviceId', $id);
            $stmt->execute();

            $stmt = DB::prepare($query3);
            $stmt->bindParam(':deviceId', $id);
            $stmt->execute();
        }

        $this->updateByPrimaryKey(
            $id, $roomId, $label, $description, $template, $data);
    }

    public function devices($ofs, $cnt)
    {
        $query = <<<SQL
            SELECT
                `rooms`.`label` as 'room',
                `devices`.`label` as 'label',
                `devices`.`id` as 'id'
            FROM `devices`, `rooms`
            WHERE `devices`.`room_id` = `rooms`.`id`
            ORDER BY `devices`.`label`
            LIMIT :ofs,:cnt
        SQL;

        $stmt = DB::prepare($query);
        $stmt->bindParam(':ofs', $ofs, PDO::PARAM_INT);
        $stmt->bindParam(':cnt', $cnt, PDO::PARAM_INT);
        $stmt->execute();

        $results = [];
        while($row = $stmt->fetch(PDO::FETCH_ASSOC))
            $results[] = (object) $row;
        return $results;
    }
}