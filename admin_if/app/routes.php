<?php

Route::get("/", function()
{
    return view("overview");
});

Route::get("/logout", function()
{
    unset($_SESSION["authorized"]);
    header("Location: " . BASE_PATH);
    return "";
});

Route::get("/overview", function()
{
    return view("overview");
});

Route::get("/server/plugins", function()
{
    $table = new TTmpPlugins();
    return view("server-plugins", ['table' => $table]);
});

Route::get("/server/devices", function()
{
    $table = new TDevices();
    return view("server-devices", ['table' => $table]);
});

Route::get("/server/new-device", function()
{
    $tableDevices = new TDevices();
    $tableRooms = new TRooms();

    return view("server-new-device", [
        'tableDevices' => $tableDevices,
        'tableRooms' => $tableRooms
    ]);
});

Route::get("/server/edit-device/{id}", function($params)
{
    return view("server-edit-device", [
        'tableDevices' => new TDevices(),
        'tableRooms' => new TRooms(),
        'tableDeviceTemplates' => new TTmpDevices(),
        'tableDevicePorts' => new TTmpDevicePorts(),
        'tableDeviceSlots' => new TTmpDeviceSlots(),
        'tableDeviceSignals' => new TTmpDeviceSignals(),
        'tableDeviceAttributes' => new TTmpDeviceAttributes(),
        'deviceId' => $params['id']
    ]);
});

Route::get("/server/edit-device/{id}/port/{port-identifier}", function($params)
{
    $tableDevices = new TDevices();
    $tableDeviceTemplates = new TTmpDevices();
    $tableDevicePorts = new TTmpDevicePorts();

    $deviceLabel = "";
    $deviceTemplate = "";

    foreach($tableDevices->selectByPrimaryKey($params['id']) as $row) {
        $deviceLabel = $row->label;
        $deviceTemplate = $row->template;
        break;
    }

    $deviceTemplateId = 0;

    if($deviceTemplate != "") {
        $templParts = explode(":", $deviceTemplate);
        $pluginIdentifier = $templParts[0];
        $deviceIdentifier = $templParts[1];
        $res = $tableDeviceTemplates->selectByIdentifiers(
            $pluginIdentifier, $deviceIdentifier);

        foreach($res as $row) {
            $deviceTemplateId = $row->id;
            break;
        }
    }

    $portDescriptor = null;

    if($deviceTemplateId != 0) {
        $res = $tableDevicePorts->selectByIdentifiers(
            $deviceTemplateId, $params['port-identifier']);

        foreach($res as $row) {
            $portDescriptor = $row;
            break;
        }
    }

    return view("server-edit-device-port", [
        'tablePortsLinks' => new TPortsLinks(),
        'tableDrivers' => new TTmpDrivers(),
        'tableDriverPorts' => new TTmpDriverPorts(),
        'tableDriverPortConfigurations' => new TTmpDriverPortConfigurations(),
        'deviceId' => $params['id'],
        'deviceLabel' => $deviceLabel,
        'portDescriptor' => $portDescriptor
    ]);
});

Route::get("/server/edit-device/{id}/slot/{slot-identifier}", function($params)
{
    $tableDevices = new TDevices();
    $tableDeviceTemplates = new TTmpDevices();
    $tableDeviceSlots = new TTmpDeviceSlots();

    $deviceLabel = "";
    $deviceTemplate = "";

    foreach($tableDevices->selectByPrimaryKey($params['id']) as $row) {
        $deviceLabel = $row->label;
        $deviceTemplate = $row->template;
        break;
    }

    $deviceTemplateId = 0;

    if($deviceTemplate != "") {
        $templParts = explode(":", $deviceTemplate);
        $pluginIdentifier = $templParts[0];
        $deviceIdentifier = $templParts[1];
        $res = $tableDeviceTemplates->selectByIdentifiers(
            $pluginIdentifier, $deviceIdentifier);

        foreach($res as $row) {
            $deviceTemplateId = $row->id;
            break;
        }
    }

    $slotDescriptor = null;

    if($deviceTemplateId != 0) {
        $res = $tableDeviceSlots->selectByIdentifiers(
            $deviceTemplateId, $params['slot-identifier']);

        foreach($res as $row) {
            $slotDescriptor = $row;
            break;
        }
    }

    return view("server-edit-device-slot", [
        'tableSlotsLinks' => new TSlotsLinks(),
        'tableDrivers' => new TTmpDrivers(),
        'tableDriverSlots' => new TTmpDriverSlots(),
        'deviceId' => $params['id'],
        'deviceLabel' => $deviceLabel,
        'slotDescriptor' => $slotDescriptor
    ]);
});

Route::get("/server/edit-device/{id}/signal/{signal-identifier}", function($params)
{
    $tableDevices = new TDevices();
    $tableDeviceTemplates = new TTmpDevices();
    $tableDeviceSignals = new TTmpDeviceSignals();

    $deviceLabel = "";
    $deviceTemplate = "";

    foreach($tableDevices->selectByPrimaryKey($params['id']) as $row) {
        $deviceLabel = $row->label;
        $deviceTemplate = $row->template;
        break;
    }

    $deviceTemplateId = 0;

    if($deviceTemplate != "") {
        $templParts = explode(":", $deviceTemplate);
        $pluginIdentifier = $templParts[0];
        $deviceIdentifier = $templParts[1];
        $res = $tableDeviceTemplates->selectByIdentifiers(
            $pluginIdentifier, $deviceIdentifier);

        foreach($res as $row) {
            $deviceTemplateId = $row->id;
            break;
        }
    }

    $signalDescriptor = null;

    if($deviceTemplateId != 0) {
        $res = $tableDeviceSignals->selectByIdentifiers(
            $deviceTemplateId, $params['signal-identifier']);

        foreach($res as $row) {
            $signalDescriptor = $row;
            break;
        }
    }

    return view("server-edit-device-signal", [
        'tableSignalsLinks' => new TSignalsLinks(),
        'tableDrivers' => new TTmpDrivers(),
        'tableDriverSignals' => new TTmpDriverSignals(),
        'deviceId' => $params['id'],
        'deviceLabel' => $deviceLabel,
        'signalDescriptor' => $signalDescriptor
    ]);
});

Route::get("/rooms", function()
{
    $table = new TRooms();
    return view("rooms", ['table' => $table]);
});

Route::get("/new-room", function()
{
    $table = new TRooms();
    return view("new-room", ['table' => $table]);
});

Route::get("/edit-room/{id}", function($params)
{
    $table = new TRooms();

    return view("edit-room", [
        'table' => $table,
        'roomId' => $params["id"]
    ]);
});