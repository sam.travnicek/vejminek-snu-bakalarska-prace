identifier = "cz.din_u10"
label = "DIN U10"
author = "Samuel Trávníček"
description = "Driver for DIN-U10 device"
version = "1.0"
entry_file = "main.lua"
dependencies = {}
optional_dependencies = {}

-- using Language Codes ISO 639-1 standard
primary_language = "en"