DIN_U10_Driver = {}

-- local function to_bin_str(num)
--     num = math.floor(num)
--     if num == 0 then
--         return "0"
--     end
--     res = ""
--     while num > 0 do
--         res = math.fmod(num, 2) .. res
--         num = num / 2
--         num = math.floor(num)
--     end
--     return res
-- end

-- //TODO: implement driver port groups (grouping ports of one device for example)

function DIN_U10_Driver:new(port)
    local obj = Driver:new("DIN_U10_1", "DIN-U10 #1")

    local out_cfg = {
        {identifier = 'digital', direction = OUTPUT, mode = LOGICAL},
        {identifier = 'pwm', direction = OUTPUT, mode = DISCRETE, min = 0, max = (1 << 12) - 1}
    }

    obj:port("out0", "Výstup 0", out_cfg)
    obj:port("out1", "Výstup 1", out_cfg)
    obj:port("out2", "Výstup 2", out_cfg)
    obj:port("out3", "Výstup 3", out_cfg)
    obj:port("out4", "Výstup 4", out_cfg)
    obj:port("out5", "Výstup 5", out_cfg)
    obj:port("out6", "Výstup 6", out_cfg)
    obj:port("out7", "Výstup 7", out_cfg)
    obj:port("out8", "Výstup 8", out_cfg)
    obj:port("out9", "Výstup 9", {
        {identifier = 'digital', direction = OUTPUT, mode = LOGICAL},
        {identifier = 'pwm', direction = OUTPUT, mode = DISCRETE, min = 0, max = (1 << 8) - 1}
    })

    local in_cfg = {
        {identifier = 'digital', direction = INPUT, mode = LOGICAL},
        {identifier = 'analog', direction = INPUT, mode = DISCRETE, min = 0, max = (1 << 12) - 1},
        {identifier = 'counter_rising_edge', direction = INPUT, mode = DISCRETE, min = 0, max = (1 << 16) - 1},
        {identifier = 'counter_falling_edge', direction = INPUT, mode = DISCRETE, min = 0, max = (1 << 16) - 1}
    }

    obj:port("in0", "Vstup 0", in_cfg)
    obj:port("in1", "Vstup 1", in_cfg)
    obj:port("in2", "Vstup 2", in_cfg)
    obj:port("in3", "Vstup 3", in_cfg)
    obj:port("in4", "Vstup 4", in_cfg)
    obj:port("in5", "Vstup 5", in_cfg)
    obj:port("in6", "Vstup 6", in_cfg)
    obj:port("in7", "Vstup 7", in_cfg)
    obj:port("in8", "Vstup 8", in_cfg)
    obj:port("in9", "Vstup 9", in_cfg)

    local DIR_OUTPUT        = 0
    local DIR_INPUT         = 1

    local MODE_DIGITAL      = 0
    local MODE_PWM          = 1
    local MODE_ANALOG       = 1
    local MODE_COUNTER_RE   = 2
    local MODE_COUNTER_FE   = 3

    local PRIORITY_VERY_LOW     = 0x3
    local PRIORITY_LOW          = 0x2
    local PRIORITY_HIGH         = 0x1
    local PRIORITY_VERY_HIGH    = 0x0

    local PCMD_RESET        = 0x000
    local PCMD_INFO         = 0x001
    local PCMD_IO_MODES     = 0x010
    local PCMD_WR_DOUTS     = 0x020
    local PCMD_WR_IO        = 0x021
    local PCMD_RD_DINS      = 0x030
    local PCMD_RD_IO        = 0x031

    local io_modes = {
        outputs = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        inputs = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
    }

    local rdata = {}
    local rdata_ptr = 1
    local cdata_len = 0

    local lastInValues = 0

    local function handle_packet(packet)
        if not packet.not_request then
            return end
        if packet.src_id ~= 0x01 then
            return end

        if packet.cmd == 0x030 then
            local states = packet.data[1] | (packet.data[2] << 8)
            for i=1,10 do
                if (states >> (i-1)) & 1 ~= (lastInValues >> (i-1)) & 1 then
                    if io_modes.inputs[i] == MODE_DIGITAL then
                        obj:write_port("in"..(i-1), (states >> (i-1)) & 1)
                    end
                end
            end
            lastInValues = states
        elseif packet.cmd == PCMD_RD_IO then
            local id = packet.data[1] & 0xF
            local dir = (packet.data[1] >> 4) & 1
            local value = packet.data[2] | (packet.data[3] << 8)

            if dir == DIR_INPUT then
                local port = obj:write_port("in"..id, value)
            end
        end
    end

    local function create_packet(dest, prio, is_request, len, cmd)
        if len > 8 then
            len = 8 end
        local packet = {}
        packet.src_id = 0x00
        packet.dest_id = dest & 0x7F
        packet.priority = prio & 0x3
        packet.not_request = is_request and 0 or 1
        packet.length = len
        packet.cmd = cmd
        packet.data = {}
        return packet
    end

    local function packet_request_info(dest, prio)
        return create_packet(dest, prio, true, 0, PCMD_INFO)
    end

    local function packet_io_modes(dest, prio, modes)
        local modes_enc = 0;
        for i=1,10 do
            modes_enc = modes_enc | ((modes.outputs[i] & 1) << (i-1))
        end
        for i=1,10 do
            modes_enc = modes_enc | ((modes.inputs[i] & 3) << ((i-1)*2+10))
        end

        local packet = create_packet(dest, prio, true, 4, PCMD_IO_MODES)
        packet.data[1] = modes_enc & 0xFF
        packet.data[2] = (modes_enc >> 8) & 0xFF
        packet.data[3] = (modes_enc >> 16) & 0xFF
        packet.data[4] = (modes_enc >> 24) & 0xFF
        return packet
    end

    local function packet_write_digital_outputs(dest, prio, values)
        value = math.floor(value)
        local packet = create_packet(dest, prio, true, 2, PCMD_WR_DOUTS)
        packet.data[1] = values & 0xFF
        packet.data[2] = (values >> 8) & 0x3
        return packet
    end

    local function packet_write_io(dest, prio, dir, id, value)
        value = math.floor(value)

        if dir == DIR_OUTPUT then
            if id > 9 then return end
        elseif dir == DIR_INPUT then
            if id > 9 then return end
        else
            return
        end

        local packet = create_packet(dest, prio, true, 3, PCMD_WR_IO)
        packet.data[1] = id | (dir << 4)
        packet.data[2] = value & 0xFF
        packet.data[3] = (value >> 8) & 0xFF
        return packet
    end

    local function packet_read_io(dest, prio, dir, id)
        if dir == DIR_OUTPUT then
            if id > 9 then return end
        elseif dir == DIR_INPUT then
            if id > 9 then return end
        else
            return
        end

        local packet = create_packet(dest, prio, true, 1, PCMD_RD_IO)
        packet.data[1] = id | (dir << 4)
        return packet
    end

    local function packet_to_bytes(packet)
        local bytes = {}

        bytes[1] = 0xAA
        bytes[2] = packet.src_id
        bytes[3] = packet.dest_id
        bytes[4] = (packet.priority << 5) | (packet.not_request << 4) | packet.length
        bytes[5] = packet.cmd & 0xFF
        bytes[6] = (packet.cmd >> 8) & 0xF

        for i=1,packet.length do
            bytes[6+i] = packet.data[i] end

        local sum = 0
        for i=1,packet.length+6 do
            sum = sum + bytes[i] end
        sum = ~sum & 0xFF
        bytes[7+packet.length] = sum

        return bytes
    end

    local serial = SerialPort:new(port, 115200)
    if not serial then
        error("Driver 'DIN_U10' partially disabled due to unavailable serial port '"..port.."'.")
        return obj
    end

    local function discrete_inputs_routine()
        for i=1,10 do
            if io_modes.inputs[i] ~= MODE_DIGITAL then
                local packet = packet_read_io(0x01, PRIORITY_VERY_LOW, DIR_INPUT, i-1)
                serial:send_bytes(packet_to_bytes(packet))
            end
        end
    end

    obj:on("tick", function(time, ticks, tps)
        if ticks % (tps/20) == 0 then
            discrete_inputs_routine()
        end

        if serial:available() and not serial:io_error() then
            local data = serial:read_bytes()
            for i, b in ipairs(data) do
                if rdata_ptr == 1 then -- Start Byte
                    if b == 0x55 then
                        rdata[1] = 0x55
                        rdata_ptr = 2
                    end
                elseif rdata_ptr == 4 then -- (Priority << 4) | Length
                    rdata[rdata_ptr] = b
                    rdata_ptr = rdata_ptr + 1
                    cdata_len = b & 0xF
                    if cdata_len > 8 then
                        cdata_len = 8
                    end
                elseif rdata_ptr < 7 then
                    rdata[rdata_ptr] = b
                    rdata_ptr = rdata_ptr + 1
                elseif rdata_ptr < 7+cdata_len then
                    rdata[rdata_ptr] = b
                    rdata_ptr = rdata_ptr + 1
                else
                    rdata[rdata_ptr] = b
                    rdata_ptr = rdata_ptr + 1

                    local sum = 0
                    for i=1,6+cdata_len do
                        sum = sum + rdata[i]
                    end
                    sum = ~sum & 0xFF

                    if sum == rdata[7+cdata_len] then -- Valid packet
                        local packet = {}
                        packet.src_id = rdata[2] & 0x7F
                        packet.dest_id = rdata[3] & 0x7F
                        packet.priority = (rdata[4] >> 5) & 0x3
                        packet.not_request = (rdata[4] >> 4) & 0x1
                        packet.length = cdata_len
                        packet.cmd = rdata[5] | (rdata[6] << 8)
                        packet.data = {}

                        for i=1,cdata_len do
                            packet.data[i] = rdata[6+i]
                        end

                        handle_packet(packet)
                    end

                    rdata_ptr = 1
                end
            end
        end
    end)

    obj:on("port_linked", function(identifier, dir, mode, cfg_identifier)
        for i=1,10 do
            if identifier == "out"..(i-1) then
                if cfg_identifier == "digital" then
                    io_modes.outputs[i] = MODE_DIGITAL
                elseif cfg_identifier == "pwm" then
                    io_modes.outputs[i] = MODE_PWM
                end
                return
            end
        end

        for i=1,10 do
            if identifier == "in"..(i-1) then
                if cfg_identifier == "digital" then
                    io_modes.inputs[i] = MODE_DIGITAL
                elseif cfg_identifier == "analog" then
                    io_modes.inputs[i] = MODE_ANALOG
                elseif cfg_identifier == "counter_rising_edge" then
                    io_modes.inputs[i] = MODE_COUNTER_RE
                elseif cfg_identifier == "counter_falling_edge" then
                    io_modes.inputs[i] = MODE_COUNTER_FE
                end
                return
            end
        end
    end)

    obj:on("ports_linkage_complete", function()
        local packet = packet_io_modes(0x01, 0, io_modes)
        serial:send_bytes(packet_to_bytes(packet))
    end)

    obj:on("port_written", function(identifier, value)
        if value < 0 then
            return end

        local io_id = -1
        local dir = nil

        for i=0,9 do
            if identifier == "out"..i then
                dir = DIR_OUTPUT
                io_id = i
                break
            end
        end

        if dir == nil then
            for i=0,9 do
                if identifier == "in"..i then
                    dir = DIR_INPUT
                    io_id = i
                    break
                end
            end
        end

        if dir == nil then
            return end

        local packet = packet_write_io(0x01, PRIORITY_HIGH, dir, io_id, value)
        serial:send_bytes(packet_to_bytes(packet))
    end)

    return obj
end