local scheduler = service("scheduler")
-- local serial = SerialPort:new("/dev/ttyUSB0", 115200)
local ExampleDriver = {}

-- //TODO: Concept of stored values (in database) of Device
-- local x = new Device("DigitalAttenuator")
-- x:stored{"enabled" = <default_value>, "rising", "value"}

Plugin:on("initialization", function()
    init_light()
    init_regulated_light()
    init_exp_regulated_light()
    init_sin2_regulated_light()
    init_discrete_watcher()
    init_logical_watcher()
    init_test_device()
    init_blinds()
    print("Načteno.")
end)

function init_light()
    local dev = Device:new("Light", "Světlo")

    local function cancelTimer(instance)
        if instance.timerTask ~= nil then
            scheduler:cancel(instance.timerTask)
            instance.timerTask = nil
        end
    end

    dev:port("light", "Světlo", {
        direction = OUTPUT,
        mode = LOGICAL,
        value_stored = true
    })

    dev:port("button", "Spínač", {
        direction = INPUT,
        mode = LOGICAL
    })

    dev:action("turn_on", "Rozsvítit", function(instance)
        instance:port("light"):write(HIGH)
        cancelTimer(instance)
    end)

    dev:action("turn_off", "Zhasnout", function(instance)
        instance:port("light"):write(LOW)
    end)

    dev:action("impulse", "Rozvítit na ...", {
        {identifier="duration", label="Trvání (sec)", type=NUMBER, default_value=3}
    }, function(instance, args)
        local osTime = os.clock()
        local validUntil = osTime + args.duration

        if instance.timerTask ~= nil then
            if validUntil > instance.timerValidUntil then
                scheduler:cancel(instance.timerTask)
            else
                return
            end
        end

        local port = instance:port("light")
        port:write(HIGH)

        instance.timerValidUntil = validUntil
        instance.timerTask = scheduler:schedule(args.duration*1000, function()
            instance.timerTask = nil
            port:write(LOW)
        end)
    end)

    dev:on("instantiation", function(instance)
        instance.lastValue = instance:port("button"):read()
    end)

    dev:on("port_changed", function(instance, port, value, prev_value)
        if port:identifier() == "button" then
            local val = port:read()
            if val == HIGH and instance.lastValue == LOW then
                instance:port("light"):toggle()
            end
            instance.lastValue = val
            cancelTimer(instance)
        elseif port:identifier() == "light" then
            if value == LOW then
                cancelTimer(instance)
            end
        end
    end)
end

function init_regulated_light()
    local dev = Device:new("RegulatedLight", "Regulované světlo")

    dev:port("light", "Světlo", {
        direction = OUTPUT,
        mode = DISCRETE,
        value_stored = true
    })

    dev:port("button", "Spínač", {
        direction = INPUT,
        mode = LOGICAL
    })

    dev:action("turn_on", "Rozsvítit", function(instance)
        instance:port("light"):write(instance:port("light"):max())
    end)

    dev:action("turn_off", "Zhasnout", function(instance)
        instance:port("light"):write(instance:port("light"):min())
    end)

    dev:on("instantiation", function(instance)
        instance.enabled = instance:port("button"):read() == HIGH
        instance.lastWrite = 0
        instance.x = 0
    end)

    dev:on("port_changed", function(instance, port, value, prev_value)
        if port:identifier() == "button" then
            instance.enabled = port:read() == HIGH
            instance.lastWrite = 0
        end
    end)

    dev:on("tick", function(instance, time, ticks, tps)
        if not instance.enabled then
            return end

        if instance.lastWrite == 0 then
            instance.lastWrite = time
        end

        if time - instance.lastWrite < 50 then
            return end

        local outPort = instance:port("light")
        if not outPort:linked() then
            return end

        local range = outPort:max() - outPort:min()
        instance.x = instance.x + (time - instance.lastWrite) / 1000
        local val = outPort:min() + range*(math.sin(math.pi*instance.x) + 1)/2
        instance.lastWrite = time
        outPort:write(val)
    end)
end

function init_exp_regulated_light()
    local dev = Device:new("ExpRegulatedLight", "Světlo regulované exponenciálním průběhem")

    dev:port("light", "Světlo", {
        direction = OUTPUT,
        mode = DISCRETE,
        value_stored = true
    })

    dev:port("button", "Spínač", {
        direction = INPUT,
        mode = LOGICAL
    })

    dev:action("turn_on", "Rozsvítit", function(instance)
        instance:port("light"):write(instance:port("light"):max())
    end)

    dev:action("turn_off", "Zhasnout", function(instance)
        instance:port("light"):write(instance:port("light"):min())
    end)

    dev:on("instantiation", function(instance)
        instance.enabled = instance:port("button"):read() == HIGH
        instance.lastWrite = 0
        instance.x = 0
    end)

    dev:on("port_changed", function(instance, port, value, prev_value)
        if port:identifier() == "button" then
            instance.enabled = port:read() == HIGH
            instance.lastWrite = 0
        end
    end)

    dev:on("tick", function(instance, time, ticks, tps)
        if not instance.enabled then
            return end

        if instance.lastWrite == 0 then
            instance.lastWrite = time
        end

        if time - instance.lastWrite < 50 then
            return end

        local outPort = instance:port("light")
        if not outPort:linked() then
            return end

        local range = outPort:max() - outPort:min()
        instance.x = instance.x + (time - instance.lastWrite) / 3000
        while instance.x >= 1 do
            instance.x = instance.x - 1
        end

        local function transform(x, base)
            if x < 0 or x >= 1 then
                return 0 end

            local function baseTransform(x)
                return math.pow(base, x)
            end

            if x < 0.5 then
                return (math.pow(base, 2*x)-1)/(baseTransform(1)-1)
            else
                return (math.pow(base, 2*(1-x))-1)/(baseTransform(1)-1)
            end
        end

        local val = outPort:min() + range*transform(instance.x, 1000)
        instance.lastWrite = time
        outPort:write(val)
    end)
end

function init_sin2_regulated_light()
    local dev = Device:new("Sin2RegulatedLight", "Světlo regulované průběhem sin^2")

    dev:port("light", "Světlo", {
        direction = OUTPUT,
        mode = DISCRETE,
        value_stored = true
    })

    dev:port("button", "Spínač", {
        direction = INPUT,
        mode = LOGICAL
    })

    dev:action("turn_on", "Rozsvítit", function(instance)
        instance:port("light"):write(instance:port("light"):max())
    end)

    dev:action("turn_off", "Zhasnout", function(instance)
        instance:port("light"):write(instance:port("light"):min())
    end)

    dev:on("instantiation", function(instance)
        instance.enabled = instance:port("button"):read() == HIGH
        instance.lastWrite = 0
        instance.x = 0
    end)

    dev:on("port_changed", function(instance, port, value, prev_value)
        if port:identifier() == "button" then
            instance.enabled = port:read() == HIGH
            instance.lastWrite = 0
        end
    end)

    dev:on("tick", function(instance, time, ticks, tps)
        if not instance.enabled then
            return end

        if instance.lastWrite == 0 then
            instance.lastWrite = time
        end

        if time - instance.lastWrite < 50 then
            return end

        local outPort = instance:port("light")
        if not outPort:linked() then
            return end

        local range = outPort:max() - outPort:min()
        instance.x = instance.x + (time - instance.lastWrite) / 1000
        local val = outPort:min() + range*math.pow((math.sin(math.pi*instance.x) + 1)/2, 2)
        instance.lastWrite = time
        outPort:write(val)
    end)
end

-- function init_switch()
--     local dev = Device:new("Switch", "Vypínač")

--     dev:port("input", "Vstup", {
--         direction = INPUT,
--         mode = LOGICAL
--     })

--     dev:emitted_event("changed")

--     dev:on("port_changed", function(instance, port, value, prev_value)
--         instance:fire("changed")
--     end)
-- end

function init_discrete_watcher()
    local dev = Device:new("DiscreteWatcher", "Sledovač diskrétní hodnoty")

    dev:port("input", "Vstup", { direction = INPUT, mode = DISCRETE })
    dev:port("output", "Výstup", { direction = OUTPUT, mode = DISCRETE })

    dev:on("port_changed", function(instance, port, value, prev_value)
        -- warning("changed")
        if port:identifier() == "input" then
            local outPort = instance:port("output")
            local outRange = outPort:max() - outPort:min()
            local inRange = port:max() - port:min()
            local val = outPort:min() + outRange*(port:read() - port:min())/inRange
            outPort:write(val)
        end
    end)
end

function init_logical_watcher()
    local dev = Device:new("LogicalWatcher", "Sledovač logické hodnoty")

    dev:port("input", "Vstup", { direction = INPUT, mode = LOGICAL })
    dev:port("output", "Výstup", { direction = OUTPUT, mode = LOGICAL })

    dev:on("port_changed", function(instance, port, value, prev_value)
        -- warning("changed")
        if port:identifier() == "input" then
            instance:port("output"):write(port:read())
        end
    end)
end

function init_test_device()
    local dev = Device:new("TestDevice", "Testovací zařízení")

    dev:port("trigger", "Spoušť", { direction = INPUT, mode = LOGICAL })

    dev:slot("number", "Number", DRIVER_IN, NUMBER)
    dev:slot("result", "Result", DRIVER_OUT, BOOLEAN)

    dev:signal("trigger_sig", "Spoušť", DRIVER_IN)

    dev:on("port_changed", function(instance, port, value, prev_value)
        if port:identifier() == "trigger" then
            local x = math.random(0, 100)
            instance:slot("number"):write(x)
            instance:signal("trigger_sig"):invoke()
        end
    end)

    dev:on("slot_written", function(instance, slot, value)
        if slot:identifier() == "result" then
            info("[DEV] SLOT '" .. slot:identifier() .. "' written: " .. (value and "true" or "false"))
        end
    end)
end

function init_blinds()
    local dev = Device:new("blinds", "Rolety")

    dev:port("motor_up", "Motor - nahoru", { direction = OUTPUT, mode = LOGICAL })
    dev:port("motor_down", "Motor - dolů", { direction = OUTPUT, mode = LOGICAL })
    dev:port("push_button_up", "Tlačítko - nahoru", { direction = INPUT, mode = LOGICAL })
    dev:port("push_button_down", "Tlačítko - dolů", { direction = INPUT, mode = LOGICAL })
    dev:port("push_button_stop", "Tlačítko - stop", { direction = INPUT, mode = LOGICAL })
    dev:port("limit_switch_upper", "Koncový spínač - horní", { direction = INPUT, mode = LOGICAL })
    dev:port("limit_switch_lower", "Koncový spínač - dolní", { direction = INPUT, mode = LOGICAL })

    dev:slot("speed", "Rychlost", DRIVER_IN, NUMBER)

    dev:emitted_event("down", "Zatáhly se")
    dev:emitted_event("up", "Vytáhly se")
    dev:emitted_event("stopped", "Zastavily")
    dev:emitted_event("movement_down_inhibited", "Pohyb dolů zablokován")

    dev:attribute("motors_speed", "Rychlost motorů", NUMBER)
    dev:attribute("unsafe_wind", "Fouká silný vítr?", CONDITION)

    dev:action("move_up", "Posuv nahoru", function(instance)
        instance:port("motor_down"):write(LOW)
        instance:port("motor_up"):write(HIGH)
    end)

    dev:action("move_down", "Posuv dolů", function(instance)
        instance:port("motor_up"):write(LOW)
        instance:port("motor_down"):write(HIGH)
    end)

    dev:action("stop", "Stop", function(instance)
        instance:port("motor_up"):write(LOW)
        instance:port("motor_down"):write(LOW)
        instance:fire("stopped")
    end)

    dev:on("port_changed", function(instance, port, value, prev_value)
        if value ~= HIGH then
            return
        end

        local pid = port:identifier()

        if pid == "limit_switch_upper" then
            instance:port("motor_up"):write(LOW)
            instance:fire("up")
        elseif pid == "limit_switch_lower" then
            instance:port("motor_down"):write(LOW)
            instance:fire("down")
        elseif pid == "push_button_up" then
            instance:port("motor_up"):write(HIGH)
            instance:port("motor_down"):write(LOW)
        elseif pid == "push_button_down" then
            if not instance:attribute("unsafe_wind") then
                instance:port("motor_up"):write(LOW)
                instance:port("motor_down"):write(HIGH)
            else
                instance:fire("movement_down_inhibited")
            end
        elseif pid == "push_button_stop" then
            instance:port("motor_down"):write(LOW)
            instance:port("motor_up"):write(LOW)
            instance:fire("stopped")
        elseif pid == "motor_up" then
            if value == HIGH then
                instance:port("motor_down"):write(LOW)
            end
        elseif pid == "motor_down" then
            if value == HIGH then
                instance:port("motor_up"):write(LOW)
            end
        end
    end)

    local scheduler = service('scheduler')

    local function check_wind_condition(instance)
        debug('Checking wind condition')

        if instance:attribute("unsafe_wind") then
            instance:port("motor_down"):write(LOW)
            instance:port("motor_up"):write(HIGH)
        end

        scheduler:schedule(30000, function()
            check_wind_condition(instance)
        end)
    end

    dev:on("instantiation", function(instance)
        instance:slot("speed"):write(instance:attribute("motors_speed"))
        check_wind_condition(instance)
    end)
end