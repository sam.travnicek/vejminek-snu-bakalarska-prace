identifier = "cz.allcomp"
label = "Allcomp"
author = "Samuel Trávníček"
description = "Standard library provided by Allcomp a.s."
version = "1.0"
entry_file = "main.lua"

-- using Language Codes ISO 639-1 standard
primary_language = "en"