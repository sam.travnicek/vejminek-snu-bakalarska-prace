#ifndef PLUGINS_DEVICE_TEMPLATE_HPP
#define PLUGINS_DEVICE_TEMPLATE_HPP

#include <vector>
#include <string>
#include <ulib/Logger.hpp>
#include <lua5.3/lua.hpp>
#include "lua/Sandbox.hpp"
#include "type.hpp"
#include <nlohmann/json.hpp>
#include <any>

// TODO: replace 'event' function for registering event hadlers by 'subscribe'

class DeviceTemplateManager;

struct DeviceTemplate
{
    struct Attribute
    {
        enum Type { NUMBER = 0, BOOLEAN, STRING, TIME, CONDITION, _ATTRIBUTE_END_ };

        std::string identifier;
        std::string label;
        Type type;

        nlohmann::json serialize();
    };

    struct Port
    {
        enum Direction { OUTPUT = 0, INPUT, _DIRECTION_END_ };
        enum Mode { LOGICAL = 0, DISCRETE, _MODE_END_ };

        std::string identifier;
        std::string label;
        Direction direction;
        Mode mode;
        bool optional;
        float default_value;
        bool value_stored;

        nlohmann::json serialize();
    };

    struct Slot
    {
        enum Direction { DRIVER_OUT = 0, DRIVER_IN, _DIRECTION_END_ };
        enum Type { NUMBER = 0, BOOLEAN, STRING, _TYPE_END_ };

        std::string identifier;
        std::string label;
        Direction direction;
        Type type;

        nlohmann::json serialize();
    };

    struct Signal
    {
        enum Direction { DRIVER_OUT = 0, DRIVER_IN, _DIRECTION_END_ };

        std::string identifier;
        std::string label;
        Direction direction;

        nlohmann::json serialize();
    };

    struct Event
    {
        std::string identifier;
        std::string label;

        nlohmann::json serialize();
    };

    struct Action
    {
        struct Argument {
            enum Type { NUMBER = 0, BOOLEAN, STRING, _TYPE_END_ };

            std::string identifier;
            std::string label;
            Type type;
            std::any default_value;

            nlohmann::json serialize();
        };

        std::string identifier;
        std::string label;
        std::vector<Argument> arguments;
        int reference;

        nlohmann::json serialize();
    };

private:
    static int clw__index(lua_State *L);
    static int clw__newindex(lua_State *L);
    static int clw_new(lua_State *L);

    static int ilw__index(lua_State *L);
    static int ilw__newindex(lua_State *L);
    static int ilw_label(lua_State *L);
    static int ilw_attribute(lua_State *L);
    static int ilw_port(lua_State *L);
    static int ilw_slot(lua_State *L);
    static int ilw_signal(lua_State *L);
    static int ilw_action(lua_State *L);
    static int ilw_emitted_event(lua_State *L);
    static int ilw_on(lua_State *L);

    std::string _identifier;
    std::string _label;
    std::vector<Port> _ports;
    std::vector<Attribute> _attributes;
    std::vector<Action> _actions;
    std::vector<Slot> _slots;
    std::vector<Signal> _signals;
    std::vector<Event> _emitted_events;

    // event callbacks' references:
    int _ecb_instantiation;
    int _ecb_port_changed;
    int _ecb_slot_written;
    int _ecb_signal_invoked;
    int _ecb_tick;

public:
    static void inject(DeviceTemplateManager &dtm, Sandbox &db, ulib::Logger &logger);

    DeviceTemplate(const std::string &identifier);
    ~DeviceTemplate() {}

    inline const std::string& identifier() const
        { return _identifier; }

    inline const std::string& label() const
        { return _label; }

    inline std::vector<Port>& ports()
        { return _ports; }

    inline std::vector<Slot>& slots()
        { return _slots; }

    inline std::vector<Signal>& signals()
        { return _signals; }

    inline std::vector<Attribute>& attributes()
        { return _attributes; }

    inline std::vector<Action>& actions()
        { return _actions; }

    inline std::vector<Event>& emitted_events()
        { return _emitted_events; }

    void attribute(const Attribute &attr);

    void port(const Port &port);

    void slot(const Slot &slot);

    void signal(const Signal &signal);

    void action(const Action &act);

    void emitted_event(const Event &event);

    inline int ecb_instantion() const
        { return _ecb_instantiation; }

    inline int ecb_port_changed() const
        { return _ecb_port_changed; }

    inline int ecb_slot_written() const
        { return _ecb_slot_written; }

    inline int ecb_signal_invoked() const
        { return _ecb_signal_invoked; }

    inline int ecb_tick() const
        { return _ecb_tick; }

    nlohmann::json serialize_reduced();
};

struct DeviceTemplateManager
{
    virtual void register_device_template(DeviceTemplate *dev) = 0;
};

#endif /* PLUGINS_DEVICE_TEMPLATE_HPP */