#ifndef PLUGINS_DEVICES_SERVICE_HPP
#define PLUGINS_DEVICES_SERVICE_HPP

#include "lua/LuaState.hpp"
#include "vejminek/Device.hpp"

class DevicesService
{
    static int lw__index(lua_State *L);
    static int lw__newindex(lua_State *L);
    static int lw_find(lua_State *L);

    ulib::Logger _logger;
    DeviceManager &_dm;
    int _reference;

public:
    DevicesService(LuaState &ls, ulib::LoggerConf &logger_cfg, DeviceManager &dm);

    inline int reference() const
        { return _reference; }
};

#endif /* PLUGINS_DEVICES_SERVICE_HPP */