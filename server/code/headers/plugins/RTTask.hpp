#ifndef PLUGINS_TASK_HPP
#define PLUGINS_TASK_HPP

#include <ulib/types.hpp>
#include <nlohmann/json.hpp>
#include <ulib/DateTime.hpp>
#include <string>
#include <any>

struct RTTaskRunScript
{
    int ref;
};

struct RTTaskWritePort
{
    uint device_id;
    std::string port_identifier;
    double port_value;
};

struct RTTaskCallAction
{
    uint device_id;
    std::string action_identifier;
    nlohmann::json args;
};

struct RTRepeatedTask
{
    enum Type { RUN_SCRIPT = 0, WRITE_PORT, CALL_ACTION, _TYPE_END_ };

    uint32_t id;
    ulib::Time execution_time;
    Type type;
    std::any data;
};

struct RTOneTimeTask
{
    enum Type { RUN_SCRIPT = 0, WRITE_PORT, CALL_ACTION, _TYPE_END_ };

    uint32_t id;
    ulib::DateTime execution_time;
    Type type;
    std::any data;
};

struct SortRTRepeatedTask
{
    bool operator()(const RTRepeatedTask &left, const RTRepeatedTask &right) const
    {
        return left.execution_time < right.execution_time;
    }
};

struct SortRTOneTimeTask
{
    bool operator()(const RTOneTimeTask &left, const RTOneTimeTask &right) const
    {
        return left.execution_time < right.execution_time;
    }
};

#endif /* PLUGINS_TASK_HPP */