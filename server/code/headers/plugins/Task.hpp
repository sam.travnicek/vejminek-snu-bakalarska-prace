#ifndef PLUGINS_TASK_HPP
#define PLUGINS_TASK_HPP

#include <ulib/types.hpp>

struct Task
{
    uint id;
    int64_t time;
    uint delay;
    int repeat;
    int ref;
};

struct SortTask
{
    bool operator()(const Task &left, const Task &right) const
    {
        return (left.time + left.delay) < (right.time + right.delay);
    }
};

#endif /* PLUGINS_TASK_HPP */