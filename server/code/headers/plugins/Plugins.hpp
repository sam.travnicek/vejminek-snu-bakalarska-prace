#ifndef PLUGINS_PLUGINS_HPP
#define PLUGINS_PLUGINS_HPP

#include <string>
#include <vector>
#include <ulib/types.hpp>
#include <ulib-net/MySQL.hpp>
#include "lua/LuaState.hpp"
#include "plugins/Plugin.hpp"
#include "Ticking.hpp"

class Plugins : public Ticking
{
    std::vector<Plugin*> _plugins;
    ulib::Logger _logger;
    ulib::net::MySQL &_mysql;
    // TODO: implement dependencies and optional dependencies

public:
    Plugins(
        LuaState &ls, const std::string &path, const ulib::LoggerConf &logger_cgf,
        SchedulerService &scheduler, ulib::net::MySQL &mysql);
    ~Plugins();

    void init();
    void tick(int64_t time, ulong ticks, ulong tps) override;
    void ports_linkage_complete();
    void slots_linkage_complete();
    void signals_linkage_complete();

    DeviceTemplate* find_template(
        const std::string &plugin_identifier, const std::string &template_identifier);

    Driver* find_driver(const std::string &plugin_identifier, const std::string &driver_identifier);

    std::vector<DeviceTemplate*> device_templates();
};

#endif /* PLUGINS_PLUGINS_HPP */