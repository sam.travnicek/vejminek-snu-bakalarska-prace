#ifndef PLUGINS_DRIVER_LUA_HPP
#define PLUGINS_DRIVER_LUA_HPP

#include "vejminek/Driver.hpp"
#include "lua/Sandbox.hpp"

class DriverLUA : public Driver
{
    static int clw__index(lua_State *L);
    static int clw__newindex(lua_State *L);
    static int clw_new(lua_State *L);

    static int ilw__index(lua_State *L);
    static int ilw__newindex(lua_State *L);
    static int ilw_identifier(lua_State *L);
    static int ilw_label(lua_State *L);
    static int ilw_write_port(lua_State *L);
    static int ilw_write_slot(lua_State *L);
    static int ilw_invoke_signal(lua_State *L);
    static int ilw_on(lua_State *L);
    static int ilw_port(lua_State *L);
    static int ilw_slot(lua_State *L);
    static int ilw_signal(lua_State *L);

    LuaState _ls;

    int _ecb_tick;
    // int _ecb_written;
    // int _ecb_linked;
    // int _ecb_linkage_complete;
    int _ecb_port_written;
    int _ecb_port_linked;
    int _ecb_ports_linkage_complete;
    int _ecb_slot_written;
    int _ecb_slot_linked;
    int _ecb_slots_linkage_complete;
    int _ecb_signal_invoked;
    int _ecb_signal_linked;
    int _ecb_signals_linkage_complete;

public:
    static void inject(DriverManager &dm, Sandbox &db, ulib::Logger &logger);

    DriverLUA(const std::string &identifier, const ulib::LoggerConf &logger_cfg, const LuaState &ls)
        : Driver(identifier, logger_cfg), _ls(ls), _ecb_tick(0),
          _ecb_port_written(0), _ecb_port_linked(0), _ecb_ports_linkage_complete(0),
          _ecb_slot_written(0), _ecb_slot_linked(0), _ecb_slots_linkage_complete(0),
          _ecb_signal_invoked(0), _ecb_signal_linked(0), _ecb_signals_linkage_complete(0) {}

    virtual ~DriverLUA() {}

    virtual void tick(int64_t time, ulong ticks, ulong tps) override;

    virtual void driver_port_written(const std::string &identifier, double value) override;

    virtual void driver_port_linked(
        const std::string &identifier, int direction, int mode,
        const std::string &cfg_identifier) override;

    virtual void ports_linkage_complete() override;

    virtual void driver_slot_written(
        const std::string &identifier, std::any &value, DeviceTemplate::Slot::Type type) override;

    virtual void driver_slot_linked(const std::string &identifier) override;

    virtual void slots_linkage_complete() override;

    virtual void driver_signal_invoked(const std::string &identifier) override;

    virtual void driver_signal_linked(const std::string &identifier) override;

    virtual void signals_linkage_complete() override;
};

#endif /* PLUGINS_DRIVER_LUA_HPP */