#ifndef PLUGINS_RT_SCHEDULER_SERVICE_HPP_
#define PLUGINS_RT_SCHEDULER_SERVICE_HPP_

#include "lua/LuaState.hpp"
#include "plugins/Task.hpp"
#include <set>
#include <ulib/Logger.hpp>
#include "Ticking.hpp"

// Real Time Scheduler Service

class RTSchedulerService : public Ticking
{
    static int lw__index(lua_State *L);
    static int lw__newindex(lua_State *L);
    static int lw_schedule(lua_State *L);
    static int lw_cancel(lua_State *L);

    LuaState &_ls;
    ulib::Logger _logger;
    int _ref;
    uint _task_counter;

    std::multiset<Task, SortTask> _tasks;

public:
    SchedulerService(LuaState &ls, ulib::LoggerConf &logger_cfg);

    void tick(int64_t time, ulong ticks, ulong tps) override;
    void schedule(const Task &task);
    void cancel(uint id);

    inline int reference() const
        { return _ref; }
};

#endif // PLUGINS_RT_SCHEDULER_SERVICE_HPP_