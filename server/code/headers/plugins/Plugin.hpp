#ifndef PLUGINS_PLUGIN_HPP
#define PLUGINS_PLUGIN_HPP

#include <string>
#include <ulib/Logger.hpp>
#include <ulib/UUID.hpp>
#include "lua/Sandbox.hpp"
#include "plugins/DeviceTemplate.hpp"
#include "plugins/SchedulerService.hpp"
#include "vejminek/Driver.hpp"
#include "Ticking.hpp"

class Plugin : public Sandbox, public DeviceTemplateManager, public DriverManager, public Ticking
{
    static const std::string DEFAULT_ENTRY_FILE;

    static int clw__index(lua_State *L);
    static int clw__newindex(lua_State *L);
    static int clw_on(lua_State *L);
    static int clw_member(lua_State *L);

    static int lw_service(lua_State *L);
    static int lw_include(lua_State *L);

    ulib::Logger _logger;
    std::string _directory;
    bool _invalid, _loaded;
    std::vector<DeviceTemplate*> _dts;
    std::vector<Driver*> _drivers;

    std::string _identifier, _label, _version, _author, _description, _entry_file;
    int _ecb_tick, _ecb_init;

public:
    Plugin(
        LuaState &ls, const std::string &plugin_dir, const ulib::LoggerConf &logger_cfg,
        SchedulerService &scheduler);
    virtual ~Plugin();

    bool init();
    void tick(int64_t time, ulong ticks, ulong tps) override;
    void ports_linkage_complete();
    void slots_linkage_complete();
    void signals_linkage_complete();

    inline const std::string& identifier() const
        { return _identifier; }

    inline const std::string& label() const
        { return _label; }

    inline const std::string& version() const
        { return _version; }

    inline const std::string& author() const
        { return _author; }

    inline const std::string& description() const
        { return _description; }

    inline const std::string& entry_file() const
        { return _entry_file; }

    inline const std::string& directory() const
        { return _directory; }

    inline ulib::Logger& logger()
        { return _logger; }

    inline bool valid() const
        { return !_invalid; }

    inline bool loaded() const
        { return _loaded; }

    virtual std::vector<DeviceTemplate*>& device_templates()
        { return _dts; }

    virtual std::vector<Driver*>& drivers()
        { return _drivers; }

    virtual void register_device_template(DeviceTemplate *dt) override;

    void register_driver(Driver *driver) override;

    Driver* driver(const std::string &identifier) override;
};

#endif  /* MODULE_PLUGIN_HPP */