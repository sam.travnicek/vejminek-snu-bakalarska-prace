#ifndef LUA_LUA_STATE_HPP
#define LUA_LUA_STATE_HPP

#include <lua5.3/lua.hpp>
#include <ulib/types.hpp>
#include <string>

class LuaState
{
    const bool _temp;
    lua_State *_L;

public:
    LuaState();
    LuaState(lua_State *L);
    virtual ~LuaState();


    inline void open_libs()
        { luaL_openlibs(_L); }


    bool run_script(const std::string &str);

    inline bool load_script(const std::string &str)
        { return luaL_loadstring(_L, str.c_str()) == LUA_OK; }

    bool run_file(const std::string &path);

    inline bool load_file(const std::string &path)
        { return luaL_loadfile(_L, path.c_str()) == LUA_OK; }


    inline bool new_metatable(const std::string &name)
        { return luaL_newmetatable(_L, name.c_str()); }

    inline void set_metatable(const std::string &name)
        { luaL_setmetatable(_L, name.c_str()); }

    inline void set_metatable(int idx)
        { lua_setmetatable(_L, idx); }

    inline int get_metatable(const std::string &name)
        { return luaL_getmetatable(_L, name.c_str()); }

    inline bool get_metatable(int idx)
        { return lua_getmetatable(_L, idx); }


    inline void new_table()
        { lua_newtable(_L); }

    inline void set_field(int idx, const std::string &key)
        { lua_setfield(_L, idx, key.c_str()); }

    inline int get_field(int idx, const std::string &key)
        { return lua_getfield(_L, idx, key.c_str()); }

    inline void set_table(int idx)
        { lua_settable(_L, idx); }

    inline void get_table(int idx)
        { lua_gettable(_L, idx); }

    inline void* new_user_data(size_t size)
        { return lua_newuserdata(_L, size); }


    inline void set_global(const std::string &name)
        { lua_setglobal(_L, name.c_str()); }

    inline void get_global(const std::string &name)
        { lua_getglobal(_L, name.c_str()); }

    inline void set_user_value(int idx)
        { lua_setuservalue(_L, idx); }

    inline int get_user_value(int idx)
        { return lua_getuservalue(_L, idx); }

    inline void raw_set(int idx)
        { lua_rawset(_L, idx); }

    inline void raw_set_idx(int idx, long long tab_idx)
        { lua_rawseti(_L, idx, tab_idx); }

    inline void raw_set_ptr(int idx, const void *ptr)
        { lua_rawsetp(_L, idx, ptr); }

    inline int raw_get(int idx)
        { return lua_rawget(_L, idx); }

    inline int raw_get_idx(int idx, long long tab_idx)
        { return lua_rawgeti(_L, idx, tab_idx); }

    inline int raw_get_ptr(int idx, const void *ptr)
        { return lua_rawgetp(_L, idx, ptr); }

    inline size_t raw_length(int idx)
        { return lua_rawlen(_L, idx); }

    inline size_t length(int idx)
        { return luaL_len(_L, idx); }


    inline int ref(int tbl_idx)
        { return luaL_ref(_L, tbl_idx); }

    inline void unref(int tbl_idx, int idx)
        { luaL_unref(_L, tbl_idx, idx); }

    inline int reg_ref()
        { return ref(LUA_REGISTRYINDEX); }

    inline void reg_unref(int idx)
        { unref(LUA_REGISTRYINDEX, idx); }

    inline void reg_push(int idx)
        { lua_rawgeti(_L, LUA_REGISTRYINDEX, idx); }


    inline void copy(int idx)
        { lua_pushvalue(_L, idx); }

    inline void push_nil()
        { lua_pushnil(_L); }

    inline void push_upvalue(int idx)
        { lua_pushvalue(_L, lua_upvalueindex(idx)); }

    inline void push_global_table()
        { lua_pushglobaltable(_L); }

    inline void push(lua_CFunction func)
        { lua_pushcfunction(_L, func); }

    inline void push(lua_CFunction func, int num_upvalues)
        { lua_pushcclosure(_L, func, num_upvalues); }

    inline void push(const std::string &value)
        { lua_pushstring(_L, value.c_str()); }

    inline void push(const char *value)
        { lua_pushstring(_L, value); }

    inline void push(int value)
        { lua_pushinteger(_L, value); }

    inline void push(uint value)
        { lua_pushinteger(_L, value); }

    inline void push(long value)
        { lua_pushinteger(_L, value); }

    inline void push(ulong value)
        { lua_pushinteger(_L, value); }

    inline void push(long long value)
        { lua_pushinteger(_L, value); }

    inline void push(unsigned long long value)
        { lua_pushinteger(_L, value); }

    inline void push(double value)
        { lua_pushnumber(_L, value); }

    inline void push(bool value)
        { lua_pushboolean(_L, value); }

    inline void push(void *ptr)
        { lua_pushlightuserdata(_L, ptr); }

    inline void pop(int n)
        { lua_pop(_L, n); }

    inline void pop()
        { pop(1); }

    inline void remove(int idx)
        { lua_remove(_L, idx); }

    inline void insert(int idx)
        { lua_insert(_L, idx); }


    inline std::string to_string(int idx)
        { return std::string(lua_tostring(_L, idx)); }

    inline double to_double(int idx)
        { return lua_tonumber(_L, idx); }

    inline bool to_bool(int idx)
        { return lua_toboolean(_L, idx); }

    inline int to_int(int idx)
        { return lua_tointeger(_L, idx); }

    inline unsigned int to_uint(int idx)
        { return lua_tointeger(_L, idx); }

    inline long to_long(int idx)
        { return lua_tointeger(_L, idx); }

    inline unsigned long to_ulong(int idx)
        { return lua_tointeger(_L, idx); }

    inline long long to_longlong(int idx)
        { return lua_tointeger(_L, idx); }

    inline unsigned long long to_ulonglong(int idx)
        { return lua_tointeger(_L, idx); }


    template<typename T>
    inline T* to_ptr(int idx)
        { return reinterpret_cast<T*>(lua_touserdata(_L, idx)); }

    template<typename T>
    inline T& to_ref(int idx)
        { return *reinterpret_cast<T*>(lua_touserdata(_L, idx)); }


    inline bool is_string(int idx)
        { return lua_isstring(_L, idx); }

    inline bool is_int(int idx)
        { return lua_isinteger(_L, idx); }

    inline bool is_double(int idx)
        { return lua_isnumber(_L, idx); }

    inline bool is_bool(int idx)
        { return lua_isboolean(_L, idx); }

    inline bool is_nil(int idx)
        { return lua_isnil(_L, idx); }

    inline bool is_none_or_nil(int idx)
        { return lua_isnoneornil(_L, idx); }

    inline bool is_none(int idx)
        { return lua_isnone(_L, idx); }

    inline bool is_ptr(int idx)
        { return lua_islightuserdata(_L, idx); }

    inline bool is_userdata(int idx)
        { return lua_isuserdata(_L, idx); }

    inline bool is_table(int idx)
        { return lua_istable(_L, idx); }

    inline bool is_obj(int idx)
        { return is_userdata(idx) || is_table(idx); }

    inline bool is_thread(int idx)
        { return lua_isthread(_L, idx); }

    inline bool is_function(int idx)
        { return lua_isfunction(_L, idx); }

    inline bool is_cfunction(int idx)
        { return lua_iscfunction(_L, idx); }


    inline int top()
        { return lua_gettop(_L); }

    inline void top(int idx)
        { lua_settop(_L, idx); }

    inline void clean()
        { lua_settop(_L, 0); }

    inline bool check_stack(int extra)
        { return lua_checkstack(_L, extra); }


    inline lua_Hook get_hook()
        { return lua_gethook(_L); }

    inline int get_hook_count()
        { return lua_gethookcount(_L); }

    inline int get_hook_mask()
        { return lua_gethookmask(_L); }

    inline bool get_info(const std::string &what, lua_Debug &ar)
        { return lua_getinfo(_L, what.c_str(), &ar); }

    inline std::string get_local(const lua_Debug &ar, int n)
    {
        const char *ret = lua_getlocal(_L, &ar, n);
        if(ret == nullptr)
            return std::string();
        return std::string(ret);
    }

    inline bool get_stack(int level, lua_Debug &ar)
        { return lua_getstack(_L, level, &ar); }

    inline std::string get_upvalue(int func_index, int n)
    {
        const char *ret = lua_getupvalue(_L, func_index, n);
        if(ret == nullptr)
            return std::string();
        return std::string(ret);
    }

    inline void set_hook(lua_Hook f, int mask, int count)
        { lua_sethook(_L, f, mask, count); }

    inline std::string set_local(const lua_Debug &ar, int n)
    {
        const char *ret = lua_getlocal(_L, &ar, n);
        if(ret == nullptr)
            return std::string();
        return std::string(ret);
    }

    inline void set_upvalue(int func_idx, int upvalue_idx)
        { lua_setupvalue(_L, func_idx, upvalue_idx); }

    inline void* upvalue_id(int func_idx, int upvalue_idx)
        { return lua_upvalueid(_L, func_idx, upvalue_idx); }

    inline void upvalue_join(int func_idx1, int upvalue_idx1, int func_idx2, int upvalue_idx2)
        { lua_upvaluejoin(_L, func_idx1, upvalue_idx1, func_idx2, upvalue_idx2); }

    int current_line();

    std::string source();

    std::string short_source();

    inline bool pcall(uint num_args, uint num_res)
        { return lua_pcall(_L, num_args, num_res, 0) == LUA_OK; }

    inline bool pcall(uint num_args)
        { return pcall(num_args, 0); }

    inline bool pcall()
        { return pcall(0); }

    inline int type(int idx)
        { return lua_type(_L, idx); }

    inline std::string type_name(int idx)
        { return std::string(lua_typename(_L, type(idx))); }

    inline bool next(int idx)
        { return lua_next(_L, idx); }


    inline lua_CFunction at_panic(lua_CFunction func)
        { return lua_atpanic(_L, func); }


    inline lua_State* state() { return _L; }

    inline bool temporary() const { return _temp; }
};

#endif /* LUA_LUA_STATE_HPP */