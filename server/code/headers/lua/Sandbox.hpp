#ifndef LUA_SANDBOX_HPP
#define LUA_SANDBOX_HPP

#include "lua/LuaState.hpp"
#include <ulib/Logger.hpp>

class Sandbox : public LuaState
{
    static void lua_output(lua_State *L, uint level);
    static int lw_print(lua_State *L);
    static int lw_warning(lua_State *L);
    static int lw_error(lua_State *L);
    static int lw_debug(lua_State *L);
    static int lw_load(lua_State *L);
    static int lw_module(lua_State *L);

    int _reference;

public:
    Sandbox(LuaState &ls);
    Sandbox(lua_State *L, int reference)
        : LuaState(L), _reference(reference) {}
    virtual ~Sandbox();

    void load_G();
    void load_safe_tools();
    void load_logging_tools(ulib::Logger &logger);
    void load_file_tools(ulib::Logger &logger, const std::string &dir);
    void load_module_tools(ulib::Logger &logger, const std::string &dir, LuaState &ls);
    //TODO: implement string array access (load_string_extension)

    virtual void push_environment();
    virtual bool environment_script(const std::string &str);
    virtual bool environment_file(const std::string &path);

    int reference() const { return _reference; }
};

#endif /* LUA_SANDBOX_HPP */