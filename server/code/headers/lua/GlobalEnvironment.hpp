#ifndef LUA_GLOBAL_ENVIRONMENT_HPP
#define LUA_GLOBAL_ENVIRONMENT_HPP

#include <string>
#include "lua/Sandbox.hpp"

class GlobalEnvironment
{
    static const std::string INITIAL_CODE;
    static std::string PROGRAM_DIR;

public:

    static void initialize(LuaState &ls);
    static void load_tools(Sandbox &sb);
    static void load_globals(Sandbox &sb);

    static void set(LuaState &ls, void *ptr);
    static void get(LuaState &ls, void *ptr);
};

#endif /* LUA_GLOBAL_ENVIRONMENT_HPP */