#ifndef LUA_SERIAL_PORT_LUA_HPP
#define LUA_SERIAL_PORT_LUA_HPP

#include <ulib/SerialPort.hpp>
#include "lua/Sandbox.hpp"

class SerialPortLUA : public ulib::SerialPort
{
    static int clw__index(lua_State *L);
    static int clw__newindex(lua_State *L);
    static int clw_new(lua_State *L);

    static int ilw__gc(lua_State *L);
    static int ilw__index(lua_State *L);
    static int ilw__newindex(lua_State *L);
    static int ilw_io_error(lua_State *L);
    static int ilw_available(lua_State *L);
    static int ilw_send(lua_State *L);
    static int ilw_send_byte(lua_State *L);
    static int ilw_send_bytes(lua_State *L);
    static int ilw_read(lua_State *L);
    static int ilw_read_all(lua_State *L);
    static int ilw_read_byte(lua_State *L);
    static int ilw_read_bytes(lua_State *L);

    void _init_fd();
    bool _available();

	int _epoll_fd;
    bool _serial_error;

public:
    static void inject(Sandbox &sb, ulib::Logger &logger);

    SerialPortLUA(const std::string &port, int baudrate);
    SerialPortLUA(const std::string &port, int baudrate, char bits, char parity, char stop_bits);
};

#endif /* LUA_SERIAL_PORT_LUA_HPP */