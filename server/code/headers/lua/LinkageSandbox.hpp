#ifndef LUA_LINKAGE_SANDBOX_HPP
#define LUA_LINKAGE_SANDBOX_HPP

#include "lua/Sandbox.hpp"
#include "plugins/SchedulerService.hpp"
#include "plugins/DevicesService.hpp"

class LinkageSandbox : public Sandbox
{
    static int lw_device(lua_State *L);
    static int lw_service(lua_State *L);

public:
    LinkageSandbox(
        LuaState &ls, DeviceManager &dm, SchedulerService &scheduler,
        DevicesService &ds, ulib::Logger &logger);
    virtual ~LinkageSandbox() {}
};

#endif /* LUA_LINKAGE_SANDBOX_HPP */