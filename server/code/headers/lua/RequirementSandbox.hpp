#ifndef LUA_REQUIREMEMT_SANDBOX_HPP
#define LUA_REQUIREMEMT_SANDBOX_HPP

#include "lua/Sandbox.hpp"
#include "plugins/Plugin.hpp"

struct RequirementSandbox : public Sandbox
{
    RequirementSandbox(LuaState &ls, ulib::Logger &logger, const std::string &dir);
    virtual ~RequirementSandbox() {}
};

#endif /* LUA_REQUIREMEMT_SANDBOX_HPP */