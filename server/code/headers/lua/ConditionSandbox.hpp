#ifndef LUA_CONDITION_SANDBOX_HPP
#define LUA_CONDITION_SANDBOX_HPP

#include "lua/Sandbox.hpp"

class DeviceManager;

class ConditionSandbox : public Sandbox
{
    static int lw_device(lua_State *L);

public:
    ConditionSandbox(LuaState &ls, DeviceManager &dm, ulib::Logger &logger);
    virtual ~ConditionSandbox() {}
};

#endif /* LUA_CONDITION_SANDBOX_HPP */