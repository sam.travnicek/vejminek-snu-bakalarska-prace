#ifndef LUA_CLOSURE_LS_HPP
#define LUA_CLOSURE_LS_HPP

#include "lua/LuaState.hpp"
#include <ulib/Logger.hpp>
#include <vector>

// TODO: add property 'owner' (pointer to plugin) to every object/function
// if anything goes wrong -> disable plugin

struct ClosureLS : public LuaState
{
    enum ClosureType { CT_FUNCTION, CT_METHOD };

    enum ArgType {
        BOOL = 0, INT, DOUBLE, STRING, POINTER, USERDATA, TABLE, OBJECT, THREAD, FUNCTION,
        CFUNCTION, __ARG_TYPE_END__
    };

private:
    static std::string _type_string(ArgType type);
    static std::string _number_postfix(int n);

    static const std::vector<std::string> _ARG_TYPE_STR;

    lua_Debug _debug;
    std::string _name;
    ulib::Logger *_logger;
    uint _num_args;
    ClosureType _type;
    std::vector<std::vector<ArgType>> _arg_lists;

    bool _type_check(int idx, ArgType type);
    ArgType _type_of(int idx);

public:
    ClosureLS(
        lua_State *L, int logger_upvalue_idx, const std::string &name, ClosureType type);
    virtual ~ClosureLS() {}

    std::string msg_prefix();
    std::string msg_postfix();

    bool check_protected_fields(const std::vector<std::string> &fields);
    void error_protected_obj();
    [[deprecated]] bool check_args(const std::vector<ArgType> &req_args, uint optional_from);
    [[deprecated]] bool check_arg(int n, const std::vector<ArgType> &types);
    [[deprecated]] bool check_closure(const std::vector<uint> &req_nargs, ArgType obj_type);

    bool check_object(ArgType type);

    inline void argument_list(const std::vector<ArgType> &args)
        { _arg_lists.push_back(args); }

    int check_arguments();

    [[deprecated]] inline bool check_arg(int n, const ArgType &type)
        { return check_arg(n, std::vector<ArgType>({type})); }

    [[deprecated]] inline bool check_args(const std::vector<ArgType> &req_args)
        { return check_args(req_args, -1); }

    [[deprecated]] inline bool check_closure(const std::vector<uint> &req_nargs)
        { return check_closure(req_nargs, OBJECT); }

    [[deprecated]] inline bool check_closure(uint req_nargs, ArgType obj_type)
        { return check_closure(std::vector<uint>({req_nargs}), obj_type); }

    [[deprecated]] inline bool check_closure(uint req_nargs)
        { return check_closure(req_nargs, OBJECT); }

    inline void closure_name(const std::string &name)
        { _name = name; }

    inline ulib::Logger& logger()
        { return *_logger; }

    inline uint num_args() const
        { return _num_args; }
};

#endif /* LUA_CLOSURE_LS_HPP */