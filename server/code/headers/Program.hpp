#ifndef PROGRAM_HPP
#define PROGRAM_HPP

#include <string>
#include <ulib/Logger.hpp>
#include <ulib/types.hpp>
#include <ulib-net/MySQL.hpp>
#include "Configuration.hpp"
#include "vejminek/Core.hpp"
#include "Ticking.hpp"
#include "structures/channel_descriptor.hpp"

class Program : public Ticking
{
    friend class Configuration;

    std::unique_ptr<Configuration> _config;
    std::unique_ptr<Core> _core;
    ulib::Logger _logger;
    ulib::net::MySQL _database;
    std::string _program_path;
    bool _sig_exit;
    std::vector<channel_descriptor_t> _channel_descriptors;

public:
    static const std::string VERSION;
    static const std::string CONFIG_PATH;

    static bool DEBUG_MODE;
    static bool DEBUG_DEVICES;
    static bool DEBUG_CHANNELS;

    Program(const std::string &program_path, const ulib::LoggerConf &logger_cfg);
    ~Program();

    bool init();
    void tick(int64_t time, ulong ticks, ulong tps) override;

    inline bool sig_exit() const
        { return _sig_exit; }
};

#endif /* PROGRAM_HPP */