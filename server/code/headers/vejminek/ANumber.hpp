#ifndef VEJMINEK_ANUMBER_HPP
#define VEJMINEK_ANUMBER_HPP

#include "vejminek/Attribute.hpp"

class ANumber : public Attribute
{
    double _value;

public:
    ANumber(DeviceTemplate::Attribute &attr_templ)
        : Attribute(attr_templ), _value(0) {}

    inline void value(double val)
        { _value = val; }

    inline double value() const
        { return _value; }
};

#endif /* VEJMINEK_ANUMBER_HPP */