#ifndef VEJMINEK_DEVICE_HPP
#define VEJMINEK_DEVICE_HPP

#include <map>
#include <string>
#include <nlohmann/json.hpp>
#include <ulib/Logger.hpp>
#include "plugins/DeviceTemplate.hpp"
#include "lua/ConditionSandbox.hpp"
#include "vejminek/Port.hpp"
#include "vejminek/Slot.hpp"
#include "vejminek/Signal.hpp"
#include "vejminek/Attribute.hpp"
#include "vejminek/Room.hpp"
#include "Ticking.hpp"

class Device
    : public Port::EventHandler, public Slot::EventHandler, public Signal::EventHandler,
      public Ticking
{
    static int ilw__index(lua_State *L);
    static int ilw__newindex(lua_State *L);
    // static int ilw_member(lua_State *L);
    static int ilw_id(lua_State *L);
    static int ilw_label(lua_State *L);
    static int ilw_description(lua_State *L);
    static int ilw_room(lua_State *L);
    static int ilw_port(lua_State *L);
    static int ilw_slot(lua_State *L);
    static int ilw_signal(lua_State *L);
    static int ilw_action(lua_State *L);
    static int ilw_attribute(lua_State *L);
    static int ilw_on(lua_State *L);
    static int ilw_fire(lua_State *L);

    uint _id;
    std::string _label;
    std::string _description;
    DeviceTemplate &_dt;
    LuaState &_ls;
    ulib::Logger &_logger;
    Room &_room;
    std::map<std::string, Port*> _ports;
    std::map<std::string, Slot*> _slots;
    std::map<std::string, Signal*> _signals;
    std::map<std::string, Attribute*> _attributes;
    std::map<std::string, std::vector<int>> _events;

public:
    Device(
        uint id, const std::string &label, const std::string &description, DeviceTemplate &dt,
        LuaState &ls, ConditionSandbox &cond_sb, ulib::Logger &logger, Room &room,
        const nlohmann::json &data);
    virtual ~Device();

    inline uint id() const
        { return _id; }

    inline const std::string& label() const
        { return _label; }

    inline const std::string& description() const
        { return _description; }

    inline DeviceTemplate& device_template()
        { return _dt; }

    inline std::map<std::string, Port*>& ports()
        { return _ports; }

    inline std::map<std::string, Slot*>& slots()
        { return _slots; }

    virtual void port_changed(Port &port, double previous_value) override;

    virtual void port_written(Port &port, double previous_value) override;

    virtual void slot_written(Slot &slot) override;

    virtual void signal_invoked(Signal &signal) override;

    virtual void tick(int64_t time, ulong ticks, ulong tps) override;

    Port* port(const std::string &identifier);

    Slot* slot(const std::string &identifier);

    Signal* signal(const std::string &identifier);

    bool call_action(
        LuaState &ls, const std::string &identifier, nlohmann::json &args,
        bool called_externally);

    nlohmann::json serialize_reduced();

    nlohmann::json serialize_ports_reduced();

    nlohmann::json serialize_port_values();
};

struct DeviceManager
{
    virtual Device* device(uint id) = 0;
    virtual Device* device(const std::string &label) = 0;
};

#endif /* VEJMINEK_DEVICE_HPP */