#ifndef VEJMINEK_PORT_CONFIGURATION_HPP
#define VEJMINEK_PORT_CONFIGURATION_HPP

// TODO: use wstring for naming (also for devices names etc. excluding driver and plugin names)
// This also means implementation of wstrings in Logger

struct PortConfiguration
{
    std::string identifier;
    std::string label;
    int direction;
    int mode;
    float min;
    float max;
};

#endif /* VEJMINEK_PORT_CONFIGURATION_HPP */