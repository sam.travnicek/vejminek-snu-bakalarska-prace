#ifndef VEJMINEK_SLOT_HPP
#define VEJMINEK_SLOT_HPP

#include <string>
#include "lua/LuaState.hpp"
#include "plugins/DeviceTemplate.hpp"
#include <any>

class Device;

struct Slot
{
    struct EventHandler
    {
        virtual void slot_written(Slot &slot) {}
    };

private:
    static int ilw__index(lua_State *L);
    static int ilw__newindex(lua_State *L);
    static int ilw_identifier(lua_State *L);
    static int ilw_label(lua_State *L);
    static int ilw_write(lua_State *L);
    static int ilw_read(lua_State *L);
    static int ilw_type(lua_State *L);
    static int ilw_linked(lua_State *L);

    std::vector<EventHandler*> _event_handlers;
    Device &_device;
    DeviceTemplate::Slot &_st;
    std::any _value;
    bool _linked;

public:
    Slot(
        Device &device, DeviceTemplate::Slot &slot_template, LuaState &ls,
        ulib::Logger &logger);

    ~Slot();

    bool write(double value);

    bool write(bool value);

    bool write(const std::string &value);

    inline void link()
        { _linked = true; }

    inline const std::string& identifier() const
        { return _st.identifier; }

    inline const std::string& label() const
        { return _st.label; }

    inline DeviceTemplate::Slot::Type type() const
        { return _st.type; }

    inline DeviceTemplate::Slot::Direction direction() const
        { return _st.direction; }

    inline const Device& device() const
        { return _device; }

    inline const DeviceTemplate::Slot& slot_template() const
        { return _st; }

    inline std::any& value()
        { return _value; }

    inline void event_handler(EventHandler &eh)
        { _event_handlers.push_back(&eh); }
};

#endif /* VEJMINEK_SLOT_HPP */