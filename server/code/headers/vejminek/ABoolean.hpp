#ifndef VEJMINEK_ABOOLEAN_HPP
#define VEJMINEK_ABOOLEAN_HPP

#include "vejminek/Attribute.hpp"

class ABoolean : public Attribute
{
    bool _value;

public:
    ABoolean(DeviceTemplate::Attribute &attr_templ)
        : Attribute(attr_templ), _value(false) {}

    inline void value(bool val)
        { _value = val; }

    inline bool value() const
        { return _value; }
};

#endif /* VEJMINEK_ABOOLEAN_HPP */