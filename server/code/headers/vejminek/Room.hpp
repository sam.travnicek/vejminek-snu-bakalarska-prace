#ifndef VEJMINEK_ROOM_HPP
#define VEJMINEK_ROOM_HPP

#include <string>

struct Room
{
    std::string label;
    double floor;

    Room(const std::string &label, double floor)
        : label(label), floor(floor) {}
};

#endif /* VEJMINEK_ROOM_HPP */