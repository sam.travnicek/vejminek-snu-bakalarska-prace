#ifndef VEJMINEK_ATIME_HPP
#define VEJMINEK_ATIME_HPP

#include <ulib/Time.hpp>
#include "vejminek/Attribute.hpp"

class ATime : public Attribute
{
    ulib::Time _value;

public:
    ATime(DeviceTemplate::Attribute &attr_templ)
        : Attribute(attr_templ), _value(0) {}

    inline void value(const ulib::Time &val)
        { _value = val; }

    inline const ulib::Time& value() const
        { return _value; }
};

#endif /* VEJMINEK_ATIME_HPP */