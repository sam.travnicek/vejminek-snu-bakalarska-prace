#ifndef VEJMINEK_SLOT_DESCRIPTOR_HPP
#define VEJMINEK_SLOT_DESCRIPTOR_HPP

#include "type.hpp"

struct SlotDescriptor
{
    std::string identifier;
    std::string label;
    int type;
    int direction;
};

#endif /* VEJMINEK_SLOT_DESCRIPTOR_HPP */