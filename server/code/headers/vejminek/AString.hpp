#ifndef VEJMINEK_ASTRING_HPP
#define VEJMINEK_ASTRING_HPP

#include <string>
#include "vejminek/Attribute.hpp"

class AString : public Attribute
{
    std::string _value;

public:
    AString(DeviceTemplate::Attribute &attr_templ)
        : Attribute(attr_templ), _value("") {}

    inline void value(const std::string &val)
        { _value = val; }

    inline const std::string& value() const
        { return _value; }
};

#endif /* VEJMINEK_ASTRING_HPP */