#ifndef VEJMINEK_ATTRIBUTE_HPP
#define VEJMINEK_ATTRIBUTE_HPP

#include "plugins/DeviceTemplate.hpp"
#include <string>

class Attribute
{
    DeviceTemplate::Attribute &_attr_templ;

public:
    Attribute(DeviceTemplate::Attribute &attr_templ)
        : _attr_templ(attr_templ) {}

    virtual ~Attribute() {}

    inline DeviceTemplate::Attribute& attr_template()
        { return _attr_templ; }

    inline DeviceTemplate::Attribute::Type type() const
        { return _attr_templ.type; }
};

#endif /* VEJMINEK_ATTRIBUTE_HPP */