#ifndef VEJMINEK_PORT_DESCRIPTOR_HPP
#define VEJMINEK_PORT_DESCRIPTOR_HPP

#include <vector>
#include "PortConfiguration.hpp"

struct PortDescriptor
{
    // TODO: implement port groups (port group can be device - e.g. #1:DIN-U10) and 'inactivating port' = feature for offline devices (they can be seen on web even of they are offline)
    std::string identifier;
    std::string label;
    std::vector<PortConfiguration> configurations;
};

#endif /* VEJMINEK_PORT_DESCRIPTOR_HPP */