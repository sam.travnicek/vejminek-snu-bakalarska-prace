#ifndef VEJMINEK_SIGNAL_DESCRIPTOR_HPP
#define VEJMINEK_SIGNAL_DESCRIPTOR_HPP

#include "type.hpp"

struct SignalDescriptor
{
    std::string identifier;
    std::string label;
    int direction;
};

#endif /* VEJMINEK_SIGNAL_DESCRIPTOR_HPP */