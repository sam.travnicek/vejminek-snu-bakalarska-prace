#ifndef VEJMINEK_PORT_HPP
#define VEJMINEK_PORT_HPP

#include <string>
#include <ulib/Logger.hpp>
#include "lua/LuaState.hpp"
#include "plugins/DeviceTemplate.hpp"

struct PortConfiguration;
class Device;

struct Port
{
    struct EventHandler
    {
        virtual void port_changed(Port &port, double previous_value) {}
        virtual void port_written(Port &port, double previous_value) {}
    };

private:
    static int ilw__index(lua_State *L);
    static int ilw__newindex(lua_State *L);
    static int ilw_identifier(lua_State *L);
    static int ilw_label(lua_State *L);
    static int ilw_write(lua_State *L);
    static int ilw_toggle(lua_State *L);
    static int ilw_read(lua_State *L);
    static int ilw_min(lua_State *L);
    static int ilw_max(lua_State *L);
    static int ilw_written_externally(lua_State *L);
    static int ilw_linked(lua_State *L);
    static int ilw_configuration_identifier(lua_State *L);

    std::vector<EventHandler*> _event_handlers;
    Device &_device;
    DeviceTemplate::Port &_pt;
    PortConfiguration *_driver_linkage; // driver port linked to device port
    std::string _preferred_cfg_name; // preferred driver port configuration name
    double _value; // Logical: 0/1, Discrete 0-1
    bool _initial_write;
    bool _written_by_extsrc; // written by external source (ex. channels)

public:
    Port(
        Device &device, DeviceTemplate::Port &port_template, LuaState &ls,
        ulib::Logger &logger);
    ~Port();

    bool write(double value, bool silent, bool by_extsrc);

    inline bool write(double value)
        { return write(value, false, false); }

    inline bool external_write(double value)
        { return write(value, false, true);}

    inline void link(PortConfiguration *config)
        { _driver_linkage = config; }

    inline const std::string& identifier() const
        { return _pt.identifier; }

    inline const std::string& label() const
        { return _pt.label; }

    inline const Device& device() const
        { return _device; }

    inline const DeviceTemplate::Port& port_template() const
        { return _pt; }

    inline double value() const
        { return _value; }

    float min() const;

    float max() const;

    inline void event_handler(EventHandler &eh)
        { _event_handlers.push_back(&eh); }

    nlohmann::json serialize_reduced();

    nlohmann::json serialize_reduced_noid();
};

#endif /* VEJMINEK_PORT_HPP */