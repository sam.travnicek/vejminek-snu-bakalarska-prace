#ifndef VEJMINEK_ACONDITION_HPP
#define VEJMINEK_ACONDITION_HPP

#include "vejminek/Attribute.hpp"
#include "lua/Sandbox.hpp"

class ACondition : public Attribute
{
    Sandbox &_sb;
    ulib::Logger &_logger;
    int _script_reference;
    bool _loaded;
    bool _output;

public:
    ACondition(DeviceTemplate::Attribute &attr_templ, Sandbox &sb, ulib::Logger &logger);
    virtual ~ACondition() {}

    bool script(const std::string &code);
    bool run();

    inline bool check() const
        { return _output; }
};

#endif /* VEJMINEK_ACONDITION_HPP */