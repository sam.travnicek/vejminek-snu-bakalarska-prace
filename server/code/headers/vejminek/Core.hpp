#ifndef VEJMINEK_CORE_HPP_
#define VEJMINEK_CORE_HPP_

#include <ulib-net/MySQL.hpp>
#include <map>
#include "plugins/SchedulerService.hpp"
#include "plugins/DevicesService.hpp"
#include "plugins/Plugins.hpp"
#include "vejminek/Device.hpp"
#include "vejminek/Room.hpp"
#include "lua/LuaState.hpp"
#include "Ticking.hpp"
#include "structures/channel_descriptor.hpp"

#include <allkol/server_side/TCPChannel.hpp>
#include <allkol/server_side/TLSChannel.hpp>
#include <allkol/server_side/WSChannel.hpp>
#include <allkol/Answer.hpp>

// TODO: Make Core, Program, etc. static (there is only one instance), so any
// object can access it without need of getting a reference

class Core
    : public DeviceManager, public Ticking, public Port::EventHandler,
      public allkol::MessageHandler
{
    static constexpr size_t IDENTIFIER_MAX_LENGTH = 100;

    std::map<uint, Room*> _rooms;
    std::map<uint, Device*> _devices;
    LuaState *_ls;
    ConditionSandbox *_cond_env;
    SchedulerService *_scheduler_serv;
    DevicesService *_devices_serv;
    Plugins *_plugins;
    ulib::Logger _logger;
    ulib::Logger _devices_logger;
    ulib::Logger _channels_logger;
    ulib::net::MySQL &_db;
    std::vector<Port*> _ports_to_save;

    const std::vector<channel_descriptor_t> &_channel_descriptors;
    std::vector<std::shared_ptr<allkol::Channel>> _channels;

    std::mutex _uni_mutex;
    std::mutex _channel_descriptors_mutex;

    //TODO: Make Core to be 'Driver Manager' instead of plugins

    void _load_rooms();
    void _load_devices();
    void _load_ports_links();
    void _load_slots_links();
    void _load_signals_links();
    void _load_testbench(const std::string &dir);
    void _open_channels();

    bool _channel_async(uint id);
    bool _uuid_allowed(uint channel_id, const std::string &uuid);

    bool _handle_message(allkol::Message &message, allkol::Answer &answer);

public:
    static bool identifier_valid(const std::string &identifier);

    Core(
        const std::string &plugins_dir, const std::string &testbench_dir,
        ulib::LoggerConf &logger_cfg, ulib::net::MySQL &db,
        const std::vector<channel_descriptor_t> &channel_descriptors);
    virtual ~Core();

    void tick(int64_t time, ulong ticks, ulong tps) override;

    void port_changed(Port &port, double previous_value) override;

    Device* device(uint id) override;

    Device* device(const std::string &name) override;

    virtual void on_message(
        uint channel_id, allkol::Client &client, allkol::Message &message) override;
};

#endif // VEJMINEK_CORE_HPP_