#ifndef VEJMINEK_SIGNAL_HPP
#define VEJMINEK_SIGNAL_HPP

#include <string>
#include "lua/LuaState.hpp"
#include "plugins/DeviceTemplate.hpp"

class Device;

struct Signal
{
    struct EventHandler
    {
        virtual void signal_invoked(Signal &signal) {}
    };

private:
    static int ilw__index(lua_State *L);
    static int ilw__newindex(lua_State *L);
    static int ilw_identifier(lua_State *L);
    static int ilw_label(lua_State *L);
    static int ilw_invoke(lua_State *L);
    static int ilw_linked(lua_State *L);

    std::vector<EventHandler*> _event_handlers;
    Device &_device;
    DeviceTemplate::Signal &_st;
    bool _linked;

public:
    Signal(
        Device &device, DeviceTemplate::Signal &signal_template, LuaState &ls,
        ulib::Logger &logger);

    ~Signal();

    void invoke();

    inline void link()
        { _linked = true; }

    inline const std::string& identifier() const
        { return _st.identifier; }

    inline const std::string& label() const
        { return _st.label; }

    inline const Device& device() const
        { return _device; }

    inline const DeviceTemplate::Signal::Direction direction() const
        { return _st.direction; }

    inline const DeviceTemplate::Signal& signal_template() const
        { return _st; }

    inline void event_handler(EventHandler &eh)
        { _event_handlers.push_back(&eh); }
};

#endif /* VEJMINEK_SIGNAL_HPP */