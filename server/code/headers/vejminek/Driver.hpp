#ifndef VEJMINEK_DRIVER_HPP
#define VEJMINEK_DRIVER_HPP

#include <string>
#include <map>
#include <ulib/Logger.hpp>
#include "vejminek/Port.hpp"
#include "vejminek/Slot.hpp"
#include "vejminek/Signal.hpp"
#include "vejminek/PortDescriptor.hpp"
#include "vejminek/SlotDescriptor.hpp"
#include "vejminek/SignalDescriptor.hpp"
#include "Ticking.hpp"

class DriverManager;

class Driver
    : public Port::EventHandler, public Slot::EventHandler,
      public Signal::EventHandler, public Ticking
{
    std::vector<PortDescriptor> _ports;
    std::vector<SlotDescriptor> _slots;
    std::vector<SignalDescriptor> _signals;

    std::map<std::string, std::vector<Port*>> _input_ports_links;
    std::map<Port*, std::vector<std::string>> _output_ports_links;

    std::map<std::string, std::vector<Slot*>> _drout_slots_links;
    std::map<Slot*, std::vector<std::string>> _drin_slots_links;

    std::map<std::string, std::vector<Signal*>> _drout_signals_links;
    std::map<Signal*, std::vector<std::string>> _drin_signals_links;

protected:
    std::map<std::string, DeviceTemplate::Slot::Type> _slot_types;

    ulib::Logger _logger;
    std::string _identifier;
    std::string _label;

    void port(const PortDescriptor &pd);

    void slot(const SlotDescriptor &slot);

    void signal(const SignalDescriptor &signal);

public:
    Driver(const std::string &identifier, const ulib::LoggerConf &logger_cfg);
    virtual ~Driver() {}

    inline const std::string& identifier() const
        { return _identifier; }

    inline const std::string& label() const
        { return _label; }

    inline void label(const std::string &label)
        { _label = label; }

    bool link_port(const std::string &identifier, Port &port, const std::string &preferred_cfg);

    void port_changed(Port &port, double previous_value) override;

    void write_port(const std::string &identifier, double value);

    virtual void driver_port_written(const std::string &identifier, double value) = 0;

    virtual void driver_port_linked(
        const std::string &identifier, int direction, int mode,
        const std::string &cfg_identifier) = 0;

    virtual void ports_linkage_complete() = 0;

    bool link_slot(const std::string &identifier, Slot &slot);

    void slot_written(Slot &slot) override;

    void write_slot(const std::string &identifier, double value);

    void write_slot(const std::string &identifier, bool value);

    void write_slot(const std::string &identifier, const std::string &value);

    virtual void driver_slot_written(
        const std::string &identifier, std::any &value, DeviceTemplate::Slot::Type type) = 0;

    virtual void driver_slot_linked(const std::string &identifier) = 0;

    virtual void slots_linkage_complete() = 0;

    bool link_signal(const std::string &identifier, Signal &signal);

    void signal_invoked(Signal &signal) override;

    void invoke_signal(const std::string &identifier);

    virtual void driver_signal_invoked(const std::string &identifier) = 0;

    virtual void driver_signal_linked(const std::string &identifier) = 0;

    virtual void signals_linkage_complete() = 0;

    inline std::vector<PortDescriptor>& ports()
        { return _ports; }

    inline std::vector<SlotDescriptor>& slots()
        { return _slots; }

    inline std::vector<SignalDescriptor>& signals()
        { return _signals; }
};

struct DriverManager
{
    virtual void register_driver(Driver *driver) = 0;
    virtual Driver* driver(const std::string &identifier) = 0;
};

#endif /* VEJMINEK_DRIVER_HPP */