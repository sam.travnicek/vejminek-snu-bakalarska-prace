#ifndef DATABASE_HPP
#define DATABASE_HPP

#include <ulib-net/MySQL.hpp>
#include "plugins/Plugin.hpp"

struct Database
{
    static void clear_tmps(ulib::net::MySQL &mysql);

    static void insert(ulib::net::MySQL &mysql, Plugin &plugin);

    static void insert_driver(
        std::unique_ptr<sql::Connection> &con, const std::string &plugin_identifier,
        Driver &driver);

    static void insert_driver_port(
        std::unique_ptr<sql::Connection> &con, uint driver_id, const PortDescriptor &pd);

    static void insert_driver_slot(
        std::unique_ptr<sql::Connection> &con, uint driver_id, const SlotDescriptor &slot);

    static void insert_driver_signal(
        std::unique_ptr<sql::Connection> &con, uint driver_id, const SignalDescriptor &signal);

    static void insert_device(
        std::unique_ptr<sql::Connection> &con, const std::string &plugin_identifier,
        DeviceTemplate &dt);

    static void insert_device_action(
        std::unique_ptr<sql::Connection> &con, uint device_id, DeviceTemplate::Action &action);

    static void insert_device_action_arg(
        std::unique_ptr<sql::Connection> &con, uint action_id,
        DeviceTemplate::Action::Argument &arg);

    static void insert_device_attribute(
        std::unique_ptr<sql::Connection> &con, uint device_id, DeviceTemplate::Attribute &attr);

    static void insert_device_emitted_event(
        std::unique_ptr<sql::Connection> &con, uint device_id, DeviceTemplate::Event &event);

    static void insert_device_port(
        std::unique_ptr<sql::Connection> &con, uint device_id, DeviceTemplate::Port &port);

    static void insert_device_slot(
        std::unique_ptr<sql::Connection> &con, uint device_id, DeviceTemplate::Slot &slot);

    static void insert_device_signal(
        std::unique_ptr<sql::Connection> &con, uint device_id, DeviceTemplate::Signal &signal);

    static uint last_insertion_id(std::unique_ptr<sql::Connection> &con);

    static float last_port_value(
        std::unique_ptr<sql::Connection> &con, Port &port);

    static void save_port_value(
        std::unique_ptr<sql::Connection> &con, Port &port);

    static void insert_port_value(
        std::unique_ptr<sql::Connection> &con, Port &port);

    static void clear_port_values(std::unique_ptr<sql::Connection> &con);
};

#endif /* DATABASE_HPP */