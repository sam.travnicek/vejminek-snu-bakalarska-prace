#ifndef TICKING_HPP
#define TICKING_HPP

struct Ticking
{
    virtual void tick(int64_t time, ulong ticks, ulong tps) = 0;
    virtual ~Ticking() {}
};

struct TickingManager
{
    virtual void register_ticking(Ticking &ticking) = 0;
};

#endif /* TICKING_HPP */