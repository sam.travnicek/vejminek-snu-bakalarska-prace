#ifndef CONFIGURATION_HPP_
#define CONFIGURATION_HPP_

#include <string>
#include <nlohmann/json.hpp>

class Program;

class Configuration
{
    std::string _path;
    Program &_program;

    bool _load_database(nlohmann::json &cfg_database);
    bool _load_channels(nlohmann::json &cfg_channels);

public:
    static const char *DEFAULT_CONFIG_JSON;

    Configuration(const std::string &path, Program &program);

    bool load();
};

#endif /* CONFIGURATION_HPP_ */