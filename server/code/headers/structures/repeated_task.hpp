#ifndef STRUCTURES_REPEATED_TASK_HPP_
#define STRUCTURES_REPEATED_TASK_HPP_

#include <cstdint>
#include <array>
#include <string>
#include <ulib/DateTime.hpp>

struct repeated_task_t
{
    enum Type { RUN_SCRIPT = 0, WRITE_PORT, CALL_ACTION, _TYPE_END_ };

    uint32_t id;
    std::array<bool, 7> days;
    ulib::Time execution_time;
    Type type;
    std::string data;
    std::string description;
    ulib::DateTime last_time_executed;
    bool disabled;
};


#endif // STRUCTURES_REPEATED_TASK_HPP_