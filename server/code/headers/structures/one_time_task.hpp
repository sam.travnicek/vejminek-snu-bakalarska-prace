#ifndef STRUCTURES_ONE_TIME_TASK_HPP_
#define STRUCTURES_ONE_TIME_TASK_HPP_

#include <cstdint>
#include <array>
#include <string>
#include <ulib/DateTime.hpp>

struct one_time_task_t
{
    enum Type { RUN_SCRIPT = 0, WRITE_PORT, CALL_ACTION, _TYPE_END_ };

    uint32_t id;
    ulib::DateTime execution_time;
    bool executed;
    Type type;
    std::string data;
    std::string description;
};


#endif // STRUCTURES_ONE_TIME_TASK_HPP_