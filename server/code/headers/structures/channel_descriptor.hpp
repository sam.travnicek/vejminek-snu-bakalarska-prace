#ifndef STRUCTURES_CHANNEL_DESCRIPTOR_HPP_
#define STRUCTURES_CHANNEL_DESCRIPTOR_HPP_

#include <vector>
#include <string>

struct channel_descriptor_t
{
    enum ChannelType { TCP, TLS, WS, WSS };
    enum ClientsChecking { NONE, WHITELIST, BLACKLIST, _CLIENTS_CHECKING_END_ };

    ChannelType type;
    ClientsChecking clients_checking;
    std::vector<std::string> uuids;
    std::size_t max_clients;
    std::string ip_address;
    long port;
    std::string key_path;
    std::string cert_path;
};


#endif // STRUCTURES_CHANNEL_DESCRIPTOR_HPP_