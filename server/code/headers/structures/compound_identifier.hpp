#ifndef STRUCTURES_COMPOUND_IDENTIFIER_HPP_
#define STRUCTURES_COMPOUND_IDENTIFIER_HPP_

#include <string>

struct compound_identifier_t
{
    static compound_identifier_t parse(const std::string &id);

    std::string left;
    std::string right;
};


#endif // STRUCTURES_COMPOUND_IDENTIFIER_HPP_