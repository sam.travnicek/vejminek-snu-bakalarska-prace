#ifndef UNITYPE_TYPE_HPP
#define UNITYPE_TYPE_HPP

#include <ulib/types.hpp>
#include <string>
#include <vector>

namespace unitype {

// using type_t = uint;
typedef uint type_t;

// using int_t = int64_t;
// using real_t = long double;
// using bool_t = bool;
// using string_t = std::string; // used for enums too
typedef double number_t;
typedef bool bool_t;
typedef std::string string_t;

constexpr type_t T_NULL = 0;
constexpr type_t T_NUMBER = 1;
constexpr type_t T_BOOL = 2;
constexpr type_t T_STRING = 3;

class InvalidTypeConversion : std::exception
{
    type_t _type;

public:
    InvalidTypeConversion(type_t type)
        : _type(type) {}

    ~InvalidTypeConversion() {}

    const char* what() const noexcept
    {
        std::string msg("Invalid conversion to type ");
        switch (_type) {
        case T_NUMBER:
            msg += "NUMBER.";
            break;
        case T_BOOL:
            msg += "BOOL.";
            break;
        case T_STRING:
            msg += "STRING.";
            break;
        case T_NULL:
            msg += "NULL.";
            break;
        default:
            msg += "<UNKNOWN>.";
            break;
        }
        return msg.c_str();
    }

    type_t type() const noexcept
    {
        return _type;
    }
};

struct TypeParams
{
    virtual ~TypeParams() {}
};

struct NumberParams : public TypeParams
{
    number_t min;
    number_t max;

    NumberParams(number_t min, number_t max)
        : min(min), max(max) {}
};

}

#endif /* UNITYPE_TYPE_HPP */