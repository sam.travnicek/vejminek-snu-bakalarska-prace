#ifndef UNITYPE_VAR_HPP
#define UNITYPE_VAR_HPP

#include "type.hpp"

namespace unitype {

//TODO: implement move constructor, fix memory leakage in copy constructor
//TODO: implement operators ==, !=
class var final
{
    static void* make_pointer(number_t val)
    {
        number_t *v = new number_t;
        *v = val;
        return (void*)v;
    }

    static void* make_pointer(bool_t val)
    {
        bool_t *v = new bool_t;
        *v = val;
        return (void*)v;
    }

    static void* make_pointer(const string_t &val)
    {
        string_t *v = new string_t;
        *v = val;
        return (void*)v;
    }

    type_t _type;
    void *_value;

public:
    var(std::nullptr_t value)
        : _type(T_NULL), _value(value) {}

    var(int8_t value)
        : var((number_t)value) {}

    var(int16_t value)
        : var((number_t)value) {}

    var(int32_t value)
        : var((number_t)value) {}

    var(uint8_t value)
        : var((number_t)value) {}

    var(uint16_t value)
        : var((number_t)value) {}

    var(uint32_t value)
        : var((number_t)value) {}

    var(double value)
        : var((number_t)value) {}

    var(number_t value)
    {
        _type = T_NUMBER;
        _value = make_pointer(value);
    }

    var(bool_t value)
    {
        _type = T_BOOL;
        _value = make_pointer(value);
    }

    var(const string_t &value)
    {
        _type = T_STRING;
        _value = make_pointer(value);
    }

    var(const char *str)
    {
        _type = T_STRING;
        string_t *v = new string_t(str);
        _value = (void*)v;
    }

    var(const var &x)
    {
        ~var();

        _type = (type_t)x;
        if(_type == T_NUMBER)
            _value = make_pointer((number_t)x);
        else if(_type == T_BOOL)
            _value = make_pointer((bool_t)x);
        else
            _value = make_pointer((string_t)x);
    }

    ~var()
    {
        if(_type == T_NUMBER)
            delete (number_t*)_value;
        else if(_type == T_BOOL)
            delete (bool_t*)_value;
        else if(_type == T_STRING)
            delete (string_t*)_value;
    }

    operator number_t() const
    {
        if(_type == T_NULL)
            return 0;
        else if(_type == T_NUMBER)
            return *((number_t*)_value);
        else if(_type == T_BOOL)
            return (number_t)(*((bool_t*)_value));
        else if(_type == T_STRING) {
            try {
                return (number_t)std::stod(*((string_t*)_value));
            } catch(...) {}
        }

        throw InvalidTypeConversion(T_NUMBER);
    }

    operator bool_t() const
    {
        if(_type == T_NULL)
            return false;
        else if(_type == T_NUMBER)
            return (bool_t)(*((number_t*)_value));
        else if(_type == T_BOOL)
            return *((bool_t*)_value);
        else if(_type == T_STRING) {
            string_t& val = *((string_t*)_value);
            if(val == "true")
                return true;
            else if(val == "false")
                return false;
            else {
                try {
                    return (bool_t)std::stod(val);
                } catch(...) {}
            }
        }

        throw InvalidTypeConversion(T_BOOL);
    }

    operator string_t() const
    {
        if(_type == T_NUMBER)
            return std::to_string(*((number_t*)_value));
        else if(_type == T_BOOL)
            return *((bool_t*)_value) ? "true" : "false";
        else if(_type == T_STRING)
            return *((string_t*)_value);

        throw InvalidTypeConversion(T_STRING);
    }

    operator type_t() const { return _type; }

    bool operator==(const var &x) const
    {
        if(_type != x._type)
            return false;
        if(_type == T_NUMBER)
            return (number_t)*this == (number_t)x;
        else if(_type == T_BOOL)
            return (bool_t)*this == (bool_t)x;
        else if(_type == T_STRING)
            return (string_t)*this == (string_t)x;
        else // case of nullpointers
            return true;
    }

    bool operator!=(const var &x) const
    {
        return !operator==(x);
    }
};

}

#endif /* UNITYPE_VAR_HPP */