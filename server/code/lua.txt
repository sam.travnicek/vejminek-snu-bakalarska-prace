PUSH value to stack:
    void lua_pushnil (lua_State *L);
    void lua_pushboolean (lua_State *L, int bool);
    void lua_pushnumber (lua_State *L, double n);
    void lua_pushlstring (lua_State *L, const char *s, size_t length);
    void lua_pushstring (lua_State *L, const char *s);

CHECK LUA type:
    int lua_is... (lua_State *L, int index);

    lua_is... checks if value can be converted to that type, so any number for example satisfies lua_isstring

    lua_type returns these values:
        LUA_TNIL, LUA_TBOOLEAN, LUA_TNUMBER, LUA_TSTRING, LUA_TTABLE, LUA_TFUNCTION, LUA_TUSERDATA, and LUA_TTHREAD

GET value from stack:
    The first element has index 1, not 0 !!!

    int            lua_toboolean (lua_State *L, int index);
    double         lua_tonumber (lua_State *L, int index);
    const char    *lua_tostring (lua_State *L, int index);
    size_t         lua_strlen (lua_State *L, int index);

    when the value hasn't correct type, in case of lua_toboolean, lua_tonumber and lua_strlen 0 is returned, NULL in other cases

other stack operations:
    int   lua_gettop (lua_State *L);
    void  lua_settop (lua_State *L, int index);
    void  lua_pushvalue (lua_State *L, int index); ... push a copy of value on index to stack
    void  lua_remove (lua_State *L, int index);
    void  lua_insert (lua_State *L, int index); ... moves the top element into the given position
    void  lua_replace (lua_State *L, int index); ... pops a value from the top and sets it as the value of the given index,

    lua_settop(L, 0) ... empties stack

    pop n elements from the stack:
        #define lua_pop(L,n)  lua_settop(L, -(n)-1)