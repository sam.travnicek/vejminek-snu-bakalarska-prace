#include "structures/compound_identifier.hpp"

compound_identifier_t compound_identifier_t::parse(const std::string &id)
{
    compound_identifier_t cid;
    bool left_sect = true;

    for(uint i = 0; i < id.length(); ++i) {
        char c = id.at(i);
        if(c == ':') {
            left_sect = false;
            continue;
        }
        if(left_sect) {
            cid.left += c;
        } else {
            cid.right += c;
        }
    }

    return cid;
}