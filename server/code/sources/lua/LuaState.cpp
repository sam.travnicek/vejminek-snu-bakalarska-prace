#include "lua/LuaState.hpp"

LuaState::LuaState()
    : _temp(false)
{
    _L = luaL_newstate();
}

LuaState::LuaState(lua_State *_L)
    : _temp(true), _L(_L) {}

LuaState::~LuaState()
{
    if(!_temp)
        lua_close(_L);
}

bool LuaState::run_script(const std::string &str)
{
    if(luaL_loadstring(_L, str.c_str()) != LUA_OK)
        return false;
    if(lua_pcall(_L, 0, 0, 0) != LUA_OK)
        return false;
    return true;
}

bool LuaState::run_file(const std::string &path)
{
    if(luaL_loadfile(_L, path.c_str()) != LUA_OK)
        return false;
    if(lua_pcall(_L, 0, 0, 0) != LUA_OK)
        return false;
    return true;
}

int LuaState::current_line()
{
    lua_Debug ar;
    get_stack(1, ar);
    get_info("l", ar);
    return ar.currentline;
}

std::string LuaState::source()
{
    lua_Debug ar;
    get_stack(1, ar);
    get_info("S", ar);
    const char* src = ar.source;
    if(src == nullptr)
        return std::string();
    return std::string(src);
}

std::string LuaState::short_source()
{
    lua_Debug ar;
    get_stack(1, ar);
    get_info("S", ar);
    const char* src = ar.short_src;
    if(src == nullptr)
        return std::string();
    return std::string(src);
}