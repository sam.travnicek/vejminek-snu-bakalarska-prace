#include "lua/ConditionSandbox.hpp"
#include "vejminek/Device.hpp"
#include "lua/GlobalEnvironment.hpp"

ConditionSandbox::ConditionSandbox(
        LuaState &ls, DeviceManager &dm, ulib::Logger &logger)
    : Sandbox(ls)
{
    load_safe_tools();
    load_G();
    GlobalEnvironment::load_globals(*this);

    push_environment();

    push(&dm);
    push(&logger);
    push(lw_device, 2);
    set_field(-2, "device");

    pop(); // pop environment
}

int ConditionSandbox::lw_device(lua_State *L)
{
    LuaState ls(L);
    int num_args = ls.top();

    ls.push_upvalue(1);
    DeviceManager &dm = ls.to_ref<DeviceManager>(-1);

    ls.push_upvalue(2);
    ulib::Logger &logger = ls.to_ref<ulib::Logger>(-1);

    lua_Debug ar;
    ls.get_stack(1, ar);
    ls.get_info("nSl", ar);

    if(num_args != 1) {
        logger << ar.short_src << ":" << ar.currentline
            << ": expected 1 argument (method 'action')" << ulib::lerror;
        ls.push_nil();
        return 1;
    }

    if(!ls.is_int(1)) {
        logger << ar.short_src << ":" << ar.currentline
            << ": the 1st argument must be an integral value (method 'action')" << ulib::lerror;
        ls.push_nil();
        return 1;
    }

    int id = ls.to_int(1);
    if(id < 0) {
        ls.push_nil();
        return 1;
    }

    Device *dev = dm.device((unsigned)id);
    if(!dev) {
        ls.push_nil();
        return 1;
    }

    GlobalEnvironment::get(ls, dev);
    return 1;
}