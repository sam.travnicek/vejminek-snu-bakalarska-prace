#include "lua/RequirementSandbox.hpp"
#include "vejminek/Device.hpp"
#include "lua/GlobalEnvironment.hpp"

RequirementSandbox::RequirementSandbox(LuaState &ls, ulib::Logger &logger, const std::string &dir)
    : Sandbox(ls)
{
    load_safe_tools();
    load_logging_tools(logger);
    load_file_tools(logger, dir);
    load_module_tools(logger, dir, ls);
    load_G();

    GlobalEnvironment::load_globals(*this);
    GlobalEnvironment::load_tools(*this);
}