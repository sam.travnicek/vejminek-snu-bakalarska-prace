#include "lua/ClosureLS.hpp"
#include <sstream>

const std::vector<std::string> ClosureLS::_ARG_TYPE_STR = {
    "BOOL", "INT", "DOUBLE", "STRING", "POINTER", "USERDATA", "TABLE",
    "OBJECT", "THREAD", "FUNCTION", "CFUNCTION" };

std::string ClosureLS::_type_string(ArgType type)
{
    switch(type) {
    case BOOL:
        return "a boolean value";
    case INT:
        return "an integral value";
    case DOUBLE:
        return "a number";
    case STRING:
        return "a string";
    case POINTER:
        return "a pointer";
    case USERDATA:
        return "userdata";
    case TABLE:
        return "a table";
    case OBJECT:
        return "an object";
    case THREAD:
        return "a thread";
    case FUNCTION:
        return "a function";
    case CFUNCTION:
        return "a C function";
    default:
        return "?";
    }
}

std::string ClosureLS::_number_postfix(int n)
{
    switch (n % 10) {
    case 1:
        return "st";
    case 2:
        return "nd";
    case 3:
        return "rd";
    default:
        return "th";
    }
}

bool ClosureLS::_type_check(int idx, ArgType type)
{
    switch(type) {
    case BOOL:
        return is_bool(idx);
    case INT:
        return is_int(idx);
    case DOUBLE:
        return is_double(idx);
    case STRING:
        return is_string(idx);
    case POINTER:
        return is_ptr(idx);
    case USERDATA:
        return is_userdata(idx);
    case TABLE:
        return is_table(idx);
    case OBJECT:
        return is_obj(idx);
    case THREAD:
        return is_thread(idx);
    case FUNCTION:
        return is_function(idx);
    case CFUNCTION:
        return is_cfunction(idx);
    default:
        return false;
    }
}

ClosureLS::ArgType ClosureLS::_type_of(int idx)
{
    switch(type(idx)) {
    case LUA_TBOOLEAN:
        return BOOL;
    case LUA_TNUMBER:
        return is_int(idx) ? INT : DOUBLE;
    case LUA_TSTRING:
        return STRING;
    case LUA_TLIGHTUSERDATA:
        return POINTER;
    case LUA_TUSERDATA:
        return USERDATA;
    case LUA_TTABLE:
        return TABLE;
    case LUA_TTHREAD:
        return THREAD;
    case LUA_TFUNCTION:
        return FUNCTION;
    }

    return __ARG_TYPE_END__;
}

ClosureLS::ClosureLS(
        lua_State *L, int logger_upvalue_idx, const std::string &name, ClosureType type)
    : LuaState(L), _name(name), _type(type)
{
    // for method: the first argument is an object, not method argument
    if(type == CT_METHOD) {
        if(top() == 0)
            _num_args = 0;
        else
            _num_args = (unsigned)(top() - 1);
    } else
        _num_args = (unsigned)top();

    push_upvalue(logger_upvalue_idx);
    _logger = to_ptr<ulib::Logger>(-1);
    pop();

    get_stack(1, _debug);
    get_info("nSl", _debug);
}

std::string ClosureLS::msg_prefix()
{
    std::stringstream ss;
    ss << _debug.short_src << ":" << _debug.currentline << ":";
    return ss.str();
}

std::string ClosureLS::msg_postfix()
{
    std::stringstream ss;
    ss << "(" << (_type == CT_FUNCTION ? "function" : "method") << " '" << _name << "')";
    return ss.str();
}

bool ClosureLS::check_protected_fields(const std::vector<std::string> &fields)
{
    if(!is_string(2))
        return false;

    std::string index = to_string(2);

    if(std::find(fields.begin(), fields.end(), index) != fields.end()) {
        *_logger << msg_prefix() << " attempt to modify protected field '"
            << index << "'" << ulib::lerror;
        return false;
    }

    return true;
}

void ClosureLS::error_protected_obj()
{
    *_logger << msg_prefix() << ": attempt to modify protected object" << ulib::lerror;
}

bool ClosureLS::check_object(ArgType type)
{
    if(_type != CT_METHOD)
        return true;

    if(is_obj(1)) {
        if(type == TABLE && !is_table(1)) {
            *_logger << msg_prefix() << " object must be " << _type_string(TABLE)
                << " " << msg_postfix() << ulib::lerror;
            return false;
        }
        if(type == USERDATA && !is_userdata(1)) {
            *_logger << msg_prefix() << " object must be " << _type_string(USERDATA)
                << " " << msg_postfix() << ulib::lerror;
            return false;
        }
    } else {
        *_logger << msg_prefix() << " called without an object "
            << msg_postfix() << ulib::lerror;
        return false;
    }

    return true;
}

int ClosureLS::check_arguments()
{
    int first_idx = _type == CT_FUNCTION ? 1 : 2;
    int matching_config = -1;

    for(size_t ci = 0; ci < _arg_lists.size(); ++ci) {
        std::vector<ArgType> &args = _arg_lists[ci];

        if(args.size() != _num_args)
            continue;

        size_t num_matching = 0;
        for(size_t i = 0; i < args.size(); ++i) {
            if(_type_of(i + first_idx) != args[i])
                break;
            ++num_matching;
        }

        if(num_matching == args.size()) {
            matching_config = ci;
            break;
        }
    }

    if(matching_config < 0) {
        std::string closure_word = _type == CT_FUNCTION ? "function" : "method";

        if(_arg_lists.size() == 1) {
            std::string amount_word = _num_args < _arg_lists[0].size() ? "few" : "many";
            *_logger << msg_prefix() << " too " << amount_word << " arguments to ";
            *_logger << closure_word << " '" << _name << "(";
        } else {
            *_logger << msg_prefix() << " no matching " << closure_word << " for call '";
            *_logger << _name << "(";
        }

        for(size_t i = 0; i < _num_args; ++i) {
            ArgType at = _type_of(first_idx + i);
            if(i != 0)
                *_logger << ", ";
            *_logger << (at < _ARG_TYPE_STR.size() ? _ARG_TYPE_STR[at] : "?");
        }

        if(_arg_lists.size() == 0)
            *_logger << ")'\n\ncandidate:\n  '" << _name << "()'";
        else if(_arg_lists.size() == 1)
            *_logger << ")'\n\ncandidate:\n";
        else
            *_logger << ")'\n\ncandidates:\n";

        for(size_t ci = 0; ci < _arg_lists.size(); ++ci) {
            *_logger << "  '" << _name << "(";
            for(size_t i = 0; i < _arg_lists[ci].size(); ++i) {
                ArgType at = _arg_lists[ci][i];
                if(i != 0)
                    *_logger << ", ";
                *_logger << (at < _ARG_TYPE_STR.size() ? _ARG_TYPE_STR[at] : "?");
            }
            *_logger << ")'\n";
        }

        *_logger << ulib::lerror;
    }

    return matching_config;
}

bool ClosureLS::check_args(const std::vector<ArgType> &req_args, uint optional_from)
{
    int first_idx = _type == CT_FUNCTION ? 1 : 2;

    for(uint i = 0; i < req_args.size(); ++i) {
        ArgType arg_type = req_args[i];

        bool check = true;
        if(optional_from >= 0 && i >= optional_from) {
            if(_num_args > i)
                check = _type_check(i + first_idx, arg_type);
        } else
            check = _type_check(i + first_idx, arg_type);

        if(!check) {
            *_logger << msg_prefix() << " the "
                << (i+1) << _number_postfix(i+1) << " argument must be "
                << _type_string(arg_type) << " " << msg_postfix() << ulib::lerror;
            return false;
        }
    }

    return true;
}

bool ClosureLS::check_arg(int n, const std::vector<ArgType> &types)
{
    if(types.size() == 0)
        return false;

    int first_idx = _type == CT_FUNCTION ? 1 : 2;
    bool match = false;

    for(uint i = 0; i < types.size(); ++i) {
        if(_type_check(n + first_idx, types[i])) {
            match = true;
            break;
        }
    }

    if(!match) {
        *_logger << msg_prefix() << " the " << (n+1) << _number_postfix(n+1) << " argument must be "
            << _type_string(types[0]);

        for(uint i = 1; i < types.size(); ++i) {
            if(i < types.size()-1)
                *_logger << ", ";
            else
                *_logger << " or ";
            *_logger << _type_string(types[i]);
        }

        *_logger << " " << msg_postfix() << ulib::lerror;
    }

    return match;
}

bool ClosureLS::check_closure(const std::vector<uint> &req_nargs, ArgType obj_type)
{
    if(_type == CT_METHOD) {
        if(is_obj(1)) {
            if(obj_type == TABLE && !is_table(1)) {
                *_logger << msg_prefix() << " object must be " << _type_string(TABLE)
                    << " " << msg_postfix() << ulib::lerror;
                return false;
            }
            if(obj_type == USERDATA && !is_userdata(1)) {
                *_logger << msg_prefix() << " object must be " << _type_string(USERDATA)
                    << " " << msg_postfix() << ulib::lerror;
                return false;
            }
        } else {
            *_logger << msg_prefix() << " called without an object "
                << msg_postfix() << ulib::lerror;
            return false;
        }
    }

    if(req_nargs.size() == 1) {
        if(_num_args != req_nargs[0]) {
            *_logger << msg_prefix();
            if(req_nargs[0] == 0)
                *_logger << " no arguments expected ";
            else if(req_nargs[0] == 1)
                *_logger << " expected 1 argument ";
            else
                *_logger << " expected " << req_nargs[0] << " arguments ";
            *_logger << msg_postfix() << ulib::lwarn;
        }
    } else if(req_nargs.size() > 1) {
        bool match = false;

        for(uint num : req_nargs) {
            if(_num_args == num) {
                match = true;
                break;
            }
        }

        if(!match) {
            *_logger << msg_prefix() << " expected " << req_nargs[0];

            for(uint i = 1; i < req_nargs.size(); i++) {
                if(i < req_nargs.size()-1)
                    *_logger << ", ";
                else
                    *_logger << " or ";
                *_logger << req_nargs[i];
            }

            *_logger << " arguments " << msg_postfix() << ulib::lwarn;
        }
    }

    return true;
}