#include "lua/LinkageSandbox.hpp"
#include "plugins/DeviceTemplate.hpp"
#include "lua/GlobalEnvironment.hpp"
#include "lua/ClosureLS.hpp"

LinkageSandbox::LinkageSandbox(
        LuaState &ls, DeviceManager &dm, SchedulerService &scheduler,
        DevicesService &ds, ulib::Logger &logger)
    : Sandbox(ls)
{
    load_safe_tools();
    load_logging_tools(logger);
    GlobalEnvironment::load_globals(*this);

    push_environment();

    push(&dm);
    push(&logger);
    push(lw_device, 2);
    set_field(-2, "device");

    push(&logger);
    push(scheduler.reference());
    push(ds.reference());
    push(lw_service, 3);
    set_field(-2, "service");

    pop(); // pop environment
}

int LinkageSandbox::lw_device(lua_State *L)
{
    ClosureLS ls(L, 2, "device", ClosureLS::CT_FUNCTION);

    if(!ls.check_closure(1) || !ls.check_args({ClosureLS::INT})) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    DeviceManager &dm = ls.to_ref<DeviceManager>(-1);

    int id = ls.to_int(1);
    if(id < 0) {
        ls.push_nil();
        return 1;
    }

    Device *dev = dm.device((unsigned)id);
    if(!dev) {
        ls.push_nil();
        return 1;
    }

    GlobalEnvironment::get(ls, dev);
    return 1;
}

int LinkageSandbox::lw_service(lua_State *L)
{
    ClosureLS ls(L, 1, "service", ClosureLS::CT_FUNCTION);
    ulib::Logger &logger = ls.logger();

    std::string service_name = ls.to_string(1);

    if(service_name == "scheduler") {
        ls.push_upvalue(2);
        ls.raw_get(LUA_REGISTRYINDEX);
        return 1;
    } else if(service_name == "devices") {
        ls.push_upvalue(3);
        ls.raw_get(LUA_REGISTRYINDEX);
        return 1;
    } else {
        logger << ls.msg_prefix() << " unknown service '" << service_name << "' "
            << ls.msg_postfix() << ulib::lerror;
        ls.push_nil();
        return 1;
    }
}