#include "lua/SerialPortLUA.hpp"
#include "lua/GlobalEnvironment.hpp"
#include "lua/ClosureLS.hpp"

extern "C" {
#include <sys/epoll.h>
}

SerialPortLUA::SerialPortLUA(const std::string &port, int baudrate)
    : ulib::SerialPort(port, baudrate), _serial_error(false)
{
    open();
    _init_fd();
}

SerialPortLUA::SerialPortLUA(
        const std::string &port, int baudrate, char bits, char parity, char stop_bits)
    : ulib::SerialPort(port, baudrate, bits, parity, stop_bits), _serial_error(false)
{
    open();
    _init_fd();
}

void SerialPortLUA::_init_fd()
{
    _epoll_fd = epoll_create1(0);

    if(_epoll_fd < 0) {
		close();
        throw ulib::SerialPortFailure("Failed to create epoll FD.");
	}

	int fd = file_descriptor();
    epoll_event ep_event;
    memset(&ep_event, 0, sizeof(epoll_event));
    ep_event.data.fd = fd;
    ep_event.events = EPOLLIN;

    if(epoll_ctl(_epoll_fd, EPOLL_CTL_ADD, fd, &ep_event) != 0) {
        ::close(_epoll_fd);
        close();
        throw ulib::SerialPortFailure("Failed to add the file descriptor to epoll.");
    }
}

bool SerialPortLUA::_available()
{
    epoll_event ep_ev;
    memset(&ep_ev, 0, sizeof(epoll_event));
    int ev_cnt = epoll_wait(_epoll_fd, &ep_ev, 1, 0);
    return ev_cnt != 0;
}

void SerialPortLUA::inject(Sandbox &sb, ulib::Logger &logger)
{
    sb.push_environment();

    // Table wrapper for class
    sb.new_table();

    // Add table to environment
    sb.push("SerialPort");
    sb.copy(-2);
    sb.raw_set(-4);

    // Metatable for table wrapper
    sb.new_table();

	sb.push("__index"); // handle function for accessing fields
    sb.push(&logger);
	sb.push(clw__index, 1);
	sb.raw_set(-3);

	sb.push("__newindex"); // handle function for modifying fields
    sb.push(&logger);
	sb.push(clw__newindex, 1);
	sb.raw_set(-3);

	sb.push("__metatable"); // prevent getting metatable (returns nil)
	sb.push_nil();
	sb.raw_set(-3);

    // Assign metatable to the instance
    sb.set_metatable(-2);

    // Pop table and environment from stack
    sb.pop(2);
}

int SerialPortLUA::clw__index(lua_State *L)
{
    LuaState ls(L);

    ls.push_upvalue(1);
    ulib::Logger &logger = ls.to_ref<ulib::Logger>(-1);

    std::string index = ls.to_string(2);

    if(index == "new") {
        ls.push(&logger);
        ls.push(clw_new, 2);
    } else if(index == "NONE")
        ls.push((int)'N');
    else if(index == "EVEN")
        ls.push((int)'E');
    else if(index == "ODD")
        ls.push((int)'O');
    else
        ls.push_nil();

    return 1;
}

int SerialPortLUA::clw__newindex(lua_State *L)
{
    ClosureLS ls(L, 1, "__newindex", ClosureLS::CT_METHOD);

    if(!ls.check_protected_fields({"new", "NONE", "EVEN", "ODD"}))
        return 0;

    ls.error_protected_obj();
    return 0;
}

int SerialPortLUA::clw_new(lua_State *L)
{
    ClosureLS ls(L, 1, "new", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_closure({2, 5}, ClosureLS::TABLE) || !ls.check_args({
        ClosureLS::STRING, ClosureLS::INT, ClosureLS::INT, ClosureLS::INT, ClosureLS::INT
    }, 2)) {
        ls.push_nil();
        return 1;
    }

    void *ptr = ls.new_user_data(sizeof(SerialPortLUA));
    SerialPortLUA *sp = nullptr;

    std::string port = ls.to_string(2);
    int baudrate = ls.to_int(3);

    if(ls.num_args() == 5) {
        char bits = ls.to_int(4);
        char parity = ls.to_int(5);
        char stop_bits = ls.to_int(6);
        try {
            sp = new(ptr) SerialPortLUA(port, baudrate, bits, parity, stop_bits);
        } catch(std::exception &ex) {
            logger << ls.msg_prefix() << " " << ex.what() << " "<< ls.msg_postfix() << ulib::lerror;
        }
    } else {
        try {
            sp = new(ptr) SerialPortLUA(port, baudrate);
        } catch(std::exception &ex) {
            logger << ls.msg_prefix() << " " << ex.what() << " "<< ls.msg_postfix() << ulib::lerror;
        }
    }

    if(!sp) {
        ls.push_nil();
        return 1;
    }

    // Metatable for userdata
    ls.new_table();

	ls.push("__gc"); // handle function for garbage collection
	ls.push(ilw__gc);
	ls.raw_set(-3);

	ls.push("__index"); // handle function for accessing fields
    ls.push(&logger);
	ls.push(ilw__index, 1);
	ls.raw_set(-3);

	ls.push("__newindex"); // handle function for modifying fields
    ls.push(&logger);
	ls.push(ilw__newindex, 1);
	ls.raw_set(-3);

	ls.push("__metatable"); // prevent getting metatable (returns nil)
	ls.push_nil();
	ls.raw_set(-3);

    // Assign metatable to the instance
    ls.set_metatable(-2);

    // Create support table for members not implemented in C++
    lua_newtable(L);
    lua_setuservalue(L, -2);

    return 1;
}

int SerialPortLUA::ilw__gc(lua_State *L)
{
    LuaState ls(L);

    SerialPortLUA &sp = ls.to_ref<SerialPortLUA>(1);
    sp.~SerialPortLUA();

    return 0;
}

int SerialPortLUA::ilw__index(lua_State *L)
{
    LuaState ls(L);

    ls.push_upvalue(1);
    ulib::Logger &logger = ls.to_ref<ulib::Logger>(-1);

    std::string index = ls.to_string(2);

    if(index == "io_error") {
        ls.push(&logger);
        ls.push(ilw_io_error, 1);
    } else if(index == "available") {
        ls.push(&logger);
        ls.push(ilw_available, 1);
    } else if(index == "send") {
        ls.push(&logger);
        ls.push(ilw_send, 1);
    } else if(index == "send_byte") {
        ls.push(&logger);
        ls.push(ilw_send_byte, 1);
    } else if(index == "send_bytes") {
        ls.push(&logger);
        ls.push(ilw_send_bytes, 1);
    } else if(index == "read") {
        ls.push(&logger);
        ls.push(ilw_read, 1);
    } else if(index == "read_all") {
        ls.push(&logger);
        ls.push(ilw_read_all, 1);
    } else if(index == "read_byte") {
        ls.push(&logger);
        ls.push(ilw_read_byte, 1);
    } else if(index == "read_bytes") {
        ls.push(&logger);
        ls.push(ilw_read_bytes, 1);
    } else {
        // Get support table for members not implemented in C++
        ls.get_user_value(1);
        ls.copy(2);
        ls.raw_get(-2);
    }

    return 1;
}

int SerialPortLUA::ilw__newindex(lua_State *L)
{
    ClosureLS ls(L, 1, "__newindex", ClosureLS::CT_METHOD);

    if(!ls.check_protected_fields({
            "error_code", "available", "send", "send_byte", "send_bytes",
            "read", "read_all", "read_byte", "read_bytes" }))
        return 0;

    // Save the new index to support table
    ls.get_user_value(1);
    ls.copy(2);
    ls.copy(3);
    ls.raw_set(-3);

    return 0;
}

int SerialPortLUA::ilw_io_error(lua_State *L)
{
    ClosureLS ls(L, 1, "io_error", ClosureLS::CT_METHOD);

    if(!ls.check_closure(0, ClosureLS::USERDATA)) {
        ls.push_nil();
        return 1;
    }

    SerialPortLUA &sp = ls.to_ref<SerialPortLUA>(1);
    ls.push(sp.io_error());

    return 1;
}

int SerialPortLUA::ilw_available(lua_State *L)
{
    ClosureLS ls(L, 1, "available", ClosureLS::CT_METHOD);

    if(!ls.check_closure(0, ClosureLS::USERDATA)) {
        ls.push_nil();
        return 1;
    }

    SerialPortLUA &sp = ls.to_ref<SerialPortLUA>(1);
    ls.push(sp._available());

    return 1;
}

int SerialPortLUA::ilw_send(lua_State *L)
{
    ClosureLS ls(L, 1, "send", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_closure(1, ClosureLS::USERDATA) || !ls.check_args({ClosureLS::STRING})) {
        ls.push_nil();
        return 1;
    }

    SerialPortLUA &sp = ls.to_ref<SerialPortLUA>(1);

    if(sp._serial_error) {
        logger << ls.msg_prefix() << " serial port not available anymore "
            << ls.msg_postfix() << ulib::lerror;
        return 0;
    }

    sp.send(ls.to_string(2));

    return 0;
}

int SerialPortLUA::ilw_send_byte(lua_State *L)
{
    ClosureLS ls(L, 1, "send_byte", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_closure(1, ClosureLS::USERDATA) || !ls.check_args({ClosureLS::INT})) {
        ls.push_nil();
        return 1;
    }

    SerialPortLUA &sp = ls.to_ref<SerialPortLUA>(1);

    if(sp._serial_error) {
        logger << ls.msg_prefix() << " serial port not available anymore "
            << ls.msg_postfix() << ulib::lerror;
        return 0;
    }

    ubyte data = ls.to_uint(2);
    sp.send(&data, 1);

    return 0;
}

int SerialPortLUA::ilw_send_bytes(lua_State *L)
{
    ClosureLS ls(L, 1, "send_bytes", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_closure(1, ClosureLS::USERDATA) || !ls.check_args({ClosureLS::TABLE})) {
        ls.push_nil();
        return 1;
    }

    SerialPortLUA &sp = ls.to_ref<SerialPortLUA>(1);

    if(sp._serial_error) {
        logger << ls.msg_prefix() << " serial port not available anymore "
            << ls.msg_postfix() << ulib::lerror;
        return 0;
    }

    std::vector<ubyte> data;
    ls.copy(2);
    size_t len = ls.length(-1);

    for(size_t i = 0; i < len; ++i) {
        ls.raw_get_idx(-1, i+1);
        data.push_back(ls.to_uint(-1));
        ls.pop();
    }

    sp.send(&data[0], data.size());

    return 0;
}

int SerialPortLUA::ilw_read(lua_State *L)
{
    ClosureLS ls(L, 1, "read", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_closure(0, ClosureLS::USERDATA)) {
        ls.push_nil();
        return 1;
    }

    SerialPortLUA &sp = ls.to_ref<SerialPortLUA>(1);

    if(!sp._available()) {
        ls.push_nil();
        return 1;
    }

    ubyte c;
    uint buf_len = sp.poll(&c, 1);

    if(buf_len == 0) {
        sp._serial_error = true;
        sp.close();
        logger << ls.msg_prefix() << ": serial port not available anymore "
            << ls.msg_postfix() << ulib::lerror;
        ls.push_nil();
        return 1;
    }

    ls.push(std::string(1, (char)c));
    return 1;
}

int SerialPortLUA::ilw_read_all(lua_State *L)
{
    ClosureLS ls(L, 1, "read_all", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_closure(0, ClosureLS::USERDATA)) {
        ls.push_nil();
        return 1;
    }

    SerialPortLUA &sp = ls.to_ref<SerialPortLUA>(1);

    if(!sp._available()) {
        ls.push_nil();
        return 1;
    }

    std::string data = "";
    ubyte buf[1025] = {0};

    do {
        uint buf_len = sp.poll(buf, 1024);
        buf[buf_len] = 0;

        if(buf_len == 0) {
            sp._serial_error = true;
            sp.close();
            logger << ls.msg_prefix() << ": serial port not available anymore "
                << ls.msg_postfix() << ulib::lerror;
            ls.push(data);
            return 1;
        }

        data.append(std::string((char*)buf));
    } while(sp._available());

    ls.push(data);
    return 1;
}

int SerialPortLUA::ilw_read_byte(lua_State *L)
{
    ClosureLS ls(L, 1, "read_byte", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_closure(0, ClosureLS::USERDATA)) {
        ls.push_nil();
        return 1;
    }

    SerialPortLUA &sp = ls.to_ref<SerialPortLUA>(1);

    if(!sp._available()) {
        ls.push_nil();
        return 1;
    }

    ubyte c;
    uint buf_len = sp.poll(&c, 1);

    if(buf_len == 0) {
        sp._serial_error = true;
        sp.close();
        logger << ls.msg_prefix() << ": serial port not available anymore "
            << ls.msg_postfix() << ulib::lerror;
        ls.push_nil();
        return 1;
    }

    ls.push((uint)c);
    return 1;
}

int SerialPortLUA::ilw_read_bytes(lua_State *L)
{
    ClosureLS ls(L, 1, "read_bytes", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_closure(0, ClosureLS::USERDATA)) {
        ls.push_nil();
        return 1;
    }

    SerialPortLUA &sp = ls.to_ref<SerialPortLUA>(1);

    if(!sp._available()) {
        ls.push_nil();
        return 1;
    }

    ubyte buf[4096] = {0};
    ls.new_table();
    uint table_ptr = 1;

    do {
        size_t buf_len = sp.poll(buf, 4096);

        if(buf_len == 0) {
            sp._serial_error = true;
            sp.close();
            logger << ls.msg_prefix() << ": serial port not available anymore "
                << ls.msg_postfix() << ulib::lerror;
            ls.push_nil();
            return 1;
        }

        for(size_t i = 0; i < buf_len; ++i) {
            ls.push((uint)buf[i]);
            ls.raw_set_idx(-2, table_ptr++);
        }

    } while(sp._available());

    return 1;
}