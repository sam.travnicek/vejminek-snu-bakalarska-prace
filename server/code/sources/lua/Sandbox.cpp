#include "lua/Sandbox.hpp"
#include <ulib/System.hpp>
#include <fstream>
#include <streambuf>
#include "lua/RequirementSandbox.hpp"
#include "lua/ClosureLS.hpp"

Sandbox::Sandbox(LuaState &ls)
    : LuaState(ls.state())
{
    new_table();
    _reference = ref(LUA_REGISTRYINDEX);
}

Sandbox::~Sandbox()
{
    if(!temporary())
        unref(LUA_REGISTRYINDEX, _reference);
}

void Sandbox::load_G()
{
    push_environment();
    get_global("_G");
    set_field(-2, "_G");
    pop();
}

void Sandbox::load_safe_tools()
{
    push_environment();

    const char *global_functions[] = {
        "assert", "ipairs", "pairs", "next", "pcall", "xpcall", "select",
        "tonumber", "tostring", "type", "unpack", "rawequal", "rawget",
        "rawlen", "rawset", "getmetatable", "setmetatable", "_VERSION" };

    for(const char *func_name : global_functions) {
        get_global(func_name);
        set_field(-2, func_name);
    }

    //--- coroutine.<> ---------------------------------------------------------

    new_table();
    copy(-1);
    set_field(-3, "coroutine");
    get_global("coroutine");

    const char *coroutine_functions[] = {
        "create", "resume", "running", "status", "wrap", "yield" };

    for(const char *func_name : coroutine_functions) {
        get_field(-1, func_name);
        set_field(-3, func_name);
    }

    pop(2);

    //--- string.<> ------------------------------------------------------------

    new_table();
    copy(-1);
    set_field(-3, "string");
    get_global("string");

    const char *string_functions[] = {
        "byte", "char", "find", "format", "gmatch", "gsub", "len", "lower",
        "match", "rep", "reverse", "sub", "upper" };

    for(const char *func_name : string_functions) {
        get_field(-1, func_name);
        set_field(-3, func_name);
    }

    pop(2);

    //--- table.<> -------------------------------------------------------------

    new_table();
    copy(-1);
    set_field(-3, "table");
    get_global("table");

    const char *table_functions[] = {
        "insert", "maxn", "remove", "sort" };

    for(const char *func_name : table_functions) {
        get_field(-1, func_name);
        set_field(-3, func_name);
    }

    pop(2);

    //--- math.<> --------------------------------------------------------------

    new_table();
    copy(-1);
    set_field(-3, "math");
    get_global("math");

    const char *math_functions[] = {
        "abs", "acos", "asin", "atan", "atan2", "ceil", "cos", "cosh", "deg",
        "exp", "floor", "fmod", "frexp", "huge", "ldexp", "log", "log10",
        "max", "min", "modf", "pi", "pow", "rad", "random", "sin", "sinh",
        "sqrt", "tan", "tanh" };

    for(const char *func_name : math_functions) {
        get_field(-1, func_name);
        set_field(-3, func_name);
    }

    pop(2);

    //--- os.<> ----------------------------------------------------------------

    new_table();
    copy(-1);
    set_field(-3, "os");
    get_global("os");

    const char *os_functions[] = {
        "clock", "date", "difftime", "time" };

    for(const char *func_name : os_functions) {
        get_field(-1, func_name);
        set_field(-3, func_name);
    }

    pop(2);

    //--------------------------------------------------------------------------

    pop(); // pop environment
}

void Sandbox::load_logging_tools(ulib::Logger &logger)
{
    push_environment();

    push(&logger);
    push(lw_print, 1);
    set_field(-2, "print");

    push(&logger);
    push(lw_print, 1);
    set_field(-2, "info");

    push(&logger);
    push(lw_warning, 1);
    set_field(-2, "warning");

    push(&logger);
    push(lw_error, 1);
    set_field(-2, "error");

    push(&logger);
    push(lw_debug, 1);
    set_field(-2, "debug");

    pop(); // pop environment
}

void Sandbox::load_file_tools(ulib::Logger &logger, const std::string &dir)
{
    push_environment();

    push(&logger);
    push(dir);
    push(lw_load, 2);
    set_field(-2, "load");

    pop(); // pop environment
}

void Sandbox::load_module_tools(ulib::Logger &logger, const std::string &dir, LuaState &ls)
{
    push_environment();

    push(&logger);
    push(dir);
    push(&ls);
    push(lw_module, 3);
    set_field(-2, "module");

    pop(); // pop environment
}

void Sandbox::push_environment()
{
    push(_reference);
    raw_get(LUA_REGISTRYINDEX);
}

bool Sandbox::environment_script(const std::string &str)
{
    if(!load_script(str))
        return false;
    push_environment();
    set_upvalue(-2, 1);
    return pcall();
}

bool Sandbox::environment_file(const std::string &path)
{
    if(!load_file(path))
        return false;
    push_environment();
    set_upvalue(-2, 1);
    return pcall();
}

void Sandbox::lua_output(lua_State *L, uint level)
{
    LuaState ls(L);
    int top = ls.top();

    ls.push_upvalue(1);
    ulib::Logger &logger = ls.to_ref<ulib::Logger>(-1);

    switch(ls.type(1)) {
    case LUA_TBOOLEAN:
        logger << (ls.to_bool(1) ? "true" : "false");
        break;
    case LUA_TNUMBER:
        logger << ls.to_double(1);
        break;
    case LUA_TSTRING:
        logger << ls.to_string(1);
        break;
    case LUA_TNIL:
        logger << "nil";
        break;
    default:
        logger << ls.type_name(1) << ": " << ls.to_ptr<void>(1);
        break;
    }

    ls.top(top);
    logger.output(level);
}

int Sandbox::lw_print(lua_State *L)
{
    lua_output(L, ulib::LoggerConf::INFO);
    return 0;
}

int Sandbox::lw_warning(lua_State *L)
{
    lua_output(L, ulib::LoggerConf::WARN);
    return 0;
}

int Sandbox::lw_error(lua_State *L)
{
    lua_output(L, ulib::LoggerConf::ERROR);
    return 0;
}

int Sandbox::lw_debug(lua_State *L)
{
    lua_output(L, ulib::LoggerConf::DEBUG);
    return 0;
}

int Sandbox::lw_load(lua_State *L)
{
    ClosureLS ls(L, 1, "load", ClosureLS::CT_FUNCTION);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_closure(1) || !ls.check_args({ClosureLS::STRING})) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(2);
    std::string dir = ls.to_string(-1);
    std::string path = ls.to_string(1);

    if(path.length() == 0) {
        logger << ls.msg_prefix() << " empty path '" << path << "' "
            << ls.msg_postfix() << ulib::lerror;
        ls.push_nil();
        return 1;
    }

    if(path.at(0) != '/')
        path = dir + "/" + path;

    if(!ulib::System::file_readable(path)) {
        logger << ls.msg_prefix() << " could not open file '" << path << "' "
            << ls.msg_postfix() << ulib::lerror;
        ls.push_nil();
        return 1;
    }

    std::ifstream file(path);
    std::string content((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

    ls.push(content);
    return 1;
}

int Sandbox::lw_module(lua_State *L)
{
    ClosureLS ls(L, 1, "module", ClosureLS::CT_FUNCTION);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_closure(1) || !ls.check_args({ClosureLS::STRING})) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(2);
    std::string dir = ls.to_string(-1);

    ls.push_upvalue(3);
    LuaState &req_ls = ls.to_ref<LuaState>(-1);

    std::string path = ls.to_string(1);

    if(path.length() == 0) {
        logger << ls.msg_prefix() << " empty path '" << path << "' "
            << ls.msg_postfix() << ulib::lerror;
        ls.push_nil();
        return 1;
    }

    if(path.at(0) != '/')
        path = dir + "/" + path;

    if(!ls.load_file(path)) {
        logger << ls.to_string(-1) << ulib::lerror;
        ls.pop();
        ls.push_nil();
        return 1;
    }

    RequirementSandbox renv(req_ls, logger, dir);
    renv.push_environment();
    renv.set_upvalue(-2, 1);

    if(!renv.pcall(0, 1)) {
        logger << renv.to_string(-1) << ulib::lerror;
        renv.pop();
        renv.push_nil();
        return 1;
    }

    return 1;
}