#include "lua/GlobalEnvironment.hpp"
#include "plugins/DeviceTemplate.hpp"
#include <ulib/System.hpp>

const std::string GlobalEnvironment::INITIAL_CODE = R"CODE(

POINTER_ADDRESSED_REGISTRY = {}

-- this function is custom modification of code from this source:
--   https://stackoverflow.com/questions/9168058/how-to-dump-a-table-to-console

function dump(obj)
    if type(obj) ~= 'table' then
        return tostring(obj)
    end

    local result = '{ '
    for k, v in pairs(obj) do
        if type(k) ~= 'number' then
            k = '"' .. k .. '"'
        end
        result = result .. '['..k..'] = ' .. dump(v) .. ', '
    end
    return result .. '} '
end

-- this function is custom modification of code from this source:
--   https://www.lua.org/pil/16.3.html

function class(...)
    local c = {} -- new class
    local parents = {...}

    for i = 1, #parents do
        if type(parents[i]) ~= "table" then
            error("Cannot inherit from #" .. i .. " object.")
            return nil
        end
    end

    -- class will search for each method in the list of its parents
    setmetatable(c, {__index = function (t, k)
        for i = 1, #parents do
            local v = parents[i][k] -- try 'i'-th superclass
            if v then
                return v
            end
        end
    end})

    -- prepare 'c' to be the metatable of its instances
    c.__index = c
    c._new = c.new or function() end

    -- define a new constructor for this new class
    function c:new()
        local o = {}
        setmetatable(o, c)
        return o
    end

    return c -- return new class
end

function extend(p)
    local c = p
    c._new = c.new
    return c
end

math.randomseed(os.time())

)CODE";

std::string GlobalEnvironment::PROGRAM_DIR = ulib::System::program_dir();

void GlobalEnvironment::initialize(LuaState &ls)
{
    ls.open_libs();
    ls.run_script(INITIAL_CODE);
}

void GlobalEnvironment::load_tools(Sandbox &sb)
{
    sb.push_environment();

    sb.get_global("dump");
    sb.set_field(-2, "dump");

    sb.get_global("class");
    sb.set_field(-2, "class");

    sb.get_global("extend");
    sb.set_field(-2, "extend");

    sb.pop();
}

void GlobalEnvironment::load_globals(Sandbox &sb)
{
    sb.push_environment();

    sb.push(PROGRAM_DIR);
    sb.set_field(-2, "PROGRAM_DIR");

    sb.push(PROGRAM_DIR + "/plugins");
    sb.set_field(-2, "PLUGINS_DIR");

    sb.push(DeviceTemplate::Attribute::NUMBER);
    sb.set_field(-2, "NUMBER");
    sb.push(DeviceTemplate::Attribute::BOOLEAN);
    sb.set_field(-2, "BOOLEAN");
    sb.push(DeviceTemplate::Attribute::STRING);
    sb.set_field(-2, "STRING");
    sb.push(DeviceTemplate::Attribute::TIME);
    sb.set_field(-2, "TIME");
    sb.push(DeviceTemplate::Attribute::CONDITION);
    sb.set_field(-2, "CONDITION");

    sb.push(DeviceTemplate::Port::Direction::OUTPUT);
    sb.set_field(-2, "OUTPUT");
    sb.push(DeviceTemplate::Port::Direction::INPUT);
    sb.set_field(-2, "INPUT");

    sb.push(DeviceTemplate::Slot::Direction::DRIVER_OUT);
    sb.set_field(-2, "DRIVER_OUT");
    sb.push(DeviceTemplate::Slot::Direction::DRIVER_IN);
    sb.set_field(-2, "DRIVER_IN");

    sb.push(DeviceTemplate::Port::Mode::LOGICAL);
    sb.set_field(-2, "LOGICAL");
    sb.push(DeviceTemplate::Port::Mode::DISCRETE);
    sb.set_field(-2, "DISCRETE");

    sb.push(0);
    sb.set_field(-2, "LOW");
    sb.push(1);
    sb.set_field(-2, "HIGH");

    sb.pop();
}

// Pops value from stack and saves it on index <ptr>
void GlobalEnvironment::set(LuaState &ls, void *ptr)
{
    ls.get_global("POINTER_ADDRESSED_REGISTRY");
    ls.push(ptr);
    ls.copy(-3);
    ls.raw_set(-3);
    ls.pop(2);
}

// Pushes onto stack value on index <ptr>
void GlobalEnvironment::get(LuaState &ls, void *ptr)
{
    ls.get_global("POINTER_ADDRESSED_REGISTRY");
    ls.push(ptr);
    ls.raw_get(-2);
    ls.remove(-2);
}