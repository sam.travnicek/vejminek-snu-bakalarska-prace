#include "vejminek/Signal.hpp"
#include "lua/GlobalEnvironment.hpp"
#include "lua/ClosureLS.hpp"

Signal::Signal(
        Device &device, DeviceTemplate::Signal &signal_template, LuaState &ls,
        ulib::Logger &logger)
    : _device(device), _st(signal_template), _linked(false)
{
    ls.new_table();

    // Assign a new wrapping table to Device pointer in global table LUDW_TAB
    ls.copy(-1);
    GlobalEnvironment::set(ls, this);

    // Metatable for table wrapper
    ls.new_table();

	ls.push("__index"); // handle function for accessing fields
    ls.push(this);
    ls.push(&logger);
	ls.push(ilw__index, 2);
	ls.raw_set(-3);

	ls.push("__newindex"); // handle function for modifying fields
    ls.push(&logger);
	ls.push(ilw__newindex, 1);
	ls.raw_set(-3);

	ls.push("__metatable"); // prevent getting metatable (returns nil)
	ls.push_nil();
	ls.raw_set(-3);

    // Assign metatable to the instance
    ls.set_metatable(-2);

    // Pop wrapping table
    ls.pop();
}

Signal::~Signal()
{
}

void Signal::invoke()
{
    for(EventHandler *eh : _event_handlers)
        eh->signal_invoked(*this);
}

int Signal::ilw__index(lua_State* L)
{
    LuaState ls(L);

    ls.push_upvalue(1);
    Signal &signal = ls.to_ref<Signal>(-1);

    ls.push_upvalue(2);
    ulib::Logger &logger = ls.to_ref<ulib::Logger>(-1);
    std::string index = ls.to_string(2);

    if(index == "identifier") {
        ls.push(&signal);
        ls.push(&logger);
        ls.push(ilw_identifier, 2);
    } else if(index == "label") {
        ls.push(&signal);
        ls.push(&logger);
        ls.push(ilw_label, 2);
    } else if(index == "invoke") {
        ls.push(&signal);
        ls.push(&logger);
        ls.push(ilw_invoke, 2);
    } else if(index == "linked") {
        ls.push(&signal);
        ls.push(&logger);
        ls.push(ilw_linked, 2);
    } else
        ls.push_nil();

    return 1;
}

int Signal::ilw__newindex(lua_State *L)
{
    ClosureLS ls(L, 1, "__newindex", ClosureLS::CT_METHOD);

    if(!ls.check_protected_fields({
            "identifier", "label", "invoke", "linked" }))
        return 0;

    ls.error_protected_obj();
    return 0;
}

int Signal::ilw_identifier(lua_State *L)
{
    ClosureLS ls(L, 2, "identifier", ClosureLS::CT_METHOD);

    if(!ls.check_closure(0, ClosureLS::TABLE)) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    Signal &signal = ls.to_ref<Signal>(-1);

    ls.push(signal._st.identifier);
    return 1;
}

int Signal::ilw_label(lua_State *L)
{
    ClosureLS ls(L, 2, "label", ClosureLS::CT_METHOD);

    if(!ls.check_closure(0, ClosureLS::TABLE)) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    Signal &signal = ls.to_ref<Signal>(-1);

    ls.push(signal._st.label);
    return 1;
}

int Signal::ilw_invoke(lua_State *L)
{
    ClosureLS ls(L, 2, "invoke", ClosureLS::CT_METHOD);

    if(!ls.check_object(ClosureLS::TABLE))
        return 0;

    ls.push_upvalue(1);
    Signal &signal = ls.to_ref<Signal>(-1);

    signal.invoke();
    return 0;
}

int Signal::ilw_linked(lua_State *L)
{
    ClosureLS ls(L, 2, "linked", ClosureLS::CT_METHOD);

    if(!ls.check_closure(0, ClosureLS::TABLE)) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    Signal &signal = ls.to_ref<Signal>(-1);

    ls.push(signal._linked);
    return 1;
}