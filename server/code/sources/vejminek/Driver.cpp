#include "vejminek/Driver.hpp"
#include "vejminek/Core.hpp"

Driver::Driver(const std::string &identifier, const ulib::LoggerConf &logger_cfg)
    : _logger("Driver["+identifier+"]", logger_cfg), _identifier(identifier), _label(identifier)
{
}

bool Driver::link_port(const std::string &identifier, Port &port, const std::string &preferred_cfg)
{
    int idx = -1;
    for(size_t i = 0; i < _ports.size(); ++i) {
        if(_ports[i].identifier == identifier) {
            idx = i;
            break;
        }
    }

    if(idx == -1) {
        _logger << "Attempt to link port '" << port.identifier() << "' to non-existent driver port '"
            << identifier << "'." << ulib::lerror;
        return false;
    }

    PortDescriptor &pd = _ports[idx];
    const DeviceTemplate::Port &port_template = port.port_template();

    bool match_found = false;

    if(preferred_cfg.length() > 0) {
        bool preferred_found = false;

        for(PortConfiguration &cfg : pd.configurations) {
            if(cfg.identifier != preferred_cfg)
                continue;

            if(cfg.direction == port_template.direction && cfg.mode == port_template.mode) {
                match_found = true;
                preferred_found = true;
                driver_port_linked(identifier, cfg.direction, cfg.mode, cfg.identifier);
                port.link(&cfg);
                _logger << "Port '" << port.identifier()
                    << "' linked to driver port configuration '"
                    << cfg.identifier << "'." << ulib::ldebug;
                break;
            } else {
                _logger << "Preferred driver port configuration '" << preferred_cfg << "'"
                    " incompatible with port '" << port.identifier()
                    << "'. Looking for alternatives..." << ulib::lwarn;
                preferred_found = true;
            }
        }

        if(!preferred_found)
            _logger << "Preferred driver port configuration '" << preferred_cfg << "' not found"
                " for port '"<< port.identifier() << "'. Looking for alternatives..."
                << ulib::lwarn;
    }

    if(!match_found) {
        for(PortConfiguration &cfg : pd.configurations) {
            if(cfg.direction == port_template.direction && cfg.mode == port_template.mode) {
                match_found = true;
                driver_port_linked(identifier, cfg.direction, cfg.mode, cfg.identifier);
                port.link(&cfg);
                _logger << "Port '" << port.identifier()
                    << "' linked to driver port configuration '"
                    << cfg.identifier << "'." << ulib::ldebug;
                break;
            }
        }
    }

    if(!match_found) {
        _logger << "Attempt to link port '" << port.identifier()
            << "' to incompatible driver port '" << identifier << "'." << ulib::lerror;
        return false;
    }

    if(port_template.direction == DeviceTemplate::Port::INPUT) {

        if(_input_ports_links.find(identifier) == _input_ports_links.end())
            _input_ports_links[identifier] = std::vector<Port*>();

        std::vector<Port*>& ports = _input_ports_links[identifier];

        if(std::find(ports.begin(), ports.end(), &port) == ports.end())
            ports.push_back(&port);

    } else if(port_template.direction == DeviceTemplate::Port::OUTPUT) {

        if(_output_ports_links.find(&port) == _output_ports_links.end()) {
            _output_ports_links[&port] = std::vector<std::string>();
            port.event_handler(*this);
        }

        std::vector<std::string>& dports = _output_ports_links[&port];

        if(std::find(dports.begin(), dports.end(), identifier) == dports.end())
            dports.push_back(identifier);

    }

    return true;
}

void Driver::port_changed(Port &port, double previous_value)
{
    if(_output_ports_links.find(&port) == _output_ports_links.end())
        return;

    for(std::string &identifier : _output_ports_links[&port])
        driver_port_written(identifier, port.value());
}

void Driver::write_port(const std::string &identifier, double value)
{
    if(_input_ports_links.find(identifier) == _input_ports_links.end())
        return;

    for(Port *port : _input_ports_links[identifier])
        port->write(value);
}

bool Driver::link_slot(const std::string &identifier, Slot &slot)
{
    int idx = -1;
    for(size_t i = 0; i < _slots.size(); ++i) {
        if(_slots[i].identifier == identifier) {
            idx = i;
            break;
        }
    }

    if(idx == -1) {
        _logger << "Attempt to link slot '" << slot.identifier()
            << "' to non-existent driver slot '" << identifier << "'." << ulib::lerror;
        return false;
    }

    SlotDescriptor &dslot = _slots[idx];

    if(dslot.type != slot.type()) {
        _logger << "Attempt to link slot '" << slot.identifier()
            << "' to incompatible driver slot '" << identifier << "'." << ulib::lerror;
        return false;
    }

    driver_slot_linked(identifier);
    slot.link();

    _logger << "Slot '" << slot.identifier()
        << "' linked to driver slot '" << identifier << "'." << ulib::ldebug;

    DeviceTemplate::Slot slot_template = slot.slot_template();

    if(slot_template.direction == DeviceTemplate::Slot::DRIVER_OUT) {

        if(_drout_slots_links.find(identifier) == _drout_slots_links.end())
            _drout_slots_links[identifier] = std::vector<Slot*>();

        std::vector<Slot*>& slots = _drout_slots_links[identifier];

        if(std::find(slots.begin(), slots.end(), &slot) == slots.end())
            slots.push_back(&slot);

    } else if(slot_template.direction == DeviceTemplate::Slot::DRIVER_IN) {

        if(_drin_slots_links.find(&slot) == _drin_slots_links.end()) {
            _drin_slots_links[&slot] = std::vector<std::string>();
            slot.event_handler(*this);
        }

        std::vector<std::string>& dslots = _drin_slots_links[&slot];

        if(std::find(dslots.begin(), dslots.end(), identifier) == dslots.end())
            dslots.push_back(identifier);

    }

    return true;
}

void Driver::slot_written(Slot &slot)
{
    if(_drin_slots_links.find(&slot) == _drin_slots_links.end())
        return;

    for(std::string &identifier : _drin_slots_links[&slot])
        driver_slot_written(identifier, slot.value(), slot.type());
}

void Driver::write_slot(const std::string &identifier, double value)
{
    if(_drout_slots_links.find(identifier) == _drout_slots_links.end())
        return;

    for(Slot *slot : _drout_slots_links[identifier])
        slot->write(value);
}

void Driver::write_slot(const std::string &identifier, bool value)
{
    if(_drout_slots_links.find(identifier) == _drout_slots_links.end())
        return;

    for(Slot *slot : _drout_slots_links[identifier])
        slot->write(value);
}

void Driver::write_slot(const std::string &identifier, const std::string &value)
{
    if(_drout_slots_links.find(identifier) == _drout_slots_links.end())
        return;

    for(Slot *slot : _drout_slots_links[identifier])
        slot->write(value);
}

bool Driver::link_signal(const std::string &identifier, Signal &signal)
{
    int idx = -1;
    for(size_t i = 0; i < _signals.size(); ++i) {
        if(_signals[i].identifier == identifier) {
            idx = i;
            break;
        }
    }

    if(idx == -1) {
        _logger << "Attempt to link signal '" << signal.identifier()
            << "' to non-existent driver signal '" << identifier << "'." << ulib::lerror;
        return false;
    }

    driver_signal_linked(identifier);
    signal.link();

    _logger << "Signal '" << signal.identifier()
        << "' linked to driver signal '" << identifier << "'." << ulib::ldebug;

    DeviceTemplate::Signal signal_template = signal.signal_template();

    if(signal_template.direction == DeviceTemplate::Signal::DRIVER_OUT) {

        if(_drout_signals_links.find(identifier) == _drout_signals_links.end())
            _drout_signals_links[identifier] = std::vector<Signal*>();

        std::vector<Signal*>& signals = _drout_signals_links[identifier];

        if(std::find(signals.begin(), signals.end(), &signal) == signals.end())
            signals.push_back(&signal);

    } else if(signal_template.direction == DeviceTemplate::Signal::DRIVER_IN) {

        if(_drin_signals_links.find(&signal) == _drin_signals_links.end()) {
            _drin_signals_links[&signal] = std::vector<std::string>();
            signal.event_handler(*this);
        }

        std::vector<std::string>& dsignals = _drin_signals_links[&signal];

        if(std::find(dsignals.begin(), dsignals.end(), identifier) == dsignals.end())
            dsignals.push_back(identifier);

    }

    return true;
}

void Driver::signal_invoked(Signal &signal)
{
    if(_drin_signals_links.find(&signal) == _drin_signals_links.end())
        return;

    for(std::string &identifier : _drin_signals_links[&signal])
        driver_signal_invoked(identifier);
}

void Driver::invoke_signal(const std::string &identifier)
{
    if(_drout_signals_links.find(identifier) == _drout_signals_links.end())
        return;

    for(Signal *signal : _drout_signals_links[identifier])
        signal->invoke();
}

void Driver::port(const PortDescriptor &pd)
{
    for(size_t i = 0; i < _ports.size(); ++i) {
        if(_ports[i].identifier == pd.identifier) {
            _ports[i] = pd;
            return;
        }
    }
    _ports.push_back(pd);
}

void Driver::slot(const SlotDescriptor &sd)
{
    for(size_t i = 0; i < _slots.size(); ++i) {
        if(_slots[i].identifier == sd.identifier) {
            _slots[i] = sd;
            _slot_types[sd.identifier] = static_cast<DeviceTemplate::Slot::Type>(sd.type);
            return;
        }
    }
    _slots.push_back(sd);
    _slot_types[sd.identifier] = static_cast<DeviceTemplate::Slot::Type>(sd.type);;
}

void Driver::signal(const SignalDescriptor &sd)
{
    for(size_t i = 0; i < _signals.size(); ++i) {
        if(_signals[i].identifier == sd.identifier) {
            _signals[i] = sd;
            return;
        }
    }
    _signals.push_back(sd);
}