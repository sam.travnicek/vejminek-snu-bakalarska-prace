#include "vejminek/ACondition.hpp"

ACondition::ACondition(
        DeviceTemplate::Attribute &attr_templ, Sandbox &sb,
        ulib::Logger &logger)
    : Attribute(attr_templ), _sb(sb), _logger(logger), _script_reference(0),
      _output(false)
{
}

bool ACondition::script(const std::string &code)
{
    _loaded = _sb.load_script(code);

    if(!_loaded) {
        std::string msg = _sb.to_string(-1);
        for(uint i = 0; i < msg.length(); ++i) {
            if(msg.at(i) == ']') {
                msg = "<Condition> " + msg.substr(i+1);
                break;
            }
        }
        _logger << msg << ulib::lerror;
        _sb.pop();
        return false;
    }

    _script_reference = _sb.ref(LUA_REGISTRYINDEX);

    return true;
}

bool ACondition::run()
{
    if(!_loaded)
        return false;

    _sb.push(_script_reference);
    _sb.raw_get(LUA_REGISTRYINDEX);

    _sb.push_environment();
    _sb.set_upvalue(-2, 1);

    if(!_sb.pcall(0, 1)) {
        std::string msg = _sb.to_string(-1);
        for(uint i = 0; i < msg.length(); ++i) {
            if(msg.at(i) == ']') {
                msg = "<Condition>" + msg.substr(i+1);
                break;
            }
        }
        _logger << msg << ulib::lerror;
        _sb.pop();
        return false;
    }

    if(_sb.is_bool(-1))
        _output = _sb.to_bool(-1);
    else
        _output = false;

    _sb.pop();
    return true;
}