#include "vejminek/Slot.hpp"
#include "lua/GlobalEnvironment.hpp"
#include "lua/ClosureLS.hpp"

Slot::Slot(
        Device &device, DeviceTemplate::Slot &slot_template, LuaState &ls,
        ulib::Logger &logger)
    : _device(device), _st(slot_template), _linked(false)
{
    switch (slot_template.type) {
    case DeviceTemplate::Slot::NUMBER:
        _value = std::make_any<double>(0);
        break;
    case DeviceTemplate::Slot::BOOLEAN:
        _value = std::make_any<bool>(false);
        break;
    case DeviceTemplate::Slot::STRING:
        _value = std::make_any<std::string>("");
        break;
    default:
        break;
    }

    ls.new_table();

    // Assign a new wrapping table to Device pointer in global table LUDW_TAB
    ls.copy(-1);
    GlobalEnvironment::set(ls, this);

    // Metatable for table wrapper
    ls.new_table();

	ls.push("__index"); // handle function for accessing fields
    ls.push(this);
    ls.push(&logger);
	ls.push(ilw__index, 2);
	ls.raw_set(-3);

	ls.push("__newindex"); // handle function for modifying fields
    ls.push(&logger);
	ls.push(ilw__newindex, 1);
	ls.raw_set(-3);

	ls.push("__metatable"); // prevent getting metatable (returns nil)
	ls.push_nil();
	ls.raw_set(-3);

    // Assign metatable to the instance
    ls.set_metatable(-2);

    // Pop wrapping table
    ls.pop();
}

Slot::~Slot()
{
}

bool Slot::write(double value)
{
    if(_st.type != DeviceTemplate::Slot::NUMBER)
        return false;
    _value = std::make_any<double>(value);

    for(EventHandler *eh : _event_handlers)
        eh->slot_written(*this);

    return true;
}

bool Slot::write(bool value)
{
    if(_st.type != DeviceTemplate::Slot::BOOLEAN)
        return false;
    _value = std::make_any<bool>(value);

    for(EventHandler *eh : _event_handlers)
        eh->slot_written(*this);

    return true;
}

bool Slot::write(const std::string &value)
{
    if(_st.type != DeviceTemplate::Slot::STRING)
        return false;
    _value = std::make_any<std::string>(value);

    for(EventHandler *eh : _event_handlers)
        eh->slot_written(*this);

    return true;
}

int Slot::ilw__index(lua_State* L)
{
    LuaState ls(L);

    ls.push_upvalue(1);
    Slot &slot = ls.to_ref<Slot>(-1);

    ls.push_upvalue(2);
    ulib::Logger &logger = ls.to_ref<ulib::Logger>(-1);
    std::string index = ls.to_string(2);

    if(index == "identifier") {
        ls.push(&slot);
        ls.push(&logger);
        ls.push(ilw_identifier, 2);
    } else if(index == "label") {
        ls.push(&slot);
        ls.push(&logger);
        ls.push(ilw_label, 2);
    } else if(index == "write") {
        ls.push(&slot);
        ls.push(&logger);
        ls.push(ilw_write, 2);
    } else if(index == "read") {
        ls.push(&slot);
        ls.push(&logger);
        ls.push(ilw_read, 2);
    } else if(index == "type") {
        ls.push(&slot);
        ls.push(&logger);
        ls.push(ilw_type, 2);
    } else if(index == "linked") {
        ls.push(&slot);
        ls.push(&logger);
        ls.push(ilw_linked, 2);
    } else
        ls.push_nil();

    return 1;
}

int Slot::ilw__newindex(lua_State *L)
{
    ClosureLS ls(L, 1, "__newindex", ClosureLS::CT_METHOD);

    if(!ls.check_protected_fields({
            "identifier", "label", "write", "read", "type", "linked" }))
        return 0;

    ls.error_protected_obj();
    return 0;
}

int Slot::ilw_identifier(lua_State *L)
{
    ClosureLS ls(L, 2, "identifier", ClosureLS::CT_METHOD);

    if(!ls.check_closure(0, ClosureLS::TABLE)) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    Slot &slot = ls.to_ref<Slot>(-1);

    ls.push(slot._st.identifier);
    return 1;
}

int Slot::ilw_label(lua_State *L)
{
    ClosureLS ls(L, 2, "label", ClosureLS::CT_METHOD);

    if(!ls.check_closure(0, ClosureLS::TABLE)) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    Slot &slot = ls.to_ref<Slot>(-1);

    ls.push(slot._st.label);
    return 1;
}

int Slot::ilw_write(lua_State *L)
{
    ClosureLS ls(L, 2, "write", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_object(ClosureLS::TABLE))
        return 0;

    ls.argument_list({ClosureLS::INT});
    ls.argument_list({ClosureLS::DOUBLE});
    ls.argument_list({ClosureLS::BOOL});
    ls.argument_list({ClosureLS::STRING});

    int argument_list = ls.check_arguments();
    if(argument_list < 0)
        return 0;

    ls.push_upvalue(1);
    Slot &slot = ls.to_ref<Slot>(-1);

    if(argument_list == 0 || argument_list == 1) {
        if(slot.type() != DeviceTemplate::Slot::NUMBER) {
            logger << ls.msg_prefix() << " attempt to write invalid value to slot '"
                << slot.identifier() << "', type 'NUMBER' expected " << ls.msg_postfix()
                << ulib::lerror;
            return 0;
        }
        slot.write(ls.to_double(2));
    } else if(argument_list == 2) {
        if(slot.type() != DeviceTemplate::Slot::BOOLEAN) {
            logger << ls.msg_prefix() << " attempt to write invalid value to slot '"
                << slot.identifier() << "', type 'BOOLEAN' expected " << ls.msg_postfix()
                << ulib::lerror;
            return 0;
        }
        slot.write(ls.to_bool(2));
    } else {
        if(slot.type() != DeviceTemplate::Slot::STRING) {
            logger << ls.msg_prefix() << " attempt to write invalid value to slot '"
                << slot.identifier() << "', type 'STRING' expected " << ls.msg_postfix()
                << ulib::lerror;
            return 0;
        }
        slot.write(ls.to_string(2));
    }

    return 0;
}

int Slot::ilw_read(lua_State *L)
{
    ClosureLS ls(L, 2, "read", ClosureLS::CT_METHOD);

    if(!ls.check_closure(0, ClosureLS::TABLE)) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    Slot &slot = ls.to_ref<Slot>(-1);

    if(slot.type() == DeviceTemplate::Slot::NUMBER)
        ls.push(std::any_cast<double>(slot.value()));
    else if(slot.type() == DeviceTemplate::Slot::BOOLEAN)
        ls.push(std::any_cast<bool>(slot.value()));
    else if(slot.type() == DeviceTemplate::Slot::STRING)
        ls.push(std::any_cast<std::string>(slot.value()));

    return 1;
}

int Slot::ilw_type(lua_State *L)
{
    ClosureLS ls(L, 2, "type", ClosureLS::CT_METHOD);

    if(!ls.check_closure(0, ClosureLS::TABLE)) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    Slot &slot = ls.to_ref<Slot>(-1);

    ls.push(slot.type());
    return 1;
}

int Slot::ilw_linked(lua_State *L)
{
    ClosureLS ls(L, 2, "linked", ClosureLS::CT_METHOD);

    if(!ls.check_closure(0, ClosureLS::TABLE)) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    Slot &slot = ls.to_ref<Slot>(-1);

    ls.push(slot._linked);
    return 1;
}