#include "vejminek/Core.hpp"
#include "vejminek/Room.hpp"
#include "lua/LinkageSandbox.hpp"
#include "lua/GlobalEnvironment.hpp"
#include <ulib-net/MySQL.hpp>
#include <nlohmann/json.hpp>
#include "Database.hpp"
#include <thread>
#include <algorithm>
#include "structures/compound_identifier.hpp"
#include "Program.hpp"

//TODO: move SQL queries to 'Database' class

const char *SQL_SELECT_ROOMS = R"SQL(
    SELECT
        `id`, `label`, `floor`
    FROM `rooms`;
)SQL";

const char *SQL_SELECT_DEVICES = R"SQL(
    SELECT
        `id`, `room_id`, `label`, `description`, `template`, `data`
    FROM `devices`;
)SQL";

const char *SQL_SELECT_PORTS_LINKS = R"SQL(
    SELECT
        `id`, `driver`, `driver_port`, `device_id`, `device_port`,
        `preferred_cfg`
    FROM `ports_links`;
)SQL";

const char *SQL_SELECT_SLOTS_LINKS = R"SQL(
    SELECT
        `id`, `driver`, `driver_slot`, `device_id`, `device_slot`
    FROM `slots_links`;
)SQL";

const char *SQL_SELECT_SIGNALS_LINKS = R"SQL(
    SELECT
        `id`, `driver`, `driver_signal`, `device_id`, `device_signal`
    FROM `signals_links`;
)SQL";

bool Core::identifier_valid(const std::string &identifier)
{
    if(identifier.length() == 0 || identifier.length() > IDENTIFIER_MAX_LENGTH)
        return false;

    for(char c : identifier) {
        bool valid =
            c == '.' ||
            c == '_' ||
            (c >= '0' && c <= '9') ||
            (c >= 'a' && c <= 'z') ||
            (c >= 'A' && c <= 'Z');
        if(!valid)
            return false;
    }
    return true;
}

Core::Core(const std::string &plugins_dir, const std::string &testbench_dir,
        ulib::LoggerConf &logger_cfg, ulib::net::MySQL &db,
        const std::vector<channel_descriptor_t> &channel_descriptors)
    : _logger("Core", logger_cfg),
      _devices_logger("Core", logger_cfg),
      _channels_logger("Core", logger_cfg),
      _db(db),
      _channel_descriptors(channel_descriptors)
{
    if(!Program::DEBUG_CHANNELS)
        _channels_logger.configuration().print_level(ulib::LoggerConf::INFO);

    if(!Program::DEBUG_DEVICES)
        _devices_logger.configuration().print_level(ulib::LoggerConf::INFO);

    _ls = new LuaState();
    GlobalEnvironment::initialize(*_ls);

    _logger << "Cleaning up the database..." << ulib::linfo;
    Database::clear_tmps(db);

    _cond_env = new ConditionSandbox(*_ls, *this, _logger);
    _scheduler_serv = new SchedulerService(*_ls, logger_cfg);
    _devices_serv = new DevicesService(*_ls, logger_cfg, *this);
    _plugins = new Plugins(*_ls, plugins_dir, logger_cfg, *_scheduler_serv, db);
    _plugins->init();

    _load_rooms();
    _load_devices();
    _load_ports_links();
    _load_slots_links();
    _load_signals_links();
    _load_testbench(testbench_dir);
    _open_channels();
}

Core::~Core()
{
    // First, all objects in LUA must be garbage collected, then Plugins can be deleted.
    // The reason is Plugins are Device Template Managers and must be present when Device Templates
    //  are being garbage collected.

    for(auto it = _devices.begin(); it != _devices.end(); ++it)
        delete it->second;

    for(auto it = _rooms.begin(); it != _rooms.end(); ++it)
        delete it->second;

    delete _cond_env;
    delete _ls;

    delete _scheduler_serv;
    delete _devices_serv;
    delete _plugins;
}

void Core::tick(int64_t time, ulong ticks, ulong tps)
{
    std::lock_guard<std::mutex> lock_mutex(_uni_mutex);

    _plugins->tick(time, ticks, tps);
    _scheduler_serv->tick(time, ticks, tps);

    for(auto it = _devices.begin(); it != _devices.end(); ++it)
        it->second->tick(time, ticks, tps);

    if(ticks % (30*tps) == 0) { // Save port values every 30 seconds
        std::unique_ptr<sql::Connection> con(_db.connect());
        for(Port *port : _ports_to_save) {
            Database::save_port_value(con, *port);
            //TODO: pass parameters by value to make it thread safe
            //TODO: remove join call and make thread class member
            // std::thread db_thread([&] {
            //     std::unique_ptr<sql::Connection> con(_db.connect());
            //     Database::save_port_value(con, *port);
            // });
            // db_thread.join();
        }
        if(_ports_to_save.size() == 1)
            _logger << "1 port saved to the database." << ulib::ldebug;
        else if(_ports_to_save.size() > 1)
            _logger << _ports_to_save.size() << " ports saved to the database." << ulib::ldebug;
        _ports_to_save.clear();
    }
}

void Core::port_changed(Port &port, double previous_value)
{
    // Ports that store their values in the database are handled here
    // (registered in _load_devices())

    auto lookup = std::find(_ports_to_save.begin(), _ports_to_save.end(), &port);

    if(lookup == _ports_to_save.end())
        _ports_to_save.push_back(&port);
}

Device* Core::device(uint id)
{
    if(_devices.find(id) == _devices.end())
        return nullptr;
    return _devices[id];
}

Device* Core::device(const std::string &label)
{
    for(auto it = _devices.begin(); it != _devices.end(); ++it)
        if(it->second->label() == label)
            return it->second;
    return nullptr;
}

void Core::_load_rooms()
{
    std::unique_ptr<sql::Connection> con(_db.connect());
    std::unique_ptr<sql::Statement> stmt;
    std::unique_ptr<sql::ResultSet> rs;

    stmt.reset(con->createStatement());
    rs.reset(stmt->executeQuery(SQL_SELECT_ROOMS));

    while(rs->next()) {
        uint id = rs->getUInt("id");
        std::string label = rs->getString("label");
        double floor = rs->getDouble("floor");

        _rooms[id] = new Room(label, floor);
        _logger << "Room loaded: #" << id << " '" << label << "'" << ulib::ldebug;
    }
}

void Core::_load_devices()
{
    std::unique_ptr<sql::Connection> con(_db.connect());
    std::unique_ptr<sql::Statement> stmt;
    std::unique_ptr<sql::ResultSet> rs;

    stmt.reset(con->createStatement());
    rs.reset(stmt->executeQuery(SQL_SELECT_DEVICES));

    while(rs->next()) {
        uint id = rs->getUInt("id");

        std::string temp_str = rs->getString("template");
        std::string plugin_identifier = "";
        std::string dt_identifier = "";
        bool pl_sect = true;

        for(uint i = 0; i < temp_str.length(); ++i) {
            char c = temp_str.at(i);
            if(c == ':') {
                pl_sect = false;
                continue;
            }
            if(pl_sect) {
                plugin_identifier += c;
            } else {
                dt_identifier += c;
            }
        }

        DeviceTemplate *dt = _plugins->find_template(plugin_identifier, dt_identifier);
        if(!dt) {
            _logger << "Invalid device #" << id << " found in the database: template '"
                << temp_str << "' not found" << ulib::lerror;
            continue;
        }

        uint room_id = rs->getUInt("room_id");
        if(_rooms.find(room_id) == _rooms.end()) {
            _logger << "Invalid device #" << id << " found in the database: room #"
                << room_id << " not found" << ulib::lerror;
            continue;
        }

        std::string label = rs->getString("label");
        std::string decription = rs->getString("description");
        std::string data = rs->getString("data");

        try {
            nlohmann::json jdata;
            if(data.size() > 0)
                jdata = nlohmann::json::parse(data);
            Device *dev = new Device(
                id, label, decription, *dt, *_ls, *_cond_env, _devices_logger, *_rooms[room_id], jdata);
            _devices[id] = dev;
            _logger << "Device loaded: #" << id << " '" << label << "'" << ulib::ldebug;
        } catch(...) {
            _logger << "Could not parse data of device #" << id << "." << ulib::lerror;
            continue;
        }
    }

    // Load last port values and register as EventHandler to ports storing their values in database

    for(auto it = _devices.begin(); it != _devices.end(); ++it) {
        Device *dev = it->second;
        for(auto pit = dev->ports().begin(); pit != dev->ports().end(); ++pit) {
            Port *port = pit->second;
            if(port->port_template().value_stored) {
                port->write(Database::last_port_value(con, *port));
                port->event_handler(*this);
            }
        }
    }

    // Clear all last port values and reinsert only ones of existing ports

    Database::clear_port_values(con);

    for(auto it = _devices.begin(); it != _devices.end(); ++it) {
        Device *dev = it->second;
        for(auto pit = dev->ports().begin(); pit != dev->ports().end(); ++pit) {
            Port *port = pit->second;
            if(port->port_template().value_stored)
                Database::insert_port_value(con, *port);
        }
    }
}

void Core::_load_ports_links()
{
    std::unique_ptr<sql::Connection> con(_db.connect());
    std::unique_ptr<sql::Statement> stmt;
    std::unique_ptr<sql::ResultSet> rs;

    stmt.reset(con->createStatement());
    rs.reset(stmt->executeQuery(SQL_SELECT_PORTS_LINKS));

    while(rs->next()) {
        uint id = rs->getUInt("id");
        std::string driver_str = rs->getString("driver");
        compound_identifier_t driver_id = compound_identifier_t::parse(driver_str);
        std::string driver_port = rs->getString("driver_port");
        uint device_id = rs->getUInt("device_id");
        std::string device_port = rs->getString("device_port");
        std::string preferred_cfg = rs->getString("preferred_cfg");

        if(driver_port.length() == 0)
            continue;

        Driver *driver = _plugins->find_driver(driver_id.left, driver_id.right);
        if(!driver) {
            _logger << "Invalid ports' link #" << id << " found in the database: driver '"
                << driver_str << "' not found" << ulib::lerror;
            continue;
        }

        if(_devices.find(device_id) == _devices.end()) {
            _logger << "Invalid ports' link #" << id << " found in the database: device #"
                << device_id << " not found" << ulib::lerror;
            continue;
        }

        Device *device = _devices[device_id];
        Port *port = device->port(device_port);

        if(!port) {
            _logger << "Invalid ports' link #" << id << " found in the database: port '"
                << device_port << "' not found on device #" << device_id << ulib::lerror;
            continue;
        }

        driver->link_port(driver_port, *port, preferred_cfg);
    }

    _plugins->ports_linkage_complete();
}

void Core::_load_slots_links()
{
    std::unique_ptr<sql::Connection> con(_db.connect());
    std::unique_ptr<sql::Statement> stmt;
    std::unique_ptr<sql::ResultSet> rs;

    stmt.reset(con->createStatement());
    rs.reset(stmt->executeQuery(SQL_SELECT_SLOTS_LINKS));

    while(rs->next()) {
        uint id = rs->getUInt("id");
        std::string driver_str = rs->getString("driver");
        compound_identifier_t driver_id = compound_identifier_t::parse(driver_str);
        std::string driver_slot = rs->getString("driver_slot");
        uint device_id = rs->getUInt("device_id");
        std::string device_slot = rs->getString("device_slot");

        if(driver_slot.length() == 0)
            continue;

        Driver *driver = _plugins->find_driver(driver_id.left, driver_id.right);
        if(!driver) {
            _logger << "Invalid slots' link #" << id << " found in the database: driver '"
                << driver_str << "' not found" << ulib::lerror;
            continue;
        }

        if(_devices.find(device_id) == _devices.end()) {
            _logger << "Invalid slots' link #" << id << " found in the database: device #"
                << device_id << " not found" << ulib::lerror;
            continue;
        }

        Device *device = _devices[device_id];
        Slot *slot = device->slot(device_slot);

        if(!slot) {
            _logger << "Invalid slots' link #" << id << " found in the database: slot '"
                << device_slot << "' not found on device #" << device_id << ulib::lerror;
            continue;
        }

        driver->link_slot(driver_slot, *slot);
    }

    _plugins->slots_linkage_complete();
}

void Core::_load_signals_links()
{
    std::unique_ptr<sql::Connection> con(_db.connect());
    std::unique_ptr<sql::Statement> stmt;
    std::unique_ptr<sql::ResultSet> rs;

    stmt.reset(con->createStatement());
    rs.reset(stmt->executeQuery(SQL_SELECT_SIGNALS_LINKS));

    while(rs->next()) {
        uint id = rs->getUInt("id");
        std::string driver_str = rs->getString("driver");
        compound_identifier_t driver_id = compound_identifier_t::parse(driver_str);
        std::string driver_signal = rs->getString("driver_signal");
        uint device_id = rs->getUInt("device_id");
        std::string device_signal = rs->getString("device_signal");

        if(driver_signal.length() == 0)
            continue;

        Driver *driver = _plugins->find_driver(driver_id.left, driver_id.right);
        if(!driver) {
            _logger << "Invalid signals' link #" << id << " found in the database: driver '"
                << driver_str << "' not found" << ulib::lerror;
            continue;
        }

        if(_devices.find(device_id) == _devices.end()) {
            _logger << "Invalid signals' link #" << id << " found in the database: device #"
                << device_id << " not found" << ulib::lerror;
            continue;
        }

        Device *device = _devices[device_id];
        Signal *signal = device->signal(device_signal);

        if(!signal) {
            _logger << "Invalid signals' link #" << id << " found in the database: signal '"
                << device_signal << "' not found on device #" << device_id << ulib::lerror;
            continue;
        }

        driver->link_signal(driver_signal, *signal);
    }

    _plugins->signals_linkage_complete();
}

void Core::_load_testbench(const std::string &dir)
{
    LinkageSandbox link_sb(*_ls, *this, *_scheduler_serv, *_devices_serv, _logger);

    if(!link_sb.environment_file(dir + "/test_bench.lua")) {
        _logger << link_sb.to_string(-1) << ulib::lerror;
        link_sb.pop();
    }
}

void Core::_open_channels()
{
    for(const channel_descriptor_t &desc : _channel_descriptors) {
        if(desc.type == channel_descriptor_t::TCP) {
            std::shared_ptr<allkol::TCPChannel> channel
                = std::make_shared<allkol::TCPChannel>(desc.max_clients, *this, _channels_logger);
            channel->open(desc.ip_address, desc.port);
            _logger << "Channel [#" << channel->id() << ", TCP] opened: "
                << desc.ip_address << ":" << desc.port << ulib::linfo;
            _channels.push_back(std::move(channel));
        } else if(desc.type == channel_descriptor_t::TLS) {
            std::shared_ptr<allkol::TLSChannel> channel
                = std::make_shared<allkol::TLSChannel>(desc.max_clients, *this, _channels_logger);
            channel->key_path(desc.key_path);
            channel->cert_path(desc.cert_path);
            channel->open(desc.ip_address, desc.port);
            _logger << "Channel [#" << channel->id() << ", TLS] opened: "
                << desc.ip_address << ":" << desc.port << ulib::linfo;
            _channels.push_back(std::move(channel));
        } else if(desc.type == channel_descriptor_t::WS) {
            std::shared_ptr<allkol::WSChannel> channel
                = std::make_shared<allkol::WSChannel>(desc.max_clients, *this, _channels_logger);
            channel->open(desc.ip_address, desc.port);
            _logger << "Channel [#" << channel->id() << ", WS] opened: "
                << desc.ip_address << ":" << desc.port << ulib::linfo;
            _channels.push_back(std::move(channel));
        } else if(desc.type == channel_descriptor_t::WSS) {
            //TODO: implement WSS type
            _logger << "We are sorry. WSS channel is not fully supported yet." << ulib::lwarn;
        }
    }
}

bool Core::_channel_async(uint id)
{
    std::lock_guard<std::mutex> lock_descs(_channel_descriptors_mutex);
    if(id >= _channel_descriptors.size())
        return false;
    const channel_descriptor_t &desc = _channel_descriptors.at(id);
    return desc.type == channel_descriptor_t::WS || desc.type == channel_descriptor_t::WSS;
}

bool Core::_uuid_allowed(uint channel_id, const std::string &uuid)
{
    std::lock_guard<std::mutex> lock_descs(_channel_descriptors_mutex);
    if(channel_id >= _channel_descriptors.size())
        return false;
    const channel_descriptor_t &desc = _channel_descriptors.at(channel_id);
    if(desc.clients_checking == channel_descriptor_t::NONE)
        return true;
    for(const std::string &cuuid : desc.uuids)
        if(cuuid == uuid)
            return desc.clients_checking == channel_descriptor_t::WHITELIST;
    return desc.clients_checking == channel_descriptor_t::BLACKLIST;
}

void Core::on_message(uint channel_id, allkol::Client &client, allkol::Message &message)
{
    _channels_logger << "[CH " << channel_id << "] Message received from '"
        << client.uuid() << "'@" << client.ip_address() << ": "
        << message.serialize() << ulib::ldebug;

    bool channel_async = _channel_async(channel_id);
    if(channel_async)
        _uni_mutex.lock();

    std::string cmd = message.command();
    nlohmann::json &jdata = message.data();

    if(cmd == "authorize") {
        allkol::Answer answer(message.identifier(), cmd);
        if(jdata.find("uuid") == jdata.end())
            answer.error("Missing field 'uuid'.");
        else {
            nlohmann::json &juuid = jdata["uuid"];
            if(!juuid.is_string())
                answer.error("Invalid field 'uuid'.");
            else {
                std::string uuid = juuid;
                client.authorised(uuid);
                _channels_logger << "Client '" << client.uuid() << "'@" << client.ip_address()
                    << " authorized." << ulib::ldebug;
            }
        }
        client.send(answer);
    } else if(cmd == "ping") {
        allkol::Answer answer(message.identifier(), "pong");
        answer.ok();
        client.send(answer);
    } else {
        allkol::Answer answer(message.identifier(), cmd);
        if(!client.authorised() || !_uuid_allowed(channel_id, client.uuid()))
            answer.error("Permission denied.");
        else if(!_handle_message(message, answer))
            answer.error("Unknown command.");
        client.send(answer);
    }

    if(channel_async)
        _uni_mutex.unlock();
}

bool Core::_handle_message(allkol::Message &message, allkol::Answer &answer)
{
    std::string cmd = message.command();
    nlohmann::json &jmsgdata = message.data();
    nlohmann::json &jansdata = answer.data();

    if(cmd == "list_rooms") {
        answer.ok();
        nlohmann::json jrooms = nlohmann::json::array();
        for(auto &it : _rooms) {
            nlohmann::json jroom;
            jroom["label"] = it.second->label;
            jroom["floor"] = it.second->floor;
            jrooms.push_back(jroom);
        }
        jansdata["rooms"] = jrooms;
        return true;
    }

    if(cmd == "list_device_templates") {
        answer.ok();
        nlohmann::json jdts = nlohmann::json::array();
        std::vector<DeviceTemplate*> dts = _plugins->device_templates();
        for(DeviceTemplate *dt : dts)
            jdts.push_back(dt->serialize_reduced());
        jansdata["device_templates"] = jdts;
        return true;
    }

    if(cmd == "list_devices") {
        answer.ok();
        nlohmann::json jdevs = nlohmann::json::array();
        for(auto &it : _devices) {
            Device *dev = it.second;
            jdevs.push_back(dev->serialize_reduced());
        }
        jansdata["devices"] = jdevs;
        return true;
    }

    if(cmd == "list_devices_ports") {
        answer.ok();
        for(auto &it : _devices) {
            Device *dev = it.second;
            jansdata[std::to_string(dev->id())] = dev->serialize_ports_reduced();
        }
        return true;
    }

    if(cmd == "list_devices_port_values") {
        answer.ok();
        for(auto &it : _devices) {
            Device *dev = it.second;
            jansdata[std::to_string(dev->id())] = dev->serialize_port_values();
        }
        return true;
    }

    if(cmd == "list_device_port_values") {
        if(jmsgdata.find("device") == jmsgdata.end()) {
            answer.error("Field 'device' missing.");
            return true;
        }

        nlohmann::json &jdev = jmsgdata["device"];
        if(!jdev.is_number_unsigned()) {
            answer.error("Invalid field 'device'.");
            return true;
        }
        uint device_id = jdev;

        Device *dev = device(device_id);
        if(!dev) {
            answer.error("Device not found.");
            return true;
        }

        answer.ok();
        jansdata = dev->serialize_port_values();
        return true;
    }

    if(cmd == "write_device_port") {
        if(jmsgdata.find("device") == jmsgdata.end()) {
            answer.error("Field 'device' missing.");
            return true;
        }

        nlohmann::json &jdev = jmsgdata["device"];
        if(!jdev.is_number_unsigned()) {
            answer.error("Invalid field 'device'.");
            return true;
        }
        uint device_id = jdev;

        if(jmsgdata.find("port") == jmsgdata.end()) {
            answer.error("Field 'port' missing.");
            return true;
        }

        nlohmann::json &jport = jmsgdata["port"];
        if(!jport.is_string()) {
            answer.error("Invalid field 'port'.");
            return true;
        }
        std::string port_id = jport;

        if(jmsgdata.find("value") == jmsgdata.end()) {
            answer.error("Field 'value' missing.");
            return true;
        }

        nlohmann::json &jvalue = jmsgdata["value"];
        if(!jvalue.is_number()) {
            answer.error("Invalid field 'value'.");
            return true;
        }
        double value = jvalue;

        Device *dev = device(device_id);
        if(!dev) {
            answer.error("Device not found.");
            return true;
        }

        Port *port = dev->port(port_id);
        if(!port) {
            answer.error("Port not found.");
            return true;
        }

        if(!port->external_write(value)) {
            answer.error("Invalid value.");
            return true;
        }

        answer.ok();
        return true;
    }

    if(cmd == "call_device_action") {
        if(jmsgdata.find("device") == jmsgdata.end()) {
            answer.error("Field 'device' missing.");
            return true;
        }

        nlohmann::json &jdev = jmsgdata["device"];
        if(!jdev.is_number_unsigned()) {
            answer.error("Invalid field 'device'.");
            return true;
        }
        uint device_id = jdev;

        if(jmsgdata.find("action") == jmsgdata.end()) {
            answer.error("Field 'action' missing.");
            return true;
        }

        nlohmann::json &jaction = jmsgdata["action"];
        if(!jaction.is_string()) {
            answer.error("Invalid field 'action'.");
            return true;
        }
        std::string action_id = jaction;

        if(jmsgdata.find("arguments") != jmsgdata.end()) {
            nlohmann::json &jargs = jmsgdata["arguments"];
            if(!jargs.is_object()) {
                answer.error("Invalid field 'arguments'.");
                return true;
            }
        } else {
            jmsgdata["arguments"] = nlohmann::json();
        }

        Device *dev = device(device_id);
        if(!dev) {
            answer.error("Device not found.");
            return true;
        }

        if(!dev->call_action(*_ls, action_id, jmsgdata["arguments"], true)) {
            answer.error("Failed to call the action. Try checking input arguments.");
            return true;
        }

        answer.ok();
        return true;
    }

    return false;
}