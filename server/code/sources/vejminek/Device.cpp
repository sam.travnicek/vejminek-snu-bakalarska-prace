#include "vejminek/Device.hpp"
#include "vejminek/Port.hpp"
#include "vejminek/ANumber.hpp"
#include "vejminek/ABoolean.hpp"
#include "vejminek/AString.hpp"
#include "vejminek/ATime.hpp"
#include "vejminek/ACondition.hpp"
#include "lua/GlobalEnvironment.hpp"
#include "lua/ClosureLS.hpp"
#include <new>
#include <algorithm>

Device::Device(
        uint id, const std::string &label, const std::string &description, DeviceTemplate &dt,
        LuaState &ls, ConditionSandbox &cond_sb, ulib::Logger &logger, Room &room,
        const nlohmann::json &data)
    : _id(id), _label(label), _description(description), _dt(dt), _ls(ls), _logger(logger),
      _room(room)
{
    // implement Ports according to template:
    for(DeviceTemplate::Port &pt : dt.ports()) {
        Port *port = new Port(*this, pt, ls, logger);
        port->event_handler(*this);
        _ports[pt.identifier] = port;
    }

    // implement Slots according to template:
    for(DeviceTemplate::Slot &st : dt.slots()) {
        Slot *slot = new Slot(*this, st, ls, logger);
        slot->event_handler(*this);
        _slots[st.identifier] = slot;
    }

    // implement Signals according to template:
    for(DeviceTemplate::Signal &st : dt.signals()) {
        Signal *signal = new Signal(*this, st, ls, logger);
        signal->event_handler(*this);
        _signals[st.identifier] = signal;
    }

    for(DeviceTemplate::Attribute &attr : dt.attributes()) {
        switch(attr.type) {
        case DeviceTemplate::Attribute::NUMBER:
            _attributes[attr.identifier] = new ANumber(attr);
            break;
        case DeviceTemplate::Attribute::BOOLEAN:
            _attributes[attr.identifier] = new ABoolean(attr);
            break;
        case DeviceTemplate::Attribute::STRING:
            _attributes[attr.identifier] = new AString(attr);
            break;
        case DeviceTemplate::Attribute::TIME:
            _attributes[attr.identifier] = new ATime(attr);
            break;
        case DeviceTemplate::Attribute::CONDITION:
            _attributes[attr.identifier] = new ACondition(attr, cond_sb, logger);
            break;
        default:
            break;
        }
    }

    for(DeviceTemplate::Event &event : dt.emitted_events())
        _events[event.identifier] = std::vector<int>();

    if(data.find("attributes") != data.end()) {
        auto &jattributes = data["attributes"];

        if(jattributes.is_object())
        for(auto &jattr : jattributes.items()) {
            if(_attributes.find(jattr.key()) == _attributes.end())
                continue;
            Attribute *attr = _attributes[jattr.key()];
            auto &jval = jattr.value();
            switch(attr->type()) {
            case DeviceTemplate::Attribute::NUMBER:
                if(jval.is_number())
                    ((ANumber*)attr)->value((double)jval);
                break;
            case DeviceTemplate::Attribute::BOOLEAN:
                if(jval.is_boolean())
                    ((ABoolean*)attr)->value((bool)jval);
                break;
            case DeviceTemplate::Attribute::STRING:
                if(jval.is_string()) {
                    std::string str = jval;
                    ((AString*)attr)->value(str);
                }
                break;
            case DeviceTemplate::Attribute::TIME:
                if(jval.is_string()) {
                    std::string tstr = jval;
                    ((ATime*)attr)->value(ulib::Time(tstr));
                }
                break;
            case DeviceTemplate::Attribute::CONDITION:
                if(jval.is_string()) {
                    ACondition *acond = (ACondition*)attr;
                    std::string code = jval;
                    if(!acond->script(code))
                        _logger << "Failed to load a script of attribute '" << jattr.key()
                            << "' of device #" << _id << ulib::lerror;
                }
                break;
            default:
                break;
            }
        }
    }

    ls.new_table();

    // Assign a new wrapping table to Device pointer in global table LUDW_TAB
    ls.copy(-1);
    GlobalEnvironment::set(ls, this);

    // Metatable for table wrapper
    ls.new_table();

	ls.push("__index"); // handle function for accessing fields
    ls.push(this);
    ls.push(&logger);
    ls.push(&dt);
	ls.push(ilw__index, 3);
	ls.raw_set(-3);

	ls.push("__newindex"); // handle function for modifying fields
    ls.push(&logger);
	ls.push(ilw__newindex, 1);
	ls.raw_set(-3);

	ls.push("__metatable"); // prevent getting metatable (returns nil)
	ls.push_nil();
	ls.raw_set(-3);

    // Assign metatable to the instance
    ls.set_metatable(-2);

    if(dt.ecb_instantion() != 0) {
        // retrieve callback function for event 'instantiation'
        ls.push(dt.ecb_instantion());
        ls.raw_get(LUA_REGISTRYINDEX);

        // trigger event 'instantiation' with arguments: 'instance'
        ls.copy(-2);
        if(!ls.pcall(1)) {
            logger << ls.to_string(-1) << ulib::lerror;
            ls.pop();
        }
    }

    // Pop wrapping table
    ls.pop();
}

Device::~Device()
{
    for(auto it = _ports.begin(); it != _ports.end(); ++it)
        delete it->second;
}

int Device::ilw__index(lua_State *L)
{
    LuaState ls(L);

    ls.push_upvalue(1);
    Device &dev = ls.to_ref<Device>(-1);

    ls.push_upvalue(2);
    ulib::Logger &logger = ls.to_ref<ulib::Logger>(-1);

    ls.push_upvalue(3);
    DeviceTemplate &dt = ls.to_ref<DeviceTemplate>(-1);

    std::string index = ls.to_string(2);

    if(index == "id") {
        ls.push(&dev);
        ls.push(&logger);
        ls.push(ilw_id, 2);
    } else if(index == "label") {
        ls.push(&dev);
        ls.push(&logger);
        ls.push(ilw_label, 2);
    } else if(index == "description") {
        ls.push(&dev);
        ls.push(&logger);
        ls.push(ilw_description, 2);
    } else if(index == "room") {
        ls.push(&dev);
        ls.push(&logger);
        ls.push(ilw_room, 2);
    } else if(index == "port") {
        ls.push(&dev);
        ls.push(&logger);
        ls.push(ilw_port, 2);
    } else if(index == "slot") {
        ls.push(&dev);
        ls.push(&logger);
        ls.push(ilw_slot, 2);
    } else if(index == "signal") {
        ls.push(&dev);
        ls.push(&logger);
        ls.push(ilw_signal, 2);
    } else if(index == "action") {
        ls.push(&dev);
        ls.push(&logger);
        ls.push(&dt);
        ls.push(ilw_action, 3);
    } else if(index == "attribute") {
        ls.push(&dev);
        ls.push(&logger);
        ls.push(ilw_attribute, 2);
    } else if(index == "on") {
        ls.push(&dev);
        ls.push(&logger);
        ls.push(ilw_on, 2);
    } else if(index == "fire") {
        ls.push(&dev);
        ls.push(&logger);
        ls.push(ilw_fire, 2);
    } else
        ls.push_nil();

    return 1;
}

int Device::ilw__newindex(lua_State *L)
{
    ClosureLS ls(L, 1, "__newindex", ClosureLS::CT_METHOD);

    if(!ls.check_protected_fields({
        "id", "label", "description", "room", "port", "action", "attribute", "on", "fire"
    })) {
        return 0;
    }

    // Assign new value to field
    ls.copy(1);
    ls.copy(2);
    ls.copy(3);
    ls.raw_set(-3);

    return 0;
}

int Device::ilw_id(lua_State *L)
{
    ClosureLS ls(L, 2, "id", ClosureLS::CT_METHOD);

    if(!ls.check_closure(0, ClosureLS::TABLE)) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    Device &dev = ls.to_ref<Device>(-1);

    ls.push(dev._id);
    return 1;
}

int Device::ilw_label(lua_State *L)
{
    ClosureLS ls(L, 2, "label", ClosureLS::CT_METHOD);

    if(!ls.check_closure(0, ClosureLS::TABLE)) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    Device &dev = ls.to_ref<Device>(-1);

    ls.push(dev._label);
    return 1;
}

int Device::ilw_description(lua_State *L)
{
    ClosureLS ls(L, 2, "description", ClosureLS::CT_METHOD);

    if(!ls.check_closure(0, ClosureLS::TABLE)) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    Device &dev = ls.to_ref<Device>(-1);

    ls.push(dev._description);
    return 1;
}

int Device::ilw_room(lua_State *L)
{
    ClosureLS ls(L, 2, "room", ClosureLS::CT_METHOD);

    if(!ls.check_closure(0, ClosureLS::TABLE)) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    Device &dev = ls.to_ref<Device>(-1);

    ls.push(dev._room.label);
    return 1;
}

int Device::ilw_port(lua_State *L)
{
    ClosureLS ls(L, 2, "port", ClosureLS::CT_METHOD);

    if(!ls.check_closure(1, ClosureLS::TABLE) || !ls.check_args({ClosureLS::STRING})) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    Device &dev = ls.to_ref<Device>(-1);
    ulib::Logger &logger = ls.logger();

    std::string port_name = ls.to_string(2);
    Port *p = dev.port(port_name);
    if(!p) {
        logger << ls.msg_prefix() << " port '" << port_name << "' not found "
            << ls.msg_postfix() << ulib::lerror;
        ls.push_nil();
        return 1;
    }

    GlobalEnvironment::get(ls, p);
    return 1;
}

int Device::ilw_slot(lua_State *L)
{
    ClosureLS ls(L, 2, "slot", ClosureLS::CT_METHOD);

    if(!ls.check_closure(1, ClosureLS::TABLE) || !ls.check_args({ClosureLS::STRING})) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    Device &dev = ls.to_ref<Device>(-1);
    ulib::Logger &logger = ls.logger();

    std::string slot_name = ls.to_string(2);
    Slot *s = dev.slot(slot_name);
    if(!s) {
        logger << ls.msg_prefix() << " slot '" << slot_name << "' not found "
            << ls.msg_postfix() << ulib::lerror;
        ls.push_nil();
        return 1;
    }

    GlobalEnvironment::get(ls, s);
    return 1;
}

int Device::ilw_signal(lua_State *L)
{
    ClosureLS ls(L, 2, "signal", ClosureLS::CT_METHOD);

    if(!ls.check_closure(1, ClosureLS::TABLE) || !ls.check_args({ClosureLS::STRING})) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    Device &dev = ls.to_ref<Device>(-1);
    ulib::Logger &logger = ls.logger();

    std::string signal_name = ls.to_string(2);
    Signal *s = dev.signal(signal_name);
    if(!s) {
        logger << ls.msg_prefix() << " signal '" << signal_name << "' not found "
            << ls.msg_postfix() << ulib::lerror;
        ls.push_nil();
        return 1;
    }

    GlobalEnvironment::get(ls, s);
    return 1;
}

int Device::ilw_action(lua_State *L)
{
    ClosureLS ls(L, 2, "action", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_closure({1, 2}, ClosureLS::TABLE)
            || !ls.check_args({ClosureLS::STRING, ClosureLS::TABLE}, 1))
        return 0;

    ls.push_upvalue(1);
    Device &dev = ls.to_ref<Device>(-1);

    ls.push_upvalue(3);
    DeviceTemplate &dt = ls.to_ref<DeviceTemplate>(-1);

    std::string action_identifier = ls.to_string(2);

    int action_idx = -1;
    for(size_t i = 0; i < dt.actions().size(); ++i) {
        DeviceTemplate::Action &action = dt.actions()[i];
        if(action.identifier == action_identifier) {
            action_idx = i;
            break;
        }
    }

    if(action_idx < 0) {
        logger << ls.msg_prefix() << " action '" << action_identifier << "' not found "
            << ls.msg_postfix() << ulib::lerror;
        return 0;
    }

    DeviceTemplate::Action &action = dt.actions()[action_idx];

    ls.push(action.reference);
    ls.raw_get(LUA_REGISTRYINDEX);
    GlobalEnvironment::get(ls, &dev);
    ls.new_table();

    if(ls.num_args() == 2) {
        for(DeviceTemplate::Action::Argument &arg : action.arguments) {
            ls.push(arg.identifier);
            ls.copy(-1);
            ls.raw_get(3);

            if(ls.is_nil(-1)) {
                if(arg.type == DeviceTemplate::Action::Argument::NUMBER) {
                    ls.push(std::any_cast<double>(arg.default_value));
                    ls.raw_set(-3);
                } else if(arg.type == DeviceTemplate::Action::Argument::BOOLEAN) {
                    ls.push(std::any_cast<bool>(arg.default_value));
                    ls.raw_set(-3);
                } else if(arg.type == DeviceTemplate::Action::Argument::STRING) {
                    ls.push(std::any_cast<std::string>(arg.default_value));
                    ls.raw_set(-3);
                } else
                    ls.pop(2);

                continue;
            }

            if(arg.type >= DeviceTemplate::Action::Argument::_TYPE_END_) {
                ls.pop(2);
                continue;
            }

            int type;

            if(arg.type == DeviceTemplate::Action::Argument::NUMBER)
                type = LUA_TNUMBER;
            else if(arg.type == DeviceTemplate::Action::Argument::BOOLEAN)
                type = LUA_TBOOLEAN;
            else
                type = LUA_TSTRING;

            if(ls.type(-1) != type) {
                logger << ls.msg_prefix() << " argument '" << arg.identifier << "' type mismatch "
                    << ls.msg_postfix() << ulib::lerror;
                ls.pop(2);
            } else
                ls.raw_set(-3);
        }
    }

    ls.push(false); // flag: called from external source (ex. channels)

    if(!ls.pcall(3)) {
        logger << ls.to_string(-1) << ulib::lerror;
        ls.pop();
    }

    return 0;
}

int Device::ilw_attribute(lua_State *L)
{
    ClosureLS ls(L, 2, "attribute", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_closure(1, ClosureLS::TABLE) || !ls.check_args({ClosureLS::STRING})) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    Device &dev = ls.to_ref<Device>(-1);

    std::string attr_name = ls.to_string(2);

    if(dev._attributes.find(attr_name) == dev._attributes.end()) {
        logger << ls.msg_prefix() << " attribute '" <<  attr_name << "' not found "
            << ls.msg_postfix() << ulib::lerror;
        ls.push_nil();
        return 1;
    }

    Attribute *attr = dev._attributes[attr_name];
    switch (attr->type()) {
    case DeviceTemplate::Attribute::NUMBER:
        ls.push(((ANumber*)attr)->value());
        return 1;
    case DeviceTemplate::Attribute::BOOLEAN:
        ls.push(((ABoolean*)attr)->value());
        return 1;
    case DeviceTemplate::Attribute::STRING:
        ls.push(((AString*)attr)->value());
        return 1;
    case DeviceTemplate::Attribute::TIME:
        ls.push((std::string)(((ATime*)attr)->value()));
        return 1;
    case DeviceTemplate::Attribute::CONDITION:
        {
            ACondition *acond = (ACondition*)attr;
            if(!acond->run()) {
                logger << ls.msg_prefix() << " failed to evaluate a condition "
                    << ls.msg_postfix() << ulib::lerror;
                ls.push(false);
                return 1;
            }
            ls.push(acond->check());
        }
        return 1;
    default:
        logger << ls.msg_prefix() << " unknown attribute type "
            << ls.msg_postfix() << ulib::lerror;
        ls.push_nil();
        return 1;
    }
}

int Device::ilw_on(lua_State *L)
{
    ClosureLS ls(L, 2, "on", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_closure(2, ClosureLS::TABLE)
            || !ls.check_args({ClosureLS::STRING, ClosureLS::FUNCTION}))
        return 0;

    ls.push_upvalue(1);
    Device &dev = ls.to_ref<Device>(-1);

    std::string event_name = ls.to_string(2);

    if(dev._events.find(event_name) == dev._events.end()) {
        logger << ls.msg_prefix() << " unknown event '" << event_name << "' "
            << ls.msg_postfix() << ulib::lerror;
        return 0;
    }

    std::vector<int>& callbacks = dev._events[event_name];

    ls.copy(3);
    int reference = ls.ref(LUA_REGISTRYINDEX);
    callbacks.push_back(reference);

    return 0;
}

int Device::ilw_fire(lua_State *L)
{
    ClosureLS ls(L, 2, "fire", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    ls.push_upvalue(1);
    Device &dev = ls.to_ref<Device>(-1);

    std::string event_name = ls.to_string(2);

    if(dev._events.find(event_name) == dev._events.end()) {
        logger << ls.msg_prefix() << " unknown event '" << event_name << "' "
            << ls.msg_postfix() << ulib::lerror;
        return 0;
    }

    std::vector<int>& callbacks = dev._events[event_name];

    for(int cb_ref : callbacks) {
        ls.push(cb_ref);
        ls.raw_get(LUA_REGISTRYINDEX);
        GlobalEnvironment::get(ls, &dev);

        if(!ls.pcall(1)) {
            logger << ls.to_string(-1) << ulib::lerror;
            ls.pop();
        }
    }

    return 0;
}

void Device::port_changed(Port &port, double previous_value)
{
    if(_dt.ecb_port_changed() == 0)
        return;

    _ls.push(_dt.ecb_port_changed());
    _ls.raw_get(LUA_REGISTRYINDEX);

    GlobalEnvironment::get(_ls, this);
    GlobalEnvironment::get(_ls, &port);
    _ls.push(port.value());
    _ls.push(previous_value);

    if(!_ls.pcall(4)) {
        _logger << _ls.to_string(-1) << ulib::lerror;
        _ls.pop();
    }
}

void Device::port_written(Port &port, double previous_value)
{
    _logger << "[W](" << previous_value << " -> " << port.value() << "): device #" << _id
        << " '" << _label << "', port '" << port.identifier() << "'." << ulib::ldebug;
}

void Device::slot_written(Slot &slot)
{
    _logger << "[W](";

    if(slot.type() == DeviceTemplate::Slot::NUMBER)
        _logger << std::any_cast<double>(slot.value());
    else if(slot.type() == DeviceTemplate::Slot::BOOLEAN)
        _logger << std::any_cast<bool>(slot.value());
    else if(slot.type() == DeviceTemplate::Slot::STRING)
        _logger << std::any_cast<std::string>(slot.value());

    _logger << "): device #" << _id << " '" << _label << "', slot '" << slot.identifier()
        << "'." << ulib::ldebug;

    if(_dt.ecb_slot_written() == 0)
        return;

    _ls.push(_dt.ecb_slot_written());
    _ls.raw_get(LUA_REGISTRYINDEX);

    GlobalEnvironment::get(_ls, this);
    GlobalEnvironment::get(_ls, &slot);

    if(slot.type() == DeviceTemplate::Slot::NUMBER)
        _ls.push(std::any_cast<double>(slot.value()));
    else if(slot.type() == DeviceTemplate::Slot::BOOLEAN)
        _ls.push(std::any_cast<bool>(slot.value()));
    else if(slot.type() == DeviceTemplate::Slot::STRING)
        _ls.push(std::any_cast<std::string>(slot.value()));
    else
        _ls.push_nil();

    if(!_ls.pcall(3)) {
        _logger << _ls.to_string(-1) << ulib::lerror;
        _ls.pop();
    }
}

void Device::signal_invoked(Signal &signal)
{
    _logger << "[I]: device #" << _id << " '" << _label << "', signal '" << signal.identifier()
        << "'." << ulib::ldebug;

    if(_dt.ecb_signal_invoked() == 0)
        return;

    _ls.push(_dt.ecb_signal_invoked());
    _ls.raw_get(LUA_REGISTRYINDEX);

    GlobalEnvironment::get(_ls, this);
    GlobalEnvironment::get(_ls, &signal);

    if(!_ls.pcall(2)) {
        _logger << _ls.to_string(-1) << ulib::lerror;
        _ls.pop();
    }
}

void Device::tick(int64_t time, ulong ticks, ulong tps)
{
    if(_dt.ecb_tick() == 0)
        return;

    _ls.push(_dt.ecb_tick());
    _ls.raw_get(LUA_REGISTRYINDEX);

    GlobalEnvironment::get(_ls, this);
    _ls.push(time);
    _ls.push(ticks);
    _ls.push(tps);

    if(!_ls.pcall(4)) {
        _logger << _ls.to_string(-1) << ulib::lerror;
        _ls.pop();
    }
}

Port* Device::port(const std::string &identifier)
{
    if(_ports.find(identifier) != _ports.end())
        return _ports[identifier];
    return nullptr;
}

Slot* Device::slot(const std::string &identifier)
{
    if(_slots.find(identifier) != _slots.end())
        return _slots[identifier];
    return nullptr;
}

Signal* Device::signal(const std::string &identifier)
{
    if(_signals.find(identifier) != _signals.end())
        return _signals[identifier];
    return nullptr;
}

bool Device::call_action(
    LuaState &ls, const std::string &identifier, nlohmann::json &args,
    bool called_externally)
{
    int action_idx = -1;
    std::vector<DeviceTemplate::Action> &actions = _dt.actions();

    for(size_t i = 0; i < actions.size(); ++i) {
        DeviceTemplate::Action &action = actions[i];
        if(action.identifier == identifier) {
            action_idx = i;
            break;
        }
    }

    if(action_idx < 0) {
        _logger << "Action '" << identifier << "' on device #" << _id << " not found."
            << ulib::lerror;
        return false;
    }

    DeviceTemplate::Action &action = actions[action_idx];

    int stack_top = ls.top();

    ls.push(action.reference);
    ls.raw_get(LUA_REGISTRYINDEX);
    GlobalEnvironment::get(ls, this);
    ls.new_table();

    for(DeviceTemplate::Action::Argument &arg : action.arguments) {
        using Arg = DeviceTemplate::Action::Argument;

        if(args.find(arg.identifier) == args.end()) {
            if(arg.type == Arg::NUMBER) {
                ls.push(arg.identifier);
                ls.push(std::any_cast<double>(arg.default_value));
                ls.raw_set(-3);
            } else if(arg.type == Arg::BOOLEAN) {
                ls.push(arg.identifier);
                ls.push(std::any_cast<bool>(arg.default_value));
                ls.raw_set(-3);
            } else if(arg.type == Arg::STRING) {
                ls.push(arg.identifier);
                ls.push(std::any_cast<std::string>(arg.default_value));
                ls.raw_set(-3);
            }
            continue;
        }

        bool type_missmatch =
            (arg.type == Arg::NUMBER && !args[arg.identifier].is_number())
            || (arg.type == Arg::BOOLEAN && !args[arg.identifier].is_boolean())
            || (arg.type == Arg::STRING && !args[arg.identifier].is_string());
        if(type_missmatch) {
            _logger << "Action '" << identifier << "' on device #" << _id << " - argument '"
                << arg.identifier << "' type mismatch." << ulib::lerror;
            return false;
        }

        ls.push(arg.identifier);

        if(arg.type == Arg::NUMBER)
            ls.push((double)args[arg.identifier]);
        else if(arg.type == Arg::BOOLEAN)
            ls.push((bool)args[arg.identifier]);
        else if(arg.type == Arg::STRING) {
            std::string arg_val = args[arg.identifier];
            ls.push(arg_val);
        } else {
            _logger << "Action '" << identifier << "' on device #" << _id << " - argument '"
                << arg.identifier << "' value invalid." << ulib::lerror;
            ls.top(stack_top);
            return false;
        }

        ls.raw_set(-3);
    }

    ls.push(called_externally);

    if(!ls.pcall(3)) {
        _logger << "Action '" << identifier << "' on device #" << _id << " call failure: "
            << ls.to_string(-1) << ulib::lerror;
        ls.top(stack_top);
        return false;
    }

    ls.top(stack_top); // clear stack from pushed values to be sure no one was left there
    return true;
}

nlohmann::json Device::serialize_reduced()
{
    nlohmann::json jdev;
    jdev["template"] = _dt.identifier();
    jdev["room"] = _room.label;
    jdev["id"] = _id;
    jdev["label"] = _label;
    jdev["description"] = _description;
    return jdev;
}

nlohmann::json Device::serialize_ports_reduced()
{
    nlohmann::json jports;
    for(auto &it : _ports)
        jports[it.first] = it.second->serialize_reduced_noid();
    return jports;
}

nlohmann::json Device::serialize_port_values()
{
    nlohmann::json jports;
    for(auto &it : _ports)
        jports[it.first] = it.second->value();
    return jports;
}