#include "vejminek/Port.hpp"
#include "vejminek/Device.hpp"
#include "lua/GlobalEnvironment.hpp"
#include "lua/ClosureLS.hpp"
#include "vejminek/PortConfiguration.hpp"

Port::Port(
        Device &device, DeviceTemplate::Port &port_template, LuaState &ls,
        ulib::Logger &logger)
    : _device(device), _pt(port_template), _driver_linkage(nullptr), _value(0),
      _initial_write(true)
{
    ls.new_table();

    // Assign a new wrapping table to Device pointer in global table LUDW_TAB
    ls.copy(-1);
    GlobalEnvironment::set(ls, this);

    // Metatable for table wrapper
    ls.new_table();

	ls.push("__index"); // handle function for accessing fields
    ls.push(this);
    ls.push(&logger);
	ls.push(ilw__index, 2);
	ls.raw_set(-3);

	ls.push("__newindex"); // handle function for modifying fields
    ls.push(&logger);
	ls.push(ilw__newindex, 1);
	ls.raw_set(-3);

	ls.push("__metatable"); // prevent getting metatable (returns nil)
	ls.push_nil();
	ls.raw_set(-3);

    // Assign metatable to the instance
    ls.set_metatable(-2);

    // Pop wrapping table
    ls.pop();
}

Port::~Port()
{
}

bool Port::write(double value, bool silent, bool by_extsrc)
{
    if(_pt.mode == DeviceTemplate::Port::LOGICAL && value != 0 && value != 1)
        return false;

    if(_driver_linkage) {
        float min = _driver_linkage->min;
        float max = _driver_linkage->max;

        if(_pt.mode == DeviceTemplate::Port::DISCRETE && (value < min || value > max))
            return false;
    }

    double prev_value = _value;
    _value = value;
    _written_by_extsrc = by_extsrc;

    for(EventHandler *eh : _event_handlers)
        eh->port_written(*this, prev_value);

    if(!silent && (prev_value != _value || _initial_write))
        for(EventHandler *eh : _event_handlers)
            eh->port_changed(*this, prev_value);

    _initial_write = false;
    return true;
}

float Port::min() const
{
    if(!_driver_linkage)
        return 0;
    return _driver_linkage->min;
}

float Port::max() const
{
    if(!_driver_linkage)
        return 1;
    return _driver_linkage->max;
}

int Port::ilw__index(lua_State* L)
{
    LuaState ls(L);

    ls.push_upvalue(1);
    Port &port = ls.to_ref<Port>(-1);

    ls.push_upvalue(2);
    ulib::Logger &logger = ls.to_ref<ulib::Logger>(-1);
    std::string index = ls.to_string(2);

    if(index == "identifier") {
        ls.push(&port);
        ls.push(&logger);
        ls.push(ilw_identifier, 2);
    } else if(index == "label") {
        ls.push(&port);
        ls.push(&logger);
        ls.push(ilw_label, 2);
    } else if(index == "write") {
        ls.push(&port);
        ls.push(&logger);
        ls.push(ilw_write, 2);
    } else if(index == "toggle") {
        ls.push(&port);
        ls.push(&logger);
        ls.push(ilw_toggle, 2);
    } else if(index == "read") {
        ls.push(&port);
        ls.push(&logger);
        ls.push(ilw_read, 2);
    } else if(index == "min") {
        ls.push(&port);
        ls.push(&logger);
        ls.push(ilw_min, 2);
    } else if(index == "max") {
        ls.push(&port);
        ls.push(&logger);
        ls.push(ilw_max, 2);
    } else if(index == "written_externally") {
        ls.push(&port);
        ls.push(&logger);
        ls.push(ilw_written_externally, 2);
    } else if(index == "linked") {
        ls.push(&port);
        ls.push(&logger);
        ls.push(ilw_linked, 2);
    } else if(index == "configuration_identifier") {
        ls.push(&port);
        ls.push(&logger);
        ls.push(ilw_configuration_identifier, 2);
    } else
        ls.push_nil();

    return 1;
}

int Port::ilw__newindex(lua_State *L)
{
    ClosureLS ls(L, 1, "__newindex", ClosureLS::CT_METHOD);

    if(!ls.check_protected_fields({
            "identifier", "label", "write", "toggle", "read", "min", "max", "linked",
            "configuration_identifier"}))
        return 0;

    ls.error_protected_obj();
    return 0;
}

int Port::ilw_identifier(lua_State *L)
{
    ClosureLS ls(L, 2, "identifier", ClosureLS::CT_METHOD);

    if(!ls.check_closure(0, ClosureLS::TABLE)) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    Port &port = ls.to_ref<Port>(-1);

    ls.push(port._pt.identifier);
    return 1;
}

int Port::ilw_label(lua_State *L)
{
    ClosureLS ls(L, 2, "label", ClosureLS::CT_METHOD);

    if(!ls.check_closure(0, ClosureLS::TABLE)) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    Port &port = ls.to_ref<Port>(-1);

    ls.push(port._pt.label);
    return 1;
}

int Port::ilw_write(lua_State *L)
{
    ClosureLS ls(L, 2, "write", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_closure({1, 2}, ClosureLS::TABLE))
        return 0;

    if(!ls.check_arg(0, {ClosureLS::INT, ClosureLS::DOUBLE, ClosureLS::BOOL}))
        return 0;

    if(ls.num_args() > 1 && !ls.check_arg(1, ClosureLS::BOOL))
        return 0;

    ls.push_upvalue(1);
    Port &port = ls.to_ref<Port>(-1);
    bool silent_write = false;

    if(ls.num_args() == 2)
        silent_write = ls.to_bool(3);

    double value;

    if(ls.is_bool(2))
        value = ls.to_bool(2) ? 1 : 0;
    else
        value = ls.to_double(2);

    if(!port.write(value, silent_write, false)) {
        logger << ls.msg_prefix() << " attempt to write invalid value '" << value << "' to port '"
            << port._pt.identifier << "' " << ls.msg_postfix() << ulib::lerror;
    }

    return 0;
}

int Port::ilw_toggle(lua_State *L)
{
    ClosureLS ls(L, 2, "toggle", ClosureLS::CT_METHOD);

    if(!ls.check_object(ClosureLS::TABLE))
        return 0;

    ls.argument_list({});
    int arg_list = ls.check_arguments();

    if(arg_list < 0)
        return 0;

    ls.push_upvalue(1);
    Port &port = ls.to_ref<Port>(-1);

    port.write(port.max() - port._value);
    return 0;
}

int Port::ilw_read(lua_State *L)
{
    ClosureLS ls(L, 2, "read", ClosureLS::CT_METHOD);

    if(!ls.check_closure(0, ClosureLS::TABLE)) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    Port &port = ls.to_ref<Port>(-1);

    ls.push(port._value);
    return 1;
}

int Port::ilw_min(lua_State *L)
{
    ClosureLS ls(L, 2, "min", ClosureLS::CT_METHOD);

    if(!ls.check_closure(0, ClosureLS::TABLE)) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    Port &port = ls.to_ref<Port>(-1);
    ls.push(port.min());
    return 1;
}

int Port::ilw_max(lua_State *L)
{
    ClosureLS ls(L, 2, "max", ClosureLS::CT_METHOD);

    if(!ls.check_closure(0, ClosureLS::TABLE)) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    Port &port = ls.to_ref<Port>(-1);
    ls.push(port.max());
    return 1;
}

int Port::ilw_written_externally(lua_State *L)
{
    ClosureLS ls(L, 2, "written_externally", ClosureLS::CT_METHOD);

    if(!ls.check_closure(0, ClosureLS::TABLE)) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    Port &port = ls.to_ref<Port>(-1);

    ls.push(port._written_by_extsrc);
    return 1;
}

int Port::ilw_linked(lua_State *L)
{
    ClosureLS ls(L, 2, "linked", ClosureLS::CT_METHOD);

    if(!ls.check_closure(0, ClosureLS::TABLE)) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    Port &port = ls.to_ref<Port>(-1);

    ls.push(port._driver_linkage != nullptr);
    return 1;
}

int Port::ilw_configuration_identifier(lua_State *L)
{
    ClosureLS ls(L, 2, "configuration_identifier", ClosureLS::CT_METHOD);

    if(!ls.check_closure(0, ClosureLS::TABLE)) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    Port &port = ls.to_ref<Port>(-1);

    if(!port._driver_linkage) {
        ls.push_nil();
        return 1;
    }

    ls.push(port._driver_linkage->identifier);
    return 1;
}

nlohmann::json Port::serialize_reduced()
{
    nlohmann::json jport = serialize_reduced_noid();
    jport["identifier"] = identifier();
    return jport;
}

nlohmann::json Port::serialize_reduced_noid()
{
    nlohmann::json jport;
    jport["value"] = _value;
    jport["min"] = min();
    jport["max"] = max();
    jport["linked"] = _driver_linkage != nullptr;
    return jport;
}