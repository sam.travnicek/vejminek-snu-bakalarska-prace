#include "Program.hpp"

#include <ulib/System.hpp>
#include <fstream>
#include <iomanip>
#include <lua5.3/lua.hpp>
#include "vejminek/Device.hpp"
#include "vejminek/ACondition.hpp"
#include "lua/LinkageSandbox.hpp"
#include <ulib-net/MySQL.hpp>
#include "Base64.hpp"
#include "lua/GlobalEnvironment.hpp"

const std::string Program::VERSION = "1.0";
const std::string Program::CONFIG_PATH = "config.json";

bool Program::DEBUG_MODE = false;
bool Program::DEBUG_DEVICES = false;
bool Program::DEBUG_CHANNELS = false;

Program::Program(const std::string &program_path, const ulib::LoggerConf &logger_cfg)
    : _logger("Program", logger_cfg),
      _program_path(program_path),
      _sig_exit(false)
{
}

Program::~Program()
{
}

bool Program::init()
{
    _logger << "** Version " << VERSION << ulib::linfo;

    ulib::System::make_dir(_program_path + "/logs");
    ulib::System::make_dir(_program_path + "/logs/general");
    ulib::System::make_dir(_program_path + "/plugins");

    if(!ulib::System::file_exist(_program_path + "/test_bench.lua")) {
		std::ofstream tb_ofile(_program_path + "/test_bench.lua");
        tb_ofile << "print('Test Bench loaded.')";
        tb_ofile.close();
    }

    _logger << "Loading configuration from '" << CONFIG_PATH << "'..." << ulib::linfo;

	if(!ulib::System::file_readable(_program_path + "/" + CONFIG_PATH)) {
		_logger.warn("Could not read configuration file, creating one...");
		std::ofstream cfg_ofile(_program_path + "/" + CONFIG_PATH);
        cfg_ofile << Configuration::DEFAULT_CONFIG_JSON;
        cfg_ofile.close();
        _logger << "Please edit '" << CONFIG_PATH << "' and rerun the program." << ulib::linfo;
        return false;
	}

    _config = std::make_unique<Configuration>(_program_path + "/" + CONFIG_PATH, *this);

    if(!_config->load()) {
        _logger << "Try deleting '" << CONFIG_PATH << "' file." << ulib::linfo;
        return false;
    }

    // TODO: handle failed connection to database
    _core = std::make_unique<Core>(
        _program_path + "/plugins", _program_path, _logger.configuration(), _database,
        _channel_descriptors);

    return true;
}

void Program::tick(int64_t time, ulong ticks, ulong tps)
{
    _core->tick(time, ticks, tps);
}