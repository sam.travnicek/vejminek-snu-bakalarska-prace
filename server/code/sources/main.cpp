#include <csignal>
#include <string>
#include <sys/time.h>
#include <ulib/DateTime.hpp>
#include <ulib/System.hpp>
#include <ulib/Logger.hpp>
#include <cmath>
#include <cstdlib>
#include "Program.hpp"

constexpr int TPS = 100;
constexpr int LOOP_PERIOD = 1000/TPS;

constexpr unsigned int LOG_LEVEL = ulib::LoggerConf::WARN;
constexpr bool PRINT_DATETIME = true;
constexpr bool PRINT_DATE = false;
constexpr bool LOG_DATETIME = true;
constexpr bool LOG_DATE = true;
constexpr bool LOG_LEVEL_COLORFUL = true;
constexpr bool LOG_LEVEL_BOLD = true;

volatile bool sig_exit = false;

void isr_handler(int sig_number);

int main(int argc, char *argv[])
{
	for(int i = 1; i < argc; i++) {
		if(std::string(argv[i]) == "-debug")
			Program::DEBUG_MODE = true;
		else if(std::string(argv[i]) == "-debug_devices") {
			Program::DEBUG_MODE = true;
			Program::DEBUG_DEVICES = true;
		} else if(std::string(argv[i]) == "-debug_channels") {
			Program::DEBUG_MODE = true;
			Program::DEBUG_CHANNELS = true;
		} else if(std::string(argv[i]) == "-debug_all") {
			Program::DEBUG_MODE = true;
			Program::DEBUG_DEVICES = true;
			Program::DEBUG_CHANNELS = true;
		}
	}

	std::string program_path = ulib::System::dir_name(argv[0]);

	ulib::LoggerConf logger_cfg(
		program_path + "/logs/general/{DATE}.log", LOG_LEVEL,
		Program::DEBUG_MODE ? ulib::LoggerConf::DEBUG : ulib::LoggerConf::INFO,
		PRINT_DATETIME, PRINT_DATE, LOG_DATETIME, LOG_DATE,
		LOG_LEVEL_COLORFUL, LOG_LEVEL_BOLD);

	Program program(program_path, logger_cfg);

	struct sigaction sig_act = {0};
	sig_act.sa_handler = isr_handler;
	sig_act.sa_flags = 0;
	sigaction(SIGINT, &sig_act, NULL);
	sigaction(SIGTERM, &sig_act, NULL);

	srand(time(NULL));

	if(program.init()) {
		int64_t last_time = ulib::DateTime::now();
		ulong ticks = 0;

		while(!sig_exit && !program.sig_exit()) {
			ulib::System::usleep(200);

			int64_t curr_time = ulib::DateTime::now();
			if(curr_time - last_time >= LOOP_PERIOD) {
				last_time += LOOP_PERIOD;
				program.tick(curr_time, ticks, TPS);
				ticks++;
				// printf("TICK\n");
			}
		}
	}

	ulib::Logger logger("", logger_cfg);
	logger << "Exiting." << ulib::linfo;

	return 0;
}

void isr_handler(int sig_number)
{
	sig_exit = true;
}