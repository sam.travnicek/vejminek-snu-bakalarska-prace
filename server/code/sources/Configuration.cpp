#include "Configuration.hpp"
#include <ulib/System.hpp>
#include <nlohmann/json.hpp>
#include "Program.hpp"

Configuration::Configuration(const std::string &path, Program &program)
    : _path(path), _program(program)
{
}

bool Configuration::load()
{
    ulib::Logger &logger = _program._logger;

    std::ifstream cfg_file(_program._program_path + "/" + Program::CONFIG_PATH);
	if(!cfg_file.is_open()) {
		logger.fatal("Failed to open configuration file.");
        return false;
    }

    nlohmann::json cfg;
    try {
	    cfg_file >> cfg;
        cfg_file.close();
    } catch(...) {
        logger << "Failed to parse configuration file." << ulib::lfatal;
        cfg_file.close();
        return false;
    }

    if(cfg.find("database") == cfg.end()) {
        logger << "Failed to load database parameters." << ulib::lfatal;
        return false;
    } else {
        try {
            if(!_load_database(cfg["database"])) {
                logger << "Failed to load database parameters." << ulib::lfatal;
                return false;
            }
        } catch(...) {
            logger << "Failed to load database parameters." << ulib::lfatal;
            return false;
        }
    }

    if(cfg.find("channels") != cfg.end()) {
        try {
            if(!_load_channels(cfg["channels"])) {
                logger << "Failed to load channels." << ulib::lfatal;
                return false;
            }
        } catch(...) {
            logger << "Failed to load channels." << ulib::lfatal;
            return false;
        }
    }

    return true;
}

bool Configuration::_load_database(nlohmann::json &cfg_database)
{
    ulib::Logger &logger = _program._logger;
    ulib::net::MySQL &mysql = _program._database;

    if(cfg_database.find("host") == cfg_database.end()) {
        logger << "Database host not found. Using default value." << ulib::lwarn;
    } else {
        nlohmann::json &jhost = cfg_database["host"];
        if(!jhost.is_string()) {
            logger << "Invalid format of database host." << ulib::lfatal;
            return false;
        } else {
            std::string host = jhost;
            mysql.host(host);
        }
    }

    if(cfg_database.find("name") == cfg_database.end()) {
        logger << "Database name not found. Using default value." << ulib::lwarn;
    } else {
        nlohmann::json &jname = cfg_database["name"];
        if(!jname.is_string()) {
            logger << "Invalid format of database name." << ulib::lfatal;
            return false;
        } else {
            std::string name = jname;
            mysql.name(name);
        }
    }

    if(cfg_database.find("user") == cfg_database.end()) {
        logger << "Database user not found. Using default value." << ulib::lwarn;
    } else {
        nlohmann::json &juser = cfg_database["user"];
        if(!juser.is_string()) {
            logger << "Invalid format of database user." << ulib::lfatal;
            return false;
        } else {
            std::string user = juser;
            mysql.user(user);
        }
    }

    if(cfg_database.find("password") == cfg_database.end()) {
        logger << "Database password not found. Using default value." << ulib::lwarn;
    } else {
        nlohmann::json &jpass = cfg_database["password"];
        if(!jpass.is_string()) {
            logger << "Invalid format of database password." << ulib::lfatal;
            return false;
        } else {
            std::string password = jpass;
            mysql.password(password);
        }
    }

    if(cfg_database.find("port") == cfg_database.end()) {
        logger << "Database port not found. Using default value." << ulib::lwarn;
    } else {
        nlohmann::json &jport = cfg_database["port"];
        if(!jport.is_number_integer()) {
            logger << "Invalid format of database port." << ulib::lfatal;
            return false;
        } else {
            int port = jport;
            mysql.port(port);
        }
    }

    logger << "Database configuration:\n";
    logger << "- host: " << mysql.host() << '\n';
    logger << "- name: " << mysql.name() << '\n';
    logger << "- user: " << mysql.user() << '\n';
    logger << "- password: ";
    for(uint i = 0; i < mysql.password().length(); i++)
        logger << '*';
    logger << "\n- port: " << mysql.port() << ulib::linfo;

    return true;
}

bool Configuration::_load_channels(nlohmann::json &cfg_channels)
{
    ulib::Logger &logger = _program._logger;

    if(!cfg_channels.is_array()) {
        logger << "Invalid format of channels." << ulib::lerror;
        return false;
    }

    uint counter = 0;

    for(auto &it : cfg_channels) {
        if(!it.is_object()) {
            logger << "Invalid format of channel #" << counter
                << " Channel discarded." << ulib::lerror;
            ++counter;
            continue;
        }

        channel_descriptor_t descriptor;

        if(it.find("protocol") == it.end()) {
            logger << "Missing field 'protocol' of channel #" << counter
                << " Channel discarded." << ulib::lerror;
            ++counter;
            continue;
        }

        nlohmann::json &jprotocol = it["protocol"];
        if(!jprotocol.is_string()) {
            logger << "Invalid format of field 'protocol' of channel #" << counter
                << " Channel discarded." << ulib::lerror;
            ++counter;
            continue;
        }

        std::string protocol = jprotocol;
        bool protocol_secure = false;

        if(protocol == "tcp")
            descriptor.type = channel_descriptor_t::TCP;
        else if(protocol == "tls") {
            descriptor.type = channel_descriptor_t::TLS;
            protocol_secure = true;
        } else if(protocol == "ws")
            descriptor.type = channel_descriptor_t::WS;
        else if(protocol == "wss") {
            descriptor.type = channel_descriptor_t::WSS;
            protocol_secure = true;
        } else {
            logger << "Unknown protocol '" << protocol << "' of channel #" << counter
                << " Channel discarded." << ulib::lerror;
            ++counter;
            continue;
        }

        if(protocol_secure) {
            if(it.find("key_path") == it.end()) {
                logger << "Missing field 'key_path' of channel #" << counter
                    << " Channel discarded." << ulib::lerror;
                ++counter;
                continue;
            }

            nlohmann::json &jkey_path = it["key_path"];
            if(!jkey_path.is_string()) {
                logger << "Invalid format of field 'key_path' of channel #" << counter
                    << " Channel discarded." << ulib::lerror;
                ++counter;
                continue;
            }

            descriptor.key_path = jkey_path;

            if(it.find("certificate_path") == it.end()) {
                logger << "Missing field 'certificate_path' of channel #" << counter
                    << " Channel discarded." << ulib::lerror;
                ++counter;
                continue;
            }

            nlohmann::json &jcertificate_path = it["certificate_path"];
            if(!jcertificate_path.is_string()) {
                logger << "Invalid format of field 'certificate_path' of channel #" << counter
                    << " Channel discarded." << ulib::lerror;
                ++counter;
                continue;
            }

            descriptor.cert_path = jcertificate_path;
        }

        descriptor.max_clients = 100;
        descriptor.clients_checking = channel_descriptor_t::NONE;
        descriptor.ip_address = "0.0.0.0";

        if(it.find("clients") != it.end()) {
            nlohmann::json &jclients = it["clients"];

            if(!jclients.is_object()) {
                logger << "Invalid format of field 'clients' of channel #" << counter
                    << " Channel discarded." << ulib::lerror;
                ++counter;
                continue;
            }

            if(jclients.find("checking") != jclients.end()) {
                nlohmann::json &jchecking = jclients["checking"];

                if(!jchecking.is_string()) {
                    logger << "Invalid format of field 'clients.checking' of channel #" << counter
                        << " Channel discarded." << ulib::lerror;
                    ++counter;
                    continue;
                }

                std::string checking = jchecking;
                std::string uuids_field = "";

                if(checking == "none")
                    descriptor.clients_checking = channel_descriptor_t::NONE;
                else if(checking == "whitelist") {
                    descriptor.clients_checking = channel_descriptor_t::WHITELIST;
                    uuids_field = "whitelist";
                } else if(checking == "blacklist") {
                    descriptor.clients_checking = channel_descriptor_t::BLACKLIST;
                    uuids_field = "blacklist";
                }

                if(uuids_field.length() > 0) {
                    if(jclients.find(uuids_field) == jclients.end()) {
                        logger << "Missing field 'clients." << uuids_field << "' of channel #"
                            << counter << " Channel discarded." << ulib::lerror;
                        ++counter;
                        continue;
                    }

                    nlohmann::json &juuids = jclients[uuids_field];

                    if(!juuids.is_array()) {
                        logger << "Invalid format of field 'clients." << uuids_field
                            << "' of channel #" << counter << " Channel discarded." << ulib::lerror;
                        ++counter;
                        continue;
                    }

                    for(auto &uuids_it : juuids) {
                        if(!uuids_it.is_string()) {
                            logger << "Invalid format of UUID in field 'clients." << uuids_field
                                << "' of channel #" << counter << " UUID discarded."
                                << ulib::lerror;
                            continue;
                        }
                        std::string uuid = uuids_it;
                        descriptor.uuids.push_back(uuid);
                    }
                }
            }

            if(jclients.find("limit") != jclients.end()) {
                nlohmann::json &jlimit = jclients["limit"];
                if(!jlimit.is_number_unsigned()) {
                    logger << "Invalid format of field 'clients.limit" << "' of channel #"
                        << counter << " Channel discarded." << ulib::lerror;
                    ++counter;
                    continue;
                }
                descriptor.max_clients = jlimit;
            }
        }

        if(it.find("port") == it.end()) {
            logger << "Missing field 'port' of channel #" << counter
                << " Channel discarded." << ulib::lerror;
            ++counter;
            continue;
        }

        nlohmann::json &jport = it["port"];
        if(!jport.is_number_integer()) {
            logger << "Invalid format of field 'port' of channel #" << counter
                << " Channel discarded." << ulib::lerror;
            ++counter;
            continue;
        }

        descriptor.port = jport;

        if(it.find("ip_address") != it.end()) {
            nlohmann::json &jip = it["ip_address"];
            if(!jip.is_string()) {
                logger << "Invalid format of field 'ip_address' of channel #" << counter
                    << " Channel discarded." << ulib::lerror;
                ++counter;
                continue;
            }

            descriptor.ip_address = jip;
        }

        _program._channel_descriptors.push_back(descriptor);
    }

    return true;
}

const char *Configuration::DEFAULT_CONFIG_JSON = R"CFGFILE(
{
    "channels": [
        {
            "protocol": "ws",
            "ip_address": "0.0.0.0",
            "port": 8083,
            "clients": {
                "checking": "whitelist",
                "limit": 10,
                "whitelist": [
                    "d5572b97-42e6-49ae-bc74-cfc77e3a2cdd",
                    "d85ee646-73fd-4aa0-b107-3f815c405ba7"
                ]
            }
        }
    ],
    "database": {
        "host": "localhost",
        "name": "vejminek",
        "password": "QaWsEd0987",
        "port": 3306,
        "user": "master"
    }
}
)CFGFILE";