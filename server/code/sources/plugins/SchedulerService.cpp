#include "plugins/SchedulerService.hpp"
#include "lua/ClosureLS.hpp"

SchedulerService::SchedulerService(LuaState &ls, ulib::LoggerConf &logger_cfg)
    : _ls(ls), _logger("-S-> Scheduler", logger_cfg), _task_counter(0)
{
    // Table wrapper for class
    ls.new_table();

    // Register SchedulerService as service
    ls.copy(-1);
    _ref = ls.ref(LUA_REGISTRYINDEX);

    // Metatable for table wrapper
    ls.new_table();

	ls.push("__index"); // handle function for accessing fields
    ls.push(this);
	ls.push(lw__index, 1);
	ls.raw_set(-3);

	ls.push("__newindex"); // handle function for modifying fields
    ls.push(&_logger);
	ls.push(lw__newindex, 1);
	ls.raw_set(-3);

	ls.push("__metatable"); // prevent getting metatable (returns nil)
	ls.push_nil();
	ls.raw_set(-3);

    // Assign metatable to the instance
    ls.set_metatable(-2);

    // Pop table from stack
    ls.pop();
}

void SchedulerService::tick(int64_t time, ulong ticks, ulong tps)
{
    bool done = false;

    while(!done) {
        std::vector<Task> repeated_tasks;
        done = true;

        for(auto it = _tasks.begin(); it != _tasks.end();) {
            if(it->time+it->delay > time)
                break;

            _ls.push(it->ref);
            _ls.raw_get(LUA_REGISTRYINDEX);

            if(_ls.is_function(-1)) {
                if(!_ls.pcall()) {
                    _logger << _ls.to_string(-1) << ulib::lerror;
                    _ls.pop();
                }
            }

            if(it->repeat > 0 || it->repeat == -1) {
                Task new_task;
                new_task.id = it->id;
                new_task.time = it->time + it->delay;
                new_task.delay = it->delay;
                if(it->repeat == -1)
                    new_task.repeat = -1;
                else
                    new_task.repeat = it->repeat - 1;
                new_task.ref = it->ref;
                repeated_tasks.push_back(new_task);
            } else {
                _ls.unref(LUA_REGISTRYINDEX, it->ref);
            }

            it = _tasks.erase(it);
        }

        if(repeated_tasks.size() > 0)
            done = false;

        for(Task &task : repeated_tasks)
            _tasks.insert(task);
    }
}

void SchedulerService::schedule(const Task &task)
{
    _tasks.insert(task);
}

void SchedulerService::cancel(uint id)
{
    for(auto it = _tasks.begin(); it != _tasks.end(); ++it) {
        if((*it).id == id) {
            _tasks.erase(it);
            break;
        }
    }
}

int SchedulerService::lw__index(lua_State* L)
{
    LuaState ls(L);

    ls.push_upvalue(1);
    SchedulerService &serv = ls.to_ref<SchedulerService>(-1);

    std::string index = ls.to_string(2);

    if(index == "schedule") {
        ls.push(&serv);
        ls.push(&(serv._logger));
        ls.push(lw_schedule, 2);
    } else if(index == "cancel") {
        ls.push(&serv);
        ls.push(&(serv._logger));
        ls.push(lw_cancel, 2);
    } else
        ls.push_nil();

    return 1;
}

int SchedulerService::lw__newindex(lua_State *L)
{
    ClosureLS ls(L, 1, "__newindex", ClosureLS::CT_FUNCTION);

    if(!ls.check_protected_fields({"schedule", "cancel"}))
        return 0;

    ls.error_protected_obj();
    return 0;
}

int SchedulerService::lw_schedule(lua_State *L)
{
    ClosureLS ls(L, 2, "schedule", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_object(ClosureLS::TABLE))
        return 0;

    ls.argument_list({ClosureLS::INT, ClosureLS::FUNCTION});
    ls.argument_list({ClosureLS::DOUBLE, ClosureLS::FUNCTION});
    ls.argument_list({ClosureLS::INT, ClosureLS::FUNCTION, ClosureLS::INT});
    ls.argument_list({ClosureLS::DOUBLE, ClosureLS::FUNCTION, ClosureLS::INT});

    int argument_list = ls.check_arguments();
    if(argument_list < 0)
        return 0;

    ls.push_upvalue(1);
    SchedulerService &scheduler = ls.to_ref<SchedulerService>(-1);

    int repeat = 0;

    if(argument_list >= 2)
        repeat = ls.to_int(4);

    long delay_ms = ls.to_long(2);
    if(delay_ms < 0) {
        logger << ls.msg_prefix() << " invalid delay " << ls.msg_postfix() << ulib::lerror;
        ls.push_nil();
        return 1;
    }

    Task task;
    task.id = scheduler._task_counter++;
    task.time = ulib::DateTime::now();
    task.delay = delay_ms;
    task.repeat = repeat;

    ls.copy(3);
    task.ref = ls.ref(LUA_REGISTRYINDEX);

    scheduler.schedule(task);

    ls.push(task.id);
    return 1;
}

int SchedulerService::lw_cancel(lua_State *L)
{
    ClosureLS ls(L, 2, "cancel", ClosureLS::CT_METHOD);

    if(!ls.check_closure(1, ClosureLS::TABLE) || !ls.check_args({ClosureLS::INT}))
        return 0;

    ls.push_upvalue(1);
    SchedulerService &scheduler = ls.to_ref<SchedulerService>(-1);

    uint task_id = ls.to_uint(2);
    scheduler.cancel(task_id);

    return 0;
}
