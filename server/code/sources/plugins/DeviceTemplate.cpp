#include "plugins/DeviceTemplate.hpp"
#include "lua/ClosureLS.hpp"
#include "vejminek/Core.hpp"

#include <new>
#include <cstdio>
#include <algorithm>

DeviceTemplate::DeviceTemplate(const std::string &identifier)
    : _identifier(identifier), _label(identifier),
      _ecb_instantiation(0), _ecb_port_changed(0), _ecb_slot_written(0),
      _ecb_signal_invoked(0), _ecb_tick(0)
{
}

void DeviceTemplate::inject(DeviceTemplateManager &dtm, Sandbox &sb, ulib::Logger &logger)
{
    sb.push_environment();

    // Table wrapper for class
    sb.new_table();

    // Add table to environment
    sb.push("Device");
    sb.copy(-2);
    sb.raw_set(-4);

    // Metatable for table wrapper
    sb.new_table();

	sb.push("__index"); // handle function for accessing fields
    sb.push(&dtm);
    sb.push(&logger);
	sb.push(clw__index, 2);
	sb.raw_set(-3);

	sb.push("__newindex"); // handle function for modifying fields
    sb.push(&logger);
	sb.push(clw__newindex, 1);
	sb.raw_set(-3);

	sb.push("__metatable"); // prevent getting metatable (returns nil)
	sb.push_nil();
	sb.raw_set(-3);

    // Assign metatable to the instance
    sb.set_metatable(-2);

    // Pop table and environment from stack
    sb.pop(2);
}

int DeviceTemplate::clw__index(lua_State* L)
{
    LuaState ls(L);

    ls.push_upvalue(1);
    DeviceTemplateManager &dtm = ls.to_ref<DeviceTemplateManager>(-1);

    ls.push_upvalue(2);
    ulib::Logger &logger = ls.to_ref<ulib::Logger>(-1);

    std::string index = ls.to_string(2);

    if(index == "new") {
        ls.push(&dtm);
        ls.push(&logger);
        ls.push(clw_new, 2);
    } else
        ls.push_nil();

    return 1;
}

int DeviceTemplate::clw__newindex(lua_State *L)
{
    ClosureLS ls(L, 1, "__newindex", ClosureLS::CT_METHOD);

    if(!ls.check_protected_fields({"new"}))
        return 0;

    ls.error_protected_obj();
    return 0;
}

int DeviceTemplate::clw_new(lua_State *L)
{
    ClosureLS ls(L, 2, "new", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_closure({1, 2}, ClosureLS::TABLE)
            || !ls.check_args({ClosureLS::STRING, ClosureLS::STRING}, 1)) {
        ls.push_nil();
        return 1;
    }

    std::string identifier = ls.to_string(2);

    if(!Core::identifier_valid(identifier)) {
        logger << ls.msg_prefix() << " invalid identifier '" << identifier << "' "
            << ls.msg_postfix() << ulib::lerror;
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    DeviceTemplateManager &dtm = ls.to_ref<DeviceTemplateManager>(-1);

    DeviceTemplate *dt = new DeviceTemplate(identifier);
    dtm.register_device_template(dt);

    if(ls.num_args() == 2)
        dt->_label = ls.to_string(3);

    // Table wrapper for instance
    ls.new_table();

    // Metatable for table wrapper
    ls.new_table();

	ls.push("__index"); // handle function for accessing fields
    ls.push(dt);
    ls.push(&logger);
	ls.push(ilw__index, 2);
	ls.raw_set(-3);

	ls.push("__newindex"); // handle function for modifying fields
    ls.push(&logger);
	ls.push(ilw__newindex, 1);
	ls.raw_set(-3);

	ls.push("__metatable"); // prevent getting metatable (returns nil)
	ls.push_nil();
	ls.raw_set(-3);

    // Assign metatable to the instance
    ls.set_metatable(-2);

    return 1;
}

int DeviceTemplate::ilw__index(lua_State* L)
{
    LuaState ls(L);

    ls.push_upvalue(1);
    DeviceTemplate &dt = ls.to_ref<DeviceTemplate>(-1);

    ls.push_upvalue(2);
    ulib::Logger &logger = ls.to_ref<ulib::Logger>(-1);

    std::string index = ls.to_string(2);

    if(index == "label") {
        ls.push(&dt);
        ls.push(&logger);
        ls.push(ilw_label, 2);
    } else if(index == "attribute") {
        ls.push(&dt);
        ls.push(&logger);
        ls.push(ilw_attribute, 2);
    } else if(index == "port") {
        ls.push(&dt);
        ls.push(&logger);
        ls.push(ilw_port, 2);
    } else if(index == "slot") {
        ls.push(&dt);
        ls.push(&logger);
        ls.push(ilw_slot, 2);
    } else if(index == "signal") {
        ls.push(&dt);
        ls.push(&logger);
        ls.push(ilw_signal, 2);
    } else if(index == "action") {
        ls.push(&dt);
        ls.push(&logger);
        ls.push(ilw_action, 2);
    } else if(index == "emitted_event") {
        ls.push(&dt);
        ls.push(&logger);
        ls.push(ilw_emitted_event, 2);
    } else if(index == "on") {
        ls.push(&dt);
        ls.push(&logger);
        ls.push(ilw_on, 2);
    } else
        ls.push_nil();

    return 1;
}

int DeviceTemplate::ilw__newindex(lua_State *L)
{
    ClosureLS ls(L, 1, "__newindex", ClosureLS::CT_METHOD);

    if(!ls.check_protected_fields({
            "label", "attribute", "port", "slot", "signal", "action",
            "emitted_event", "on" }))
        return 0;

    // Assign new value to field
    ls.copy(1);
    ls.copy(2);
    ls.copy(3);
    ls.raw_set(-3);

    return 0;
}

int DeviceTemplate::ilw_label(lua_State *L)
{
    ClosureLS ls(L, 2, "label", ClosureLS::CT_METHOD);

    if(!ls.check_closure(2, ClosureLS::TABLE)
            || !ls.check_args({ClosureLS::STRING}))
        return 0;

    ls.push_upvalue(1);
    DeviceTemplate &dt = ls.to_ref<DeviceTemplate>(-1);

    dt._label = ls.to_string(2);
    return 0;
}

int DeviceTemplate::ilw_attribute(lua_State *L)
{
    // attribute(name, type)
    // attribute(name, label, type)

    ClosureLS ls(L, 2, "attribute", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_object(ClosureLS::TABLE))
        return 0;

    ls.argument_list({ClosureLS::STRING, ClosureLS::INT});
    ls.argument_list({ClosureLS::STRING, ClosureLS::STRING, ClosureLS::INT});

    int argument_list = ls.check_arguments();
    if(argument_list < 0)
        return 0;

    ls.push_upvalue(1);
    DeviceTemplate &dt = ls.to_ref<DeviceTemplate>(-1);

    Attribute attr;
    attr.identifier = ls.to_string(2);
    attr.label = attr.identifier;

    if(argument_list == 0) {
        attr.type = static_cast<DeviceTemplate::Attribute::Type>(ls.to_int(3));
    } else {
        attr.label = ls.to_string(3);
        attr.type = static_cast<DeviceTemplate::Attribute::Type>(ls.to_int(4));
    }

    if(attr.type >= Attribute::_ATTRIBUTE_END_) {
        logger << ls.msg_prefix() << ": invalid ATTRIBUTE_TYPE value "
            << ls.msg_postfix() << ulib::lerror;
        return 0;
    }

    dt.attribute(attr);
    return 0;
}

int DeviceTemplate::ilw_port(lua_State *L)
{
    // port(identifier)
    // port(identifier, label)
    // port(identifier, configuration)
    // port(identifier, label, configuration)

    ClosureLS ls(L, 2, "port", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_closure({1, 2, 3}, ClosureLS::TABLE))
        return 0;

    if(!ls.check_arg(0, ClosureLS::STRING))
        return 0;

    if(ls.num_args() == 2) {
        if(!ls.check_arg(1, {ClosureLS::STRING, ClosureLS::TABLE}))
            return 0;
    }

    if(ls.num_args() == 3) {
        if(!ls.check_arg(1, ClosureLS::STRING))
            return 0;

        if(!ls.check_arg(2, ClosureLS::TABLE))
            return 0;
    }

    // if(!ls.check_closure({1, 2}, ClosureLS::TABLE)
    //         || !ls.check_args({ClosureLS::STRING, ClosureLS::TABLE}, 1))
    //     return 0;

    ls.push_upvalue(1);
    DeviceTemplate &dt = ls.to_ref<DeviceTemplate>(-1);
    std::string port_identifier = ls.to_string(2);

    Port port;
    port.identifier = port_identifier;
    port.label = port_identifier;
    port.direction = Port::Direction::INPUT;
    port.mode = Port::Mode::LOGICAL;
    port.optional = false;
    port.default_value = 0;
    port.value_stored = false;

    int cfg_pos = -1;
    if(ls.num_args() == 2) {
        if(ls.is_string(3))
            port.label = ls.to_string(3);
        else
            cfg_pos = 3;
    } else if(ls.num_args() == 3) {
        port.label = ls.to_string(3);
        cfg_pos = 4;
    }

    if(cfg_pos != -1) {
        if(ls.get_field(cfg_pos, "label") != LUA_TNIL) {
            if(!ls.is_string(-1)) {
                logger << ls.msg_prefix()
                    << " parameter 'label' must be a string, port '"
                    << port_identifier << "' discarded " << ls.msg_postfix() << ulib::lerror;
                return 0;
            }

            port.label = ls.to_string(-1);
        }
        ls.pop();

        if(ls.get_field(cfg_pos, "direction") != LUA_TNIL) {
            if(!ls.is_int(-1)) {
                logger << ls.msg_prefix()
                    << " parameter 'direction' must be a PORT_DIRECTION value, port '"
                    << port_identifier << "' discarded " << ls.msg_postfix() << ulib::lerror;
                return 0;
            }

            int dir = ls.to_int(-1);
            if(dir >= Port::Direction::_DIRECTION_END_) {
                logger << ls.msg_prefix()
                    << " parameter 'direction' is not a valid PORT_DIRECTION value, port '"
                    << port_identifier << "' discarded " << ls.msg_postfix() << ulib::lerror;
                return 0;
            }

            port.direction = static_cast<DeviceTemplate::Port::Direction>(dir);
        }
        ls.pop();

        if(ls.get_field(cfg_pos, "mode") != LUA_TNIL) {
            if(!ls.is_int(-1)) {
                logger << ls.msg_prefix()
                    << " parameter 'mode' must be a PORT_MODE value, port '"
                    << port_identifier << "' discarded " << ls.msg_postfix() << ulib::lerror;
                return 0;
            }

            int mode = ls.to_int(-1);
            if(mode >= Port::Mode::_MODE_END_) {
                logger << ls.msg_prefix()
                    << " parameter 'mode' is not a valid PORT_MODE value, port '"
                    << port_identifier << "' discarded " << ls.msg_postfix() << ulib::lerror;
                return 0;
            }

            port.mode = static_cast<DeviceTemplate::Port::Mode>(mode);
        }
        ls.pop();

        if(ls.get_field(cfg_pos, "optional") != LUA_TNIL) {
            if(!ls.is_bool(-1)) {
                logger << ls.msg_prefix()
                    << " parameter 'optional' must be a boolean value, port '"
                    << port_identifier << "' discarded " << ls.msg_postfix() << ulib::lerror;
                return 0;
            }

            port.optional = ls.to_bool(-1);
        }
        ls.pop();

        if(ls.get_field(cfg_pos, "default_value") != LUA_TNIL) {
            if(!ls.is_double(-1)) {
                logger << ls.msg_prefix()
                    << " parameter 'default_value' must be a numerical value, port '"
                    << port_identifier << "' discarded " << ls.msg_postfix() << ulib::lerror;
                return 0;
            }

            float default_value = ls.to_double(-1);
            bool invalid =
                port.mode == Port::Mode::LOGICAL && default_value != 0 && default_value != 1;
            if(invalid) {
                logger << ls.msg_prefix()
                    << " parameter 'default_value' is not a valid LOGICAL value, port '"
                    << port_identifier << "' discarded " << ls.msg_postfix() << ulib::lerror;
                return 0;
            }

            port.default_value = default_value;
        }
        ls.pop();

        if(ls.get_field(cfg_pos, "value_stored") != LUA_TNIL) {
            if(!ls.is_bool(-1)) {
                logger << ls.msg_prefix()
                    << " parameter 'value_stored' must be a boolean value, port '"
                    << port_identifier << "' discarded " << ls.msg_postfix() << ulib::lerror;
                return 0;
            }

            port.value_stored = ls.to_bool(-1);
        }
        ls.pop();
    }

    dt.port(port);

    return 0;
}

int DeviceTemplate::ilw_slot(lua_State *L)
{
    // slot(identifier, direction, type)
    // or slot(identifier, label, direction, type)

    ClosureLS ls(L, 2, "slot", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_object(ClosureLS::TABLE))
        return 0;

    ls.argument_list({ClosureLS::STRING, ClosureLS::INT, ClosureLS::INT});
    ls.argument_list({ClosureLS::STRING, ClosureLS::STRING, ClosureLS::INT, ClosureLS::INT});

    int argument_list = ls.check_arguments();
    if(argument_list < 0)
        return 0;

    ls.push_upvalue(1);
    DeviceTemplate &dt = ls.to_ref<DeviceTemplate>(-1);

    Slot slot;
    slot.identifier = ls.to_string(2);
    slot.label = slot.identifier;

    int dir_offs = 3;

    if(argument_list == 1) {
        slot.label = ls.to_string(3);
        dir_offs = 4;
    }

    uint direction = ls.to_uint(dir_offs);

    if(direction >= DeviceTemplate::Slot::_DIRECTION_END_) {
        logger << ls.msg_prefix() << " invalid direction of slot '"
            << slot.identifier << "', slot discarded " << ls.msg_postfix()
            << ulib::lerror;
        return 0;
    }

    uint type = ls.to_uint(dir_offs+1);

    if(type >= DeviceTemplate::Slot::_TYPE_END_) {
        logger << ls.msg_prefix() << " invalid type of slot '"
            << slot.identifier << "', slot discarded " << ls.msg_postfix()
            << ulib::lerror;
        return 0;
    }

    slot.direction = static_cast<DeviceTemplate::Slot::Direction>(direction);
    slot.type = static_cast<DeviceTemplate::Slot::Type>(type);

    dt.slot(slot);
    return 0;
}

int DeviceTemplate::ilw_signal(lua_State *L)
{
    // signal(identifier, direction)
    // or signal(identifier, label, direction)

    ClosureLS ls(L, 2, "signal", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_object(ClosureLS::TABLE))
        return 0;

    ls.argument_list({ClosureLS::STRING, ClosureLS::INT});
    ls.argument_list({ClosureLS::STRING, ClosureLS::STRING, ClosureLS::INT});

    int argument_list = ls.check_arguments();
    if(argument_list < 0)
        return 0;

    ls.push_upvalue(1);
    DeviceTemplate &dt = ls.to_ref<DeviceTemplate>(-1);

    Signal sig;
    sig.identifier = ls.to_string(2);
    sig.label = sig.identifier;

    int dir_offs = 3;

    if(argument_list == 1) {
        sig.label = ls.to_string(3);
        dir_offs = 4;
    }

    uint direction = ls.to_uint(dir_offs);

    if(direction >= DeviceTemplate::Signal::_DIRECTION_END_) {
        logger << ls.msg_prefix() << " invalid direction of signal '"
            << sig.identifier << "', signal discarded " << ls.msg_postfix()
            << ulib::lerror;
        return 0;
    }

    sig.direction = static_cast<DeviceTemplate::Signal::Direction>(direction);

    dt.signal(sig);
    return 0;
}

int DeviceTemplate::ilw_action(lua_State *L)
{
    // action(identifier, arguments, callback)
    // action(identifier, callback)
    // action(identifier, label, arguments, callback)
    // action(identifier, label, callback)

    ClosureLS ls(L, 2, "action", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_object(ClosureLS::TABLE))
        return 0;

    ls.argument_list({ClosureLS::STRING, ClosureLS::TABLE, ClosureLS::FUNCTION});
    ls.argument_list({ClosureLS::STRING, ClosureLS::FUNCTION});
    ls.argument_list({ClosureLS::STRING, ClosureLS::STRING, ClosureLS::TABLE, ClosureLS::FUNCTION});
    ls.argument_list({ClosureLS::STRING, ClosureLS::STRING, ClosureLS::FUNCTION});

    int argument_list = ls.check_arguments();
    if(argument_list < 0)
        return 0;

    Action action;
    action.identifier = ls.to_string(2);
    action.label = action.identifier;

    int args_ptr = 0;
    int cb_ptr = 0;

    if(argument_list == 0) {
        args_ptr = 3;
        cb_ptr = 4;
    } else if(argument_list == 1) {
        cb_ptr = 3;
    } else if(argument_list == 2) {
        action.label = ls.to_string(3);
        args_ptr = 4;
        cb_ptr = 5;
    } else {
        action.label = ls.to_string(3);
        cb_ptr = 4;
    }

    ls.push_upvalue(1);
    DeviceTemplate &dt = ls.to_ref<DeviceTemplate>(-1);

    if(args_ptr) {
        ls.copy(args_ptr);
        ls.push_nil();

        while(ls.next(-2)) {

            if(!ls.is_int(-2) || !ls.is_table(-1)) {
                logger << ls.msg_prefix() << " invalid input argument "
                    << ls.msg_postfix() << ulib::lerror;
                ls.pop();
                continue;
            }

            DeviceTemplate::Action::Argument arg;

            if(ls.get_field(-1, "identifier") == LUA_TNIL) {
                logger << ls.msg_prefix() << " missing parameter 'identifier', "
                    "action discarded " << ls.msg_postfix() << ulib::lerror;
                return 0;
            }

            if(!ls.is_string(-1)) {
                logger << ls.msg_prefix() <<" parameter 'identifier' must be a string "
                    "value, action discarded " << ls.msg_postfix() << ulib::lerror;
                return 0;
            }

            arg.identifier = ls.to_string(-1);
            arg.label = arg.identifier;
            ls.pop();

            if(ls.get_field(-1, "label") != LUA_TNIL) {
                if(!ls.is_string(-1)) {
                    logger << ls.msg_prefix() <<" parameter 'label' must be a string "
                        "value, action discarded " << ls.msg_postfix() << ulib::lerror;
                    return 0;
                }

                arg.label = ls.to_string(-1);
            }
            ls.pop();

            if(ls.get_field(-1, "type") == LUA_TNIL) {
                logger << ls.msg_prefix() << " missing parameter 'type', "
                    "action discarded " << ls.msg_postfix() << ulib::lerror;
                return 0;
            }

            if(!ls.is_int(-1)) {
                logger << ls.msg_prefix() <<" parameter 'type' must be an ARGUMENT_TYPE "
                    "value, action discarded " << ls.msg_postfix() << ulib::lerror;
                return 0;
            }

            int arg_type = ls.to_int(-1);
            ls.pop();

            if(arg_type < 0 || arg_type >= DeviceTemplate::Action::Argument::_TYPE_END_) {
                logger << ls.msg_prefix() << " argument '" << arg.identifier
                    << "' has invalid ARGUMENT_TYPE value, action discarded "
                    << ls.msg_postfix() << ulib::lerror;
                return 0;
            }

            arg.type = (DeviceTemplate::Action::Argument::Type)arg_type;

            if(ls.get_field(-1, "default_value") != LUA_TNIL) {
                if(arg.type == DeviceTemplate::Action::Argument::NUMBER) {
                    if(!ls.is_double(-1)) {
                        logger << ls.msg_prefix() <<" parameter 'default_value' must be a numeric "
                            "value, action discarded " << ls.msg_postfix() << ulib::lerror;
                        return 0;
                    }
                    arg.default_value = std::make_any<double>(ls.to_double(-1));
                } else if(arg.type == DeviceTemplate::Action::Argument::BOOLEAN) {
                    if(!ls.is_bool(-1)) {
                        logger << ls.msg_prefix() <<" parameter 'default_value' must be a boolean "
                            "value, action discarded " << ls.msg_postfix() << ulib::lerror;
                        return 0;
                    }
                    arg.default_value = std::make_any<bool>(ls.to_bool(-1));
                } else if(arg.type == DeviceTemplate::Action::Argument::STRING) {
                    if(!ls.is_string(-1)) {
                        logger << ls.msg_prefix() <<" parameter 'default_value' must be a string "
                            "value, action discarded " << ls.msg_postfix() << ulib::lerror;
                        return 0;
                    }
                    arg.default_value = std::make_any<std::string>(ls.to_string(-1));
                }
            } else {
                if(arg.type == DeviceTemplate::Action::Argument::NUMBER)
                    arg.default_value = std::make_any<double>(0);
                else if(arg.type == DeviceTemplate::Action::Argument::BOOLEAN)
                    arg.default_value = std::make_any<bool>(false);
                else if(arg.type == DeviceTemplate::Action::Argument::STRING)
                    arg.default_value = std::make_any<std::string>("");
            }
            ls.pop();

            action.arguments.push_back(arg);
            ls.pop();
        }
        ls.pop(2);
    }

    ls.copy(cb_ptr);
    action.reference = ls.ref(LUA_REGISTRYINDEX);

    dt.action(action);
    return 0;
}

int DeviceTemplate::ilw_emitted_event(lua_State *L)
{
    // emitted_event(identifier)
    // emitted_event(identifier, label)

    ClosureLS ls(L, 2, "emitted_event", ClosureLS::CT_METHOD);

    if(!ls.check_object(ClosureLS::TABLE))
        return 0;

    ls.argument_list({ClosureLS::STRING});
    ls.argument_list({ClosureLS::STRING, ClosureLS::STRING});

    int argument_list = ls.check_arguments();
    if(argument_list < 0)
        return 0;

    ls.push_upvalue(1);
    DeviceTemplate &dt = ls.to_ref<DeviceTemplate>(-1);

    Event ev;
    ev.identifier = ls.to_string(2);
    ev.label = ev.identifier;

    if(argument_list == 1)
        ev.label = ls.to_string(3);

    dt.emitted_event(ev);
    return 0;
}

int DeviceTemplate::ilw_on(lua_State *L)
{
    ClosureLS ls(L, 2, "on", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_closure(2, ClosureLS::TABLE)
            || !ls.check_args({ClosureLS::STRING, ClosureLS::FUNCTION}))
        return 0;

    ls.push_upvalue(1);
    DeviceTemplate &dt = ls.to_ref<DeviceTemplate>(-1);

    std::string event_name = ls.to_string(2);

    if(event_name == "instantiation") {
        ls.copy(3);
        dt._ecb_instantiation = ls.ref(LUA_REGISTRYINDEX);
    } else if(event_name == "port_changed") {
        ls.copy(3);
        dt._ecb_port_changed = ls.ref(LUA_REGISTRYINDEX);
    } else if(event_name == "slot_written") {
        ls.copy(3);
        dt._ecb_slot_written = ls.ref(LUA_REGISTRYINDEX);
    } else if(event_name == "signal_invoked") {
        ls.copy(3);
        dt._ecb_signal_invoked = ls.ref(LUA_REGISTRYINDEX);
    } else if(event_name == "tick") {
        ls.copy(3);
        dt._ecb_tick = ls.ref(LUA_REGISTRYINDEX);
    } else {
        logger << ls.msg_prefix() << " unknown event '" << event_name << "' "
            << ls.msg_postfix() << ulib::lwarn;
    }

    return 0;
}

void DeviceTemplate::attribute(const Attribute &attr)
{
    for(size_t i = 0; i < _attributes.size(); ++i) {
        if(_attributes[i].identifier == attr.identifier) {
            _attributes[i] = attr;
            return;
        }
    }
    _attributes.push_back(attr);
}

void DeviceTemplate::port(const Port &port)
{
    for(size_t i = 0; i < _ports.size(); ++i) {
        if(_ports[i].identifier == port.identifier) {
            _ports[i] = port;
            return;
        }
    }
    _ports.push_back(port);
}

void DeviceTemplate::slot(const Slot &slot)
{
    for(size_t i = 0; i < _slots.size(); ++i) {
        if(_slots[i].identifier == slot.identifier) {
            _slots[i] = slot;
            return;
        }
    }
    _slots.push_back(slot);
}

void DeviceTemplate::signal(const Signal &signal)
{
    for(size_t i = 0; i < _signals.size(); ++i) {
        if(_signals[i].identifier == signal.identifier) {
            _signals[i] = signal;
            return;
        }
    }
    _signals.push_back(signal);
}

void DeviceTemplate::action(const Action &act)
{
    for(size_t i = 0; i < _actions.size(); ++i) {
        if(_actions[i].identifier == act.identifier) {
            _actions[i] = act;
            return;
        }
    }
    _actions.push_back(act);
}

void DeviceTemplate::emitted_event(const Event &event)
{
    for(size_t i = 0; i < _emitted_events.size(); ++i) {
        if(_emitted_events[i].identifier == event.identifier) {
            _emitted_events[i] = event;
            return;
        }
    }
    _emitted_events.push_back(event);
}

nlohmann::json DeviceTemplate::Attribute::serialize()
{
    nlohmann::json jattr;

    jattr["identifier"] = identifier;
    jattr["label"] = label;

    switch(type) {
        case NUMBER:
            jattr["type"] = "number";
            break;
        case BOOLEAN:
            jattr["type"] = "boolean";
            break;
        case STRING:
            jattr["type"] = "string";
            break;
        case TIME:
            jattr["type"] = "time";
            break;
        case CONDITION:
            jattr["type"] = "condition";
            break;
        default:
            jattr["type"] = "unknown";
            break;
    }

    return jattr;
}

nlohmann::json DeviceTemplate::Port::serialize()
{
    nlohmann::json jport;
    jport["identifier"] = identifier;
    jport["label"] = label;
    jport["direction"] = direction == OUTPUT ? "output" : "input";
    jport["mode"] = mode == LOGICAL ? "logical" : "discrete";
    jport["optional"] = optional;
    jport["default_value"] = default_value;
    jport["value_stored"] = value_stored;
    return jport;
}

nlohmann::json DeviceTemplate::Slot::serialize()
{
    nlohmann::json jslot;

    jslot["identifier"] = identifier;
    jslot["label"] = label;
    jslot["direction"] = direction == DRIVER_OUT ? "driver_out" : "driver_in";

    switch(type) {
        case NUMBER:
            jslot["type"] = "number";
            break;
        case BOOLEAN:
            jslot["type"] = "boolean";
            break;
        case STRING:
            jslot["type"] = "string";
            break;
        default:
            jslot["type"] = "unknown";
            break;
    }

    return jslot;
}

nlohmann::json DeviceTemplate::Signal::serialize()
{
    nlohmann::json jsignal;
    jsignal["identifier"] = identifier;
    jsignal["label"] = label;
    jsignal["direction"] = direction == DRIVER_OUT ? "driver_out" : "driver_in";
    return jsignal;
}

nlohmann::json DeviceTemplate::Event::serialize()
{
    nlohmann::json jevent;
    jevent["identifier"] = identifier;
    jevent["label"] = label;
    return jevent;
}

nlohmann::json DeviceTemplate::Action::Argument::serialize()
{
    nlohmann::json jarg;

    jarg["identifier"] = identifier;
    jarg["label"] = label;

    switch(type) {
        case NUMBER:
            jarg["type"] = "number";
            jarg["default_value"] = std::any_cast<double>(default_value);
            break;
        case BOOLEAN:
            jarg["type"] = "boolean";
            jarg["default_value"] = std::any_cast<bool>(default_value);
            break;
        case STRING:
            jarg["type"] = "string";
            jarg["default_value"] = std::any_cast<std::string>(default_value);
            break;
        default:
            jarg["type"] = "unknown";
            break;
    }

    return jarg;
}

nlohmann::json DeviceTemplate::Action::serialize()
{
    nlohmann::json jaction;

    jaction["identifier"] = identifier;
    jaction["label"] = label;

    nlohmann::json jargs = nlohmann::json::array();
    for(Argument &arg : arguments)
        jargs.push_back(arg.serialize());
    jaction["arguments"] = jargs;

    return jaction;
}

nlohmann::json DeviceTemplate::serialize_reduced()
{
    nlohmann::json jdt;

    jdt["identifier"] = _identifier;
    jdt["label"] = _label;

    nlohmann::json jports = nlohmann::json::array();
    for(Port &p : _ports)
        jports.push_back(p.serialize());
    jdt["ports"] = jports;

    nlohmann::json jactions = nlohmann::json::array();
    for(Action &a : _actions)
        jactions.push_back(a.serialize());
    jdt["actions"] = jactions;

    return jdt;
}