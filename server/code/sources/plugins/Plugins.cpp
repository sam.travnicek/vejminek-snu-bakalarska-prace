#include "plugins/Plugins.hpp"
#include <ulib/Logger.hpp>
#include <ulib/System.hpp>
#include <ulib/StringUtil.hpp>
#include <algorithm>
#include "Database.hpp"

using namespace ulib;

extern "C" {
#include <dirent.h>
}

Plugins::Plugins(
        LuaState &ls, const std::string &path, const LoggerConf &logger_cgf,
        SchedulerService &scheduler, ulib::net::MySQL &mysql)
    : _logger("Plugins", logger_cgf), _mysql(mysql)
{
    struct dirent *entry = nullptr;
    DIR *dp = nullptr;

    dp = opendir(path.c_str());
    if(!dp) {
        _logger << "Failed to open plugins' directory." << ulib::lerror;
        return;
    }

    while((entry = readdir(dp))) {
        std::string dirname(entry->d_name);
        if(dirname == "." || dirname == "..")
            continue;
        std::string plugin_dir = path + "/" + dirname;
        if(!System::dir_accessible(plugin_dir))
            continue;
        std::string input_file = plugin_dir + "/plugin.lua";
        if(!System::file_readable(input_file))
            continue;

        _logger << "Plugin found at '" << plugin_dir << "'\n" << ulib::linfo;

        Plugin *plugin = new Plugin(ls, plugin_dir, logger_cgf, scheduler);

        if(!plugin->valid()) {
            _logger << "Failed to load plugin." << ulib::lerror;
            delete plugin;
            continue;
        }

        if(!plugin->loaded()) {
            _logger << "Failed to load plugin '" << plugin->identifier() << "'." << ulib::lerror;
            delete plugin;
            continue;
        }

        _logger << "Plugin '" << plugin->identifier() << "' ";
        if(plugin->version().empty())
            _logger << "(unknown version)";
        else
            _logger << "v" << plugin->version();
        _logger << " by " << StringUtil::empty_or(plugin->author(), "unknown author") << " loaded.";
        _logger.info();
        _plugins.push_back(plugin);
    }

    std::vector<std::string> ids_to_erase;

    for(auto it = _plugins.begin(); it != _plugins.end(); ++it) {
        for(auto it2 = _plugins.begin(); it2 != _plugins.end(); ++it2) {
            if(*it != *it2 && (*it2)->identifier() == (*it)->identifier()) {
                std::string id = (*it)->identifier();
                if(std::find(ids_to_erase.begin(), ids_to_erase.end(), id)
                        == ids_to_erase.end())
                    ids_to_erase.push_back(id);
                break;
            }
        }
    }

    for(std::string &id : ids_to_erase) {
        _logger << "Collision of plugins' identifiers '" << id << "'" << ulib::lerror;
        _logger << "Plugins disabled." << ulib::lwarn;

        for(auto it = _plugins.begin(); it != _plugins.end();) {
            if((*it)->identifier() == id) {
                Plugin *p = *it;
                it = _plugins.erase(it);
                delete (p);
                continue;
            }
            ++it;
        }
    }

    closedir(dp);
}

Plugins::~Plugins()
{
    for(Plugin *plugin : _plugins)
        delete plugin;
}

void Plugins::init()
{
    for(Plugin *plugin : _plugins)
        plugin->init();

    _logger << "Filling the database..." << ulib::linfo;
    for(Plugin *plugin : _plugins)
        Database::insert(_mysql, *plugin);
    _logger << "Done." << ulib::linfo;
}

void Plugins::tick(int64_t time, ulong ticks, ulong tps)
{
    for(Plugin *plugin : _plugins)
        plugin->tick(time, ticks, tps);
}

void Plugins::ports_linkage_complete()
{
    for(Plugin *plugin : _plugins)
        plugin->ports_linkage_complete();
}

void Plugins::slots_linkage_complete()
{
    for(Plugin *plugin : _plugins)
        plugin->slots_linkage_complete();
}

void Plugins::signals_linkage_complete()
{
    for(Plugin *plugin : _plugins)
        plugin->signals_linkage_complete();
}

DeviceTemplate* Plugins::find_template(
    const std::string &plugin_identifier, const std::string &template_identifier)
{
    for(Plugin *p : _plugins) {
        if(p->identifier() == plugin_identifier) {
            for(DeviceTemplate *dt : p->device_templates())
                if(dt->identifier() == template_identifier)
                    return dt;
            return nullptr;
        }
    }
    return nullptr;
}

Driver* Plugins::find_driver(
    const std::string &plugin_identifier, const std::string &driver_identifier)
{
    for(Plugin *p : _plugins) {
        if(p->identifier() == plugin_identifier) {
            for(Driver *driver : p->drivers())
                if(driver->identifier() == driver_identifier)
                    return driver;
            return nullptr;
        }
    }
    return nullptr;
}

std::vector<DeviceTemplate*> Plugins::device_templates()
{
    std::vector<DeviceTemplate*> templates;

    for(Plugin *p : _plugins) {
        const std::vector<DeviceTemplate*> &dt = p->device_templates();
        templates.insert(templates.end(), dt.begin(), dt.end());
    }

    return templates;
}