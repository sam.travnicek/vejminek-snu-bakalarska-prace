#include "plugins/DevicesService.hpp"
#include "lua/GlobalEnvironment.hpp"
#include "lua/ClosureLS.hpp"

DevicesService::DevicesService(LuaState &ls, ulib::LoggerConf &logger_cfg, DeviceManager &dm)
    : _logger("-S-> Devices", logger_cfg), _dm(dm)
{
    // Table wrapper for class
    ls.new_table();

    // Register SchedulerService as service
    ls.copy(-1);
    _reference = ls.ref(LUA_REGISTRYINDEX);

    // Metatable for table wrapper
    ls.new_table();

	ls.push("__index"); // handle function for accessing fields
    ls.push(this);
	ls.push(lw__index, 1);
	ls.raw_set(-3);

	ls.push("__newindex"); // handle function for modifying fields
    ls.push(&_logger);
	ls.push(lw__newindex, 1);
	ls.raw_set(-3);

	ls.push("__metatable"); // prevent getting metatable (returns nil)
	ls.push_nil();
	ls.raw_set(-3);

    // Assign metatable to the instance
    ls.set_metatable(-2);

    // Pop table from stack
    ls.pop();
}

int DevicesService::lw__index(lua_State* L)
{
    LuaState ls(L);

    ls.push_upvalue(1);
    DevicesService &serv = ls.to_ref<DevicesService>(-1);

    std::string index = ls.to_string(2);

    if(index == "find") {
        ls.push(&serv);
        ls.push(&(serv._logger));
        ls.push(lw_find, 2);
    } else
        ls.push_nil();

    return 1;
}

int DevicesService::lw__newindex(lua_State *L)
{
    ClosureLS ls(L, 1, "__newindex", ClosureLS::CT_METHOD);

    if(!ls.check_protected_fields({"find"}))
        return 0;

    ls.error_protected_obj();
    return 0;
}

int DevicesService::lw_find(lua_State *L)
{
    ClosureLS ls(L, 2, "find", ClosureLS::CT_METHOD);

    if(!ls.check_closure(1, ClosureLS::TABLE))
        return 0;

    if(!ls.check_arg(0, {ClosureLS::INT, ClosureLS::STRING}))
        return 0;

    ls.push_upvalue(1);
    DevicesService &serv = ls.to_ref<DevicesService>(-1);
    DeviceManager &dm = serv._dm;

    Device *dev = nullptr;

    if(ls.is_int(2)) {
        int id = ls.to_int(2);
        if(id >= 0)
            dev = dm.device((unsigned)id);
    } else {
        std::string name = ls.to_string(2);
        dev = dm.device(name);
    }

    if(!dev) {
        ls.push_nil();
        return 1;
    }

    GlobalEnvironment::get(ls, dev);
    return 1;
}
