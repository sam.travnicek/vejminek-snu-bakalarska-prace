#include "plugins/Plugin.hpp"
#include "plugins/DriverLUA.hpp"
#include "lua/SerialPortLUA.hpp"
#include "lua/RequirementSandbox.hpp"
#include "lua/GlobalEnvironment.hpp"
#include "lua/ClosureLS.hpp"
#include "vejminek/Core.hpp"
#include <ulib/System.hpp>
#include <ulib/StringUtil.hpp>
#include <algorithm>

const std::string Plugin::DEFAULT_ENTRY_FILE = "main.lua";

Plugin::Plugin(
        LuaState &ls, const std::string &plugin_dir, const ulib::LoggerConf &logger_cfg,
        SchedulerService &scheduler)
    : Sandbox(ls), _logger("Plugins[?]", logger_cfg), _directory(plugin_dir),
      _invalid(false), _loaded(false), _identifier(""), _label(""), _version(""), _author(""),
      _description(""), _entry_file(DEFAULT_ENTRY_FILE), _ecb_tick(0), _ecb_init(0)
{
    // Prepare plugin's environment:
    load_safe_tools();
    load_logging_tools(_logger);
    load_file_tools(_logger, plugin_dir);
    load_module_tools(_logger, plugin_dir, *this);
    load_G();
    GlobalEnvironment::load_globals(*this);
    GlobalEnvironment::load_tools(*this);
    DeviceTemplate::inject(*this, *this, _logger);
    DriverLUA::inject(*this, *this, _logger);
    SerialPortLUA::inject(*this, _logger);

    push_environment();

    // Table wrapper for Plugin
    new_table();

    // Make table public in environment
    copy(-1);
    set_field(-3, "Plugin");

    // Metatable for table wrapper
    new_table();

	push("__index"); // handle function for accessing fields
    push(this);
	push(clw__index, 1);
	raw_set(-3);

	push("__newindex"); // handle function for modifying fields
    push(&_logger);
	push(clw__newindex, 1);
	raw_set(-3);

	push("__metatable"); // prevent getting metatable (returns nil)
	push_nil();
	raw_set(-3);

    // Assign metatable to the instance
    set_metatable(-2);

    // Pop table from stack
    pop();

    // Global functions
    push(&_logger);
    push(scheduler.reference());
    push(lw_service, 2);
    set_field(-2, "service");

    push(&_logger);
    push(this);
    push(lw_include, 2);
    set_field(-2, "include");

    // Pop environment
    pop();

    // Load plugin.lua environment:
    LuaState cfg_env;

    if(!cfg_env.run_file(_directory+"/plugin.lua")) {
        _logger << cfg_env.to_string(-1) << ulib::lerror;
        _invalid = true;
        return;
    }

    cfg_env.push_global_table();
    cfg_env.push_nil();

    while(cfg_env.next(-2)) {
        std::string key = cfg_env.to_string(-2);

        if(key == "identifier") {
            if(cfg_env.is_string(-1))
                _identifier = cfg_env.to_string(-1);
            else
                _logger << _directory << "/plugin.lua: value of '" << key
                    << "' must be a string" << ulib::lwarn;
        } else if(key == "label") {
            if(cfg_env.is_string(-1))
                _label = cfg_env.to_string(-1);
            else
                _logger << _directory << "/plugin.lua: value of '" << key
                    << "' must be a string" << ulib::lwarn;
        } else if(key == "version") {
            if(cfg_env.is_string(-1))
                _version = cfg_env.to_string(-1);
            else
                _logger << _directory << "/plugin.lua: value of '" << key
                    << "' must be a string" << ulib::lwarn;
        } else if(key == "author") {
            if(cfg_env.is_string(-1))
                _author = cfg_env.to_string(-1);
            else
                _logger << _directory << "/plugin.lua: value of '" << key
                    << "' must be a string" << ulib::lwarn;
        } else if(key == "description") {
            if(cfg_env.is_string(-1))
                _description = cfg_env.to_string(-1);
            else
                _logger << _directory << "/plugin.lua: value of '" << key
                    << "' must be a string" << ulib::lwarn;
        } else if(key == "entry_file") {
            if(cfg_env.is_string(-1))
                _entry_file = cfg_env.to_string(-1);
            else
                _logger << _directory << "/plugin.lua: value of '" << key
                    << "' must be a string" << ulib::lwarn;
        }

        cfg_env.pop();
    }

    cfg_env.pop();

    if(!Core::identifier_valid(_identifier)) {
        _logger << "Invalid plugin identifier '" << _identifier << "' at '" << _directory << "'."
            << ulib::lerror;
        return;
    }

    if(_label == "")
        _label = _identifier;

    _logger.name("Plugins[" + _identifier + "]");

    std::string entry_path = _directory + "/" + _entry_file;

    if(!ulib::System::file_readable(entry_path)) {
        _logger << "Failed to open entry file!" << ulib::lerror;
        return;
    }

    if(!environment_file(entry_path)) {
        _logger << to_string(-1) << ulib::lerror;
        pop();
        _invalid = true;
        return;
    }

    _loaded = true;
}

Plugin::~Plugin()
{
    for(DeviceTemplate *dt : _dts)
        delete dt;

    for(Driver *driver : _drivers)
        delete driver;
}

bool Plugin::init()
{
    if(_invalid || !_loaded)
        return false;

    if(_ecb_init == 0)
        return false;

    push(_ecb_init);
    raw_get(LUA_REGISTRYINDEX);

    if(!pcall()) {
        _logger << to_string(-1) << ulib::lerror;
        pop();
        return false;
    }

    return true;
}

void Plugin::tick(int64_t time, ulong ticks, ulong tps)
{
    for(Driver *driver : _drivers)
        driver->tick(time, ticks, tps);

    if(_invalid || !_loaded)
        return;

    if(_ecb_tick == 0)
        return;

    push(_ecb_tick);
    raw_get(LUA_REGISTRYINDEX);

    push(time);
    push(ticks);
    push(tps);

    if(!pcall(3)) {
        _logger << to_string(-1) << ulib::lerror;
        pop();
    }
}

void Plugin::ports_linkage_complete()
{
    if(_invalid || !_loaded)
        return;

    for(Driver *driver : _drivers)
        driver->ports_linkage_complete();
}

void Plugin::slots_linkage_complete()
{
    if(_invalid || !_loaded)
        return;

    for(Driver *driver : _drivers)
        driver->slots_linkage_complete();
}

void Plugin::signals_linkage_complete()
{
    if(_invalid || !_loaded)
        return;

    for(Driver *driver : _drivers)
        driver->signals_linkage_complete();
}

void Plugin::register_device_template(DeviceTemplate *dt)
{
    if(std::find(_dts.begin(), _dts.end(), dt) == _dts.end())
        _dts.push_back(dt);
}

void Plugin::register_driver(Driver *driver)
{
    for(Driver *d : _drivers) {
        if(d->identifier() == driver->identifier()) {
            delete driver;
            return;
        }
    }

    _drivers.push_back(driver);
}

Driver* Plugin::driver(const std::string &identifier)
{
    for(Driver *driver : _drivers)
        if(driver->identifier() == identifier)
            return driver;
    return nullptr;
}

int Plugin::clw__index(lua_State* L)
{
    LuaState ls(L);

    ls.push_upvalue(1);
    Plugin &plugin = ls.to_ref<Plugin>(-1);
    ulib::Logger &logger = plugin._logger;

    std::string index = ls.to_string(2);

    if(index == "on") {
        ls.push(&plugin);
        ls.push(&logger);
        ls.push(clw_on, 2);
    } else if(
            index == "identifier" || index == "label" || index == "version"
            || index == "author" || index == "description"
            || index == "entry_file" || index == "directory") {
        ls.push(&plugin);
        ls.push(&logger);
        ls.push(index);
        ls.push(clw_member, 3);
    } else
        ls.push_nil();

    return 1;
}

int Plugin::clw__newindex(lua_State *L)
{
    ClosureLS ls(L, 1, "__newindex", ClosureLS::CT_METHOD);

    if(!ls.check_protected_fields({
            "on", "identifier", "label", "version", "author", "description",
            "entry_file", "directory" }))
        return 0;

    ls.error_protected_obj();
    return 0;
}

int Plugin::clw_on(lua_State *L)
{
    ClosureLS ls(L, 2, "on", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_closure(2, ClosureLS::TABLE)
            || !ls.check_args({ClosureLS::STRING, ClosureLS::FUNCTION}))
        return 0;

    ls.push_upvalue(1);
    Plugin &plugin = ls.to_ref<Plugin>(-1);

    std::string event_name = ls.to_string(2);

    if(event_name == "tick") {
        ls.copy(3);
        plugin._ecb_tick = ls.ref(LUA_REGISTRYINDEX);
    } else if (event_name == "initialization") {
        ls.copy(3);
        plugin._ecb_init = ls.ref(LUA_REGISTRYINDEX);
    } else
        logger << ls.msg_prefix() << " unknown event '" << event_name << "' "
            << ls.msg_postfix() << ulib::lerror;

    return 0;
}

int Plugin::clw_member(lua_State *L)
{
    ClosureLS ls(L, 2, "", ClosureLS::CT_METHOD);

    ls.push_upvalue(1);
    Plugin &plugin = ls.to_ref<Plugin>(-1);

    ls.push_upvalue(3);
    std::string member_name = ls.to_string(-1);
    ls.closure_name(member_name);

    if(!ls.check_closure(0, ClosureLS::TABLE))
        return 0;

    if(member_name == "identifier")
        ls.push(plugin._identifier);
    else if(member_name == "label")
        ls.push(plugin._label);
    else if(member_name == "version")
        ls.push(plugin._version);
    else if(member_name == "author")
        ls.push(plugin._author);
    else if(member_name == "description")
        ls.push(plugin._description);
    else if(member_name == "entry_file")
        ls.push(plugin._entry_file);
    else if(member_name == "directory")
        ls.push(plugin._directory);
    else
        ls.push_nil();

    return 1;
}

int Plugin::lw_service(lua_State *L)
{
    ClosureLS ls(L, 1, "service", ClosureLS::CT_FUNCTION);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_closure(1) || !ls.check_args({ClosureLS::STRING})) {
        ls.push_nil();
        return 1;
    }

    std::string service_name = ls.to_string(1);

    if(service_name == "scheduler") {
        ls.push_upvalue(2);
        ls.raw_get(LUA_REGISTRYINDEX);
        return 1;
    } else {
        logger << ls.msg_prefix() << " unknown service '" << service_name << "' "
            << ls.msg_postfix() << ulib::lerror;
        ls.push_nil();
        return 1;
    }
}

int Plugin::lw_include(lua_State *L)
{
    ClosureLS ls(L, 1, "include", ClosureLS::CT_FUNCTION);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_closure(1) || !ls.check_args({ClosureLS::STRING})) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(2);
    Plugin &plugin = ls.to_ref<Plugin>(-1);

    std::string path = ls.to_string(1);

    if(path.length() == 0) {
        logger << ls.msg_prefix() << " empty path '" << path << "' "
            << ls.msg_postfix() << ulib::lerror;
        return 0;
    }

    if(path.at(0) != '/')
        path = plugin.directory() + "/" + path;

    if(!ls.load_file(path)) {
        logger << ls.to_string(-1) << ulib::lerror;
        ls.pop();
        return 0;
    }

    plugin.push_environment();
    plugin.set_upvalue(-2, 1);

    if(!plugin.pcall()) {
        logger << plugin.to_string(-1) << ulib::lerror;
        plugin.pop();
        return 0;
    }

    return 0;
}