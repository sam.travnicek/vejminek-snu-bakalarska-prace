#include "plugins/DriverLUA.hpp"
#include "lua/GlobalEnvironment.hpp"
#include "lua/ClosureLS.hpp"
#include "vejminek/Core.hpp"

void DriverLUA::inject(DriverManager &dm, Sandbox &sb, ulib::Logger &logger)
{
    sb.push_environment();

    // Table wrapper for class
    sb.new_table();

    // Add table to environment
    sb.push("Driver");
    sb.copy(-2);
    sb.raw_set(-4);

    // Metatable for table wrapper
    sb.new_table();

	sb.push("__index"); // handle function for accessing fields
    sb.push(&dm);
    sb.push(&logger);
	sb.push(clw__index, 2);
	sb.raw_set(-3);

	sb.push("__newindex"); // handle function for modifying fields
    sb.push(&logger);
	sb.push(clw__newindex, 1);
	sb.raw_set(-3);

	sb.push("__metatable"); // prevent getting metatable (returns nil)
	sb.push_nil();
	sb.raw_set(-3);

    // Assign metatable to the instance
    sb.set_metatable(-2);

    // Pop table and environment from stack
    sb.pop(2);
}

int DriverLUA::clw__index(lua_State* L)
{
    LuaState ls(L);

    ls.push_upvalue(1);
    DriverManager &dm = ls.to_ref<DriverManager>(-1);

    ls.push_upvalue(2);
    ulib::Logger &logger = ls.to_ref<ulib::Logger>(-1);

    std::string index = ls.to_string(2);

    if(index == "new") {
        ls.push(&dm);
        ls.push(&logger);
        ls.push(clw_new, 2);
    } else
        ls.push_nil();

    return 1;
}

int DriverLUA::clw__newindex(lua_State *L)
{
    ClosureLS ls(L, 1, "__newindex", ClosureLS::CT_METHOD);

    if(!ls.check_protected_fields({"new"}))
        return 0;

    ls.error_protected_obj();
    return 0;
}

int DriverLUA::clw_new(lua_State *L)
{
    ClosureLS ls(L, 2, "new", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_closure({1, 2}, ClosureLS::TABLE)
            || !ls.check_args({ClosureLS::STRING, ClosureLS::STRING}, 1)) {
        ls.push_nil();
        return 0;
    }

    ls.push_upvalue(1);
    DriverManager &dm = ls.to_ref<DriverManager>(-1);

    std::string identifier = ls.to_string(2);

    if(!Core::identifier_valid(identifier)) {
        logger << ls.msg_prefix() << " invalid identifier '" << identifier << "' "
            << ls.msg_postfix() << ulib::lerror;
        ls.push_nil();
        return 1;
    }

    if(dm.driver(identifier)) {
        logger << ls.msg_prefix() << " duplicite identifier of driver '" << identifier << "' "
            << ls.msg_postfix() << ulib::lerror;
        ls.push_nil();
        return 1;
    }

    DriverLUA *driver = new DriverLUA(identifier, logger.configuration(), ls);
    dm.register_driver(driver);

    if(ls.num_args() == 2)
        driver->_label = ls.to_string(3);

    // Table wrapper for instance
    ls.new_table();

    // Assign a new wrapping table to instance pointer in global table LUDW_TAB
    ls.copy(-1);
    GlobalEnvironment::set(ls, driver);

    // Metatable for table wrapper
    ls.new_table();

	ls.push("__index"); // handle function for accessing fields
    ls.push(driver);
    ls.push(&logger);
	ls.push(ilw__index, 2);
	ls.raw_set(-3);

	ls.push("__newindex"); // handle function for modifying fields
    ls.push(driver);
    ls.push(&logger);
	ls.push(ilw__newindex, 2);
	ls.raw_set(-3);

	ls.push("__metatable"); // prevent getting metatable (returns nil)
	ls.push_nil();
	ls.raw_set(-3);

    // Assign metatable to the instance
    ls.set_metatable(-2);

    return 1;
}

int DriverLUA::ilw__index(lua_State* L)
{
    LuaState ls(L);

    ls.push_upvalue(1);
    DriverLUA &driver = ls.to_ref<DriverLUA>(-1);

    ls.push_upvalue(2);
    ulib::Logger &logger = ls.to_ref<ulib::Logger>(-1);

    std::string index = ls.to_string(2);

    if(index == "identifier") {
        ls.push(&driver);
        ls.push(&logger);
        ls.push(ilw_identifier, 2);
    } else if(index == "label") {
        ls.push(&driver);
        ls.push(&logger);
        ls.push(ilw_label, 2);
    } else if(index == "write_port") {
        ls.push(&driver);
        ls.push(&logger);
        ls.push(ilw_write_port, 2);
    } else if(index == "write_slot") {
        ls.push(&driver);
        ls.push(&logger);
        ls.push(ilw_write_slot, 2);
    } else if(index == "invoke_signal") {
        ls.push(&driver);
        ls.push(&logger);
        ls.push(ilw_invoke_signal, 2);
    } else if(index == "on") {
        ls.push(&driver);
        ls.push(&logger);
        ls.push(ilw_on, 2);
    } else if(index == "port") {
        ls.push(&driver);
        ls.push(&logger);
        ls.push(ilw_port, 2);
    } else if(index == "slot") {
        ls.push(&driver);
        ls.push(&logger);
        ls.push(ilw_slot, 2);
    } else if(index == "signal") {
        ls.push(&driver);
        ls.push(&logger);
        ls.push(ilw_signal, 2);
    } else
        ls.push_nil();

    return 1;
}

int DriverLUA::ilw__newindex(lua_State *L)
{
    ClosureLS ls(L, 1, "__newindex", ClosureLS::CT_METHOD);

    if(!ls.check_protected_fields({
            "identifier", "label", "write_port", "write_slot", "invoke_signal",
            "on", "port", "slot", "signal"}))
        return 0;

    // Assign new value to field
    ls.copy(1);
    ls.copy(2);
    ls.copy(3);
    ls.raw_set(-3);

    return 0;
}

int DriverLUA::ilw_identifier(lua_State *L)
{
    ClosureLS ls(L, 2, "identifier", ClosureLS::CT_METHOD);

    if(!ls.check_closure(0, ClosureLS::TABLE)) {
        ls.push_nil();
        return 1;
    }

    ls.push_upvalue(1);
    DriverLUA &driver = ls.to_ref<DriverLUA>(-1);

    ls.push(driver._identifier);
    return 1;
}

int DriverLUA::ilw_label(lua_State *L)
{
    ClosureLS ls(L, 2, "label", ClosureLS::CT_METHOD);

    if(!ls.check_closure(0, ClosureLS::TABLE))
        return 0;

    ls.push_upvalue(1);
    DriverLUA &driver = ls.to_ref<DriverLUA>(-1);

    ls.push(driver._label);
    return 1;
}

int DriverLUA::ilw_write_port(lua_State *L)
{
    ClosureLS ls(L, 2, "write_port", ClosureLS::CT_METHOD);

    if(!ls.check_object(ClosureLS::TABLE))
        return 0;

    ls.argument_list({ClosureLS::STRING, ClosureLS::INT});
    ls.argument_list({ClosureLS::STRING, ClosureLS::DOUBLE});

    if(ls.check_arguments() < 0)
        return 0;

    ls.push_upvalue(1);
    DriverLUA &driver = ls.to_ref<DriverLUA>(-1);

    driver.write_port(ls.to_string(2), ls.to_double(3));
    return 0;
}

int DriverLUA::ilw_write_slot(lua_State *L)
{
    ClosureLS ls(L, 2, "write_slot", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_closure(2, ClosureLS::TABLE))
        return 0;

    if(!ls.check_arg(0, ClosureLS::STRING))
        return 0;

    if(!ls.check_arg(1, {ClosureLS::INT, ClosureLS::DOUBLE, ClosureLS::BOOL, ClosureLS::STRING}))
        return 0;

    ls.push_upvalue(1);
    DriverLUA &driver = ls.to_ref<DriverLUA>(-1);

    std::string slot_id = ls.to_string(2);
    if(driver._slot_types.find(slot_id) == driver._slot_types.end())
        return 0;
    DeviceTemplate::Slot::Type slot_type = driver._slot_types[slot_id];

    if(ls.is_int(3) || ls.is_double(3)) {
        if(slot_type != DeviceTemplate::Slot::NUMBER) {
            logger << ls.msg_prefix() << " attempt to write invalid value to slot '"
                << slot_id << "', type 'NUMBER' expected " << ls.msg_postfix() << ulib::lerror;
            return 0;
        }
        driver.write_slot(slot_id, ls.to_double(3));
    } else if(ls.is_bool(3)) {
        if(slot_type != DeviceTemplate::Slot::BOOLEAN) {
            logger << ls.msg_prefix() << " attempt to write invalid value to slot '"
                << slot_id << "', type 'BOOLEAN' expected " << ls.msg_postfix() << ulib::lerror;
            return 0;
        }
        driver.write_slot(slot_id, ls.to_bool(3));
    } else { // string
        if(slot_type != DeviceTemplate::Slot::STRING) {
            logger << ls.msg_prefix() << " attempt to write invalid value to slot '"
                << slot_id << "', type 'STRING' expected " << ls.msg_postfix() << ulib::lerror;
            return 0;
        }
        driver.write_slot(slot_id, ls.to_string(3));
    }

    return 0;
}

int DriverLUA::ilw_invoke_signal(lua_State *L)
{
    ClosureLS ls(L, 2, "invoke_signal", ClosureLS::CT_METHOD);

    if(!ls.check_closure(1, ClosureLS::TABLE) || !ls.check_args({ClosureLS::STRING}))
        return 0;

    ls.push_upvalue(1);
    DriverLUA &driver = ls.to_ref<DriverLUA>(-1);

    driver.invoke_signal(ls.to_string(2));
    return 0;
}

int DriverLUA::ilw_on(lua_State *L)
{
    ClosureLS ls(L, 2, "on", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_closure(2, ClosureLS::TABLE)
            || !ls.check_args({ClosureLS::STRING, ClosureLS::FUNCTION}))
        return 0;

    ls.push_upvalue(1);
    DriverLUA &driver = ls.to_ref<DriverLUA>(-1);

    std::string event_name = ls.to_string(2);

    if(event_name == "tick") {
        ls.copy(3);
        driver._ecb_tick = ls.ref(LUA_REGISTRYINDEX);
    } else if(event_name == "port_written") {
        ls.copy(3);
        driver._ecb_port_written = ls.ref(LUA_REGISTRYINDEX);
    } else if(event_name == "port_linked") {
        ls.copy(3);
        driver._ecb_port_linked = ls.ref(LUA_REGISTRYINDEX);
    } else if(event_name == "ports_linkage_complete") {
        ls.copy(3);
        driver._ecb_ports_linkage_complete = ls.ref(LUA_REGISTRYINDEX);
    } else if(event_name == "slot_written") {
        ls.copy(3);
        driver._ecb_slot_written = ls.ref(LUA_REGISTRYINDEX);
    } else if(event_name == "slot_linked") {
        ls.copy(3);
        driver._ecb_slot_linked = ls.ref(LUA_REGISTRYINDEX);
    } else if(event_name == "slots_linkage_complete") {
        ls.copy(3);
        driver._ecb_slots_linkage_complete = ls.ref(LUA_REGISTRYINDEX);
    } else if(event_name == "signal_invoked") {
        ls.copy(3);
        driver._ecb_signal_invoked = ls.ref(LUA_REGISTRYINDEX);
    } else if(event_name == "signal_linked") {
        ls.copy(3);
        driver._ecb_signal_linked = ls.ref(LUA_REGISTRYINDEX);
    } else if(event_name == "signals_linkage_complete") {
        ls.copy(3);
        driver._ecb_signals_linkage_complete = ls.ref(LUA_REGISTRYINDEX);
    } else {
        logger << ls.msg_prefix() << " unknown event '" << event_name << "' "
            << ls.msg_postfix() << ulib::lwarn;
    }

    return 0;
}

int DriverLUA::ilw_port(lua_State *L)
{
    // port(identifier, configuration(s))
    // or port(identifier, label, configuration(s))

    ClosureLS ls(L, 2, "port", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_closure({2, 3}, ClosureLS::TABLE))
        return 0;

    if(!ls.check_arg(0, ClosureLS::STRING))
        return 0;

    if(ls.num_args() == 2) {
        if(!ls.check_arg(1, {ClosureLS::STRING, ClosureLS::TABLE}))
            return 0;
    }

    if(ls.num_args() == 3) {
        if(!ls.check_arg(1, ClosureLS::STRING))
            return 0;

        if(!ls.check_arg(2, ClosureLS::TABLE))
            return 0;
    }

    ls.push_upvalue(1);
    DriverLUA &driver = ls.to_ref<DriverLUA>(-1);
    std::string port_identifier = ls.to_string(2);

    PortDescriptor pd;
    pd.identifier = port_identifier;

    if(ls.num_args() == 3)
        pd.label = ls.to_string(3);
    else
        pd.label = port_identifier;

    auto &cfgs = pd.configurations;

    int cfg_pos = ls.num_args() == 3 ? 4 : 3;

    // table can be single configuration or 'array' of configurations
    if(ls.length(cfg_pos) != 0) { // case of 'array'
        ls.push_nil();

        while(ls.next(cfg_pos)) {
            PortConfiguration cfg;
            cfg.min = 0;
            cfg.max = 1;

            if(!ls.is_table(-1)) {
                logger << ls.msg_prefix() << " invalid descriptor, port discarded "
                    << ls.msg_postfix() << ulib::lerror;
                return 0;
            }

            if(ls.get_field(-1, "identifier") == LUA_TNIL) {
                logger << ls.msg_prefix() << " missing parameter 'identifier', "
                    "configuration discarded " << ls.msg_postfix() << ulib::lerror;
                ls.pop(2);
                continue;
            }

            if(!ls.is_string(-1)) {
                logger << ls.msg_prefix() <<" parameter 'identifier' must be a string "
                    "value, configuration discarded " << ls.msg_postfix() << ulib::lerror;
                ls.pop(2);
                continue;
            }

            cfg.identifier = ls.to_string(-1);
            cfg.label = cfg.identifier;
            ls.pop();

            if(ls.get_field(-1, "label") != LUA_TNIL) {
                if(!ls.is_string(-1)) {
                    logger << ls.msg_prefix() <<" parameter 'label' must be a string "
                        "value, configuration discarded " << ls.msg_postfix() << ulib::lerror;
                    ls.pop(2);
                    continue;
                }

                cfg.label = ls.to_string(-1);
            }
            ls.pop();

            if(ls.get_field(-1, "direction") == LUA_TNIL) {
                logger << ls.msg_prefix() << " missing parameter 'direction', "
                    "configuration discarded " << ls.msg_postfix() << ulib::lerror;
                ls.pop(2);
                continue;
            }

            if(!ls.is_int(-1)) {
                logger << ls.msg_prefix() <<" parameter 'direction' must be a PORT_DIRECTION "
                    "value, configuration discarded " << ls.msg_postfix() << ulib::lerror;
                ls.pop(2);
                continue;
            }

            cfg.direction = ls.to_int(-1);
            if(cfg.direction >= DeviceTemplate::Port::Direction::_DIRECTION_END_) {
                logger << ls.msg_prefix()
                    << " parameter 'direction' is not a valid PORT_DIRECTION value, "
                    "configuration discarded " << ls.msg_postfix() << ulib::lerror;
                ls.pop(2);
                continue;
            }
            ls.pop();

            if(ls.get_field(-1, "mode") == LUA_TNIL) {
                logger << ls.msg_prefix() << " missing parameter 'mode', "
                    "configuration discarded " << ls.msg_postfix() << ulib::lerror;
                ls.pop(2);
                continue;
            }

            if(!ls.is_int(-1)) {
                logger << ls.msg_prefix() << " parameter 'mode' must be a PORT_MODE value, "
                    "configuration discarded " << ls.msg_postfix() << ulib::lerror;
                ls.pop(2);
                continue;
            }

            cfg.mode = ls.to_int(-1);
            if(cfg.mode >= DeviceTemplate::Port::Mode::_MODE_END_) {
                logger << ls.msg_prefix() << " parameter 'mode' is not a valid PORT_MODE "
                    "value, configuration discarded " << ls.msg_postfix() << ulib::lerror;
                ls.pop(2);
                continue;
            }
            ls.pop();

            if(cfg.mode != DeviceTemplate::Port::Mode::LOGICAL) { // min, max definition required
                if(ls.get_field(-1, "min") == LUA_TNIL) {
                    logger << ls.msg_prefix() << " missing parameter 'min', "
                        "port discarded " << ls.msg_postfix() << ulib::lerror;
                    ls.pop(2);
                    continue;
                }

                if(!ls.is_double(-1)) {
                    logger << ls.msg_prefix() << " parameter 'min' must be a numeric value, "
                        "port discarded " << ls.msg_postfix() << ulib::lerror;
                    ls.pop(2);
                    continue;
                }

                cfg.min = ls.to_double(-1);
                ls.pop();

                if(ls.get_field(-1, "max") == LUA_TNIL) {
                    logger << ls.msg_prefix() << " missing parameter 'max', "
                        "port discarded " << ls.msg_postfix() << ulib::lerror;
                    ls.pop(2);
                    continue;
                }

                if(!ls.is_double(-1)) {
                    logger << ls.msg_prefix() << " parameter 'max' must be a numeric value, "
                        "port discarded " << ls.msg_postfix() << ulib::lerror;
                    ls.pop(2);
                    continue;
                }

                cfg.max = ls.to_double(-1);
                ls.pop();
            }

            ls.pop();

            cfgs.push_back(cfg);
        }

    } else { // case of single configuration
        PortConfiguration cfg;
        cfg.min = 0;
        cfg.max = 1;
        cfg.identifier = "";
        cfg.label = "";

        // parameter 'name' is not required in case of single configuration
        if(ls.get_field(cfg_pos, "identifier") != LUA_TNIL) {
            if(!ls.is_string(-1)) {
                logger << ls.msg_prefix() <<" parameter 'identifier' must be a string "
                    "value, configuration discarded " << ls.msg_postfix() << ulib::lerror;
                return 0;
            }

            cfg.identifier = ls.to_string(-1);
            cfg.label = cfg.identifier;
        }
        ls.pop();

        if(ls.get_field(cfg_pos, "label") != LUA_TNIL) {
            if(!ls.is_string(-1)) {
                logger << ls.msg_prefix() <<" parameter 'label' must be a string "
                    "value, configuration discarded " << ls.msg_postfix() << ulib::lerror;
                return 0;
            }

            cfg.label = ls.to_string(-1);
        }
        ls.pop();

        if(ls.get_field(cfg_pos, "direction") == LUA_TNIL) {
            logger << ls.msg_prefix() << " missing parameter 'direction', "
                "port discarded " << ls.msg_postfix() << ulib::lerror;
            return 0;
        }

        if(!ls.is_int(-1)) {
            logger << ls.msg_prefix() <<" parameter 'direction' must be a PORT_DIRECTION "
                "value, port discarded " << ls.msg_postfix() << ulib::lerror;
            return 0;
        }

        cfg.direction = ls.to_int(-1);
        if(cfg.direction >= DeviceTemplate::Port::Direction::_DIRECTION_END_) {
            logger << ls.msg_prefix()
                << " parameter 'direction' is not a valid PORT_DIRECTION value, "
                "port discarded " << ls.msg_postfix() << ulib::lerror;
            return 0;
        }
        ls.pop();

        if(ls.get_field(cfg_pos, "mode") == LUA_TNIL) {
            logger << ls.msg_prefix() << " missing parameter 'mode', "
                "port discarded " << ls.msg_postfix() << ulib::lerror;
            return 0;
        }

        if(!ls.is_int(-1)) {
            logger << ls.msg_prefix() << " parameter 'mode' must be a PORT_MODE value, "
                "port discarded " << ls.msg_postfix() << ulib::lerror;
            return 0;
        }

        cfg.mode = ls.to_int(-1);
        if(cfg.mode >= DeviceTemplate::Port::Mode::_MODE_END_) {
            logger << ls.msg_prefix() << " parameter 'mode' is not a valid PORT_MODE "
                "value, port discarded " << ls.msg_postfix() << ulib::lerror;
            return 0;
        }
        ls.pop();

        if(cfg.mode != DeviceTemplate::Port::Mode::LOGICAL) { // min, max definition required
            if(ls.get_field(cfg_pos, "min") == LUA_TNIL) {
                logger << ls.msg_prefix() << " missing parameter 'min', "
                    "port discarded " << ls.msg_postfix() << ulib::lerror;
                return 0;
            }

            if(!ls.is_double(-1)) {
                logger << ls.msg_prefix() << " parameter 'min' must be a numeric value, "
                    "port discarded " << ls.msg_postfix() << ulib::lerror;
                return 0;
            }

            cfg.min = ls.to_double(-1);
            ls.pop();

            if(ls.get_field(cfg_pos, "max") == LUA_TNIL) {
                logger << ls.msg_prefix() << " missing parameter 'max', "
                    "port discarded " << ls.msg_postfix() << ulib::lerror;
                return 0;
            }

            if(!ls.is_double(-1)) {
                logger << ls.msg_prefix() << " parameter 'max' must be a numeric value, "
                    "port discarded " << ls.msg_postfix() << ulib::lerror;
                return 0;
            }

            cfg.max = ls.to_double(-1);
            ls.pop();
        }

        cfgs.push_back(cfg);
    }

    if(cfgs.size() == 0) {
        logger << ls.msg_prefix() << " no valid configurations found, port discarded "
            << ls.msg_postfix() << ulib::lerror;
        return 0;
    }

    driver.port(pd);
    return 0;
}

int DriverLUA::ilw_slot(lua_State *L)
{
    // slot(identifier, direction, type)
    // or slot(identifier, label, direction, type)

    ClosureLS ls(L, 2, "slot", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_object(ClosureLS::TABLE))
        return 0;

    ls.argument_list({ClosureLS::STRING, ClosureLS::INT, ClosureLS::INT});
    ls.argument_list({ClosureLS::STRING, ClosureLS::STRING, ClosureLS::INT, ClosureLS::INT});

    int argument_list = ls.check_arguments();
    if(argument_list < 0)
        return 0;

    ls.push_upvalue(1);
    DriverLUA &driver = ls.to_ref<DriverLUA>(-1);

    SlotDescriptor sd;
    sd.identifier = ls.to_string(2);
    sd.label = sd.identifier;

    int dir_offs = 3;

    if(argument_list == 1) {
        sd.label = ls.to_string(3);
        dir_offs = 4;
    }

    uint direction = ls.to_uint(dir_offs);

    if(direction >= DeviceTemplate::Slot::_DIRECTION_END_) {
        logger << ls.msg_prefix() << " invalid direction of slot '"
            << sd.identifier << "', slot discarded " << ls.msg_postfix()
            << ulib::lerror;
        return 0;
    }

    uint type = ls.to_uint(dir_offs+1);

    if(type >= DeviceTemplate::Slot::_TYPE_END_) {
        logger << ls.msg_prefix() << " invalid type of slot '"
            << sd.identifier << "', slot discarded " << ls.msg_postfix()
            << ulib::lerror;
        return 0;
    }

    sd.direction = static_cast<DeviceTemplate::Slot::Direction>(direction);
    sd.type = static_cast<DeviceTemplate::Slot::Type>(type);

    driver.slot(sd);
    return 0;
}

int DriverLUA::ilw_signal(lua_State *L)
{
    // signal(identifier, direction)
    // or signal(identifier, label, direction)

    ClosureLS ls(L, 2, "signal", ClosureLS::CT_METHOD);
    ulib::Logger &logger = ls.logger();

    if(!ls.check_object(ClosureLS::TABLE))
        return 0;

    ls.argument_list({ClosureLS::STRING, ClosureLS::INT});
    ls.argument_list({ClosureLS::STRING, ClosureLS::STRING, ClosureLS::INT});

    int argument_list = ls.check_arguments();
    if(argument_list < 0)
        return 0;

    ls.push_upvalue(1);
    DriverLUA &driver = ls.to_ref<DriverLUA>(-1);

    SignalDescriptor sd;
    sd.identifier = ls.to_string(2);
    sd.label = sd.identifier;

    int dir_offs = 3;

    if(argument_list == 1) {
        sd.label = ls.to_string(3);
        dir_offs = 4;
    }

    uint direction = ls.to_uint(dir_offs);

    if(direction >= DeviceTemplate::Signal::_DIRECTION_END_) {
        logger << ls.msg_prefix() << " invalid direction of signal '"
            << sd.identifier << "', signal discarded " << ls.msg_postfix()
            << ulib::lerror;
        return 0;
    }

    sd.direction = static_cast<DeviceTemplate::Signal::Direction>(direction);

    driver.signal(sd);
    return 0;
}

void DriverLUA::tick(int64_t time, ulong ticks, ulong tps)
{
    if(_ecb_tick == 0)
        return;

    _ls.push(_ecb_tick);
    _ls.raw_get(LUA_REGISTRYINDEX);

    _ls.push(time);
    _ls.push(ticks);
    _ls.push(tps);

    if(!_ls.pcall(3)) {
        _logger << _ls.to_string(-1) << ulib::lerror;
        _ls.pop();
    }
}

void DriverLUA::driver_port_written(const std::string &identifier, double value)
{
    if(_ecb_port_written == 0)
        return;

    _ls.push(_ecb_port_written);
    _ls.raw_get(LUA_REGISTRYINDEX);

    _ls.push(identifier);
    _ls.push(value);

    if(!_ls.pcall(2)) {
        _logger << _ls.to_string(-1) << ulib::lerror;
        _ls.pop();
    }
}

void DriverLUA::driver_port_linked(
    const std::string &identifier, int direction, int mode,
    const std::string &cfg_identifier)
{
    if(_ecb_port_linked == 0)
        return;

    _ls.push(_ecb_port_linked);
    _ls.raw_get(LUA_REGISTRYINDEX);

    _ls.push(identifier);
    _ls.push(direction);
    _ls.push(mode);
    _ls.push(cfg_identifier);

    if(!_ls.pcall(4)) {
        _logger << _ls.to_string(-1) << ulib::lerror;
        _ls.pop();
    }
}

void DriverLUA::ports_linkage_complete()
{
    if(_ecb_ports_linkage_complete == 0)
        return;

    _ls.push(_ecb_ports_linkage_complete);
    _ls.raw_get(LUA_REGISTRYINDEX);

    if(!_ls.pcall()) {
        _logger << _ls.to_string(-1) << ulib::lerror;
        _ls.pop();
    }
}

void DriverLUA::driver_slot_written(
    const std::string &identifier, std::any &value, DeviceTemplate::Slot::Type type)
{
    if(_ecb_slot_written == 0)
        return;

    _ls.push(_ecb_slot_written);
    _ls.raw_get(LUA_REGISTRYINDEX);

    _ls.push(identifier);

    if(type == DeviceTemplate::Slot::NUMBER)
        _ls.push(std::any_cast<double>(value));
    else if(type == DeviceTemplate::Slot::BOOLEAN)
        _ls.push(std::any_cast<bool>(value));
    else
        _ls.push(std::any_cast<std::string>(value));

    if(!_ls.pcall(2)) {
        _logger << _ls.to_string(-1) << ulib::lerror;
        _ls.pop();
    }
}

void DriverLUA::driver_slot_linked(const std::string &identifier)
{
    if(_ecb_slot_linked == 0)
        return;

    _ls.push(_ecb_slot_linked);
    _ls.raw_get(LUA_REGISTRYINDEX);

    _ls.push(identifier);

    if(!_ls.pcall(1)) {
        _logger << _ls.to_string(-1) << ulib::lerror;
        _ls.pop();
    }
}

void DriverLUA::slots_linkage_complete()
{
    if(_ecb_slots_linkage_complete == 0)
        return;

    _ls.push(_ecb_slots_linkage_complete);
    _ls.raw_get(LUA_REGISTRYINDEX);

    if(!_ls.pcall()) {
        _logger << _ls.to_string(-1) << ulib::lerror;
        _ls.pop();
    }
}

void DriverLUA::driver_signal_invoked(const std::string &identifier)
{
    if(_ecb_signal_invoked == 0)
        return;

    _ls.push(_ecb_signal_invoked);
    _ls.raw_get(LUA_REGISTRYINDEX);

    _ls.push(identifier);

    if(!_ls.pcall(1)) {
        _logger << _ls.to_string(-1) << ulib::lerror;
        _ls.pop();
    }
}

void DriverLUA::driver_signal_linked(const std::string &identifier)
{
    if(_ecb_signal_linked == 0)
        return;

    _ls.push(_ecb_signal_linked);
    _ls.raw_get(LUA_REGISTRYINDEX);

    _ls.push(identifier);

    if(!_ls.pcall(1)) {
        _logger << _ls.to_string(-1) << ulib::lerror;
        _ls.pop();
    }
}

void DriverLUA::signals_linkage_complete()
{
    if(_ecb_signals_linkage_complete == 0)
        return;

    _ls.push(_ecb_signals_linkage_complete);
    _ls.raw_get(LUA_REGISTRYINDEX);

    if(!_ls.pcall()) {
        _logger << _ls.to_string(-1) << ulib::lerror;
        _ls.pop();
    }
}