#include "Database.hpp"
#include "vejminek/Device.hpp"

using ulib::net::MySQL;

/*
TRUNCATE `tmp_devices`;
TRUNCATE `tmp_device_actions`;
TRUNCATE `tmp_device_action_arguments`;
TRUNCATE `tmp_device_attributes`;
TRUNCATE `tmp_device_events`;
TRUNCATE `tmp_device_ports`;
TRUNCATE `tmp_device_signals`;
TRUNCATE `tmp_device_slots`;
TRUNCATE `tmp_drivers`;
TRUNCATE `tmp_driver_ports`;
TRUNCATE `tmp_driver_signals`;
TRUNCATE `tmp_driver_slots`;
TRUNCATE `tmp_plugins`;
*/

/*
SELECT concat('TRUNCATE TABLE `', TABLE_NAME, '`;')
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME LIKE 'tmp_%';
*/

const char *SQL_TRUNCATE_TMPS[] = {
    "TRUNCATE `tmp_config`;",
    "TRUNCATE `tmp_devices`;",
    "TRUNCATE `tmp_device_actions`;",
    "TRUNCATE `tmp_device_action_arguments`;",
    "TRUNCATE `tmp_device_attributes`;",
    "TRUNCATE `tmp_device_emitted_events`;",
    "TRUNCATE `tmp_device_ports`;",
    "TRUNCATE `tmp_device_signals`;",
    "TRUNCATE `tmp_device_slots`;",
    "TRUNCATE `tmp_drivers`;",
    "TRUNCATE `tmp_driver_ports`;",
    "TRUNCATE `tmp_driver_port_configurations`;",
    "TRUNCATE `tmp_driver_signals`;",
    "TRUNCATE `tmp_driver_slots`;",
    "TRUNCATE `tmp_plugins`;"
};

const char *SQL_INSERT_PLUGIN = R"SQL(
    INSERT INTO tmp_plugins
    (identifier, label, version, author, description, entry_file, directory, valid)
    VALUES(?, ?, ?, ?, ?, ?, ?, ?);
)SQL";

const char *SQL_INSERT_DRIVER = R"SQL(
    INSERT INTO tmp_drivers
    (plugin_identifier, identifier, label)
    VALUES(?, ?, ?);
)SQL";

const char *SQL_INSERT_DRIVER_PORT = R"SQL(
    INSERT INTO tmp_driver_ports
    (driver_id, identifier, label)
    VALUES(?, ?, ?);
)SQL";

const char *SQL_INSERT_DRIVER_PORT_CFG = R"SQL(
    INSERT INTO tmp_driver_port_configurations
    (port_id, direction, mode, identifier, label)
    VALUES(?, ?, ?, ?, ?);
)SQL";

const char *SQL_INSERT_DRIVER_SLOT = R"SQL(
    INSERT INTO tmp_driver_slots
    (driver_id, identifier, `type`, label, direction)
    VALUES(?, ?, ?, ?, ?);
)SQL";

const char *SQL_INSERT_DRIVER_SIGNAL = R"SQL(
    INSERT INTO tmp_driver_signals
    (driver_id, identifier, label, direction)
    VALUES(?, ?, ?, ?);
)SQL";

const char *SQL_INSERT_DEVICE = R"SQL(
    INSERT INTO tmp_devices
    (plugin_identifier, identifier, label)
    VALUES(?, ?, ?);
)SQL";

const char *SQL_INSERT_DEVICE_ACTION = R"SQL(
    INSERT INTO tmp_device_actions
    (device_id, identifier, label)
    VALUES(?, ?, ?);
)SQL";

const char *SQL_INSERT_DEVICE_ACTION_ARGUMENT = R"SQL(
    INSERT INTO tmp_device_action_arguments
    (action_id, identifier, `type`, label, `default_value`)
    VALUES(?, ?, ?, ?, ?);
)SQL";

const char *SQL_INSERT_DEVICE_ATTRIBUTE = R"SQL(
    INSERT INTO tmp_device_attributes
    (device_id, identifier, `type`, label)
    VALUES(?, ?, ?, ?);
)SQL";

const char *SQL_INSERT_DEVICE_EMITTED_EVENT = R"SQL(
    INSERT INTO tmp_device_emitted_events
    (device_id, identifier, label)
    VALUES(?, ?, ?);
)SQL";

const char *SQL_INSERT_DEVICE_PORT = R"SQL(
    INSERT INTO tmp_device_ports
    (device_id, identifier, direction, mode, optional, default_value, value_stored, label)
    VALUES(?, ?, ?, ?, ?, ?, ?, ?);
)SQL";

const char *SQL_INSERT_DEVICE_SLOT = R"SQL(
    INSERT INTO tmp_device_slots
    (device_id, identifier, `type`, label, direction)
    VALUES(?, ?, ?, ?, ?);
)SQL";

const char *SQL_INSERT_DEVICE_SIGNAL = R"SQL(
    INSERT INTO tmp_device_signals
    (device_id, identifier, label, direction)
    VALUES(?, ?, ?, ?);
)SQL";

const char *SQL_LAST_INSERTION_ID = R"SQL(
    SELECT LAST_INSERT_ID() AS id;
)SQL";

const char *SQL_LAST_PORT_VALUE = R"SQL(
    SELECT last_value
    FROM device_port_values
    WHERE device_id=? AND port_identifier=?;
)SQL";

const char *SQL_SAVE_PORT_VALUE_CNT = R"SQL(
    SELECT COUNT(*) as cnt
    FROM device_port_values
    WHERE device_id=? AND port_identifier=?;
)SQL";

const char *SQL_SAVE_PORT_VALUE_INSERT = R"SQL(
    INSERT INTO device_port_values
    (device_id, port_identifier, last_value)
    VALUES(?, ?, ?);
)SQL";

const char *SQL_SAVE_PORT_VALUE_UPDATE = R"SQL(
    UPDATE device_port_values
    SET last_value=?
    WHERE device_id=? AND port_identifier=?;
)SQL";

const char *SQL_TRUNCATE_PORT_VALUES = R"SQL(
    TRUNCATE `device_port_values`;
)SQL";

void Database::clear_tmps(MySQL &mysql)
{
    std::unique_ptr<sql::Connection> con(mysql.connect());
    std::unique_ptr<sql::Statement> stmt;
    stmt.reset(con->createStatement());
    for(size_t i = 0; i < sizeof(SQL_TRUNCATE_TMPS)/sizeof(char*); ++i)
        stmt->executeUpdate(SQL_TRUNCATE_TMPS[i]);
}

void Database::insert(MySQL &mysql, Plugin &plugin)
{
    std::unique_ptr<sql::Connection> con(mysql.connect());
    std::unique_ptr<sql::PreparedStatement> pstmt;

    pstmt.reset(con->prepareStatement(SQL_INSERT_PLUGIN));
    pstmt->setString(1, plugin.identifier());
    pstmt->setString(2, plugin.label());
    pstmt->setString(3, plugin.version());
    pstmt->setString(4, plugin.author());
    pstmt->setString(5, plugin.description());
    pstmt->setString(6, plugin.entry_file());
    pstmt->setString(7, plugin.directory());
    pstmt->setBoolean(8, plugin.valid());
    pstmt->execute();

    for(Driver *driver : plugin.drivers())
        insert_driver(con, plugin.identifier(), *driver);

    for(DeviceTemplate *dt : plugin.device_templates())
        insert_device(con, plugin.identifier(), *dt);
}

void Database::insert_driver(
    std::unique_ptr<sql::Connection> &con, const std::string &plugin_identifier, Driver &driver)
{
    std::unique_ptr<sql::PreparedStatement> pstmt;

    pstmt.reset(con->prepareStatement(SQL_INSERT_DRIVER));
    pstmt->setString(1, plugin_identifier);
    pstmt->setString(2, driver.identifier());
    pstmt->setString(3, driver.label());
    pstmt->execute();

    uint driver_id = last_insertion_id(con);

    for(const PortDescriptor &pd : driver.ports())
        insert_driver_port(con, driver_id, pd);

    for(const SlotDescriptor &slot : driver.slots())
        insert_driver_slot(con, driver_id, slot);

    for(const SignalDescriptor &sig : driver.signals())
        insert_driver_signal(con, driver_id, sig);
}

void Database::insert_driver_port(
    std::unique_ptr<sql::Connection> &con, uint driver_id, const PortDescriptor &pd)
{
    std::unique_ptr<sql::PreparedStatement> pstmt;

    pstmt.reset(con->prepareStatement(SQL_INSERT_DRIVER_PORT));
    pstmt->setUInt(1, driver_id);
    pstmt->setString(2, pd.identifier);
    pstmt->setString(3, pd.label);
    pstmt->execute();

    uint port_id = last_insertion_id(con);

    for(const PortConfiguration &cfg : pd.configurations) {
        pstmt.reset(con->prepareStatement(SQL_INSERT_DRIVER_PORT_CFG));
        pstmt->setUInt(1, port_id);
        pstmt->setUInt(2, cfg.direction);
        pstmt->setUInt(3, cfg.mode);
        pstmt->setString(4, cfg.identifier);
        pstmt->setString(5, cfg.label);
        pstmt->execute();
    }
}

void Database::insert_driver_slot(
    std::unique_ptr<sql::Connection> &con, uint driver_id, const SlotDescriptor &slot)
{
    std::unique_ptr<sql::PreparedStatement> pstmt;

    pstmt.reset(con->prepareStatement(SQL_INSERT_DRIVER_SLOT));
    pstmt->setUInt(1, driver_id);
    pstmt->setString(2, slot.identifier);
    pstmt->setInt(3, static_cast<int>(slot.type));
    pstmt->setString(4, slot.label);
    pstmt->setInt(5, slot.direction);
    pstmt->execute();
}

void Database::insert_driver_signal(
    std::unique_ptr<sql::Connection> &con, uint driver_id, const SignalDescriptor &signal)
{
    std::unique_ptr<sql::PreparedStatement> pstmt;

    pstmt.reset(con->prepareStatement(SQL_INSERT_DRIVER_SIGNAL));
    pstmt->setUInt(1, driver_id);
    pstmt->setString(2, signal.identifier);
    pstmt->setString(3, signal.label);
    pstmt->setInt(4, signal.direction);
    pstmt->execute();
}

void Database::insert_device(
    std::unique_ptr<sql::Connection> &con, const std::string &plugin_identifier, DeviceTemplate &dt)
{
    std::unique_ptr<sql::PreparedStatement> pstmt;

    pstmt.reset(con->prepareStatement(SQL_INSERT_DEVICE));
    pstmt->setString(1, plugin_identifier);
    pstmt->setString(2, dt.identifier());
    pstmt->setString(3, dt.label());
    pstmt->execute();

    uint device_id = last_insertion_id(con);

    for(DeviceTemplate::Action &action : dt.actions())
        insert_device_action(con, device_id, action);

    for(DeviceTemplate::Attribute &attr : dt.attributes())
        insert_device_attribute(con, device_id, attr);

    for(DeviceTemplate::Event &event : dt.emitted_events())
        insert_device_emitted_event(con, device_id, event);

    for(DeviceTemplate::Port &port : dt.ports())
        insert_device_port(con, device_id, port);

    for(DeviceTemplate::Slot &slot : dt.slots())
        insert_device_slot(con, device_id, slot);

    for(DeviceTemplate::Signal &sig : dt.signals())
        insert_device_signal(con, device_id, sig);
}

void Database::insert_device_action(
    std::unique_ptr<sql::Connection> &con, uint device_id, DeviceTemplate::Action &action)
{
    std::unique_ptr<sql::PreparedStatement> pstmt;

    pstmt.reset(con->prepareStatement(SQL_INSERT_DEVICE_ACTION));
    pstmt->setUInt(1, device_id);
    pstmt->setString(2, action.identifier);
    pstmt->setString(3, action.label);
    pstmt->execute();

    uint action_id = last_insertion_id(con);

    for(DeviceTemplate::Action::Argument &arg : action.arguments)
        insert_device_action_arg(con, action_id, arg);
}

void Database::insert_device_action_arg(
    std::unique_ptr<sql::Connection> &con, uint action_id, DeviceTemplate::Action::Argument &arg)
{
    std::unique_ptr<sql::PreparedStatement> pstmt;

    pstmt.reset(con->prepareStatement(SQL_INSERT_DEVICE_ACTION_ARGUMENT));
    pstmt->setUInt(1, action_id);
    pstmt->setString(2, arg.identifier);
    pstmt->setInt(3, (int)arg.type);
    pstmt->setString(4, arg.label);
    std::string default_value;
    if(arg.type == DeviceTemplate::Action::Argument::NUMBER)
        default_value = std::to_string(std::any_cast<double>(arg.default_value));
    else if(arg.type == DeviceTemplate::Action::Argument::BOOLEAN)
        default_value = std::any_cast<bool>(arg.default_value) ? "true" : "false";
    else if(arg.type == DeviceTemplate::Action::Argument::STRING)
        default_value = std::any_cast<std::string>(arg.default_value);
    pstmt->setString(5, default_value);
    pstmt->execute();
}

void Database::insert_device_attribute(
    std::unique_ptr<sql::Connection> &con, uint device_id, DeviceTemplate::Attribute &attr)
{
    std::unique_ptr<sql::PreparedStatement> pstmt;

    pstmt.reset(con->prepareStatement(SQL_INSERT_DEVICE_ATTRIBUTE));
    pstmt->setUInt(1, device_id);
    pstmt->setString(2, attr.identifier);
    pstmt->setInt(3, (int)attr.type);
    pstmt->setString(4, attr.label);
    pstmt->execute();
}

void Database::insert_device_emitted_event(
    std::unique_ptr<sql::Connection> &con, uint device_id, DeviceTemplate::Event &event)
{
    std::unique_ptr<sql::PreparedStatement> pstmt;

    pstmt.reset(con->prepareStatement(SQL_INSERT_DEVICE_EMITTED_EVENT));
    pstmt->setUInt(1, device_id);
    pstmt->setString(2, event.identifier);
    pstmt->setString(3, event.label);
    pstmt->execute();
}

void Database::insert_device_port(
    std::unique_ptr<sql::Connection> &con, uint device_id, DeviceTemplate::Port &port)
{
    std::unique_ptr<sql::PreparedStatement> pstmt;

    pstmt.reset(con->prepareStatement(SQL_INSERT_DEVICE_PORT));
    pstmt->setUInt(1, device_id);
    pstmt->setString(2, port.identifier);
    pstmt->setInt(3, port.direction);
    pstmt->setInt(4, port.mode);
    pstmt->setBoolean(5, port.optional);
    pstmt->setDouble(6, port.default_value);
    pstmt->setBoolean(7, port.value_stored);
    pstmt->setString(8, port.label);
    pstmt->execute();
}

void Database::insert_device_slot(
    std::unique_ptr<sql::Connection> &con, uint device_id, DeviceTemplate::Slot &slot)
{
    std::unique_ptr<sql::PreparedStatement> pstmt;

    pstmt.reset(con->prepareStatement(SQL_INSERT_DEVICE_SLOT));
    pstmt->setUInt(1, device_id);
    pstmt->setString(2, slot.identifier);
    pstmt->setInt(3, static_cast<int>(slot.type));
    pstmt->setString(4, slot.label);
    pstmt->setInt(5, static_cast<int>(slot.direction));
    pstmt->execute();
}

void Database::insert_device_signal(
    std::unique_ptr<sql::Connection> &con, uint device_id, DeviceTemplate::Signal &signal)
{
    std::unique_ptr<sql::PreparedStatement> pstmt;

    pstmt.reset(con->prepareStatement(SQL_INSERT_DEVICE_SIGNAL));
    pstmt->setUInt(1, device_id);
    pstmt->setString(2, signal.identifier);
    pstmt->setString(3, signal.label);
    pstmt->setInt(4, static_cast<int>(signal.direction));
    pstmt->execute();
}

uint Database::last_insertion_id(std::unique_ptr<sql::Connection> &con)
{
    std::unique_ptr<sql::Statement> stmt;
    std::unique_ptr<sql::ResultSet> rs;

    stmt.reset(con->createStatement());
    rs.reset(stmt->executeQuery(SQL_LAST_INSERTION_ID));

    if(rs->next())
        return rs->getUInt("id");
    return 0;
}

float Database::last_port_value(std::unique_ptr<sql::Connection> &con, Port &port)
{
    float value = port.port_template().default_value;

    std::unique_ptr<sql::PreparedStatement> pstmt;
    std::unique_ptr<sql::ResultSet> rs;

    pstmt.reset(con->prepareStatement(SQL_LAST_PORT_VALUE));
    pstmt->setUInt(1, port.device().id());
    pstmt->setString(2, port.identifier());
    rs.reset(pstmt->executeQuery());

    if(rs->next())
        return rs->getUInt("last_value");
    return value;
}

void Database::save_port_value(std::unique_ptr<sql::Connection> &con, Port &port)
{
    std::unique_ptr<sql::PreparedStatement> pstmt;
    std::unique_ptr<sql::ResultSet> rs;

    uint cnt = 0;

    pstmt.reset(con->prepareStatement(SQL_SAVE_PORT_VALUE_CNT));
    pstmt->setUInt(1, port.device().id());
    pstmt->setString(2, port.identifier());
    rs.reset(pstmt->executeQuery());
    if(rs->next())
        cnt = rs->getUInt("cnt");

    if(cnt == 0) {
        pstmt.reset(con->prepareStatement(SQL_SAVE_PORT_VALUE_INSERT));
        pstmt->setUInt(1, port.device().id());
        pstmt->setString(2, port.identifier());
        pstmt->setDouble(3, port.value());
        pstmt->execute();
    } else {
        pstmt.reset(con->prepareStatement(SQL_SAVE_PORT_VALUE_UPDATE));
        pstmt->setDouble(1, port.value());
        pstmt->setUInt(2, port.device().id());
        pstmt->setString(3, port.identifier());
        pstmt->execute();
    }
}

void Database::insert_port_value(std::unique_ptr<sql::Connection> &con, Port &port)
{
    std::unique_ptr<sql::PreparedStatement> pstmt;
    pstmt.reset(con->prepareStatement(SQL_SAVE_PORT_VALUE_INSERT));
    pstmt->setUInt(1, port.device().id());
    pstmt->setString(2, port.identifier());
    pstmt->setDouble(3, port.value());
    pstmt->execute();
}

void Database::clear_port_values(std::unique_ptr<sql::Connection> &con)
{
    std::unique_ptr<sql::Statement> stmt;
    stmt.reset(con->createStatement());
    stmt->executeUpdate(SQL_TRUNCATE_PORT_VALUES);
}