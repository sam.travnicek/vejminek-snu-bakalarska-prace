#ifndef ULIB_STRINGUTIL_HPP
#define ULIB_STRINGUTIL_HPP

#include <vector>
#include <string>
#include <algorithm>
#include <iterator>
#include <cstring>
#include <codecvt>
#include <locale>

namespace ulib {

class StringUtil
{
public:
	static std::vector<std::string> split(
		std::string text, const std::string &delimiter)
	{
		std::vector<std::string> vec;
		std::size_t idx = 0;
		while((idx = text.find(delimiter)) != std::string::npos) {
			vec.push_back(text.substr(0, idx));
			text.erase(0, idx + delimiter.length());
		}
		vec.push_back(text);
		return vec;
	}

	static std::string ltrim(
		const std::string &text, const std::string &chars)
	{
		unsigned int idx = 0, len = text.length();
		for(; idx < len; idx++)
			if(chars.find(text[idx]) == std::string::npos)
				break;
		return text.substr(idx);
	}

	static std::string rtrim(
		const std::string &text, const std::string &chars)
	{
		unsigned int len = text.length(), idx = len - 1;
		for(; idx >= 0; idx--)
			if(chars.find(text[idx]) == std::string::npos)
				break;
		return text.substr(0, idx+1);
	}

	static std::string trim(
		const std::string &text, const std::string &chars)
	{
		return ltrim(rtrim(text, chars), chars);
	}

	static std::string replace(
		std::string str, const std::string& orig, const std::string& repl)
	{
		size_t start_pos = str.find(orig);
		if(start_pos == std::string::npos)
			return str;
		str.replace(start_pos, orig.length(), repl);
		return str;
	}

	static std::string replace_all(
		std::string str, const std::string& orig, const std::string& repl)
	{
		if(orig.empty())
			return str;
		size_t start_pos = 0;
		while((start_pos = str.find(orig, start_pos)) != std::string::npos) {
			str.replace(start_pos, orig.length(), repl);
			start_pos += repl.length();
		}
		return str;
	}

	static std::string empty_or(const std::string &str, const std::string &repl)
	{
		if(str.empty())
			return repl;
		return str;
	}

	static std::string basic_ascii(const std::string &str)
	{
		std::string res = "";
		for(char c : str) {
			unsigned char n = (unsigned char)c;
			if(n > 127)
				res += " ";
			else
				res += c;
		}
		return res;
	}

	static std::string encode_hex(const std::string &str)
	{
		static const char * tbl = "0123456789ABCDEF";
		std::string res = "";
		for(char c : str) {
			res += tbl[(c >> 4) & 0b1111];
			res += tbl[c & 0b1111];
		}
		return res;
	}

	static std::string decode_hex(const std::string &str)
	{
		std::string res = "";
		for(uint i = 0; i < str.length(); i+=2) {
			if(i+1 >= str.length())
				break;
			char hchar = str.at(i);
			char lchar = str.at(i+1);
			char c = 0;
			if(hchar <= 0x39)
				c |= (hchar - 0x30) << 4;
			else
				c |= (hchar - 0x37) << 4;
			if(lchar <= 0x39)
				c |= lchar - 0x30;
			else
				c |= lchar - 0x37;
			res += c;
		}
		return res;
	}

	static std::wstring utf8str_to_wstr(const std::string &str)
	{
		std::wstring_convert<std::codecvt_utf16<wchar_t>, wchar_t> str_converter;
    	return str_converter.from_bytes(str);
	}

	static std::string copy(const char *str)
	{
		std::string result;
		std::copy(str, str + strlen(str), std::back_inserter(result));
		return result;
	}
};

}

#endif /* ULIB_STRINGUTIL_HPP */
