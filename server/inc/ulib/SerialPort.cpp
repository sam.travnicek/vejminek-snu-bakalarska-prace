#include <ulib/SerialPort.hpp>
#include <errno.h>

const char ulib::SerialPort::PARITY_ODD = 'o';
const char ulib::SerialPort::PARITY_EVEN = 'e';
const char ulib::SerialPort::PARITY_NONE = 'n';

ulib::SerialPort::SerialPort(const std::string &port, int baudrate)
    : _port(port), _baudrate(baudrate), _bits(8),
      _parity(SerialPort::PARITY_NONE), _stop_bits(1), _cport(-1),
      _io_error(false) {}

ulib::SerialPort::SerialPort(
        const std::string &port, int baudrate, char bits, char parity,
        char stop_bits)
    : _port(port), _baudrate(baudrate), _bits(bits), _parity(parity),
      _stop_bits(stop_bits), _cport(-1), _io_error(false) {}

void ulib::SerialPort::open()
{
    int baudr;

    switch(_baudrate) {
    case 50: baudr = B50; break;
    case 75: baudr = B75; break;
    case 110: baudr = B110; break;
    case 134: baudr = B134; break;
    case 150: baudr = B150; break;
    case 200: baudr = B200; break;
    case 300: baudr = B300; break;
    case 600: baudr = B600; break;
    case 1200: baudr = B1200; break;
    case 1800: baudr = B1800; break;
    case 2400: baudr = B2400; break;
    case 4800: baudr = B4800; break;
    case 9600: baudr = B9600; break;
    case 19200: baudr = B19200; break;
    case 38400: baudr = B38400; break;
    case 57600: baudr = B57600; break;
    case 115200: baudr = B115200; break;
    case 230400: baudr = B230400; break;
    case 460800: baudr = B460800; break;
    case 500000: baudr = B500000; break;
    case 576000: baudr = B576000; break;
    case 921600: baudr = B921600; break;
    case 1000000: baudr = B1000000; break;
    case 1152000: baudr = B1152000; break;
    case 1500000: baudr = B1500000; break;
    case 2000000: baudr = B2000000; break;
    case 2500000: baudr = B2500000; break;
    case 3000000: baudr = B3000000; break;
    case 3500000: baudr = B3500000; break;
    case 4000000: baudr = B4000000; break;
    default: throw SerialPortFailure("Invalid baudrate.");
    }

    int cbits;

    switch(_bits) {
    case 8: cbits = CS8; break;
    case 7: cbits = CS7; break;
    case 6: cbits = CS6; break;
    case 5: cbits = CS5; break;
    default: throw SerialPortFailure("Invalid number of data bits.");
    }

    int cpar, ipar;

    switch(_parity) {
    case 'N':
    case 'n':
        cpar = 0;
        ipar = IGNPAR;
        break;
    case 'E':
    case 'e':
        cpar = PARENB;
        ipar = INPCK;
        break;
    case 'O':
    case 'o':
        cpar = (PARENB | PARODD);
        ipar = INPCK;
        break;
    default:
        throw SerialPortFailure("Invalid parity.");
    }

    int bstop;

    switch(_stop_bits) {
    case 1: bstop = 0; break;
    case 2: bstop = CSTOPB; break;
    default: throw SerialPortFailure("Invalid number of stop bits.");
    }

    //blocking (it's recommended to use epoll system call)
    _cport = ::open(_port.c_str(), O_RDWR | O_NOCTTY/* | O_NDELAY*/);
    if(_cport == -1)
        throw SerialPortFailure("Unable to open port " + _port + ".");

    // lock access to port for other processes
    if(::flock(_cport, LOCK_EX | LOCK_NB) != 0)
        throw SerialPortFailure("Another process has locked the comport.");

    int error = ::tcgetattr(_cport, &_old_port_settings);
    if(error == -1) {
        ::close(_cport);
        ::flock(_cport, LOCK_UN);
        throw SerialPortFailure("Unable to read port settings.");
    }

    termios new_port_settings;
    ::memset(&new_port_settings, 0, sizeof(new_port_settings));
    new_port_settings.c_cflag = cbits | cpar | bstop | CLOCAL | CREAD;
    new_port_settings.c_iflag = ipar;
    new_port_settings.c_oflag = 0;
    new_port_settings.c_lflag = 0;
    new_port_settings.c_cc[VMIN] = 0; // block until n ubytes are received
    new_port_settings.c_cc[VTIME] = 0; //block until a timer expires (n*100ms)

    ::cfsetispeed(&new_port_settings, baudr);
    ::cfsetospeed(&new_port_settings, baudr);

    error = ::tcsetattr(_cport, TCSANOW, &new_port_settings);
    if(error == -1) {
        ::tcsetattr(_cport, TCSANOW, &_old_port_settings);
        ::close(_cport);
        ::flock(_cport, LOCK_UN);
        throw SerialPortFailure("Unable to adjust port settings.");
    }

    int status;
    if(::ioctl(_cport, TIOCMGET, &status) == -1) {
        ::tcsetattr(_cport, TCSANOW, &_old_port_settings);
        ::flock(_cport, LOCK_UN);
        throw SerialPortFailure("Unable to get port status.");
    }

    status |= TIOCM_DTR;    /* turn on DTR */
    status |= TIOCM_RTS;    /* turn on RTS */

    if(::ioctl(_cport, TIOCMSET, &status) == -1) {
        ::tcsetattr(_cport, TCSANOW, &_old_port_settings);
        ::flock(_cport, LOCK_UN);
        throw SerialPortFailure("Unable to set port status.");
    }
}

void ulib::SerialPort::close()
{
    int status;
    if(::ioctl(_cport, TIOCMGET, &status) == -1)
    ;
        //throw SerialPortFailure("Unable to get port status.");

    status &= ~TIOCM_DTR;    /* turn off DTR */
    status &= ~TIOCM_RTS;    /* turn off RTS */

    if(::ioctl(_cport, TIOCMSET, &status) == -1)
    ;
        //throw SerialPortFailure("Unable to set port status.");

    ::tcsetattr(_cport, TCSANOW, &_old_port_settings);
    ::close(_cport);
    ::flock(_cport, LOCK_UN);
}

size_t ulib::SerialPort::poll(ubyte *buff, size_t size)
{
    _io_error = false;
    int n = ::read(_cport, buff, size);
    if(errno == EIO)
        _io_error = true;
    return n;
}

std::string ulib::SerialPort::read()
{
    std::string res;
    ubyte buff[32]; int n;
    while((n = poll(buff, 31)) > 0) {
        buff[n] = '\0';
        res += std::string((char*)buff);
    }
    return res;
}

ssize_t ulib::SerialPort::send(ubyte b)
{
    _io_error = false;
    ssize_t n = ::write(_cport, &b, 1);
    if(errno == EIO)
        _io_error = true;
    return n;
}

ssize_t ulib::SerialPort::send(const ubyte *b, size_t size)
{
    _io_error = false;
    ssize_t n = ::write(_cport, b, size);
    if(errno == EIO)
        _io_error = true;
    return n;
}

ssize_t ulib::SerialPort::send(const std::string &txt)
{
    return send(reinterpret_cast<const ubyte*>(txt.data()), txt.length());
}

bool ulib::SerialPort::dcd_enabled()
{
    int status;
    ::ioctl(_cport, TIOCMGET, &status);
    return status & TIOCM_CAR;
}

bool ulib::SerialPort::cts_enabled()
{
    int status;
    ::ioctl(_cport, TIOCMGET, &status);
    return status & TIOCM_CTS;
}

bool ulib::SerialPort::dsr_enabled()
{
    int status;
    ::ioctl(_cport, TIOCMGET, &status);
    return status & TIOCM_DSR;
}


void ulib::SerialPort::dtr_enable()
{
    int status;
    if(::ioctl(_cport, TIOCMGET, &status) == -1)
        throw SerialPortFailure("Unable to get port status.");

    status |= TIOCM_DTR;
    if(::ioctl(_cport, TIOCMSET, &status) == -1)
        throw SerialPortFailure("Unable to set port status.");
}


void ulib::SerialPort::dtr_disable()
{
    int status;
    if(::ioctl(_cport, TIOCMGET, &status) == -1)
        throw SerialPortFailure("Unable to get port status.");

    status &= ~TIOCM_DTR;
    if(::ioctl(_cport, TIOCMSET, &status) == -1)
        throw SerialPortFailure("Unable to set port status.");
}


void ulib::SerialPort::rts_enable()
{
    int status;
    if(::ioctl(_cport, TIOCMGET, &status) == -1)
        throw SerialPortFailure("Unable to get port status.");

    status |= TIOCM_RTS;
    if(::ioctl(_cport, TIOCMSET, &status) == -1)
        throw SerialPortFailure("Unable to set port status.");
}


void ulib::SerialPort::rts_disable()
{
    int status;
    if(::ioctl(_cport, TIOCMGET, &status) == -1)
        throw SerialPortFailure("Unable to get port status.");

    status &= ~TIOCM_RTS;
    if(::ioctl(_cport, TIOCMSET, &status) == -1)
        throw SerialPortFailure("Unable to set port status.");
}

void ulib::SerialPort::flush_rx()
{
    _io_error = false;
    ::tcflush(_cport, TCIFLUSH);
    if(errno == EIO)
        _io_error = true;
}

void ulib::SerialPort::flush_tx()
{
    _io_error = false;
    ::tcflush(_cport, TCOFLUSH);
    if(errno == EIO)
        _io_error = true;
}

void ulib::SerialPort::flush_rxtx()
{
    _io_error = false;
    ::tcflush(_cport, TCIOFLUSH);
    if(errno == EIO)
        _io_error = true;
}