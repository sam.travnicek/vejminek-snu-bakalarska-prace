#ifndef ULIB_DATETIME_HPP
#define ULIB_DATETIME_HPP

#include <ctime>
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdint>
#include <ulib/Time.hpp>

extern "C" {
#include <sys/time.h>
}

namespace ulib {

class DateTime
{
	int _year, _month, _mday;
	int _hour, _minutes, _seconds, _milliseconds;
	int _wday, _yday;
	int64_t _timestamp;
	bool _corrupted;

public:
	DateTime(std::time_t timestamp, int millis)
		: _corrupted(false)
	{
		std::tm* now = std::localtime(&timestamp);
		_year = now->tm_year + 1900; 	// [1900...]
		_month = now->tm_mon; 			// [0 -  11]
		_mday = now->tm_mday; 			// [1 -  31]
		_hour = now->tm_hour; 			// [0 -  23]
		_minutes = now->tm_min; 		// [0 -  59]
		_seconds = now->tm_sec; 		// [0 -  59]
		_wday = now->tm_wday; 			// [0 -   6]
		_yday = now->tm_yday; 			// [0 - 365]
		_milliseconds = millis;
		_timestamp = ((int64_t)timestamp) * 1000 + millis;
	}

	DateTime(const std::string &s)
		: _year(-1), _month(-1), _mday(-1), _hour(-1), _minutes(-1),
		  _seconds(-1), _milliseconds(-1), _wday(-1), _yday(-1), _timestamp(0)
	{
		if(s.length() != 23) {
			_corrupted = true;
			return;
		}

		try {
			_year = stol(s.substr(0, 4), nullptr, 10);
			_month = stol(s.substr(5, 2), nullptr, 10) - 1;
			_mday = stol(s.substr(8, 2), nullptr, 10);
			_hour = stol(s.substr(11, 2), nullptr, 10);
			_minutes = stol(s.substr(14, 2), nullptr, 10);
			_seconds = stol(s.substr(17, 2), nullptr, 10);
			_milliseconds = stol(s.substr(20, 3), nullptr, 10);

			struct tm _tm;
			_tm.tm_year = _year - 1900;
			_tm.tm_mon = _month;
			_tm.tm_mday = _mday;
			_tm.tm_hour = _hour;
			_tm.tm_min = _minutes;
			_tm.tm_sec = _seconds;
			std::time_t rawtime = mktime(&_tm);

			_wday = _tm.tm_wday;
			_yday = _tm.tm_yday;

			_timestamp = ((int64_t)rawtime) * 1000 + _milliseconds;
		} catch(...) {
			_corrupted = true;
			return;
		}
	}

	operator std::string() const {
		std::stringstream ss;
		ss << _year << "-";
		ss << std::setw(2) << std::setfill('0') << (_month+1) << "-";
		ss << std::setw(2) << std::setfill('0') << _mday << " ";
		ss << std::setw(2) << std::setfill('0') << _hour << ":";
		ss << std::setw(2) << std::setfill('0') << _minutes << ":";
		ss << std::setw(2) << std::setfill('0') << _seconds << ".";
		ss << std::setw(3) << std::setfill('0') << _milliseconds;
		return ss.str();
	}

	std::string date_str() const {
		std::stringstream ss;
		ss << _year << "-";
		ss << std::setw(2) << std::setfill('0') << (_month+1) << "-";
		ss << std::setw(2) << std::setfill('0') << _mday;
		return ss.str();
	}

	operator int64_t() const { return _timestamp; }

	friend std::ostream& operator<<(std::ostream& os, const DateTime &t)
	{
		os << t._year << "-";
		os << std::setw(2) << std::setfill('0') << (t._month+1) << "-";
		os << std::setw(2) << std::setfill('0') << t._mday << " ";
		os << std::setw(2) << std::setfill('0') << t._hour << ":";
		os << std::setw(2) << std::setfill('0') << t._minutes << ":";
		os << std::setw(2) << std::setfill('0') << t._seconds << ".";
		os << std::setw(3) << std::setfill('0') << t._milliseconds;
		return os;
	}

	inline int year() const { return _year; }
	inline int month() const { return _month; }
	inline int wday() const { return _wday; }
	inline int mday() const { return _mday; }
	inline int yday() const { return _yday; }
	inline int hour() const { return _hour; }
	inline int min() const { return _minutes; }
	inline int sec() const { return _seconds; }
	inline int msec() const { return _milliseconds; }

	inline bool good() const { return !_corrupted; }

	inline friend bool operator==(const DateTime &a, const DateTime &b) {
		return (int64_t)a == (int64_t)b; }
	inline friend bool operator!=(const DateTime &a, const DateTime &b) {
		return (int64_t)a != (int64_t)b; }
	inline friend bool operator<=(const DateTime &a, const DateTime &b) {
		return (int64_t)a <= (int64_t)b; }
	inline friend bool operator>=(const DateTime &a, const DateTime &b) {
		return (int64_t)a >= (int64_t)b; }
	inline friend bool operator>(const DateTime &a, const DateTime &b) {
		return (int64_t)a >  (int64_t)b; }
	inline friend bool operator<(const DateTime &a, const DateTime &b) {
		return (int64_t)a <  (int64_t)b; }
	inline friend int64_t operator-(const DateTime &a, const DateTime &b) {
		return a._timestamp - b._timestamp; }

	inline bool between(DateTime& a, DateTime& b) {
		return *this >= a && *this <= b; }

	inline ulib::Time time() {
		return ulib::Time(_hour, _minutes, _seconds, _milliseconds); }

	static DateTime now()
	{
		std::time_t t = std::time(0);   // get time now
		struct timeval tv;
		gettimeofday(&tv, NULL);
		int millis = tv.tv_usec / 1000;
		return DateTime(t, millis);
	}
};

}

#endif /* ULIB_DATETIME_HPP */
