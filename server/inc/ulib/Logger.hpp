#ifndef ULIB_LOGGER_HPP
#define ULIB_LOGGER_HPP

#include <fstream>
#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm>
#include <ulib/DateTime.hpp>
#include <ulib/types.hpp>

namespace ulib {

class LoggerFlag
{
	unsigned int _type;

public:
	LoggerFlag(unsigned int type)
		: _type(type) {}

	inline unsigned int type() const { return _type; }

	/*friend std::ostream& operator<<
		(std::ostream& stream, const LoggerFlag& flag);*/
};

class LoggerConf
{
	std::vector<std::string> _out_file_paths;
	unsigned int _log_level, _print_level;
	bool _print_datetime, _print_date, _log_datetime, _log_date;
	bool _level_colorful, _level_bold;

public:
	static constexpr unsigned int
		DEBUG 	= 5,
		INFO 	= 4,
		WARN 	= 3,
		ERROR 	= 2,
		FATAL 	= 1,
		OFF 	= 0;

	static constexpr unsigned int
		COL_BLACK 		= 30,
		COL_RED 		= 31,
		COL_GREEN 		= 32,
		COL_YELLOW		= 33,
		COL_BLUE		= 34,
		COL_MAGENTA		= 35,
		COL_CYAN		= 36,
		COL_LGRAY		= 37,
		COL_DGRAY		= 90,
		COL_LRED		= 91,
		COL_LGREEN		= 92,
		COL_LYELLOW		= 93,
		COL_LBLUE		= 94,
		COL_LMAGENTA	= 95,
		COL_LCYAN		= 96,
		COL_WHITE		= 97;

	static const std::vector<std::string> LEVELS;
	static const std::vector<unsigned int> LEVEL_COLORS;

	LoggerConf(const std::vector<std::string> &out_file_paths, unsigned int log_level,
			unsigned int print_level, bool print_datetime, bool print_date,
			bool log_datetime, bool log_date, bool level_colorful,
			bool level_bold)
		: _out_file_paths(out_file_paths), _log_level(log_level),
		  _print_level(print_level), _print_datetime(print_datetime),
		  _print_date(print_date), _log_datetime(log_datetime),
		  _log_date(log_date), _level_colorful(level_colorful),
		  _level_bold(level_bold) {}

	LoggerConf(const std::string &out_file_path, unsigned int log_level,
			unsigned int print_level, bool print_datetime, bool print_date,
			bool log_datetime, bool log_date, bool level_colorful,
			bool level_bold)
		: _out_file_paths({out_file_path}), _log_level(log_level),
		  _print_level(print_level), _print_datetime(print_datetime),
		  _print_date(print_date), _log_datetime(log_datetime),
		  _log_date(log_date), _level_colorful(level_colorful),
		  _level_bold(level_bold) {}

	LoggerConf(const std::string &out_file_path, unsigned int log_level,
			unsigned int print_level, bool level_colorful, bool level_bold)
		: LoggerConf(out_file_path, log_level, print_level, false,
		  true, true, true, level_colorful, level_bold) {}

	LoggerConf(bool print_datetime, bool print_date)
		: LoggerConf("", DEBUG, DEBUG, print_datetime, print_date,
		  false, false, true, true) {}

	LoggerConf()
		: LoggerConf(false, false) {}

	inline std::vector<std::string> out_file_paths() const {
		return _out_file_paths; }
	inline unsigned int log_level() const { return _log_level; }
	inline unsigned int print_level() const { return _print_level; }
	inline bool print_datetime() const { return _print_datetime; }
	inline bool print_date() const { return _print_date; }
	inline bool log_datetime() const { return _log_datetime; }
	inline bool log_date() const { return _log_date; }
	inline bool level_colorful() const { return _level_colorful; }
	inline bool level_bold() const { return _level_bold; }

	inline void out_file_path(const std::string &value) {
		_out_file_paths = {value}; }
	inline void out_file_paths(const std::vector<std::string> &value) {
		_out_file_paths = value; }
	inline void log_level(unsigned int value) { _log_level = value; }
	inline void print_level(unsigned int value) { _print_level = value; }
	inline void print_datetime(bool value) { _print_datetime = value; }
	inline void print_date(bool value) { _print_date = value; }
	inline void log_datetime(bool value) { _log_datetime = value; }
	inline void log_date(bool value) { _log_date = value; }
	inline void level_colorful(bool value) { _level_colorful = value; }
	inline void level_bold(bool value) { _level_bold = value; }
};

class Logger
{
	std::string _name;
	std::stringstream _stream;
	LoggerConf _config;

	//TODO: implement strftime() for custom datetime formats and modify
	// string input to accept %q for milliseconds
	virtual std::string get_message(bool log, unsigned int level,
		const std::string &message);
	virtual std::string get_buff_print_message();
	virtual std::string get_buff_log_message();
	virtual void clear_buff();

public:
	Logger(const std::string &name, const LoggerConf &config)
		: _name(name), _config(config) {}

	Logger(const std::string &name)
		: _name(name) {}

	virtual ~Logger() {}

	virtual void print(unsigned int level, const std::string &message);
	virtual void log(unsigned int level, const std::string &message);
	void debug(const std::string &msg);
	void debug();
	void info(const std::string &msg);
	void info();
	void warn(const std::string &msg);
	void warn();
	void error(const std::string &msg);
	void error();
	void fatal(const std::string &msg);
	void fatal();
	void output(unsigned int level, const std::string &msg);
	void output(unsigned int level);
	static unsigned int get_level(std::string name);

	template<class T>
	inline void write(const T& item) { _stream << item; }

	template<class T>
	Logger& operator<<(const T& item)
	{
		write(item);
		return *this;
	}

	Logger& operator<<(const ulib::LoggerFlag& flag)
	{
		switch(flag.type()) {
		case ulib::LoggerConf::DEBUG:
			debug();
			break;
		case ulib::LoggerConf::INFO:
			info();
			break;
		case ulib::LoggerConf::WARN:
			warn();
			break;
		case ulib::LoggerConf::ERROR:
			error();
			break;
		case ulib::LoggerConf::FATAL:
			fatal();
			break;
		default:
			break;
		}
		return *this;
	}

	inline LoggerConf& configuration() { return _config; }

	inline std::string name() const { return _name; }
	inline void name(const std::string &value) { _name = value; }
};

extern LoggerFlag ldebug;
extern LoggerFlag linfo;
extern LoggerFlag lwarn;
extern LoggerFlag lerror;
extern LoggerFlag lfatal;

}

#endif /* ULIB_LOGGER_HPP */
