#ifndef ULIB_SYSTEM_H
#define ULIB_SYSTEM_H

#include <cstdio>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <cstdlib>
#include <climits>

extern "C" {
#include <unistd.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <libgen.h>
}

namespace ulib {

struct System
{
	static std::string exec(std::string cmd)
	{
	    std::array<char, 128> buffer;
	    std::string result;
	    std::shared_ptr<FILE> pipe(popen(cmd.c_str(), "r"), pclose);
	    if(!pipe) throw std::runtime_error("popen() failed!");
	    while(!feof(pipe.get())) {
	        if(fgets(buffer.data(), 128, pipe.get()) != nullptr)
	            result += buffer.data();
	    }
	    return result;
	}

	static void sleep(uint64_t time_ms)
	{
		::usleep(time_ms * 1000);
	}

	static void usleep(uint64_t time_us)
	{
		::usleep(time_us);
	}

	static std::string username()
	{
		char* usr = getenv("USER");
		if(usr == nullptr)
			return "(?)";
		return std::string(usr);
	}

	static bool make_dir(const std::string &name)
	{
		if(dir_exists(name))
			return true;
		int dir_err = mkdir(name.c_str(), S_IRWXU|S_IRWXG|S_IROTH|S_IXOTH);
		return dir_err != -1;
	}

	static bool dir_exists(const std::string &path)
	{
		DIR* dir = opendir(path.c_str());
		if(dir) {
			closedir(dir);
			return true;
		} else if (ENOENT == errno)
			return false;
		else
			return true;
	}

	static bool dir_accessible(const std::string &path)
	{
		DIR* dir = opendir(path.c_str());
		if(dir) {
			closedir(dir);
			return true;
		}
		return false;
	}

	static std::string dir_name(const std::string &path)
	{
		return std::string(dirname(const_cast<char*>(path.c_str())));
	}

	static std::string file_name(const std::string &path)
	{
		return std::string(basename(const_cast<char*>(path.c_str())));
	}

	static bool file_exist(const std::string &path)
	{
		return access(path.c_str(), F_OK) != -1;
	}

	static bool file_writable(const std::string &path)
	{
		FILE *file = fopen(path.c_str(), "w");
		if(file) {
			fclose(file);
			return true;
		}
		return false;
	}

	static bool file_readable(const std::string &path)
	{
		FILE *file = fopen(path.c_str(), "r");
		if(file) {
			fclose(file);
			return true;
		}
		return false;
	}

	static std::string program_dir()
	{
		char filename[PATH_MAX] = {0};
		std::string path = "";
		if(readlink("/proc/self/exe", filename, PATH_MAX) != -1)
        	path = std::string(dirname(filename));
		return path;
	}
};

struct Epollable
{
    virtual int file_descriptor() = 0;
    virtual void epoll_add(int epoll_fd) = 0;
    virtual void epoll_del(int epoll_fd) = 0;
};

}

#endif /* ULIB_SYSTEM_H */
