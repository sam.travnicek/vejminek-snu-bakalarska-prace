#ifndef ULIB_SERIAL_PORT_HPP
#define ULIB_SERIAL_PORT_HPP

#include <string>
#include <ulib/types.hpp>

extern "C" {

#include <termios.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <sys/file.h>
#include <errno.h>

#include <stdio.h>
#include <string.h>

}

namespace ulib {

class SerialPortFailure : public std::exception
{
    std::string msg;
public:
    SerialPortFailure(const std::string& msg) : msg(msg) {}
    ~SerialPortFailure() noexcept {}
    const char* what() const noexcept { return msg.c_str(); }
};

class SerialPort
{
    std::string _port;
    int _baudrate;
    char _bits;
    char _parity;
    char _stop_bits;
    int _cport;
    termios _old_port_settings;
    bool _io_error;

public:
    static const char PARITY_ODD;
    static const char PARITY_EVEN;
    static const char PARITY_NONE;

    SerialPort(const std::string &port, int baudrate);

    SerialPort(
        const std::string &port, int baudrate, char bits, char parity,
        char stop_bits);

    virtual ~SerialPort() {}

    void open();
    void close();

    size_t poll(ubyte *buff, size_t size);
    std::string read();

    ssize_t send(ubyte b);
    ssize_t send(const ubyte *b, size_t size);
    ssize_t send(const std::string &txt);

    bool dcd_enabled();
    bool cts_enabled();
    bool dsr_enabled();

    void dtr_enable();
    void dtr_disable();
    void rts_enable();
    void rts_disable();

    void flush_rx();
    void flush_tx();
    void flush_rxtx();

    inline const std::string& port() const
        { return _port; }

    inline void port(const std::string &value)
        { _port = value; }

    inline int baudrate() const
        { return _baudrate; }

    inline void baudrate(int &value)
        { _baudrate = value; }

    inline char bits() const
        { return _bits; }

    inline void bits(char &value)
        { _bits = value; }

    inline char parity() const
        { return _parity; }

    inline void parity(char &value)
        { _parity = value; }

    inline char stop_bits() const
        { return _stop_bits; }

    inline void stop_bits(char &value)
        { _stop_bits = value; }

    inline int file_descriptor() const
        { return _cport; }

    inline bool io_error() const
        { return _io_error; }
};

}

#endif /* ULIB_SERIAL_PORT_HPP */