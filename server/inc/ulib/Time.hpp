#ifndef ULIB_TIME_HPP
#define ULIB_TIME_HPP

#include <string>
#include <cstdint>
#include <sstream>
#include <iostream>
#include <ctime>
#include <iomanip>

extern "C" {
#include <sys/time.h>
}

namespace ulib {

class Time
{
	int _hour, _minutes, _seconds, _milliseconds;
	bool _corrupted, _arbitrary;

	void change(int64_t timestamp)
	{
		while(timestamp > FULL_TIMESTAMP)
			timestamp -= FULL_TIMESTAMP;
		while(timestamp < 0)
			timestamp += FULL_TIMESTAMP;

		_hour = (timestamp / 3600000) % 24;
		timestamp -= (timestamp / 3600000) * 3600000;
		_minutes = timestamp / 60000;
		timestamp -= _minutes * 60000;
		_seconds = timestamp / 1000;
		timestamp -= _seconds * 1000;
		_milliseconds = timestamp;
		_arbitrary = false;
	}

	void change(int h, int m, int s, int ms)
	{
		_corrupted = false;
		_hour = 0; _minutes = 0; _seconds = 0; _milliseconds = 0;
		int64_t timestamp = ms + s*1000 + m*60000 + h*3600000;
		change(timestamp);
	}

	static const int64_t FULL_TIMESTAMP = 86400000;

public:
	Time(int h, int m, int s, int ms) { change(h, m, s, ms); }
	Time(int h, int m, int s) { change(h, m, s, 0); }
    Time(int h, int m) { change(h, m, 0, 0); }
	Time(int64_t timestamp) { change(timestamp); }

	Time(std::string hm)
		: _hour(0), _minutes(0), _seconds(0), _milliseconds(0),
		  _corrupted(false), _arbitrary(false)
    {
		int h = 0, m = 0, s = 0, ms = 0;

		if(hm.length() == 5) {
			try {
				h = stol(hm.substr(0, 2), nullptr, 10);
				m = stol(hm.substr(3, 2), nullptr, 10);
			} catch(...) {
				_corrupted = true;
				return;
			}
		} else if(hm.length() == 8) {
			try {
				h = stol(hm.substr(0, 2), nullptr, 10);
				m = stol(hm.substr(3, 2), nullptr, 10);
				s = stol(hm.substr(6, 2), nullptr, 10);
			} catch(...) {
				_corrupted = true;
				return;
			}
		} else if(hm.length() == 12) {
			try {
				h = stol(hm.substr(0, 2), nullptr, 10);
				m = stol(hm.substr(3, 2), nullptr, 10);
				s = stol(hm.substr(6, 2), nullptr, 10);
				ms= stol(hm.substr(9, 3), nullptr, 10);
			} catch(...) {
				_corrupted = true;
				return;
			}
		} else {
			_corrupted = true;
			return;
		}

		change(h, m, s, ms);
	}

	inline int hour() const { return _hour; }
	inline int min() const { return _minutes; }
	inline int sec() const { return _seconds; }
	inline int msec() const { return _milliseconds; }

	inline bool good() const { return !_corrupted; }

	inline void arbitrary(bool value) { _arbitrary = value; }
	inline bool arbitrary() const { return _arbitrary; }

	friend bool operator==(const Time &a, const Time &b)
    {
        if(a._arbitrary || b._arbitrary)
            return true;
        return (int64_t)a == (int64_t)b;
    }

	friend bool operator!=(const Time &a, const Time &b)
    {
        if(a._arbitrary || b._arbitrary)
            return false;
        return (int64_t)a != (int64_t)b;
    }

	friend bool operator<=(const Time &a, const Time &b)
    {
        if(a._arbitrary || b._arbitrary)
            return true;
        return (int64_t)a <= (int64_t)b;
    }

	friend bool operator>=(const Time &a, const Time &b)
    {
        if(a._arbitrary || b._arbitrary)
            return true;
        return (int64_t)a >= (int64_t)b;
    }

	friend bool operator>(const Time &a, const Time &b)
    {
        if(a._arbitrary || b._arbitrary)
            return true;
        return (int64_t)a >  (int64_t)b;
    }

	friend bool operator<(const Time &a, const Time &b)
    {
        if(a._arbitrary || b._arbitrary)
            return true;
        return (int64_t)a < (int64_t)b;
    }

	inline int64_t timestamp() const {
		return _milliseconds + _seconds*1000 + _minutes*60000 + _hour*3600000; }
	operator int64_t() const { return timestamp(); }

	operator std::string() const
    {
		std::stringstream ss;
		ss << std::setw(2) << std::setfill('0') << _hour << ":";
		ss << std::setw(2) << std::setfill('0') << _minutes << ":";
		ss << std::setw(2) << std::setfill('0') << _seconds << ".";
		ss << std::setw(3) << std::setfill('0') << _milliseconds;
		return ss.str();
	}

	friend std::ostream& operator<<(std::ostream& os, const Time &t)
    {
		os << std::setw(2) << std::setfill('0') << t._hour << ":";
		os << std::setw(2) << std::setfill('0') << t._minutes << ":";
		os << std::setw(2) << std::setfill('0') << t._seconds << ".";
		os << std::setw(3) << std::setfill('0') << t._milliseconds;
		return os;
	}

	inline friend Time operator-(const Time &a, const Time &b) {
		return Time(a.timestamp() - b.timestamp()); }
	inline friend Time operator+(const Time &a, const Time &b) {
		return Time(a.timestamp() + b.timestamp()); }
	inline friend Time operator-(const Time &a, int64_t b) {
		return Time(a.timestamp() - b); }
	inline friend Time operator+(const Time &a, int64_t b) {
		return Time(a.timestamp() + b); }
	inline friend Time operator*(const Time &a, int64_t b) {
		return Time(a.timestamp() * b); }
	inline friend Time operator/(const Time &a, int64_t b) {
		return Time(a.timestamp() / b); }

	inline Time& operator-=(const Time &a) { *this = *this - a; return *this; }
	inline Time& operator+=(const Time &a) { *this = *this + a; return *this; }
	inline Time& operator-=(int64_t a) { *this = *this - a; return *this; }
	inline Time& operator+=(int64_t a) { *this = *this + a; return *this; }
	inline Time& operator*=(int64_t a) { *this = *this * a; return *this; }
	inline Time& operator/=(int64_t a) { *this = *this / a; return *this; }

	inline bool between(const Time &a, const Time &b) const {
		return *this >= a && *this <= b; }

	int& operator[](unsigned int x)
    {
		switch(x) {
		case 0: return _hour;
		case 1: return _minutes;
		case 2: return _seconds;
		case 3: return _milliseconds;
		default: return _milliseconds; //TODO: throw exception instead
		}
    }

	static Time now()
    {
		std::time_t _timestamp = std::time(0);
		std::tm* now = std::localtime(&_timestamp);
		struct timeval tv;
		gettimeofday(&tv, NULL);
		int millis = tv.tv_usec / 1000;
		return Time(now->tm_hour, now->tm_min, now->tm_sec, millis);
	}

	static Time zero_arbitrary()
    {
		Time t(0, 0);
		t.arbitrary(true);
		return t;
	}
};

}

#endif /* ULIB_TIME_HPP */
