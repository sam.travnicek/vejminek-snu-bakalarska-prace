#ifndef ULIB_UUID_HPP
#define ULIB_UUID_HPP

#include <cstdlib>
#include <string>

namespace ulib {

struct UUID
{
    static void seed_random()
    {
        srand(time(NULL));
    }

    static std::string random()
    {
        char uuid[37];
        sprintf(uuid, "%04x%04x-%04x-%04x-%04x-%04x%04x%04x",
            rand() & 0xFFFF, rand() & 0xFFFF,
            rand() & 0xFFFF,
            (rand() & 0x0FFF) | 0x4000,
            (rand() & 0x3fff) + 0x8000,
            rand() & 0xFFFF, rand() & 0xFFFF, rand() & 0xFFFF);
        return std::string(uuid);
    }
};

}


#endif /* ULIB_UUID_HPP */