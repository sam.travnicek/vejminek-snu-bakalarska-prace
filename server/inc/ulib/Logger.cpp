#include <ulib/Logger.hpp>
#include <ulib/StringUtil.hpp>
#include <ulib/DateTime.hpp>

using namespace ulib;

const std::vector<std::string> LoggerConf::LEVELS
    = {"OFF", "FATAL", "ERROR", "WARN ", "INFO ", "DEBUG"};
const std::vector<unsigned int> LoggerConf::LEVEL_COLORS
    = {49, COL_RED, COL_RED, COL_YELLOW, COL_GREEN, COL_BLUE};

LoggerFlag ulib::ldebug = LoggerFlag(LoggerConf::DEBUG);
LoggerFlag ulib::linfo = LoggerFlag(LoggerConf::INFO);
LoggerFlag ulib::lwarn = LoggerFlag(LoggerConf::WARN);
LoggerFlag ulib::lerror = LoggerFlag(LoggerConf::ERROR);
LoggerFlag ulib::lfatal = LoggerFlag(LoggerConf::FATAL);

std::string Logger::get_message(
    bool log, unsigned int level, const std::string &message)
{
    if(level > LoggerConf::DEBUG)
        level = LoggerConf::DEBUG;

    std::string msg;
    if(log && _config.log_datetime()) {
        if(_config.log_date())
            msg += "[" + (std::string)DateTime::now() + "] ";
        else
            msg += "[" + (std::string)Time::now() + "] ";
    } else if(!log && _config.print_datetime()) {
        if(_config.print_date())
            msg += "[" + (std::string)DateTime::now() + "] ";
        else
            msg += "[" + (std::string)Time::now() + "] ";
    }
    if(!log && (_config.level_colorful() || _config.level_bold())) {
        msg += "\x1B[" + std::string(_config.level_bold() ? "1" : "0") + ";";
        msg += std::to_string(LoggerConf::LEVEL_COLORS[level]) + "m";
        msg += LoggerConf::LEVELS[level] + "\x1B[0m : ";
    } else
        msg += LoggerConf::LEVELS[level] + " : ";

    if(_name != "")
        msg += "(" + _name + ") ";

    msg += message + "\n";
    return msg;
}

std::string Logger::get_buff_log_message()
{
    std::string rawmsg = _stream.str();
    if(rawmsg.length() == 0)
        return std::string();
    if(rawmsg[rawmsg.length()-1] == '\n')
        rawmsg = rawmsg.substr(0, rawmsg.length()-1);

    int nl_cnt = 0;
    for(char c : rawmsg)
        if(c == '\n')
            nl_cnt++;
    std::string msg;
    if(nl_cnt == 0)
        msg = rawmsg;
    else {
        msg += ">>\n";
        if(_config.log_datetime()) {
            if(_config.log_date())
                msg += "           ";
            msg += "               ";
        }
        msg += "      ¦ ";
        for(char c : rawmsg) {
            msg += c;
            if(c == '\n') {
                if(_config.log_datetime()) {
                    if(_config.log_date())
                        msg += "           ";
                    msg += "               ";
                }
                msg += "      ¦ ";
            }
        }
    }
    return msg;
}

std::string Logger::get_buff_print_message()
{
    std::string rawmsg = _stream.str();
    if(rawmsg.length() == 0)
        return std::string();
    if(rawmsg[rawmsg.length()-1] == '\n')
        rawmsg = rawmsg.substr(0, rawmsg.length()-1);

    int nl_cnt = 0;
    for(char c : rawmsg)
        if(c == '\n')
            nl_cnt++;
    std::string msg;
    if(nl_cnt == 0)
        msg = rawmsg;
    else {
        msg += ">>\n";
        if(_config.print_datetime()) {
            if(_config.print_date())
                msg += "           ";
            msg += "               ";
        }
        msg += "      ¦ ";
        for(char c : rawmsg) {
            msg += c;
            if(c == '\n') {
                if(_config.print_datetime()) {
                    if(_config.print_date())
                        msg += "           ";
                    msg += "               ";
                }
                msg += "      ¦ ";
            }
        }
    }
    return msg;
}

void Logger::clear_buff()
{
    _stream.str(std::string()); //clear buffer
}

void Logger::print(unsigned int level, const std::string &message)
{
    if(level > _config.print_level() || message == "" || message == "\n"
            || message == "\r\n")
        return;

    if(level > LoggerConf::ERROR)
        std::cout << get_message(false, level, message);
    else
        std::cerr << get_message(false, level, message);
}

void Logger::log(unsigned int level, const std::string &message)
{
    if(level > _config.log_level() || message == "" || message == "\n"
            || message == "\r\n")
        return;

    for(std::string &path : _config.out_file_paths()) {
        std::ofstream ofs;
        std::string p = StringUtil::replace_all(
            path, "{DATE}", DateTime::now().date_str());
        ofs.open(p, std::ofstream::out | std::ofstream::app);
        if(ofs.good()) {
            ofs << get_message(true, level, message);
            ofs.close();
        }
    }
}

void Logger::debug(const std::string &msg)
{
    print(LoggerConf::DEBUG, msg);
    log(LoggerConf::DEBUG, msg);
}

void Logger::debug()
{
    print(LoggerConf::DEBUG, get_buff_print_message());
    log(LoggerConf::DEBUG, get_buff_log_message());
    clear_buff();
}

void Logger::info(const std::string &msg)
{
    print(LoggerConf::INFO, msg);
    log(LoggerConf::INFO, msg);
}

void Logger::info()
{
    print(LoggerConf::INFO, get_buff_print_message());
    log(LoggerConf::INFO, get_buff_log_message());
    clear_buff();
}

void Logger::warn(const std::string &msg)
{
    print(LoggerConf::WARN, msg);
    log(LoggerConf::WARN, msg);
}

void Logger::warn()
{
    print(LoggerConf::WARN, get_buff_print_message());
    log(LoggerConf::WARN, get_buff_log_message());
    clear_buff();
}

void Logger::error(const std::string &msg)
{
    print(LoggerConf::ERROR, msg);
    log(LoggerConf::ERROR, msg);
}

void Logger::error()
{
    print(LoggerConf::ERROR, get_buff_print_message());
    log(LoggerConf::ERROR, get_buff_log_message());
    clear_buff();
}

void Logger::fatal(const std::string &msg)
{
    print(LoggerConf::FATAL, msg);
    log(LoggerConf::FATAL, msg);
}

void Logger::fatal()
{
    print(LoggerConf::FATAL, get_buff_print_message());
    log(LoggerConf::FATAL, get_buff_log_message());
    clear_buff();
}

void Logger::output(unsigned int level, const std::string &msg)
{
    print(level, msg);
    log(level, msg);
}

void Logger::output(unsigned int level)
{
    print(level, get_buff_print_message());
    log(level, get_buff_log_message());
    clear_buff();
}

unsigned int Logger::get_level(std::string name)
{
    std::transform(name.begin(), name.end(), name.begin(), ::tolower);
    for(unsigned int x = 0; x < LoggerConf::LEVELS.size(); x++) {
        std::string s = LoggerConf::LEVELS[x];
        std::transform(s.begin(), s.end(), s.begin(), ::tolower);
        if(s == s+name)
            return x;
    }
    return 0;
}