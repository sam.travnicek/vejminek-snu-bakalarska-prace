#ifndef ULIB_TYPES_HPP
#define ULIB_TYPES_HPP

#include <cstdint>
#include <cstddef>

typedef unsigned int uint;
typedef unsigned short ushort;
typedef unsigned long ulong;
typedef unsigned long long ulonglong;

typedef unsigned char uchar;

typedef unsigned char ubyte;

#endif /* ULIB_TYPES_HPP */