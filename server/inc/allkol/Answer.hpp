#ifndef ALLKOL_ANSWER_HPP_
#define ALLKOL_ANSWER_HPP_

#include <allkol/Message.hpp>

namespace allkol {

class Answer : public Message
{
    enum ReturnCode { OK = 0, ERROR };

    std::string _error_reason;
    ReturnCode _ret_code;

public:
    Answer(uint64_t identifier, const std::string &command)
        : Message(identifier, command, true),
          _error_reason(""), _ret_code(OK) {}

    inline void ok()
        { _ret_code = OK; }

    inline void error(const std::string &reason)
        { _ret_code = ERROR; _error_reason = reason; }

    nlohmann::json serialize() const override;

    inline bool is_ok() const
        { return _ret_code == OK; }

    inline std::string error_reason() const
        { return _error_reason; }

    inline ReturnCode return_code() const
        { return _ret_code; }
};

}

#endif // ALLKOL_ANSWER_HPP_