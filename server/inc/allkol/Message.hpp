#ifndef ALLKOL_MESSAGE_HPP
#define ALLKOL_MESSAGE_HPP

#include <nlohmann/json.hpp>
#include <string>
#include <cstdint>

namespace allkol {

class Message
{
protected:
    uint64_t _identifier;
    std::string _command;
    bool _is_answer;
    nlohmann::json _data;

public:
    Message(uint64_t identifier, const std::string &command, bool is_answer)
        : _identifier(identifier), _command(command), _is_answer(is_answer),
          _data(nlohmann::json::object()) {}

    virtual ~Message() {}

    virtual nlohmann::json serialize() const;

    inline uint64_t identifier() const
        { return _identifier; }

    inline std::string command() const
        { return _command; }

    inline bool is_answer() const
        { return _is_answer; }

    inline nlohmann::json& data()
        { return _data; }
};

}

#endif /* ALLKOL_MESSAGE_HPP */
