#include <allkol/Message.hpp>

using namespace allkol;

nlohmann::json Message::serialize() const
{
    nlohmann::json res;
    res["identifier"] = _identifier;
    res["command"] = _command;
    res["is_answer"] = _is_answer;
    res["data"] = _data;
    return res;
}