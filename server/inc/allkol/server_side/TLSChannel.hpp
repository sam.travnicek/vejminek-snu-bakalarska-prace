#ifndef ALLKOL_SERVER_SIDE_SECURE_CHANNEL_HPP
#define ALLKOL_SERVER_SIDE_SECURE_CHANNEL_HPP

#include <allkol/server_side/Channel.hpp>
#include <allkol/server_side/TLSClient.hpp>
#include <ulib-net/TLSServer.hpp>

namespace allkol {

class TLSChannel : public Channel, public ulib::net::TLSServer
{
    bool accept(ulib::net::TLSConnection *con);

    void disconnect(TLSClient *client);

public:
    TLSChannel(size_t con_limit, allkol::MessageHandler &msg_handler, ulib::Logger &logger)
        : Channel(con_limit, msg_handler, logger) {}

    virtual ~TLSChannel();

    virtual void tick() override;
};

}

#endif /* ALLKOL_SERVER_SIDE_SECURE_CHANNEL_HPP */