#ifndef ALLKOL_SERVER_SIDE_WS_CHANNEL_HPP
#define ALLKOL_SERVER_SIDE_WS_CHANNEL_HPP

#include <allkol/server_side/Channel.hpp>
#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>
#include <mutex>

namespace allkol {

class WSChannel : public Channel
{
    using ws_server = websocketpp::server<websocketpp::config::asio>;
    using con_hdl = websocketpp::connection_hdl;
    using msg_ptr = std::shared_ptr<websocketpp::config::core::message_type>;

    std::unique_ptr<std::thread> _run_thread;

    std::mutex _clients_mutex;
    std::mutex _allowed_uuids_mutex;
    ws_server _server;

    bool _on_validate(con_hdl hdl);

    void _on_fail(con_hdl hdl);

    void _on_close(con_hdl hdl);

    void _on_message(con_hdl hdl, msg_ptr msg);

    void _run();

public:
    WSChannel(
        size_t con_limit, allkol::MessageHandler &msg_handler,
        ulib::Logger &logger);

    void open(const std::string &ip, int port);

    void close();

    virtual void allowed_uuids(
        const std::vector<std::string> &allowed_uuids) override;

    virtual void broadcast(const nlohmann::json &message) override;

    virtual void broadcast_update(const nlohmann::json &message) override;

    virtual ~WSChannel();

    virtual void tick() override {}
};

}

#endif /* ALLKOL_SERVER_SIDE_WS_CHANNEL_HPP */