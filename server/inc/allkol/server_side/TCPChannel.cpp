#include <allkol/server_side/TCPChannel.hpp>
#include <allkol/server_side/TCPClient.hpp>
#include <algorithm>

extern "C" {
#include <unistd.h>
}

using namespace allkol;

TCPChannel::~TCPChannel()
{
    std::vector<Client*> cls;
    for(auto it : _clients)
        cls.push_back(it.second);
    for(Client *client : cls) {
        TCPClient *tcp_client = dynamic_cast<TCPClient*>(client);
        if(!tcp_client)
            continue;
        disconnect(tcp_client);
    }
}

void TCPChannel::disconnect(TCPClient *tcp_client)
{
    _logger << "Client '" << tcp_client->uuid() << "'@";
    _logger << tcp_client->ip_address() << " disconnected." << ulib::ldebug;

    _clients.erase(tcp_client->connection()->file_descriptor());
    delete tcp_client;
}

bool TCPChannel::accept(ulib::net::TCPConnection *con)
{
    if(_clients.size() >= _con_limit)
        return false;

    int client_fd = con->file_descriptor();
    epoll_event ep_ev;
    ep_ev.data.fd = client_fd;
    ep_ev.events = EPOLLIN;

    if(epoll_ctl(Channel::_epoll_fd, EPOLL_CTL_ADD, client_fd, &ep_ev) != 0) {
        throw ChannelFailure(
            "Failed to add the client file descriptor to epoll.\n");
    }

    _clients[client_fd] = new TCPClient(con);
    _logger << "Client ''@" << con->ip_address() << " connected." <<ulib::ldebug;

    return true;
}

void TCPChannel::tick()
{
    std::vector<ulib::net::TCPConnection*> cons = accept_all();
    for(ulib::net::TCPConnection *con : cons)
        accept(con);

    epoll_event *_ep_events = new epoll_event[_con_limit];
    int ev_cnt = epoll_wait(Channel::_epoll_fd, _ep_events, _con_limit, 0);

    char buf[Channel::READ_BUF_SIZE];
    for(int i = 0; i < ev_cnt; i++) {
        Client *client = _clients[_ep_events[i].data.fd];
        TCPClient *tcp_client = dynamic_cast<TCPClient*>(client);
        if(!tcp_client)
            continue;

        ulib::net::TCPConnection *con = tcp_client->connection();
        int x = con->read(buf, sizeof(buf)-1);
        buf[x] = 0;

        if(x > 0) {
            nlohmann::json message = nlohmann::json::parse(std::string(buf));
            _logger << "Message: " << std::setw(2) << message << ulib::ldebug;
            try {
                bool is_answer = message["is_answer"];
                uint64_t identifier = message["identifier"];
                std::string command = message["command"];

                Message msg(identifier, command, is_answer);
                msg.data() = message["data"];
                _msg_handler.on_message(id(), *client, msg);
            } catch(std::exception &ex) {
                _logger << "Failed to handle client message." << ulib::ldebug;
                _logger << ex.what() << ulib::ldebug;
            } catch(...) {
                _logger << "Failed to handle client message." << ulib::ldebug;
            }
        } else {
            disconnect(tcp_client);
        }
    }

    delete[] _ep_events;

    std::vector<Client*> clients_to_disconnect;
    for(auto it : _clients)
        if(it.second->to_disconnect())
            clients_to_disconnect.push_back(it.second);
    for(Client *client : clients_to_disconnect) {
        TCPClient *tcp_client = dynamic_cast<TCPClient*>(client);
        if(!tcp_client)
            continue;
        disconnect(tcp_client);
    }
}