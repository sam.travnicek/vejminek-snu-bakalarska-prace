#include <allkol/server_side/WSChannel.hpp>
#include <allkol/server_side/WSClient.hpp>
#include <functional>

using namespace allkol;

WSChannel::WSChannel(
        size_t con_limit, allkol::MessageHandler &msg_handler,
        ulib::Logger &logger)
    : Channel(con_limit, msg_handler, logger)
{
    _server.init_asio();
    _server.set_listen_backlog(_con_limit);
    _server.set_reuse_addr(true);
    _server.clear_access_channels(websocketpp::log::alevel::all);
    _server.clear_error_channels(websocketpp::log::alevel::all);

    auto validate_binding = std::bind(
        &WSChannel::_on_validate, this, std::placeholders::_1);
    _server.set_validate_handler(validate_binding);

    auto fail_binding = std::bind(
        &WSChannel::_on_fail, this, std::placeholders::_1);
    _server.set_fail_handler(fail_binding);

    auto close_binding = std::bind(
        &WSChannel::_on_close, this, std::placeholders::_1);
    _server.set_close_handler(close_binding);

    auto message_binding = std::bind(
        &WSChannel::_on_message, this, std::placeholders::_1,
        std::placeholders::_2);
    _server.set_message_handler(message_binding);
}

WSChannel::~WSChannel()
{
    close();
    _run_thread->join();
}

void WSChannel::open(const std::string &ip, int port)
{
    using namespace asio;

    ip::tcp::endpoint ep(ip::address::from_string(ip), port);
    _server.listen(ep); // throws an exception

    websocketpp::lib::error_code ec;
    _server.start_accept(ec);
    if(ec)
        _logger << ec.message() << ulib::lerror;

    _run_thread = std::make_unique<std::thread>(
        std::bind(&WSChannel::_run, this));
    // printf("BEFORE RUN %s:%u\n", __FILE__, __LINE__);
    // _server.run(); // throws an exception
    // printf("AFTER RUN %s:%u\n", __FILE__, __LINE__);
}

void WSChannel::close()
{
    websocketpp::lib::error_code ec;
    _server.stop_listening(ec);
    if(ec)
        _logger << ec.message() << ulib::lerror;

    std::lock_guard<std::mutex> lock_clients(_clients_mutex);

    for(auto it : _clients) {
        WSClient *client = dynamic_cast<WSClient*>(it.second);
        _logger << "Client '" << client->uuid() << "'@";
        _logger << client->ip_address() << " disconnected." << ulib::ldebug;

        websocketpp::lib::error_code ec;
        _server.close(client->connection(), websocketpp::close::status::normal,
            "Channel closed.", ec);
        if(ec)
            _logger << ec.message() << ulib::lerror;
        delete client;
    }

    _clients.clear();
    _server.stop();
}

bool WSChannel::_on_validate(con_hdl hdl)
{
    _clients_mutex.lock();
    if(_clients.size() >= _con_limit) {
        _clients_mutex.unlock();
        return false;
    }
    _clients_mutex.unlock();

    ws_server::connection_ptr con = _server.get_con_from_hdl(hdl);
    websocketpp::uri_ptr uri = con->get_uri();
    std::string query = uri->get_query();
    std::unordered_map<std::string, std::string> query_map;

    if(!query.empty()) {
        std::string key = "";
        std::string value = "";
        bool key_state = true;

        for(size_t i = 0; i < query.length(); ++i) {
            char c = query.at(i);

            if(c == '=') {
                key_state = false;
                continue;
            }

            if (c == '&' || i == query.length()-1) {
                if(c != '&') {
                    if(key_state)
                        key += c;
                    else
                        value += c;
                }
                key_state = true;
                query_map[key] = value;
                key.clear();
                value.clear();
                continue;
            }

            if(key_state)
                key += c;
            else
                value += c;
        }
    }

    WSClient *client = new WSClient(con);

    if(query_map.find("uuid") != query_map.end()) {
        std::string uuid = query_map["uuid"];
        client->authorised(uuid);
    }

    if(query_map.find("subscribe_updates") != query_map.end()) {
        std::string subscr_str = query_map["subscribe_updates"];
        client->send_updates(subscr_str == "true");
    }

    _logger << "Client '" << client->uuid() << "'@";
    _logger << client->ip_address() << " connected." << ulib::ldebug;

    std::lock_guard<std::mutex> lock_clients(_clients_mutex);
    _clients[(long)hdl.lock().get()] = client;

    return true; // accept connection, returning false rejects it
}

void WSChannel::_on_fail(con_hdl hdl)
{
    // ws_server::connection_ptr con = _server.get_con_from_hdl(hdl);
    // websocketpp::lib::error_code ec = con->get_ec();
    // _logger << "on_fail: " << ec.message() << ulib::lerror;
}

void WSChannel::_on_close(con_hdl hdl)
{
    std::lock_guard<std::mutex> lock_clients(_clients_mutex);

    long index = (long)hdl.lock().get();
    WSClient *client = dynamic_cast<WSClient*>(_clients[index]);

    _logger << "Client '" << client->uuid() << "'@";
    _logger << client->ip_address() << " disconnected." << ulib::ldebug;

    _clients.erase(index);
    delete client;
}

void WSChannel::_on_message(con_hdl hdl, msg_ptr msg)
{
    nlohmann::json message;

    try {
        message = nlohmann::json::parse(msg->get_payload());
    } catch(...) {
        _logger << "Failed to handle client message." << ulib::ldebug;
        return;
    }

    _logger << "Message: " << std::setw(2) << message << ulib::ldebug;

    std::lock_guard<std::mutex> lock_clients(_clients_mutex);
    allkol::Client *client = _clients[(long)hdl.lock().get()];
    try {
        bool is_answer = message["is_answer"];
        uint64_t identifier = message["identifier"];
        std::string command = message["command"];

        Message msg(identifier, command, is_answer);
        msg.data() = message["data"];
        _msg_handler.on_message(id(), *client, msg);
    } catch(std::exception &ex) {
        _logger << "Failed to handle client message." << ulib::ldebug;
        _logger << ex.what() << ulib::ldebug;
    } catch(...) {
        _logger << "Failed to handle client message." << ulib::ldebug;
    }
}

void WSChannel::allowed_uuids(const std::vector<std::string> &allowed_uuids)
{
    std::lock_guard<std::mutex> lock_uuids(_allowed_uuids_mutex);
    _allowed_uuids = allowed_uuids;
}

void WSChannel::broadcast(const nlohmann::json &message)
{
    std::lock_guard<std::mutex> lock_clients(_clients_mutex);
    for(auto it : _clients)
        it.second->send(message);
}

void WSChannel::broadcast_update(const nlohmann::json &message)
{
    std::lock_guard<std::mutex> lock_clients(_clients_mutex);
    for(auto it : _clients)
        if(it.second->send_updates())
            it.second->send(message);
}

void WSChannel::_run()
{
    _server.run();
}