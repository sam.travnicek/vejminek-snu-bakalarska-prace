#include <allkol/server_side/TLSClient.hpp>

using namespace allkol;

void TLSClient::send(const nlohmann::json &msg)
{
    std::string str = msg.dump();
    _connection->write(str.c_str(), str.length());
}