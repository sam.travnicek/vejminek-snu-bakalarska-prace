#ifndef ALLKOL_SERVER_SIDE_CHANNEL_HPP
#define ALLKOL_SERVER_SIDE_CHANNEL_HPP

#include <vector>
#include <nlohmann/json.hpp>
#include <ulib/Logger.hpp>
#include <allkol/server_side/ClientManager.hpp>
#include <allkol/server_side/MessageHandler.hpp>

extern "C" {
#include <sys/epoll.h>
}

namespace allkol {

class ChannelFailure : public std::exception
{
    std::string msg;

public:
    ChannelFailure(std::string msg)
        : msg(msg) {}

    ~ChannelFailure() noexcept {}

    const char* what() const noexcept
        { return msg.c_str(); }
};

class Channel : public ClientManager
{
    uint _id;

protected:
    int _epoll_fd;
    size_t _con_limit;

    //TODO: remove allowed uuids
    std::vector<std::string> _allowed_uuids;

    allkol::MessageHandler &_msg_handler;
    ulib::Logger &_logger;

public:
    static const size_t READ_BUF_SIZE = 5000;

    Channel(size_t con_limit, allkol::MessageHandler &msg_handler, ulib::Logger &logger);

    virtual ~Channel();

    virtual void tick() = 0;

    virtual void allowed_uuids(const std::vector<std::string> &allowed_uuids)
        { _allowed_uuids = allowed_uuids; }

    virtual void broadcast(const nlohmann::json &message) override;

    virtual void broadcast_update(const nlohmann::json &message) override;

    inline uint id() const
        { return _id; }
};

}

#endif /* ALLKOL_SERVER_SIDE_CHANNEL_HPP */