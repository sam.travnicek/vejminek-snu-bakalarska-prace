#include <allkol/server_side/TCPClient.hpp>

using namespace allkol;

void TCPClient::send(const nlohmann::json &msg)
{
    std::string str = msg.dump();
    _connection->write(str.c_str(), str.length());
}