#ifndef ALLKOL_SERVER_SIDE_TLS_CLIENT_HPP
#define ALLKOL_SERVER_SIDE_TLS_CLIENT_HPP

#include <allkol/server_side/Client.hpp>
#include <ulib-net/TLSConnection.hpp>

namespace allkol {

class TLSClient : public Client
{
    ulib::net::TLSConnection *_connection;

public:
    TLSClient(ulib::net::TLSConnection *con)
        : _connection(con) {}

    virtual ~TLSClient()
    {
        delete _connection;
    }

    virtual std::string ip_address() const override
        { return _connection->ip_address(); }

    virtual void send(const nlohmann::json &msg) override;

    inline ulib::net::TLSConnection* connection() const
        { return _connection; }
};

}

#endif /* ALLKOL_SERVER_SIDE_TLS_CLIENT_HPP */