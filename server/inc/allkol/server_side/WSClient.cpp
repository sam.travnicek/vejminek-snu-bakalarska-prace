#include <allkol/server_side/WSClient.hpp>

using namespace allkol;

WSClient::WSClient(ws_server::connection_ptr connection)
    : _connection(connection)
{
    _ip = _connection->get_socket().remote_endpoint().address().to_string();
}

std::string WSClient::ip_address() const
{
    return _ip;
}

void WSClient::send(const nlohmann::json &msg)
{
    _connection->send(msg.dump(), websocketpp::frame::opcode::text);
}