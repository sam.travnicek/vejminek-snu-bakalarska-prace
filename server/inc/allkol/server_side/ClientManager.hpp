#ifndef ALLKOL_SERVER_SIDE_CLIENT_MANAGER_HPP
#define ALLKOL_SERVER_SIDE_CLIENT_MANAGER_HPP

#include <unordered_map>
#include <mutex>
#include <allkol/server_side/Client.hpp>

namespace allkol {

class ClientManager
{
protected:
    std::unordered_map<long, Client*> _clients;

public:
    virtual void broadcast(const nlohmann::json &message) = 0;

    virtual void broadcast_update(const nlohmann::json &message) = 0;
};

}

#endif /* ALLKOL_SERVER_SIDE_CLIENT_MANAGER_HPP */