#ifndef ALLKOL_SERVER_SIDE_CLIENT_HPP
#define ALLKOL_SERVER_SIDE_CLIENT_HPP

#include <nlohmann/json.hpp>
#include <allkol/Message.hpp>

namespace allkol {

class Client
{
    bool _authorised;
    std::string _uuid;
    bool _send_updates;
    bool _disconnect_flag;

public:
    Client()
        : _authorised(false), _send_updates(true), _disconnect_flag(false) {}

    virtual ~Client() {}

    inline std::string uuid() const
        { return _uuid; }

    virtual std::string ip_address() const = 0;

    virtual void send(const nlohmann::json &msg) = 0;

    inline void send(const allkol::Message &msg)
        { send(msg.serialize()); }

    inline bool authorised() const
        { return _authorised; }

    inline void authorised(const std::string &uuid)
        { _authorised = true; _uuid = uuid; }

    inline bool send_updates() const
        { return _send_updates; }

    inline void send_updates(bool value)
        { _send_updates = value; }

    inline bool to_disconnect() const
        { return _disconnect_flag; }

    inline void disconnect()
        { _disconnect_flag = true; }
};

}

#endif /* ALLKOL_SERVER_SIDE_CLIENT_HPP */