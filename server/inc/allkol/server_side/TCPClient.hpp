#ifndef ALLKOL_SERVER_SIDE_TCP_CLIENT_HPP
#define ALLKOL_SERVER_SIDE_TCP_CLIENT_HPP

#include <allkol/server_side/Client.hpp>
#include <ulib-net/TCPConnection.hpp>

namespace allkol {

class TCPClient : public Client
{
    ulib::net::TCPConnection *_connection;

public:
    TCPClient(ulib::net::TCPConnection *con)
        : _connection(con) {}

    virtual ~TCPClient()
    {
        delete _connection;
    }

    virtual std::string ip_address() const override
        { return _connection->ip_address(); }

    virtual void send(const nlohmann::json &msg) override;

    inline ulib::net::TCPConnection* connection() const
        { return _connection; }
};

}

#endif /* ALLKOL_SERVER_SIDE_TCP_CLIENT_HPP */