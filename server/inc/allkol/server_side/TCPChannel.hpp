#ifndef ALLKOL_SERVER_SIDE_TCP_CHANNEL_HPP
#define ALLKOL_SERVER_SIDE_TCP_CHANNEL_HPP

#include <allkol/server_side/Channel.hpp>
#include <allkol/server_side/TCPClient.hpp>
#include <ulib-net/TCPServer.hpp>

namespace allkol {

class TCPChannel : public Channel, public ulib::net::TCPServer
{
    bool accept(ulib::net::TCPConnection *con);

    void disconnect(TCPClient *client);

public:
    TCPChannel(size_t con_limit, allkol::MessageHandler &msg_handler, ulib::Logger &logger)
        : Channel(con_limit, msg_handler, logger) {}

    virtual ~TCPChannel();

    virtual void tick() override;
};

}

#endif /* ALLKOL_SERVER_SIDE_TCP_CHANNEL_HPP */