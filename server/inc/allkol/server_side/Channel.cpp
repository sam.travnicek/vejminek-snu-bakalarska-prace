#include <allkol/server_side/Channel.hpp>
#include <algorithm>

extern "C" {
#include <unistd.h>
}

using namespace allkol;

Channel::Channel(size_t con_limit, allkol::MessageHandler &msg_handler, ulib::Logger &logger)
    : _con_limit(con_limit), _msg_handler(msg_handler), _logger(logger)
{
    static uint ID = 0;
    _id = ID++;
    _epoll_fd = epoll_create1(0);
}

Channel::~Channel()
{
    ::close(_epoll_fd);
}

void Channel::broadcast(const nlohmann::json &message)
{
    for(auto it : _clients)
        it.second->send(message);
}

void Channel::broadcast_update(const nlohmann::json &message)
{
    for(auto it : _clients)
        if(it.second->send_updates())
            it.second->send(message);
}