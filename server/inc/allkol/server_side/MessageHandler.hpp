#ifndef ALLKOL_SERVER_SIDE_MESSAGE_HANDLER_HPP_
#define ALLKOL_SERVER_SIDE_MESSAGE_HANDLER_HPP_

#include <allkol/server_side/Client.hpp>

namespace allkol {

struct MessageHandler
{
    virtual void on_message(uint channel_id, Client &client, Message &message) = 0;
};

}

#endif // ALLKOL_SERVER_SIDE_MESSAGE_HANDLER_HPP_