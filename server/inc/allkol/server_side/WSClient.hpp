#ifndef ALLKOL_SERVER_SIDE_WS_CLIENT_HPP
#define ALLKOL_SERVER_SIDE_WS_CLIENT_HPP

#include <allkol/server_side/Client.hpp>
#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>

namespace allkol {

class WSClient : public Client
{
    using ws_server = websocketpp::server<websocketpp::config::asio>;

    ws_server::connection_ptr _connection;
    std::string _ip;

public:
    WSClient(ws_server::connection_ptr connection);

    virtual ~WSClient() {}

    virtual std::string ip_address() const override;

    virtual void send(const nlohmann::json &msg) override;

    inline ws_server::connection_ptr connection() const
        { return _connection; }
};

}

#endif /* ALLKOL_SERVER_SIDE_WS_CLIENT_HPP */