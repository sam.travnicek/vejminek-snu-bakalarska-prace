#include <allkol/Answer.hpp>

using namespace allkol;

nlohmann::json Answer::serialize() const
{
    nlohmann::json res = Message::serialize();

    if(_ret_code == OK) {
        res["result"] = "ok";
    } else {
        res["result"] = "error";
        res["reason"] = _error_reason;
    }

    return res;
}