#ifndef ULIB_NET_TLS_CLIENT_HPP
#define ULIB_NET_TLS_CLIENT_HPP

#include <ulib-net/TLSConnection.hpp>
#include <string>

extern "C" {
#include <openssl/ssl.h>
}

#include <string>

namespace ulib {
namespace net {

class TLSClientFailure : public std::exception
{
    std::string msg;
public:
    TLSClientFailure(std::string msg) : msg(msg) {}
    ~TLSClientFailure() throw() {}
   const char* what() const throw() { return msg.c_str(); }
};

struct TLSClient
{
    static TLSConnection* connect(const std::string &host, int port);
};

}
}

#endif /* ULIB_NET_TLS_CLIENT_HPP */