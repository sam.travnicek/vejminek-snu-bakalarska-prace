#ifndef ULIB_MYSQL_HPP
#define ULIB_MYSQL_HPP

#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>

#include <string>

// -lmysqlcppconn switch must be used !

namespace ulib {
namespace net {

class MySQL
{
	std::string _host, _user, _password, _name;
	int _port;

public:
	MySQL()
		: _host("localhost"), _user("root"), _password(""), _name(""),
		  _port(3306) {}

	MySQL(const std::string &host, const std::string &user,
            const std::string &password, const std::string &name, int port)
		: _host(host), _user(user), _password(password), _name(name),
          _port(port) {}

	MySQL(const std::string &host, const std::string &user,
            const std::string &password, const std::string &name)
		: _host(host), _user(user), _password(password), _name(name),
          _port(3306) {}

	sql::Connection* connect(const std::string &charset)
    {
        sql::Driver *driver;
		sql::Connection *con;
		sql::ConnectOptionsMap connection_properties;

		connection_properties["hostName"] = sql::SQLString(_host);
		connection_properties["userName"] = sql::SQLString(_user);
		connection_properties["password"] = sql::SQLString(_password);
		connection_properties["schema"] = sql::SQLString(_name);
		connection_properties["port"] = _port;
		connection_properties["OPT_RECONNECT"] = (true);
		connection_properties["OPT_CHARSET_NAME"] = sql::SQLString(charset);

		driver = get_driver_instance();
		con = driver->connect(connection_properties);
		return con;
	}

	sql::Connection* connect()
    {
		return connect("utf8");
	}

	inline std::string host() const { return _host; }
	inline std::string user() const { return _user; }
	inline std::string password() const { return _password; }
	inline std::string name() const { return _name; }
	inline int port() const { return _port; }

    inline void host(const std::string &value) { _host = value; }
    inline void user(const std::string &value) { _user = value; }
    inline void password(const std::string &value) { _password = value; }
    inline void name(const std::string &value) { _name = value; }
    inline void port(int value) { _port = value; }
};

}
}

#endif /* ULIB_MYSQL_HPP */
