#ifndef ULIB_NET_TLS_WEB_SERVER_HPP
#define ULIB_NET_TLS_WEB_SERVER_HPP

#include <vector>
#include <ulib-net/TLSServer.hpp>

namespace ulib {
namespace net {

class TLSWebServer : public TLSServer
{
	static const int EPOLL_TIMEOUT = 5; // ms

    std::string (*_callback)(const std::string &ip_address,
		const std::string &data);

    void handle_connection(TLSConnection *con);

public:
	static std::string filter_url_request(const std::string &request);
	static std::string& wrap_html(std::string &html);
	static std::string& wrap_html(std::string &html,
		const std::string &return_code);

    TLSWebServer(std::string (*callback)(const std::string &ip_address,
			const std::string &data))
        : _callback(callback) {}

    void tick();

	static const std::string
		RETC_OK,
		RETC_NOCONTENT,
		RETC_BADREQUEST,
		RETC_UNAUTHORIZED,
		RETC_FORBIDDEN,
		RETC_NOTFOUND,
		RETC_INTERNALERROR,
		RETC_NOTIMPLEMENTED;
};

}
}

#endif /* ULIB_NET_TLS_WEB_SERVER_HPP */
