#include <ulib-net/TCPConnection.hpp>

extern "C" {
#include <errno.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <resolv.h>
}

using namespace ulib::net;

TCPConnection::TCPConnection(int file_descriptor)
    : _file_descriptor(file_descriptor)
{
}

TCPConnection::~TCPConnection()
{
    close();
}

void TCPConnection::close()
{
    ::close(_file_descriptor);
}

std::string TCPConnection::ip_address()
{
    sockaddr_in sin;
    socklen_t len = sizeof(sin);

    if(getsockname(_file_descriptor, (sockaddr *)&sin, &len) != -1)
        return std::string(inet_ntoa(sin.sin_addr));

    return nullptr;
}

uint16_t TCPConnection::port()
{
    sockaddr_in sin;
    socklen_t len = sizeof(sin);

    if(getsockname(_file_descriptor, (sockaddr *)&sin, &len) != -1)
        return ntohs(sin.sin_port);

    return 0;
}

int TCPConnection::read(void *buf, size_t len)
{
    return ::read(_file_descriptor, buf, len);
}

void TCPConnection::write(const void *buf, size_t len)
{
    while(::write(_file_descriptor, buf, len) == -1);
}

std::string TCPConnection::read()
{
    char c;
    std::string res = "";
    while(read(&c, 1))
        res += c;
    return res;
}

void TCPConnection::write(const std::string &data)
{
    write(data.c_str(), data.length());
}