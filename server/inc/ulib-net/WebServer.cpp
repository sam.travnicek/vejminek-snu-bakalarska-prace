#include <ulib-net/WebServer.hpp>

using namespace ulib::net;

void WebServer::tick()
{
    std::vector<TCPConnection*> cons = accept_all();

    for(TCPConnection *con : cons)
        handle_connection(con);
}

void WebServer::handle_connection(TCPConnection *con)
{
    std::string data = con->read();

    if(data.length() > 0) {
        std::string response = _callback(con->ip_address(), data);
        con->write(response.c_str(), response.length());
    }

    delete con;
}

std::string WebServer::filter_url_request(const std::string &request)
{
	if(request.substr(0, 3) == "GET") {
		unsigned int x = request.find("\n");
		std::string line = request.substr(4, x-5);
		x = line.find(" ");
		std::string req = line.substr(0, x);
		return req.substr(1);
	}
	return "";
}

std::string& WebServer::wrap_html(
    std::string& html, const std::string &return_code)
{
	int len = html.length();
	html = "HTTP/1.1 " + return_code
        + "\nContent-Type: text/html\nContent-Length: " + std::to_string(len)
        + "\nAccept-Ranges: bytes\nConnection: close\n\n" + html;
	return html;
}

std::string& WebServer::wrap_html(std::string &html)
{
	return wrap_html(html, RETC_OK);
}

const std::string WebServer::RETC_OK = "200 OK";
const std::string WebServer::RETC_NOCONTENT = "204 No Content";
const std::string WebServer::RETC_BADREQUEST = "400 Bad Request";
const std::string WebServer::RETC_UNAUTHORIZED = "401 Unauthorized";
const std::string WebServer::RETC_FORBIDDEN = "403 Forbidden";
const std::string WebServer::RETC_NOTFOUND = "404 Not Found";
const std::string WebServer::RETC_INTERNALERROR = "500 Internal Server Error";
const std::string WebServer::RETC_NOTIMPLEMENTED = "501 Not Implemented";