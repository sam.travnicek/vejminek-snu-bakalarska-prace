#include <ulib-net/TCPClient.hpp>

extern "C" {
#include <errno.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <resolv.h>
#include <netdb.h>
}

using namespace ulib::net;

TCPConnection* TCPClient::connect(const std::string &host_addr, int port)
{
    struct hostent *host;

    if((host = gethostbyname(host_addr.c_str())) == NULL)
        throw TCPClientFailure("Failed to get host.");

    int fd = socket(PF_INET, SOCK_STREAM, 0);

    struct sockaddr_in addr;
    ::memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = *(long*)(host->h_addr_list[0]);

    if (::connect(fd, (struct sockaddr*)&addr, sizeof(addr)) != 0) {
        ::close(fd);
        throw TCPClientFailure("Failed to connect to the server.");
    }

    return new TCPConnection(fd);
}