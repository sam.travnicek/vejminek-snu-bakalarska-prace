#include <ulib-net/TLSClient.hpp>

extern "C" {
#include <errno.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <resolv.h>
#include <netdb.h>
#include "openssl/ssl.h"
#include "openssl/err.h"
}

using namespace ulib::net;

TLSConnection* TLSClient::connect(const std::string &host_addr, int port)
{
    SSL_library_init();
    OpenSSL_add_all_algorithms();
    SSL_load_error_strings();
    const SSL_METHOD *method = TLS_client_method();
    SSL_CTX *ctx = SSL_CTX_new(method);
    if (ctx == NULL)
        throw TLSClientFailure("Failed to create SSL context.");

    struct hostent *host;

    if((host = gethostbyname(host_addr.c_str())) == NULL)
        throw TLSClientFailure("Failed to get host.");

    int fd = socket(PF_INET, SOCK_STREAM, 0);

    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = *(long*)(host->h_addr_list[0]);

    if (::connect(fd, (struct sockaddr*)&addr, sizeof(addr)) != 0) {
        ::close(fd);
        throw TLSClientFailure("Failed to connect to the server.");
    }

    SSL *ssl = SSL_new(ctx);
    SSL_set_fd(ssl, fd);

    if(SSL_connect(ssl) == -1)
        throw TLSClientFailure("Failed to make SSL connection.");

    return new TLSConnection(ssl);
}