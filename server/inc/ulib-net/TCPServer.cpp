#include <ulib-net/TCPServer.hpp>

extern "C" {
#include <errno.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <resolv.h>
}

using namespace ulib::net;

int TCPServer::open_socket(const std::string &ip, int port)
{
    int fd = socket(PF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
    sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = ::inet_addr(ip.c_str());

    if(bind(fd, (struct sockaddr*)&addr, sizeof(addr)) != 0) {
		::close(fd);
        throw TCPServerFailure("Failed to bind the port.");
    }

    if(listen(fd, QUEUE_LIMIT) != 0) {
		::close(fd);
        throw TCPServerFailure("Failed to configure the listening port.");
    }

    return fd;
}

TCPServer::TCPServer()
    : _file_descriptor(-1), _epoll_fd(-1), _ready(false) {}

TCPServer::~TCPServer()
{
    close();
}

void TCPServer::open(const std::string &ip, int port)
{
    if(_ready)
        throw TCPServerFailure("Server is already open!");

    _file_descriptor = open_socket(ip, port);
    if(_file_descriptor < 0)
        throw TCPServerFailure("Failed to open server socket.");

    _epoll_fd = epoll_create1(0);
    if(_epoll_fd < 0) {
        ::close(_file_descriptor);
        throw TCPServerFailure("Failed to create epoll FD.");
    }

    epoll_event ep_event = {0};
    ep_event.data.fd = _file_descriptor;
    ep_event.events = EPOLLIN;

    if(epoll_ctl(_epoll_fd, EPOLL_CTL_ADD, _file_descriptor, &ep_event) != 0) {
        ::close(_epoll_fd);
        ::close(_file_descriptor);
        throw TCPServerFailure(
            "Failed to add the server file descriptor to epoll");
    }

    _ready = true;
}

void TCPServer::close()
{
    if(!_ready)
        return;
    _ready = false;
    ::close(_file_descriptor);
    ::close(_epoll_fd);
}

TCPConnection* TCPServer::accept()
{
    epoll_event ep_ev = {0};
    int ev_cnt = epoll_wait(_epoll_fd, &ep_ev, 1, EPOLL_TIMEOUT);

    if(ev_cnt == 0)
        return nullptr;

    sockaddr_in addr = {0};
    socklen_t len = sizeof(addr);

    int client_fd = ::accept(_file_descriptor, (sockaddr*)&addr, &len);
    if(client_fd == -1)
        return nullptr;

    return new TCPConnection(client_fd);
}

std::vector<TCPConnection*> TCPServer::accept_all()
{
    std::vector<TCPConnection*> cons;
    TCPConnection *con;
    while((con = accept()) != nullptr)
        cons.push_back(con);
    return cons;
}