#ifndef ULIB_NET_TCP_CONNECTION_HPP
#define ULIB_NET_TCP_CONNECTION_HPP

#include <string>

namespace ulib {
namespace net {

class TCPConnection
{
    int _file_descriptor;

public:
    TCPConnection(int file_descriptor);
    virtual ~TCPConnection();

    void close();
    std::string ip_address();
    uint16_t port();

    int read(void *buf, size_t len);
    void write(const void *buf, size_t len);
    std::string read();
    void write(const std::string &data);

    inline int file_descriptor() { return _file_descriptor; }
};

}
}

#endif /* ULIB_NET_TCP_CONNECTION_HPP */