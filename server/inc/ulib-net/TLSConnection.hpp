#ifndef ULIB_NET_TLS_CONNECTION_HPP
#define ULIB_NET_TLS_CONNECTION_HPP

extern "C" {
#include <openssl/ssl.h>
}

#include <string>

namespace ulib {
namespace net {

class TLSConnection
{
    SSL* _ssl;

public:
    TLSConnection(SSL* ssl);
    virtual ~TLSConnection();

    void close();
    std::string ip_address();
    uint16_t port();

    int read(void *buf, size_t len);
    void write(const void *buf, size_t len);
    std::string read();
    void write(const std::string &data);

    int file_descriptor();
};

}
}

#endif /* ULIB_NET_TLS_CONNECTION_HPP */