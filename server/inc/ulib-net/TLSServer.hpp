#ifndef ULIB_NET_TLSSERVER_HPP
#define ULIB_NET_TLSSERVER_HPP

#include <string>
#include <vector>
#include <ulib-net/TLSConnection.hpp>

extern "C" {
#include <openssl/ssl.h>
#include <sys/epoll.h>
}

namespace ulib {
namespace net {

class TLSServerFailure : public std::exception
{
    std::string msg;
public:
    TLSServerFailure(std::string msg) : msg(msg) {}
    ~TLSServerFailure() throw() {}
   const char* what() const throw() { return msg.c_str(); }
};

class TLSServer
{
    static void load_certificates(SSL_CTX *ctx, const std::string& cert_path,
        const std::string& key_path);
    static int open_socket(const std::string &ip, int port);

	static const int QUEUE_LIMIT = 32;
	static const int EPOLL_TIMEOUT = 5; // ms

    SSL_CTX *_ctx;
    int _file_descriptor;
    int _epoll_fd;
    bool _ready;

    std::string _cert_path;
    std::string _key_path;

public:
    TLSServer();
    virtual ~TLSServer();

    inline int file_descriptor() { return _file_descriptor; }

    void open(const std::string &ip, int port);
    void close();

    inline void cert_path(const std::string& cert_path)
        { _cert_path = cert_path; }

    inline void key_path(const std::string& key_path)
        { _key_path = key_path; }

    inline void key_cert_path(const std::string& path)
        { _key_path = path; _cert_path = path; }

    TLSConnection* accept();
    std::vector<TLSConnection*> accept_all();
};

}
}

#endif /* ULIB_NET_TLSSERVER_HPP */
