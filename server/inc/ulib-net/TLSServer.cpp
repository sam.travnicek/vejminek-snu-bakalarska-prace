#include <ulib-net/TLSServer.hpp>

extern "C" {
#include <errno.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <resolv.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
}

using namespace ulib::net;

void TLSServer::load_certificates(
    SSL_CTX *ctx, const std::string& cert_path, const std::string& key_path)
{
    if(SSL_CTX_use_certificate_file(
            ctx, cert_path.c_str(), SSL_FILETYPE_PEM) <= 0) {
        //ERR_print_errors_fp(stderr);
        throw TLSServerFailure("Failed to load certificate.");
    }

    if(SSL_CTX_use_PrivateKey_file(
        ctx, key_path.c_str(), SSL_FILETYPE_PEM) <= 0) {
        //ERR_print_errors_fp(stderr);
        throw TLSServerFailure("Failed to load private key.");
    }

    if(!SSL_CTX_check_private_key(ctx))
        throw TLSServerFailure(
            "Private key does not match the public certificate.");
}

int TLSServer::open_socket(const std::string &ip, int port)
{
    int fd = socket(PF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
    sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = ::inet_addr(ip.c_str());

    if(bind(fd, (struct sockaddr*)&addr, sizeof(addr)) != 0) {
		::close(fd);
        throw TLSServerFailure("Failed to bind the port.");
    }

    if(listen(fd, QUEUE_LIMIT) != 0) {
		::close(fd);
        throw TLSServerFailure("Failed to configure the listening port.");
    }

    return fd;
}

TLSServer::TLSServer()
    : _ctx(nullptr), _file_descriptor(-1), _epoll_fd(-1), _ready(false) {}

TLSServer::~TLSServer()
{
    close();
}

void TLSServer::open(const std::string &ip, int port)
{
    if(_ready)
        throw TLSServerFailure("Server is already open!");

    SSL_library_init();
    OpenSSL_add_all_algorithms();
    SSL_load_error_strings();
    ERR_load_crypto_strings();
    const SSL_METHOD *method = TLS_server_method();
    _ctx = SSL_CTX_new(method);
    if(_ctx == NULL) {
        //ERR_print_errors_fp(stderr);
        throw TLSServerFailure("Failed to create SSL context.");
    }
    load_certificates(_ctx, _cert_path, _key_path);
    _file_descriptor = open_socket(ip, port);
    if(_file_descriptor < 0) {
        //SSL_CTX_free(_ctx);
        throw TLSServerFailure("Failed to open server socket.");
    }

    _epoll_fd = epoll_create1(0);
    if(_epoll_fd < 0) {
        //SSL_CTX_free(_ctx);
        ::close(_file_descriptor);
        throw TLSServerFailure("Failed to create epoll FD.");
    }

    epoll_event ep_event;
    ep_event.data.fd = _file_descriptor;
    ep_event.events = EPOLLIN;

    if(epoll_ctl(_epoll_fd, EPOLL_CTL_ADD, _file_descriptor, &ep_event) != 0) {
        ::close(_epoll_fd);
        ::close(_file_descriptor);
        throw TLSServerFailure(
            "Failed to add the server file descriptor to epoll");
    }

    _ready = true;
}

void TLSServer::close()
{
    if(!_ready)
        return;
    _ready = false;
    ::close(_file_descriptor);
    SSL_CTX_free(_ctx);
    ::close(_epoll_fd);
}

TLSConnection* TLSServer::accept()
{
    epoll_event ep_ev;
    int ev_cnt = epoll_wait(_epoll_fd, &ep_ev, 1, EPOLL_TIMEOUT);

    if(ev_cnt == 0)
        return nullptr;

    sockaddr_in addr;
    socklen_t len = sizeof(addr);

    int client_fd = ::accept(_file_descriptor, (sockaddr*)&addr, &len);
    if(client_fd == -1)
        return nullptr;

    SSL *ssl = SSL_new(_ctx);
    SSL_set_fd(ssl, client_fd);

    if(SSL_accept(ssl) == -1) {
        ERR_print_errors_fp(stderr);
        return nullptr;
    }

    return new TLSConnection(ssl);
}

std::vector<TLSConnection*> TLSServer::accept_all()
{
    std::vector<TLSConnection*> cons;
    TLSConnection *con;
    while((con = accept()) != nullptr)
        cons.push_back(con);
    return cons;
}