#ifndef ULIB_NET_TCP_SERVER_HPP
#define ULIB_NET_TCP_SERVER_HPP

#include <string>
#include <vector>
#include <ulib-net/TCPConnection.hpp>

extern "C" {
#include <sys/epoll.h>
}

namespace ulib {
namespace net {

class TCPServerFailure : public std::exception
{
    std::string msg;
public:
    TCPServerFailure(std::string msg) : msg(msg) {}
    ~TCPServerFailure() throw() {}
   const char* what() const throw() { return msg.c_str(); }
};

class TCPServer
{
    static int open_socket(const std::string& ip, int port);

	static const int QUEUE_LIMIT = 32;
	static const int EPOLL_TIMEOUT = 5; // ms

    int _file_descriptor;
    int _epoll_fd;
    bool _ready;

public:
    TCPServer();
    virtual ~TCPServer();

    inline int file_descriptor() { return _file_descriptor; }

    void open(const std::string& ip, int port);
    void close();

    TCPConnection* accept();
    std::vector<TCPConnection*> accept_all();
};

}
}

#endif /* ULIB_NET_TCP_SERVER_HPP */
