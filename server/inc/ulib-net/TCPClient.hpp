#ifndef ULIB_NET_TCP_CLIENT_HPP
#define ULIB_NET_TCP_CLIENT_HPP

#include <ulib-net/TCPConnection.hpp>
#include <string>

#include <string>

namespace ulib {
namespace net {

class TCPClientFailure : public std::exception
{
    std::string msg;
public:
    TCPClientFailure(std::string msg) : msg(msg) {}
    ~TCPClientFailure() throw() {}
   const char* what() const throw() { return msg.c_str(); }
};

struct TCPClient
{
    static TCPConnection* connect(const std::string &host, int port);
};

}
}

#endif /* ULIB_NET_TCP_CLIENT_HPP */