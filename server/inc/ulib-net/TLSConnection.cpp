#include <ulib-net/TLSConnection.hpp>

extern "C" {
#include <errno.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <resolv.h>
#include <openssl/err.h>
}

using namespace ulib::net;

TLSConnection::TLSConnection(SSL* ssl) : _ssl(ssl) {}

TLSConnection::~TLSConnection()
{
    close();
}

void TLSConnection::close()
{
    if(_ssl == nullptr || _ssl == NULL)
        return;

    int fd = SSL_get_fd(_ssl);
    SSL_free(_ssl);
    ::close(fd);
}

std::string TLSConnection::ip_address()
{
    if(_ssl == nullptr || _ssl == NULL)
        return nullptr;

    int client_fd = SSL_get_fd(_ssl);

    sockaddr_in sin;
    socklen_t len = sizeof(sin);

    if(getsockname(client_fd, (sockaddr *)&sin, &len) != -1)
        return std::string(inet_ntoa(sin.sin_addr));

    return nullptr;
}

uint16_t TLSConnection::port()
{
    if(_ssl == nullptr || _ssl == NULL)
        return 0;

    int client_fd = SSL_get_fd(_ssl);

    sockaddr_in sin;
    socklen_t len = sizeof(sin);

    if(getsockname(client_fd, (sockaddr *)&sin, &len) != -1)
        return ntohs(sin.sin_port);

    return 0;
}

int TLSConnection::read(void *buf, size_t len)
{
    if(_ssl == nullptr || _ssl == NULL)
        return 0;
    return SSL_read(_ssl, buf, len);
}

void TLSConnection::write(const void *buf, size_t len)
{
    if(_ssl == nullptr || _ssl == NULL)
        return;
    SSL_write(_ssl, buf, len);
}

int TLSConnection::file_descriptor()
{
    if(_ssl == nullptr || _ssl == NULL)
        return 0;
    return SSL_get_fd(_ssl);
}

std::string TLSConnection::read()
{
    char c;
    std::string res = "";
    while(read(&c, 1))
        res += c;
    return res;
}

void TLSConnection::write(const std::string &data)
{
    write(data.c_str(), data.length());
}