class ContentGenerator
{
    constructor(client, $siteDevicesList, $siteDevice)
    {
        this.$siteDevicesList = $siteDevicesList;
        this.$siteDevice = $siteDevice;
        this.client = client;
        this.siteSwitchLocked = false;
        this.updateDeviceIntervalId = null;

        let self = this;

        this.$siteDevicesList.html("");
        this.$siteDevice.html("");

        this.client.on("load", function() {
            self.$siteDevicesList.html("");
            self.$siteDevice.html("");
            self.generateSiteDevicesList();
            self.switchSiteDevicesList();
        });

        this.client.on("connection_lost", function() {
            alert("Spojení se serverem ztraceno.");

            self.$siteDevicesList.html("");
            self.$siteDevice.html("");
        });
    }

    $generateRoomTitle(label)
    {
        return $(`<h2 class="room-title">${label}</h2>`);
    }

    $generateDeviceLink(label)
    {
        return $(`
            <div class="device">
                <div class="icon"><i class="fas fa-lightbulb"></i></div>
                <div class="label">${label}</div>
                <div class="arrow"><i class="fas fa-arrow-right"></i></div>
            </div>
        `);
    }

    $generateDeviceAction(actionTemplate)
    {
        let args = ``;
        let targs = actionTemplate.arguments;

        if(Object.keys(targs).length > 0) {
            args = `<table class="arguments">`;
            for(let i in targs) {
                let targ = targs[i];
                args += `
                    <tr>
                        <td>${targ.label}</td>
                        <td>`;
                if(targ.type == "number")
                    args += `<input type="number" value="${targ.default_value}" data-arg-identifier="${targ.identifier}" />`;
                else if(targ.type == "boolean")
                    args += `<input type="checkbox" data-arg-identifier="${targ.identifier}" ${targ.default_value ? "checked" : ""}/>`;
                else if(targ.type == "string")
                    args += `<input type="text" value="${targ.default_value}" data-arg-identifier="${targ.identifier}" />`;

                args += `</tr>`;
            }
            args += `</table>`;
        }

        return $(`
            <div class="action${ args.length > 0 ? " with-arguments" : ""}">
                <div class="icon"><i class="fas fa-share"></i></div>
                <div class="label">${actionTemplate.label}</div>
                <div class="send"><i class="fas fa-paper-plane"></i></div>
                ${args}
            </div>
        `);
        // <div class="args">
        //     <table>

        //     </table>
        // </div>
    }

    $generateDevicePort(portTemplate)
    {
        let controls = ``;

        if(portTemplate.direction == "output") {
            if(portTemplate.mode == "logical")
                controls = `<label class="slider-switch">
                        <input type="checkbox">
                        <span class="slide-button"></span>
                    </label>`;
            else if(portTemplate.mode == "discrete")
                controls = `<input type="range" class="slider-range">`;
        } else if(portTemplate.direction == "input") {
            if(portTemplate.mode == "logical")
                controls = `<div class="indicator"></div>`;
            else if(portTemplate.mode == "discrete")
                controls = `<div class="progress-bar">
                    <div class="status" style="width="0%"></div>
                </div>`;
        }

        return $(`
            <div class="port" data-id="${portTemplate.identifier}">
                <div class="icon"><i class="fas fa-project-diagram"></i></div>
                <div class="label">${portTemplate.label}</div>
                <div class="controls">
                    ${controls}
                </div>
            </div>
        `);
    }

    clearSiteInterval()
    {
        if(this.updateDeviceIntervalId !== null) {
            clearInterval(this.updateDeviceIntervalId);
            this.updateDeviceIntervalId = null;
        }
    }

    siteInterval(...args)
    {
        this.updateDeviceIntervalId = setInterval(args);
    }

    generateSiteDevicesList()
    {
        this.clearSiteInterval();

        this.$siteDevicesList.html("");
        let self = this;

        this.client.eachRoom(function(floor, label, numDevices) {
            if(numDevices == 0)
                return;

            let roomLabel = label;
            if(label === null)
                roomLabel = "Nezařazeno";

            let $roomTitle = self.$generateRoomTitle(roomLabel);
            self.$siteDevicesList.append($roomTitle);

            self.client.eachDeviceByRoom(label, function(deviceId, device) {
                let $deviceLink = self.$generateDeviceLink(device.label);
                $deviceLink.click(function() {
                    self.generateSiteDevice(deviceId);
                    self.switchSiteDevice();
                });
                self.$siteDevicesList.append($deviceLink);
            });
        });
    }

    generateSiteDevice(deviceId)
    {
        this.clearSiteInterval();

        this.$siteDevice.html("");
        let self = this;

        let $topBar = $(`
            <div class="top-bar">
                <div class="icon"><i class="fas fa-arrow-left"></i></div>
                <h2 class="title">${this.client.deviceLabel(deviceId)}</h2>
            </div>`);
        $topBar.find(".icon").click(function() {
            self.switchSiteDevicesList();
        });
        this.$siteDevice.append($topBar);

        let $deviceIcon = $(`
            <div class="device-icon">
                <i class="fas fa-cogs"></i>
            </div>`);
        this.$siteDevice.append($deviceIcon);

        let deviceData = this.client.deviceData(deviceId);
        let template = this.client.deviceTemplate(deviceId);
        let templateActions = template.actions;
        let templatePorts = template.ports;
        let devicePorts = deviceData.ports;
        let portsData = {};

        if(templateActions.length > 0)
            this.$siteDevice.append(`<div class="splitter"></div>`);

        for(let i in templateActions) {
            let actionTemplate = templateActions[i];
            let $action = self.$generateDeviceAction(actionTemplate);
            $action.find("> .send").click(function() {
                let $actionSendBtn = $(this);
                $actionSendBtn.addClass("active");
                setTimeout(function() {
                    $actionSendBtn.removeClass("active");
                }, 200);
                let args = {};
                let targs = actionTemplate.arguments;
                for(let argi in targs) {
                    let targ = targs[argi];
                    if(targ.type == "number")
                        args[targ.identifier] = Number($action.find(`input[data-arg-identifier="${targ.identifier}"]`).val().replace(/,/g, '.'));
                    else if(targ.type == "boolean")
                        args[targ.identifier] = $action.find(`input[data-arg-identifier="${targ.identifier}"]`).is(":checked");
                    else if(targ.type == "string")
                        args[targ.identifier] = $action.find(`input[data-arg-identifier="${targ.identifier}"]`).val();
                }
                self.client.callDeviceAction(deviceId, actionTemplate.identifier, args);
            });
            self.$siteDevice.append($action);
        }

        if(templatePorts.length > 0)
            this.$siteDevice.append(`<div class="splitter"></div>`);

        for(let i in templatePorts) {
            let portTemplate = templatePorts[i];
            let port = devicePorts[portTemplate.identifier];
            let $port = self.$generateDevicePort(portTemplate);

            let portData = {
                mode: portTemplate.mode,
                direction: portTemplate.direction,
                min: port.min,
                max: port.max,
                disabled: false
            };
            let portId = portTemplate.identifier;
            portsData[portId] = portData;

            if(portData.direction == "output") {
                if(portData.mode == "logical") {
                    let $input = $port.find("input");
                    $input.change(function() {
                        portData.disabled = true;
                        self.client.writeDevicePort(deviceId, portId, $input.is(':checked') ? 1 : 0);

                        setTimeout(function() {
                            portData.disabled = false;
                        }, 100);
                    });
                } else if(portData.mode == "discrete") {
                    let $input = $port.find("input");
                    $input.attr("min", portData.min);
                    $input.attr("max", portData.max);

                    let interval = null;
                    let lastValue = null;

                    $input.on("mousedown touchstart", function() {
                        portData.disabled = true;

                        interval = setInterval(function() {
                            let val = $input.val();
                            if(val != lastValue) {
                                lastValue = val;
                                self.client.writeDevicePort(deviceId, portId, val);
                            }
                        }, 50);
                    });

                    $input.on("mouseup touchend", function() {
                        clearInterval(interval);
                        interval = null;

                        setTimeout(function() {
                            portData.disabled = false;
                        }, 100);
                    });

                    $input.change(function(e) {
                        e.preventDefault();
                    });
                }
            }

            self.$siteDevice.append($port);
        }

        this.updateDeviceIntervalId = setInterval(function() {
            self.client.eachDevicePortValue(deviceId, function(portId, value) {
                let portData = portsData[portId];
                let $controls = self.$siteDevice.find(`.port[data-id="${portId}"] > .controls`);

                if(portData.direction == "output") {
                    if(portData.mode == "logical") {
                        if(!portData.disabled) {
                            let $input = $controls.find("input");
                            $input.prop("checked", value == 1);
                        }
                    } else if(portData.mode == "discrete") {
                        if(!portData.disabled) {
                            let $input = $controls.find("input");
                            $input.val(value);
                        }
                    }
                } else if(portData.direction == "input") {
                    if(portData.mode == "logical") {
                        let $indicator = $controls.find(".indicator");
                        if(value == 1)
                            $indicator.addClass("active");
                        else
                            $indicator.removeClass("active");
                    } else if(portData.mode == "discrete") {
                        let $progress = $controls.find(".progress-bar > .status");
                        let range = portData.max - portData.min;
                        let percentage = Math.floor((value - portData.min)/range * 100);
                        $progress.css("width", percentage + "%");
                    }
                }
            });
        }, 50);
    }

    switchSiteDevicesList()
    {
        if(this.siteSwitchLocked)
            return;
        this.siteSwitchLocked = true;
        let self = this;

        this.$siteDevice.addClass("slide-left");
        setTimeout(function() {
            self.$siteDevice.addClass("hidden");
            self.$siteDevice.removeClass("slide-left");
            self.$siteDevicesList.removeClass("hidden");
            self.siteSwitchLocked = false;
        }, 125);
    }

    switchSiteDevice()
    {
        if(this.siteSwitchLocked)
            return;
        this.siteSwitchLocked = true;
        let self = this;

        this.$siteDevicesList.addClass("slide-right");
        setTimeout(function() {
            self.$siteDevicesList.addClass("hidden");
            self.$siteDevicesList.removeClass("slide-right");
            self.$siteDevice.removeClass("hidden");
            self.siteSwitchLocked = false;
        }, 125);
    }
}

$(function() {
    $.getJSON("config.json", function(config) {
        let client = new VejminekClient(
            config["host"], config["port"], config["uuid"], config["subscribe-to-updates"]);

        new ContentGenerator(client, $(".site.devices-list"), $(".site.device"));
        client.connect();
    });
});