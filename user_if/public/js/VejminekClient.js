let VejminekClient;

(function() {

    const KNOWN_EVENTS = ["load", "connection_lost"];

    VejminekClient = class
    {
        constructor(ip_address, port, uuid, subscribeToUpdates)
        {
            this.client = new allkol.Client(ip_address, port, uuid, subscribeToUpdates);
            this.client.autoReconnect(true, 5000);

            this._callbacks = {};
            for(let i in KNOWN_EVENTS)
                this._callbacks[KNOWN_EVENTS[i]] = [];

            let self = this;

            this.client.on("connect", function()
            {
                self._resetStorage();
                self._requestRooms();
            });

            this.client.on("disconnect", function() {
                let connLostCallbacks = self._callbacks["connection_lost"];
                for(let i in connLostCallbacks)
                    connLostCallbacks[i]();
            });
        }

        connect()
        {
            this.client.connect();
        }

        on(eventName, callback)
        {
            if(!KNOWN_EVENTS.includes(eventName))
                return;
            this._callbacks[eventName].push(callback);
        }

        _requestRooms()
        {
            let self = this;

            this.client.request("list_rooms", {}, function(msg) {
                console.log(msg);
                if(!msg.isOk()) {
                    console.error("(VejminekClient) Failed to load rooms.", msg.errorReason());
                    setTimeout(function() {
                        self._requestRooms();
                    }, 1000);
                    return;
                }

                self._rooms = msg.data().rooms;
                for(let i in self._rooms)
                    self._devicesByRoom[self._rooms[i].label] = [];
                self._devicesByRoom["__OTHERS__"] = [];
                self._requestTemplates();
            });
        }

        _requestTemplates()
        {
            let self = this;

            this.client.request("list_device_templates", {}, function(msg) {
                console.log(msg);
                if(!msg.isOk()) {
                    console.error("(VejminekClient) Failed to load device templates.", msg.errorReason());
                    setTimeout(function() {
                        self._requestTemplates();
                    }, 1000);
                    return;
                }

                let templates = msg.data().device_templates;
                for(let i in templates) {
                    let temp = templates[i];
                    self._deviceTemplates[temp.identifier] = {
                        label: temp.label,
                        ports: temp.ports,
                        actions: temp.actions
                    };
                }

                self._requestDevices();
            });
        }

        _requestDevices()
        {
            let self = this;

            this.client.request("list_devices", {}, function(msg) {
                console.log(msg);
                if(!msg.isOk()) {
                    console.error("(VejminekClient) Failed to load devices.", msg.errorReason());
                    setTimeout(function() {
                        self._requestDevices();
                    }, 1000);
                    return;
                }

                let devices = msg.data().devices;
                for(let i in devices) {
                    let dev = devices[i];
                    self._devices[dev.id] = {
                        label: dev.label,
                        description: dev.description,
                        room: dev.room,
                        template: dev.template
                    };
                    if(self._devicesByRoom[dev.room] !== undefined)
                        self._devicesByRoom[dev.room].push(dev.id);
                    else
                        self._devicesByRoom["__OTHERS__"].push(dev.id);
                }

                self._requestPorts();
            });
        }

        _requestPorts()
        {
            let self = this;

            this.client.request("list_devices_ports", {}, function(msg) {
                console.log(msg);
                if(!msg.isOk()) {
                    console.error("(VejminekClient) Failed to load devices' ports.", msg.errorReason());
                    setTimeout(function() {
                        self._requestPorts();
                    }, 1000);
                    return;
                }

                let ports = msg.data();
                for(let i in ports) {
                    self._devices[i].ports = ports[i];
                }

                console.log("(VejminekClient) Loading done.");
                let loadCallbacks = self._callbacks["load"];
                for(let i in loadCallbacks)
                    loadCallbacks[i]();
            });
        }

        eachRoom(callback)
        {
            for(let i in this._rooms) {
                let room = this._rooms[i];
                callback(room.floor, room.label, this._devicesByRoom[room.label].length);
            }
            callback(null, null, this._devicesByRoom["__OTHERS__"].length);
        }

        eachDeviceByRoom(roomLabel, callback)
        {
            if(roomLabel === null)
                roomLabel = "__OTHERS__";

            let deviceIds = this._devicesByRoom[roomLabel];

            for(let i in deviceIds) {
                let deviceId = deviceIds[i];
                let device = this._devices[deviceId];
                callback(deviceId, device);
            }
        }

        eachDevice(callback)
        {
            for(let deviceId in this._devices) {
                let deviceData = this._devices[deviceId];
                callback(deviceId, deviceData, this._deviceTemplates[deviceData.template]);
            }
        }

        eachDevicePortValue(deviceId, callback)
        {
            let self = this;

            this.client.request("list_device_port_values", {
                device: deviceId
            }, function(msg) {
                if(!msg.isOk()) {
                    console.error("(VejminekClient) Failed to load device's port values.", msg.errorReason());
                    return;
                }

                let dev = self._devices[deviceId];
                let devPorts = dev.ports;
                let rports = msg.data();

                for(let portId in rports) {
                    let val = rports[portId];
                    devPorts[portId].value = val;
                    callback(portId, val);
                }
            });
        }

        eachPortValue(callback)
        {
            let self = this;

            this.client.request("list_devices_port_values", function(msg) {
                if(!msg.isOk()) {
                    console.error("(VejminekClient) Failed to load devices' port values.", msg.errorReason());
                    return;
                }

                for(let deviceId in msg) {
                    let dev = self._devices[deviceId];
                    let devPorts = dev.ports;
                    let rports = msg.data()[deviceId];

                    for(let portId in rports) {
                        let val = rports[portId];
                        devPorts[portId].value = val;
                        callback(deviceId, portId, val);
                    }
                }
            });
        }

        deviceLabel(deviceId)
        {
            if(this._devices[deviceId] === undefined)
                return "";
            return this._devices[deviceId].label;
        }

        deviceData(deviceId)
        {
            if(this._devices[deviceId] === undefined)
                return null;
            return this._devices[deviceId];
        }

        deviceTemplate(deviceId)
        {
            if(this._devices[deviceId] === undefined)
                return null;
            return this._deviceTemplates[this._devices[deviceId].template];
        }

        writeDevicePort(deviceId, portId, value)
        {
            this.client.request("write_device_port", {
                device: deviceId,
                port: portId,
                value: Number(value)
            }, function(msg) {
                if(!msg.isOk()) {
                    console.error("(VejminekClient) Failed to write to device's port.", msg.errorReason());
                    return;
                }
            });
        }

        callDeviceAction(deviceId, action, args)
        {
            this.client.request("call_device_action", {
                device: deviceId,
                action: action,
                arguments: args
            }, function(msg) {
                if(!msg.isOk()) {
                    console.error("(VejminekClient) Failed to call device's action.", msg.errorReason());
                    return;
                }
            });
        }

        _resetStorage()
        {
            this._rooms = {};
            this._devicesByRoom = {};
            this._deviceTemplates = {};
            this._devices = {};
        }
    }

})();