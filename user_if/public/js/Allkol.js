let allkol = {};
allkol.CALLBACK_TIMEOUT = 10000; // ms

(function() {
    const OK = 0;
    const ERROR = 1;

    allkol.toMessage = function(jsonObj)
    {
        if(!jsonObj["is_answer"]) {
            let msg = new allkol.Message(
                jsonObj["identifier"], jsonObj["command"], jsonObj["data"]);
            return msg;
        } else {
            let msg = new allkol.Answer(
                jsonObj["identifier"], jsonObj["command"], jsonObj["data"]);
            msg._returnCode = jsonObj["result"] == "ok"
                ? OK : ERROR;

            if(jsonObj["reason"] !== undefined)
                msg._errorReason = jsonObj["reason"];
            return msg;
        }
    }

    allkol.Message = class
    {
        constructor(identifier, command, isAnswer, data)
        {
            this._identifier = identifier;
            this._command = command;
            this._isAnswer = isAnswer;
            this._data = data === undefined || data === null ? {} : data;
        }

        serialize()
        {
            return JSON.stringify({
                "identifier": this._identifier,
                "command": this._command,
                "is_answer": this._isAnswer,
                "data": this._data
            });
        }

        identifier()
        {
            return this._identifier;
        }

        command()
        {
            return this._command;
        }

        isAnswer()
        {
            return this._isAnswer;
        }

        data()
        {
            return this._data;
        }
    };

    allkol.Answer = class extends allkol.Message
    {
        constructor(identifier, command, data)
        {
            super(identifier, command, true, data);
            this.ok();
        }

        ok()
        {
            this._returnCode = OK;
            this._errorReason = "";
        }

        error(reason)
        {
            this._returnCode = ERROR;
            this._errorReason = reason;
        }

        serialize()
        {
            let res = super.serialize();

            if(this._returnCode == OK) {
                res["result"] = "ok";
            } else {
                res["result"] = "error";
                res["reason"] = this._errorReason;
            }
        }

        isOk()
        {
            return this._returnCode == OK;
        }

        returnCode()
        {
            return this._returnCode;
        }

        errorReason()
        {
            return this._errorReason;
        }
    };

    allkol.Client = class
    {
        constructor(ip_address, port, uuid, subscribeToUpdates)
        {
            this._ip_address = ip_address;
            this._port = port;
            this._uuid = uuid;
            this._subscribeToUpdates = subscribeToUpdates;
            this._socket = null;
            this._autoReconnect = false;
            this._autoReconnectInterval = 5000;

            this._connect_callbacks = [];
            this._disconnect_callbacks = [];
            this._message_callbacks = [];

            this._callbackBuffer = [];

            this._msgIdCounter = 0;

            let self = this;

            setInterval(function() {
                let timestamp = new Date().getTime();
                for(let i = self._callbackBuffer.length-1; i >= 0; --i) {
                    if(timestamp - self._callbackBuffer[i].timestamp >= allkol.CALLBACK_TIMEOUT)
                        self._callbackBuffer.splice(i, 1);
                    else
                        break;
                }
            }, allkol.CALLBACK_TIMEOUT/10);
        }

        connected()
        {
            return this._socket != null;
        }

        autoReconnect(value, interval_ms)
        {
            this._autoReconnect = value;
            if(interval_ms !== undefined)
                this._autoReconnectInterval = interval_ms;
        }

        on(event, callback)
        {
            if(event == "connect")
                this._connect_callbacks.push(callback);
            else if(event == "disconnect")
                this._disconnect_callbacks.push(callback);
            else if(event == "message")
                this._message_callbacks.push(callback);
            else
                console.error("Unknown event '" + event + "'.");
        }

        _tryReconnect()
        {
            console.log(`Reconnection attempt in ${Math.floor(this._autoReconnectInterval/1000)} seconds.`);
            let self = this;

            setTimeout(function() {
                self.connect();
            }, this._autoReconnectInterval);
        }

        _nextMessageId()
        {
            let id = this._msgIdCounter;
            if(++this._msgIdCounter >= Math.pow(2, 64))
                this._msgIdCounter = 0;
            return id;
        }

        connect()
        {
            let url = `ws://${this._ip_address}:${this._port}`;
            url += `?subscribe_updates=${this._subscribeToUpdates ? "true" : "false" }`;
            url += `&uuid=${this._uuid}`;

            this._socket = new WebSocket(url);

            let self = this;

            this._socket.onopen = function(e) {
                console.log("Connected to server.");

                for(let i = 0; i < self._connect_callbacks.length; ++i)
                    self._connect_callbacks[i]();
            }

            this._socket.onmessage = function(event) {
                let jres = JSON.parse(event.data);
                // console.log("Message received:", jres.command);

                let msg = allkol.toMessage(jres);

                if(msg.isAnswer()) {
                    for(let i = 0; i < self._callbackBuffer.length; ++i) {
                        let cbData = self._callbackBuffer[i];
                        if(cbData.identifier == msg.identifier()) {
                            if(cbData.callback !== null)
                                cbData.callback(msg);
                            if(i == 0)
                                self._callbackBuffer.shift();
                            else
                                self._callbackBuffer.splice(i, 1);
                            break;
                        }
                    }
                }

                if(self._callbackBuffer.length > 0) {
                    let bufFront = self._callbackBuffer[0];
                    if(bufFront.command == msg.command()) {
                        if(bufFront.callback !== null)
                            bufFront.callback(msg);
                        self._callbackBuffer.shift();
                    }
                }

                for(let i = 0; i < self._message_callbacks.length; ++i)
                    self._message_callbacks[i](msg);
            };

            this._socket.onclose = function(event) {
                self._socket = null;

                if(event.wasClean)
                    console.log("Connection closed.", {
                        code: event.code,
                        reason: event.reason
                    });
                else
                    console.log("Connection lost.");

                for(let i = 0; i < self._disconnect_callbacks.length; ++i)
                    self._disconnect_callbacks[i]();

                self._callbackBuffer = [];

                if(self._autoReconnect)
                    self._tryReconnect();
            };

            this._socket.onerror = function(error) {
                console.error(error.message);
            };
        }

        send(msg)
        {
            if(this._socket === null)
                return;
            if(this._socket.readyState != WebSocket.OPEN)
                return;
            this._socket.send(msg.serialize());
        }

        request(command, data, callback)
        {
            if(this._socket === null)
                return;
            if(this._socket.readyState != WebSocket.OPEN)
                return;
            let id = this._nextMessageId();
            let msg = new allkol.Message(id, command, false, data);
            this._callbackBuffer.push({
                identifier: id,
                callback: callback !== undefined ? callback : null,
                timestamp: new Date().getTime()
            });
            this.send(msg);
        }
    };

})();