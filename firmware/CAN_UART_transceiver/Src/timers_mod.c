#include "timers_mod.h"

Timer_Handle htimer2 = { // 4 kHz @ 72 MHz SystemCoreClock
    .timer = TIM2,
    .prescaler = 10u,
    .reload_value = 1800u
};

void Timer_Setup()
{
    TIM_ClockEnable(TIM2);
}

void Timer_Init(Timer_Handle *htimer)
{
    TIM_TypeDef *timer = htimer->timer;

    // Clock division ratio 1:1, upcounter
    timer->CR1 = 0;
    timer->CNT = 0;

    timer->PSC = htimer->prescaler;
    timer->ARR = htimer->reload_value;

    timer->CR1 |= TIM_CR1_CEN;
}

bool Timer_Updated(Timer_Handle *htimer)
{
    bool updated = htimer->timer->SR & TIM_SR_UIF;
    if(updated) // Must be cleared only if UIF=1
        htimer->timer->SR &= ~TIM_SR_UIF;
    return updated;
}

void TIM_ClockEnable(TIM_TypeDef *timer)
{
    if(timer == TIM1)
        RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;
    else if(timer == TIM2)
        RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
    else if(timer == TIM3)
        RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
    else if(timer == TIM4)
        RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;
}

float TIM_ClockFrequency(TIM_TypeDef *timer)
{
    uint8_t APB_mul = 1;
    uint8_t APB_pos = RCC_CFGR_PPRE2_Pos;

    if(timer != TIM1) {
        APB_mul = 2;
        APB_pos = RCC_CFGR_PPRE1_Pos;
    }

    uint8_t AHB_prescale =
        AHBPrescTable[(RCC->CFGR >> RCC_CFGR_HPRE_Pos) & 0b1111];
    uint8_t APB_prescale =
        APBPrescTable[(RCC->CFGR >> APB_pos) & 0b111];

	// uint32_t APB_timer_clk =
    //     ((SystemCoreClock >> AHB_prescale) >> APB_prescale) * APB_mul;
    float AHB_clk = SystemCoreClock / powf(2, AHB_prescale);
    float APB_clk = AHB_clk / powf(2, APB_prescale);
    float APB_timer_clk = APB_clk * APB_mul;

    return APB_timer_clk;
}