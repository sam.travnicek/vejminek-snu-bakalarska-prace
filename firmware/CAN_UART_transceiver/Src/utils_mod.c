#include <string.h>
#include "utils_mod.h"

uint32_t cpow10(uint8_t exponent)
{
    uint32_t res = 1;
    for(uint8_t i = 0; i < exponent; ++i)
        res *= 10;
    return res;
}

bool StartsWith(const char *str, const char *what)
{
  uint16_t len_what = strlen(what);
  uint16_t len_str = strlen(str);
  return len_str < len_what ? false : memcmp(what, str, len_what) == 0;
}

uint16_t ReadUInt32(const char *str, uint32_t *number)
{
	uint16_t length = 0;

	for(uint16_t i = 0;; ++i) {
		char c = *(str+i);
		if(c < '0' || c > '9')
			break;
		++length;
	}

	if(length == 0)
		return length;

	*number = 0;

	for(uint16_t i = 0; i < length; ++i)
		*number += (str[length-1-i] - '0') * cpow10(i);

	return length;
}
