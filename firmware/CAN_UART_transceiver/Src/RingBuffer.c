#include "RingBuffer.h"

void _RingBuffer_AdvanceHead(RingBuffer *buf)
{
	if(buf->full)
		buf->tail = (buf->tail + 1) % buf->capacity;

	buf->head = (buf->head + 1) % buf->capacity;
	buf->full = buf->tail == buf->head;
}

void _RingBuffer_AdvanceTail(RingBuffer *buf)
{
	buf->tail = (buf->tail + 1) % buf->capacity;
	buf->full = 0;
}

void RingBuffer_Reset(RingBuffer *buf)
{
	buf->head = 0;
	buf->tail = 0;
	buf->full = 0;
}

void RingBuffer_Init(RingBuffer *buf, volatile uint8_t *data, uint16_t capacity)
{
	buf->data = data;
	buf->capacity = capacity;
	RingBuffer_Reset(buf);
}

uint8_t RingBuffer_Full(RingBuffer *buf)
{
	return buf->full;
}

uint16_t RingBuffer_Capacity(RingBuffer *buf)
{
    return buf->capacity;
}

uint16_t RingBuffer_Size(RingBuffer *buf)
{
    uint16_t size = buf->capacity;

    if(!buf->full) {
        if(buf->head >= buf->tail)
            size = buf->head - buf->tail;
        else
            size = buf->capacity + buf->head - buf->tail;
    }

    return size;
}

uint8_t RingBuffer_Empty(RingBuffer *buf)
{
	return !buf->full && buf->head == buf->tail;
}

uint8_t RingBuffer_Push(RingBuffer *buf, uint8_t data)
{
	if(buf->full)
		return 0;

	buf->data[buf->head] = data;
	_RingBuffer_AdvanceHead(buf);
	return 1;
}

uint8_t RingBuffer_ForcePush(RingBuffer *buf, uint8_t data)
{
	uint8_t ret = !buf->full;
	buf->data[buf->head] = data;
	_RingBuffer_AdvanceHead(buf);
	return ret;
}

uint8_t RingBuffer_Pop(RingBuffer *buf, uint8_t *data)
{
	if(RingBuffer_Empty(buf))
		return 0;

	*data = buf->data[buf->tail];
	_RingBuffer_AdvanceTail(buf);
	return 1;
}