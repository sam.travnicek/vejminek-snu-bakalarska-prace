/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "usart_mod.h"
#include "bxcan_mod.h"
#include "timers_mod.h"
#include "StrBuf.h"
#include "Packet.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

uint8_t usart_buf[15];
uint8_t usart_buf_ptr = 0;
uint8_t cdata_len = 0;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

void USART_SendPacket(Packet *packet);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */


  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  /* USER CODE BEGIN 2 */

  Timer_Setup();
  Timer_Init(&htimer2);

  // Enable TJA1055
  HAL_GPIO_WritePin(CAN_nSTB_GPIO_Port, CAN_nSTB_Pin, SET);
  HAL_GPIO_WritePin(CAN_EN_GPIO_Port, CAN_EN_Pin, SET);
  HAL_Delay(200);
  HAL_GPIO_WritePin(CAN_EN_GPIO_Port, CAN_EN_Pin, RESET);
  HAL_Delay(200);
  HAL_GPIO_WritePin(CAN_EN_GPIO_Port, CAN_EN_Pin, SET);

  USART_Setup();
  USART_Init(&husart1);

  BXCAN_Setup();
  BXCAN_Init(&hbxcan1);

  uint16_t led_cntr = 0;

  for(uint8_t i = 0; i < 5; ++i) {
    HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, SET);
    HAL_Delay(100);
    HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, RESET);
    HAL_Delay(100);
  }

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while(1) {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

    if(!Timer_Updated(&htimer2))
      continue;

    // if(!HAL_GPIO_ReadPin(CAN_nERR_GPIO_Port, CAN_nERR_Pin)) {
    //   HAL_GPIO_WritePin(CAN_EN_GPIO_Port, CAN_EN_Pin, RESET);
    //   HAL_Delay(10);
    //   HAL_GPIO_WritePin(CAN_EN_GPIO_Port, CAN_EN_Pin, SET);
    //   continue;
    // }

    BXCAN_Message rmsg;
    Packet rpacket;

    while(BXCAN_Recv(&hbxcan1, &rmsg)) {
      Packet_FromMessage(&rmsg, &rpacket);
      USART_SendPacket(&rpacket);
    }

    uint8_t rdata;
    while(USART_Poll(&husart1, &rdata, 1)) {
      if(usart_buf_ptr == 0) { // Start Byte
        if(rdata == 0xAA) {
          usart_buf[0] = 0xAA;
          usart_buf_ptr = 1;
        }
      } else if(usart_buf_ptr == 3) { // (Priority << 5) | (Not Request << 4) | Length
        usart_buf[usart_buf_ptr] = rdata;
        ++usart_buf_ptr;
        cdata_len = rdata & 0xF;
        if(cdata_len > 8)
          cdata_len = 8;
      } else if(usart_buf_ptr < 6) {
        usart_buf[usart_buf_ptr] = rdata;
        ++usart_buf_ptr;
      } else if(usart_buf_ptr < 6+cdata_len) {
        usart_buf[usart_buf_ptr] = rdata;
        ++usart_buf_ptr;
      } else {
        usart_buf[usart_buf_ptr] = rdata;
        ++usart_buf_ptr;

        uint8_t sum = 0;
        for(uint8_t i = 0; i < usart_buf_ptr-1; ++i)
          sum += usart_buf[i];
        sum = ~sum;

        if(sum == usart_buf[6+cdata_len]) { // Valid packet
          Packet tpacket;
          tpacket.src_id = usart_buf[1] & 0x7F;
          tpacket.dest_id = usart_buf[2] & 0x7F;
          tpacket.priority = (usart_buf[3] >> 5) & 0x3;
          tpacket.not_request = (usart_buf[3] >> 4) & 0x1;
          tpacket.length = cdata_len;
          tpacket.cmd = usart_buf[4] | ((uint16_t)usart_buf[5] << 8);

          for(uint8_t i = 0; i < cdata_len; ++i)
            tpacket.data[i] = usart_buf[6+i];

          BXCAN_Message tmsg;
          Packet_ToMessage(&tpacket, &tmsg);
          BXCAN_Send(&hbxcan1, &tmsg);
        }

        usart_buf_ptr = 0;
      }
    }

    // void USART_SendPacket(Packet *packet)

    if(led_cntr == 0)
      HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, RESET);
    else if(led_cntr == 100)
      HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, SET);

    if(++led_cntr == 4000)
      led_cntr = 0;
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

void USART_SendPacket(Packet *packet)
{
  if(packet->length > 8)
    packet->length = 8;

  uint8_t data[15];
  data[0] = 0x55;
  data[1] = packet->src_id;
  data[2] = packet->dest_id;
  data[3] = (packet->priority << 5) | (packet->not_request << 4) | packet->length;
  data[4] = packet->cmd;
  data[5] = packet->cmd >> 8;

  for(uint8_t i = 0; i < packet->length; ++i)
    data[6+i] = packet->data[i];

  uint8_t sum = 0;
  for(uint8_t i = 0; i < 6 + packet->length; ++i)
    sum += data[i];

  sum = ~sum;
  data[6 + packet->length] = sum;

  USART_Write(&husart1, data, 7 + packet->length);
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
