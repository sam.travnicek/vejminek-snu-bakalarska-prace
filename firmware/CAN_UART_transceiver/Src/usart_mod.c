#include "usart_mod.h"

#define USART_TX_DATA_SIZE 4000
#define USART_RX_DATA_SIZE 4000

volatile uint8_t tx1_buf_data[USART_TX_DATA_SIZE];
volatile uint8_t rx1_buf_data[USART_RX_DATA_SIZE];

RingBuffer tx1_buf = { tx1_buf_data, 0, 0, 0, USART_TX_DATA_SIZE };
RingBuffer rx1_buf = { rx1_buf_data, 0, 0, 0, USART_RX_DATA_SIZE };

USART_Handle husart1 = {
    USART1, 115200, USART_STOP_BITS_1, USART_PARITY_NONE, 8, 0.f, 0,
    &tx1_buf, &rx1_buf
};

void USART_Setup()
{
    // Enable clock for GPIOB
    RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;

    // Enable clock for USART1
    RCC->APB2ENR |= RCC_APB2ENR_USART1EN;

    // Enable Interupt ReQuests
    NVIC_EnableIRQ(USART1_IRQn);

    // PB6 - USART1 Tx, PB7 - USART1 Rx
    AFIO->MAPR |= AFIO_MAPR_USART1_REMAP;
    GPIOB->CRL &= ~(0b11111111 << GPIO_CRL_MODE6_Pos);
    // PB6 - output - alternate func. push-pull, 50 MHz
    GPIOB->CRL |= 0b1011 << GPIO_CRL_MODE6_Pos;
    // PB7 - input - floating
    GPIOB->CRL |= 0b0100 << GPIO_CRL_MODE7_Pos;
}

void USART_ResetBuffers(USART_Handle *husart)
{
    RingBuffer_Reset(husart->tx_buf);
    RingBuffer_Reset(husart->rx_buf);
}

void USART_Init(USART_Handle *husart)
{
    USART_Terminate(husart);
    USART_TypeDef *usart = husart->usart;

    // Transmitter, Receiver Enabled
    usart->CR1 |= USART_CR1_RE | USART_CR1_TE;

    // Enable interrupt RXNEIE
    usart->CR1 |= USART_CR1_RXNEIE;

    // Enable interrupt PEIE if parity control is enabled, setup parity control
    if(husart->parity != USART_PARITY_NONE) {
        usart->CR1 |= USART_CR1_PEIE | USART_CR1_PCE;

        if(husart->parity == USART_PARITY_ODD)
            usart->CR1 |= USART_CR1_PS;
    }

    // Select Word Length (8 or 9 bits)
    if(husart->data_bits == 9)
        usart->CR1 |= USART_CR1_M;

    // Select Stop Bits
    usart->CR2 |= (husart->stop_bits & 0b11) << USART_CR2_STOP_Pos;

    // Compute and configure a baud rate
    uint8_t APB_pos = RCC_CFGR_PPRE2_Pos;
    if(usart != USART1)
        APB_pos = RCC_CFGR_PPRE1_Pos;

    uint8_t AHB_prescale = AHBPrescTable[(RCC->CFGR >> RCC_CFGR_HPRE_Pos) & 0b1111];
    uint8_t APB_prescale = APBPrescTable[(RCC->CFGR >> APB_pos) & 0b111];
	uint32_t APB_clk = (SystemCoreClock >> AHB_prescale) >> APB_prescale;

    float usart_div = APB_clk / (16.f * husart->baud_rate); // can be simplified but isn't for better clarity
    usart->BRR = (uint32_t)(usart_div * 16.f);
    husart->error = (usart_div - usart->BRR/16.f) / usart_div;

    // Enable USART
    usart->CR1 |= USART_CR1_UE;
    husart->active = 1;
}

void USART_Terminate(USART_Handle *husart)
{
    USART_TypeDef *usart = husart->usart;

    // Disable, Reset USART
    usart->CR1 = 0;
    usart->CR2 = 0;
    usart->CR3 = 0;

    husart->active = 0;
}

uint16_t USART_Write(USART_Handle *husart, const uint8_t *data, uint16_t length)
{
    uint16_t written = 0;

    for(uint16_t i = 0; i < length; ++i) {
        if(RingBuffer_Push(husart->tx_buf, data[i]))
            ++written;
        else
            break;
    }

    // Enable interrupt for empty TX buffer
    husart->usart->CR1 |= USART_CR1_TXEIE;
    return written;
}

uint16_t USART_Poll(USART_Handle *husart, uint8_t *data, uint16_t length)
{
    uint16_t polled = 0;

    for(uint16_t i = 0; i < length; ++i) {
        if(RingBuffer_Pop(husart->rx_buf, data+i))
            ++polled;
        else
            break;
    }

    return polled;
}

void USART1_IRQHandler(void)
{
    if(USART1->SR & USART_SR_RXNE) {
        uint8_t temp = USART1->DR;
        RingBuffer_Push(husart1.rx_buf, temp);
    }

    if(USART1->SR & USART_SR_TXE) {
        uint8_t data;
        if(RingBuffer_Pop(husart1.tx_buf, &data))
            USART1->DR = data;
        else
            USART1->CR1 &= ~USART_CR1_TXEIE;
    }
}