#!/bin/bash

################################################################################
# STM32 Flash Script                                                           #
# by Samuel Travnicek                                                          #
################################################################################

#--- Parameters ---------------------------------------------------------------#

INTERFACE=stlink-v2.cfg

ELF_FILE_DIR=./build
IOC_FILE_DIR=.

#------------------------------------------------------------------------------#

ioc_files=( ${IOC_FILE_DIR}/*.ioc )
if [ "${IOC_FILE_DIR}/*.ioc" = "${ioc_files[0]}" ]; then
    echo "IOC file not found"
    exit 1
fi

elf_files=( ${ELF_FILE_DIR}/*.elf )
if [ "${ELF_FILE_DIR}/*.elf" = "${elf_files[0]}" ]; then
    echo "ELF file not found"
    exit 2
fi

family=""

while IFS='=' read -r key value
do
    if [[ ! $key =~ ^\ *# && -n $key ]]; then
        value="${value%%\#*}"    # Delete #comments
        value="${value%%*( )}"   # Delete trailing spaces
        value="${value%\"*}"     # Delete opening string quotes
        value="${value#\"*}"     # Delete closing string quotes
        if [ "${key}" = "Mcu.Family" ]; then
            family=$(echo "${value}" | tr '[:upper:]' '[:lower:]')
        fi
    fi
done < ${ioc_files[0]}

if [ "${family}" = "" ]; then
    echo "Mcu.Family not found in .ioc file!"
    exit 3
fi

target=${family}x.cfg
echo ""
echo -e "Interface: ${INTERFACE}"
echo -e "Target:    ${target}"
echo ""

killall -s 9 openocd

openocd \
    -c "source [find interface/${INTERFACE}]" \
    -c "source [find target/${target}]" \
    -c init \
    -c "reset halt" \
    -c "flash write_image erase ${elf_files[0]}" \
    -c "verify_image ${elf_files[0]}" \
    -c reset \
    -c shutdown