#ifndef BXCAN_MOD_H_
#define BXCAN_MOD_H_

#include "main.h"
#include <stdbool.h>

typedef struct
{
    uint32_t identifier : 29;
    bool extended_identifier : 1;
    bool remote_tx_request : 1;
    uint8_t length : 4;
    uint8_t data[8];

} BXCAN_Message;

typedef struct
{
    CAN_TypeDef *bxcan;
    bool loopback_mode;
    bool silent_mode;
    bool auto_wakeup;
    bool auto_retransmission;
    bool auto_busoff_mgnt;
    bool recv_fifo_locked;
    bool chronological_tx;
    uint16_t prescaler;

} BXCAN_Handle;

extern BXCAN_Handle hbxcan1;

void BXCAN_Setup();

void BXCAN_Init(BXCAN_Handle *hbxcan);
void BXCAN_Terminate(BXCAN_Handle *hbxcan);

uint8_t BXCAN_Available(BXCAN_Handle *hbxcan);
bool BXCAN_Send(BXCAN_Handle *hbxcan, BXCAN_Message *msg);
bool BXCAN_Recv(BXCAN_Handle *hbxcan, BXCAN_Message *msg);

#endif // BXCAN_MOD_H_