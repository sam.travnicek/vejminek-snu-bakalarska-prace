#ifndef TIMERS_MOD_H_
#define TIMERS_MOD_H_

// STM32F103C8 - Timers Module

#include "main.h"
#include <math.h>
#include <stdbool.h>

typedef struct
{
    TIM_TypeDef *timer;
    uint16_t prescaler;
    uint16_t reload_value;

} Timer_Handle;

extern Timer_Handle htimer2;

void Timer_Setup();

void Timer_Init(Timer_Handle *htimer);

bool Timer_Updated(Timer_Handle *htimer);

void TIM_ClockEnable(TIM_TypeDef *timer);

float TIM_ClockFrequency(TIM_TypeDef *timer);


#endif // TIMERS_MOD_H_