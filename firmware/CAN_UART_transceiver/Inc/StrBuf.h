#ifndef STR_BUF_H_
#define STR_BUF_H_

#include <stdint.h>
#include <stdbool.h>

typedef struct
{
    char *array;
    uint16_t ptr;
    uint16_t size;

} StrBuf;

void StrBuf_Init(StrBuf *buf, char *array, uint16_t size);

void StrBuf_Reset(StrBuf *buf);

uint16_t StrBuf_UsedSpace(StrBuf *buf);

uint16_t StrBuf_RemainingSpace(StrBuf *buf);

uint16_t StrBuf_Capacity(StrBuf *buf);

char* StrBuf_Array(StrBuf *buf);

void StrBuf_UInt32(StrBuf *buf, uint32_t number);

void StrBuf_UInt32Bin(StrBuf *buf, uint32_t number, bool zero_fill);

void StrBuf_Float(StrBuf *buf, float number, uint8_t precision);

void StrBuf_String(StrBuf *buf, const char *str);

#endif // STR_BUF_H_