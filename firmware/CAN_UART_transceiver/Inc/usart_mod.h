#ifndef USART_MOD_H_
#define USART_MOD_H_

#include "main.h"
#include "RingBuffer.h"

typedef struct
{
    USART_TypeDef *usart;
    uint32_t baud_rate;
    uint8_t stop_bits;
    uint8_t parity;
    uint8_t data_bits;
    float error;
    uint8_t active;
    RingBuffer *tx_buf;
    RingBuffer *rx_buf;

} USART_Handle;

extern USART_Handle husart1;

#define USART_STOP_BITS_1   0b00
#define USART_STOP_BITS_0_5 0b01
#define USART_STOP_BITS_2   0b10
#define USART_STOP_BITS_1_5 0b11

#define USART_PARITY_NONE   0
#define USART_PARITY_ODD    1
#define USART_PARITY_EVEN   2

void USART_Setup();

void USART_ResetBuffers(USART_Handle *husart);
void USART_Init(USART_Handle *husart);
void USART_Terminate(USART_Handle *husart);

uint16_t USART_Write(USART_Handle *husart, const uint8_t *data, uint16_t length);
uint16_t USART_Poll(USART_Handle *husart, uint8_t *data, uint16_t length);

#endif // USART_MOD_H_