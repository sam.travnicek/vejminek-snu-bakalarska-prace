#ifndef RING_BUFFER_H_
#define RING_BUFFER_H_

#include <stdint.h>

typedef volatile struct
{
	volatile uint8_t *data;
	uint16_t head;
	uint16_t tail;
	uint8_t full;
	uint16_t capacity;

} RingBuffer;

void RingBuffer_Reset(RingBuffer *buf);
void RingBuffer_Init(RingBuffer *buf, volatile uint8_t *data, uint16_t capacity);
uint8_t RingBuffer_Full(RingBuffer *buf);
uint16_t RingBuffer_Capacity(RingBuffer *buf);
uint16_t RingBuffer_Size(RingBuffer *buf);
uint8_t RingBuffer_Empty(RingBuffer *buf);
uint8_t RingBuffer_Push(RingBuffer *buf, uint8_t data);
uint8_t RingBuffer_ForcePush(RingBuffer *buf, uint8_t data);
uint8_t RingBuffer_Pop(RingBuffer *buf, uint8_t *data);

#endif // RING_BUFFER_H_