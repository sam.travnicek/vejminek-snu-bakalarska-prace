#include "gpio_mod.h"
#include "indicator_mod.h"
#include "timers_mod.h"
#include "adc_mod.h"

void IO_Setup()
{
    RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
    RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
    RCC->AHBENR |= RCC_AHBENR_GPIOCEN;
    RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;

    // External interrupts:
    //   EXTI0=PA0, EXTI1=PA1, EXTI2=PA2, EXTI3=PA3, EXTI4=PA4, EXTI5=PA5,
    //   EXTI6=PA6, EXTI12=PB12, EXTI13=PB13
    // PB2 conflicts with PA2, handled by timer
    SYSCFG->EXTICR[0] = 0;
    SYSCFG->EXTICR[1] = 0;
    SYSCFG->EXTICR[2] = 0;
    SYSCFG->EXTICR[3] = SYSCFG_EXTICR4_EXTI12_PB | SYSCFG_EXTICR4_EXTI13_PB;

    NVIC_EnableIRQ(EXTI0_IRQn);
    NVIC_EnableIRQ(EXTI1_IRQn);
    NVIC_EnableIRQ(EXTI2_TSC_IRQn);
    NVIC_EnableIRQ(EXTI3_IRQn);
    NVIC_EnableIRQ(EXTI4_IRQn);
    NVIC_EnableIRQ(EXTI9_5_IRQn); // 5, 6, 7, 8, 9 EXTI interrupts
    NVIC_EnableIRQ(EXTI15_10_IRQn); // 10, 11, 12, 13 EXTI interrupts
}

void IO_Init(IO_Handle *io)
{
    IO_Mode(io, IO_MODE_DIGITAL);
}

void _IO_ExternalInterrupt(IO_Handle *io, bool enabled, bool edge_rising)
{
    int pos = -1;

    if(io->gpio == GPIOA) {
        if(io->pin <= 6)
            pos = io->pin;
    } else if(io->gpio == GPIOB) {
        if(io->pin == 12 || io->pin == 13)
            pos = io->pin;
    }

    if(pos == -1)
        return;

    if(enabled) {
        EXTI->IMR |= 1 << pos;
        EXTI->RTSR &= ~(1 << pos);
        EXTI->FTSR &= ~(1 << pos);

        if(edge_rising)
            EXTI->RTSR |= 1 << pos;
        else
            EXTI->FTSR |= 1 << pos;
    } else {
        EXTI->IMR &= ~(1 << pos);
    }
}

void _IO_ModeOutputDigital(IO_Handle *io)
{
    io->mode = IO_MODE_DIGITAL;

    GPIO_TypeDef *gpio = io->gpio;

    // General purpose output mode
    gpio->MODER &= ~(0b11 << (io->pin*2));
    gpio->MODER |= 0b01 << (io->pin*2);

    // Push-pull
    gpio->OTYPER &= ~(1 << io->pin);

    // Low Speed
    gpio->OSPEEDR &= ~(0b11 << (io->pin*2));
    gpio->OSPEEDR |= 0b00 << (io->pin*2);

    if(gpio == GPIOC)
        TIM16->DIER &= ~TIM_DIER_UIE; // Disable timer interrupt

    // Default value LOW
    gpio->ODR &= ~(1 << io->pin);
    io->prev_value = 0;
    io->value = 0;
}

void _IO_ModeOutputPWM(IO_Handle *io)
{
    io->mode = IO_MODE_PWM;

    GPIO_TypeDef *gpio = io->gpio;

    if(gpio != GPIOC) {
        // Alternate function mode
        gpio->MODER &= ~(0b11 << (io->pin*2));
        gpio->MODER |= 0b10 << (io->pin*2);
    } else { // GPIOC - PWM simulated by software
        // General purpose output mode
        gpio->MODER &= ~(0b11 << (io->pin*2));
        gpio->MODER |= 0b01 << (io->pin*2);
    }

    // Push-pull
    gpio->OTYPER &= ~(1 << io->pin);

    // High Speed
    gpio->OSPEEDR |= 0b11 << (io->pin*2);

    if(gpio == GPIOC) {
        gpio->ODR &= ~(1 << io->pin); // Default value LOW
        TIM16->DIER |= TIM_DIER_UIE; // Enable timer interrupt
    }

    // Reset value
    io->prev_value = 0;
    io->value = 0;

    // Alternate function multiplex
    if(gpio == GPIOA) {
        if(io->pin == 7) {
            gpio->AFR[0] &= ~GPIO_AFRL_AFRL7;
            gpio->AFR[0] |= 2 << GPIO_AFRL_AFRL7_Pos; // AF2=TIM3CH2
        } else if(io->pin == 8) {
            gpio->AFR[1] &= ~GPIO_AFRH_AFRH0;
            gpio->AFR[1] |= 6 << GPIO_AFRH_AFRH0_Pos; // AF6=TIM1CH1
        } else if(io->pin == 15) {
            gpio->AFR[1] &= ~GPIO_AFRH_AFRH7;
            gpio->AFR[1] |= 1 << GPIO_AFRH_AFRH7_Pos; // AF1=TIM2CH1
        }
    } else if(gpio == GPIOB) {
        if(io->pin == 0) {
            gpio->AFR[0] &= ~GPIO_AFRL_AFRL0;
            gpio->AFR[0] |= 2 << GPIO_AFRL_AFRL0_Pos; // AF2=TIM3CH3
        } else if(io->pin == 1) {
            gpio->AFR[0] &= ~GPIO_AFRL_AFRL1;
            gpio->AFR[0] |= 2 << GPIO_AFRL_AFRL1_Pos; // AF2=TIM3CH4
        } else if(io->pin == 6) {
            gpio->AFR[0] &= ~GPIO_AFRL_AFRL6;
            gpio->AFR[0] |= 5 << GPIO_AFRL_AFRL6_Pos; // AF5=TIM8CH1
        } else if(io->pin == 7) {
            gpio->AFR[0] &= ~GPIO_AFRL_AFRL7;
            gpio->AFR[0] |= 2 << GPIO_AFRL_AFRL7_Pos; // AF2=TIM4CH2
        } else if(io->pin == 14) {
            gpio->AFR[1] &= ~GPIO_AFRH_AFRH6;
            gpio->AFR[1] |= 1 << GPIO_AFRH_AFRH6_Pos; // AF1=TIM15CH1
        } else if(io->pin == 15) {
            gpio->AFR[1] &= ~GPIO_AFRH_AFRH7;
            gpio->AFR[1] |= 1 << GPIO_AFRH_AFRH7_Pos; // AF1=TIM15CH2
        }
    }
}

void _IO_ModeInputDigital(IO_Handle *io)
{
    io->mode = IO_MODE_DIGITAL;
    _IO_ExternalInterrupt(io, false, false);

    GPIO_TypeDef *gpio = io->gpio;

    // Input mode
    gpio->MODER &= ~(0b11 << (io->pin*2));

    // // Pull-up
    // gpio->PUPDR &= ~(0b11 << (io->pin*2));
    // gpio->PUPDR |= 0b01 << (io->pin*2);

    // Pull-down
    gpio->PUPDR &= ~(0b11 << (io->pin*2));
    gpio->PUPDR |= 0b10 << (io->pin*2);

    io->value = (gpio->IDR >> io->pin) & 1;
}

void _IO_ModeInputAnalog(IO_Handle *io)
{
    io->mode = IO_MODE_ANALOG;
    _IO_ExternalInterrupt(io, false, false);

    GPIO_TypeDef *gpio = io->gpio;

    // Analog mode
    gpio->MODER |= 0b11 << (io->pin*2);
}

void _IO_ModeInputCounter(IO_Handle *io, uint8_t mode)
{
    io->mode = mode;

    GPIO_TypeDef *gpio = io->gpio;

    // Input mode
    gpio->MODER &= ~(0b11 << (io->pin*2));

    if(mode == IO_MODE_COUNTER_RISE) {
        // Pull-down
        gpio->PUPDR &= ~(0b11 << (io->pin*2));
        gpio->PUPDR |= 0b10 << (io->pin*2);
    } else {
        // Pull-up
        gpio->PUPDR &= ~(0b11 << (io->pin*2));
        gpio->PUPDR |= 0b01 << (io->pin*2);
    }

    io->value = 0;

    _IO_ExternalInterrupt(io, true, mode == IO_MODE_COUNTER_RISE);
}

void IO_Mode(IO_Handle *io, uint8_t mode)
{
    if(io->direction == IO_DIR_OUTPUT) {
        if(mode == IO_MODE_DIGITAL)
            _IO_ModeOutputDigital(io);
        else if(mode == IO_MODE_PWM)
            _IO_ModeOutputPWM(io);
    } else if(io->direction == IO_DIR_INPUT) {
        if(mode == IO_MODE_DIGITAL)
            _IO_ModeInputDigital(io);
        else if(mode == IO_MODE_ANALOG)
            _IO_ModeInputAnalog(io);
        else if(mode == IO_MODE_COUNTER_FALL || mode == IO_MODE_COUNTER_RISE)
            _IO_ModeInputCounter(io, mode);
    }
}

void IO_Write(IO_Handle *io, uint16_t value)
{
    io->prev_value = io->value;
    io->value = value;

    if(io->direction == IO_DIR_INPUT)
        return;

    if(io->mode == IO_MODE_DIGITAL) {
        if(value)
            io->gpio->BSRR |= 1 << io->pin;
        else
            io->gpio->BSRR |= 1 << (16 + io->pin);
    } else if(io->mode == IO_MODE_PWM) {
        GPIO_TypeDef *gpio = io->gpio;

        if(gpio == GPIOA) {
            if(io->pin == 7)
                Timer_PWMDutyCycle(&htimer3, 2, value);
            else if(io->pin == 8)
                Timer_PWMDutyCycle(&htimer1, 1, value);
            else if(io->pin == 15)
                Timer_PWMDutyCycle(&htimer2, 1, value);
        } else if(gpio == GPIOB) {
            if(io->pin == 0)
                Timer_PWMDutyCycle(&htimer3, 3, value);
            else if(io->pin == 1)
                Timer_PWMDutyCycle(&htimer3, 4, value);
            else if(io->pin == 6)
                Timer_PWMDutyCycle(&htimer8, 1, value);
            else if(io->pin == 7)
                Timer_PWMDutyCycle(&htimer4, 2, value);
            else if(io->pin == 14)
                Timer_PWMDutyCycle(&htimer15, 1, value);
            else if(io->pin == 15)
                Timer_PWMDutyCycle(&htimer15, 2, value);
        }
    }
}

uint16_t IO_Read(IO_Handle *io)
{
    if(io->direction == IO_DIR_OUTPUT)
        return io->value;

    if(io->mode == IO_MODE_DIGITAL) {
        io->prev_value = io->value;
        io->value = (io->gpio->IDR >> io->pin) & 1;
        return io->value;
    }

    if(io->mode == IO_MODE_ANALOG) {
        io->prev_value = io->value;
        if(io->adc_value_ptr)
            io->value = *(io->adc_value_ptr);
        return io->value;
    }

    if(io->mode == IO_MODE_COUNTER_RISE || io->mode == IO_MODE_COUNTER_FALL)
        return io->value;

    return 0;
}

void EXTI0_IRQHandler()
{
    EXTI->PR |= 1 << 0;
    hin9.prev_value = hin9.value;
    ++(hin9.value);
}

void EXTI1_IRQHandler()
{
    EXTI->PR |= 1 << 1;
    hin8.prev_value = hin8.value;
    ++(hin8.value);
}

void EXTI2_TSC_IRQHandler()
{
    if((EXTI->IMR & (1 << 2)) && (EXTI->PR & (1 << 2))) {
        EXTI->PR |= 1 << 2;
        hin7.prev_value = hin7.value;
        ++(hin7.value);
    }
}

void EXTI3_IRQHandler()
{
    EXTI->PR |= 1 << 3;
    hin6.prev_value = hin6.value;
    ++(hin6.value);
}

void EXTI4_IRQHandler()
{
    EXTI->PR |= 1 << 4;
    hin5.prev_value = hin5.value;
    ++(hin5.value);
}

void EXTI9_5_IRQHandler()
{
    if((EXTI->IMR & (1 << 5)) && (EXTI->PR & (1 << 5))) {
        EXTI->PR |= 1 << 5;
        hin4.prev_value = hin4.value;
        ++(hin4.value);
    }

    if((EXTI->IMR & (1 << 6)) && (EXTI->PR & (1 << 6))) {
        EXTI->PR |= 1 << 6;
        hin3.prev_value = hin3.value;
        ++(hin3.value);
    }
}

void EXTI15_10_IRQHandler()
{
    if((EXTI->IMR & (1 << 12)) && (EXTI->PR & (1 << 12))) {
        EXTI->PR |= 1 << 12;
        hin1.prev_value = hin1.value;
        ++(hin1.value);
    }

    if((EXTI->IMR & (1 << 13)) && (EXTI->PR & (1 << 13))) {
        EXTI->PR |= 1 << 13;
        hin0.prev_value = hin0.value;
        ++(hin0.value);
    }
}

IO_Handle hout0 = {
    .gpio = GPIOA,
    .pin = 7,
    .direction = IO_DIR_OUTPUT,
    .mode = IO_MODE_DIGITAL,
    .prev_value = 0,
    .value = 0,
    .adc_value_ptr = NULL
};

IO_Handle hout1 = {
    .gpio = GPIOB,
    .pin = 0,
    .direction = IO_DIR_OUTPUT,
    .mode = IO_MODE_DIGITAL,
    .prev_value = 0,
    .value = 0,
    .adc_value_ptr = NULL
};

IO_Handle hout2 = {
    .gpio = GPIOB,
    .pin = 1,
    .direction = IO_DIR_OUTPUT,
    .mode = IO_MODE_DIGITAL,
    .prev_value = 0,
    .value = 0,
    .adc_value_ptr = NULL
};

IO_Handle hout3 = {
    .gpio = GPIOB,
    .pin = 14,
    .direction = IO_DIR_OUTPUT,
    .mode = IO_MODE_DIGITAL,
    .prev_value = 0,
    .value = 0,
    .adc_value_ptr = NULL
};

IO_Handle hout4 = {
    .gpio = GPIOB,
    .pin = 15,
    .direction = IO_DIR_OUTPUT,
    .mode = IO_MODE_DIGITAL,
    .prev_value = 0,
    .value = 0,
    .adc_value_ptr = NULL
};

IO_Handle hout5 = {
    .gpio = GPIOA,
    .pin = 8,
    .direction = IO_DIR_OUTPUT,
    .mode = IO_MODE_DIGITAL,
    .prev_value = 0,
    .value = 0,
    .adc_value_ptr = NULL
};

IO_Handle hout6 = {
    .gpio = GPIOA,
    .pin = 15,
    .direction = IO_DIR_OUTPUT,
    .mode = IO_MODE_DIGITAL,
    .prev_value = 0,
    .value = 0,
    .adc_value_ptr = NULL
};

IO_Handle hout7 = {
    .gpio = GPIOB,
    .pin = 6,
    .direction = IO_DIR_OUTPUT,
    .mode = IO_MODE_DIGITAL,
    .prev_value = 0,
    .value = 0,
    .adc_value_ptr = NULL
};

IO_Handle hout8 = {
    .gpio = GPIOB,
    .pin = 7,
    .direction = IO_DIR_OUTPUT,
    .mode = IO_MODE_DIGITAL,
    .prev_value = 0,
    .value = 0,
    .adc_value_ptr = NULL
};

IO_Handle hout9 = {
    .gpio = GPIOC,
    .pin = 13,
    .direction = IO_DIR_OUTPUT,
    .mode = IO_MODE_DIGITAL,
    .prev_value = 0,
    .value = 0,
    .adc_value_ptr = NULL
};

IO_Handle hin0 = {
    .gpio = GPIOB,
    .pin = 13,
    .direction = IO_DIR_INPUT,
    .mode = IO_MODE_DIGITAL,
    .prev_value = 0,
    .value = 0,
    .adc_value_ptr = adc3_values+0
};

IO_Handle hin1 = {
    .gpio = GPIOB,
    .pin = 12,
    .direction = IO_DIR_INPUT,
    .mode = IO_MODE_DIGITAL,
    .prev_value = 0,
    .value = 0,
    .adc_value_ptr = adc4_values+0
};

IO_Handle hin2 = {
    .gpio = GPIOB,
    .pin = 2,
    .direction = IO_DIR_INPUT,
    .mode = IO_MODE_DIGITAL,
    .prev_value = 0,
    .value = 0,
    .adc_value_ptr = adc2_values+3
};

IO_Handle hin3 = {
    .gpio = GPIOA,
    .pin = 6,
    .direction = IO_DIR_INPUT,
    .mode = IO_MODE_DIGITAL,
    .prev_value = 0,
    .value = 0,
    .adc_value_ptr = adc2_values+2
};

IO_Handle hin4 = {
    .gpio = GPIOA,
    .pin = 5,
    .direction = IO_DIR_INPUT,
    .mode = IO_MODE_DIGITAL,
    .prev_value = 0,
    .value = 0,
    .adc_value_ptr = adc2_values+1
};

IO_Handle hin5 = {
    .gpio = GPIOA,
    .pin = 4,
    .direction = IO_DIR_INPUT,
    .mode = IO_MODE_DIGITAL,
    .prev_value = 0,
    .value = 0,
    .adc_value_ptr = adc2_values+0
};

IO_Handle hin6 = {
    .gpio = GPIOA,
    .pin = 3,
    .direction = IO_DIR_INPUT,
    .mode = IO_MODE_DIGITAL,
    .prev_value = 0,
    .value = 0,
    .adc_value_ptr = adc1_values+3
};

IO_Handle hin7 = {
    .gpio = GPIOA,
    .pin = 2,
    .direction = IO_DIR_INPUT,
    .mode = IO_MODE_DIGITAL,
    .prev_value = 0,
    .value = 0,
    .adc_value_ptr = adc1_values+2
};

IO_Handle hin8 = {
    .gpio = GPIOA,
    .pin = 1,
    .direction = IO_DIR_INPUT,
    .mode = IO_MODE_DIGITAL,
    .prev_value = 0,
    .value = 0,
    .adc_value_ptr = adc1_values+1
};

IO_Handle hin9 = {
    .gpio = GPIOA,
    .pin = 0,
    .direction = IO_DIR_INPUT,
    .mode = IO_MODE_DIGITAL,
    .prev_value = 0,
    .value = 0,
    .adc_value_ptr = adc1_values+0
};