#include "timers_mod.h"
#include "gpio_mod.h"

Timer_Handle htimer1 = { // PWM 12-bit, 1098,6 Hz @ 72 MHz SystemCoreClock
    .timer = TIM1,
    .prescaler = 16,
    .reload_value = (1 << 12) - 1,
    .update_interrupt = false
};

Timer_Handle htimer2 = { // PWM 12-bit, 1098,6 Hz @ 72 MHz SystemCoreClock
    .timer = TIM2,
    .prescaler = 16,
    .reload_value = (1 << 12) - 1,
    .update_interrupt = false
};

Timer_Handle htimer3 = { // PWM 12-bit, 1098,6 Hz @ 72 MHz SystemCoreClock
    .timer = TIM3,
    .prescaler = 16,
    .reload_value = (1 << 12) - 1,
    .update_interrupt = false
};

Timer_Handle htimer4 = { // PWM 12-bit, 1098,6 Hz @ 72 MHz SystemCoreClock
    .timer = TIM4,
    .prescaler = 16,
    .reload_value = (1 << 12) - 1,
    .update_interrupt = false
};

Timer_Handle htimer8 = { // PWM 12-bit, 1098,6 Hz @ 72 MHz SystemCoreClock
    .timer = TIM8,
    .prescaler = 16,
    .reload_value = (1 << 12) - 1,
    .update_interrupt = false
};

Timer_Handle htimer15 = { // PWM 12-bit, 1098,6 Hz @ 72 MHz SystemCoreClock
    .timer = TIM15,
    .prescaler = 16,
    .reload_value = (1 << 12) - 1,
    .update_interrupt = false
};

volatile uint8_t _timer16_counter = 0;

Timer_Handle htimer16 = { // 256,2 kHz @ 72 MHz SystemCoreClock
    .timer = TIM16,
    .prescaler = 0u,
    .reload_value = 281u,
    .update_interrupt = false
};

Timer_Handle htimer17 = { // 4 kHz @ 72 MHz SystemCoreClock
    .timer = TIM17,
    .prescaler = 10u,
    .reload_value = 1800u,
    .update_interrupt = false
};

extern Timer_Handle htimer1;
extern Timer_Handle htimer2;
extern Timer_Handle htimer3;
extern Timer_Handle htimer4;
extern Timer_Handle htimer8;
extern Timer_Handle htimer15;

void Timers_Setup()
{
    TIM_ClockEnable(TIM1);
    TIM_ClockEnable(TIM2);
    TIM_ClockEnable(TIM3);
    TIM_ClockEnable(TIM4);
    TIM_ClockEnable(TIM8);
    TIM_ClockEnable(TIM15);
    TIM_ClockEnable(TIM16);
    TIM_ClockEnable(TIM17);

    NVIC_EnableIRQ(TIM1_UP_TIM16_IRQn);
}

void Timer_Init(Timer_Handle *htimer)
{
    TIM_TypeDef *timer = htimer->timer;

    // Clock division ratio 1:1, upcounter
    timer->CR1 = 0;
    timer->CNT = 0;

    timer->PSC = htimer->prescaler;
    timer->ARR = htimer->reload_value;

    if(htimer->update_interrupt)
        timer->DIER |= TIM_DIER_UIE;
    else
        timer->DIER &= ~TIM_DIER_UIE;

    timer->CR1 |= TIM_CR1_CEN;
}

void Timer_EnablePWM(Timer_Handle *htimer, uint8_t channel)
{
    TIM_TypeDef *timer = htimer->timer;
    timer->CR1 &= ~TIM_CR1_CEN;

    bool has_BDTR_reg = timer == TIM1 || timer == TIM8 || timer == TIM15
        || timer == TIM16 || timer == TIM17;

    if(has_BDTR_reg) {
        timer->BDTR |= TIM_BDTR_MOE;
        timer->BDTR |= TIM_BDTR_OSSR;
        timer->BDTR |= TIM_BDTR_OSSI;
    }

    if(channel == 1) {

        // PWM 1 mode
        timer->CCMR1 &= ~TIM_CCMR1_OC1M;
        timer->CCMR1 |= 0b110 << TIM_CCMR1_OC1M_Pos;

        // Enable preload register - mandatory for PWM
        timer->CCMR1 |= TIM_CCMR1_OC1PE;

        // Update generation - mandatory for PWM
        timer->EGR |= TIM_EGR_UG;

        // Enable output
        timer->CCER |= TIM_CCER_CC1E;

    } else if(channel == 2) {

        // PWM 1 mode
        timer->CCMR1 &= ~TIM_CCMR1_OC2M;
        timer->CCMR1 |= 0b110 << TIM_CCMR1_OC2M_Pos;

        // Enable preload register - mandatory for PWM
        timer->CCMR1 |= TIM_CCMR1_OC2PE;

        // Update generation - mandatory for PWM
        timer->EGR |= TIM_EGR_UG;

        // Enable output
        timer->CCER |= TIM_CCER_CC2E;

    } else if(channel == 3) {

        // PWM 1 mode
        timer->CCMR2 &= ~TIM_CCMR2_OC3M;
        timer->CCMR2 |= 0b110 << TIM_CCMR2_OC3M_Pos;

        // Enable preload register - mandatory for PWM
        timer->CCMR2 |= TIM_CCMR2_OC3PE;

        // Update generation - mandatory for PWM
        timer->EGR |= TIM_EGR_UG;

        // Enable output
        timer->CCER |= TIM_CCER_CC3E;

    } else if(channel == 4) {

        // PWM 1 mode
        timer->CCMR2 &= ~TIM_CCMR2_OC4M;
        timer->CCMR2 |= 0b110 << TIM_CCMR2_OC4M_Pos;

        // Enable preload register - mandatory for PWM
        timer->CCMR2 |= TIM_CCMR2_OC4PE;

        // Update generation - mandatory for PWM
        timer->EGR |= TIM_EGR_UG;

        // Enable output
        timer->CCER |= TIM_CCER_CC4E;

    } else if(channel == 5) {

        // PWM 1 mode
        timer->CCMR3 &= ~TIM_CCMR3_OC5M;
        timer->CCMR3 |= 0b110 << TIM_CCMR3_OC5M_Pos;

        // Enable preload register - mandatory for PWM
        timer->CCMR3 |= TIM_CCMR3_OC5PE;

        // Update generation - mandatory for PWM
        timer->EGR |= TIM_EGR_UG;

        // Enable output
        timer->CCER |= TIM_CCER_CC5E;

    } else if(channel == 6) {
        // PWM 1 mode
        timer->CCMR3 &= ~TIM_CCMR3_OC6M;
        timer->CCMR3 |= 0b110 << TIM_CCMR3_OC6M_Pos;

        // Enable preload register - mandatory for PWM
        timer->CCMR3 |= TIM_CCMR3_OC6PE;

        // Update generation - mandatory for PWM
        timer->EGR |= TIM_EGR_UG;

        // Enable output
        timer->CCER |= TIM_CCER_CC6E;

    }

    timer->CR1 |= TIM_CR1_CEN;
}

void Timer_PWMDutyCycle(Timer_Handle *htimer, uint8_t channel, uint32_t dc)
{
    TIM_TypeDef *timer = htimer->timer;

    if(channel == 1)
        timer->CCR1 = dc;
    else if(channel == 2)
        timer->CCR2 = dc;
    else if(channel == 3)
        timer->CCR3 = dc;
    else if(channel == 4)
        timer->CCR4 = dc;
    else if(channel == 5)
        timer->CCR5 = dc;
    else if(channel == 6)
        timer->CCR6 = dc;
}

bool Timer_Updated(Timer_Handle *htimer)
{
    bool updated = htimer->timer->SR & TIM_SR_UIF;
    if(updated) // Must be cleared only if UIF=1
        htimer->timer->SR &= ~TIM_SR_UIF;
    return updated;
}

void TIM_ClockEnable(TIM_TypeDef *timer)
{
    if(timer == TIM1)
        RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;
    else if(timer == TIM2)
        RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
    else if(timer == TIM3)
        RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
    else if(timer == TIM4)
        RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;
    else if(timer == TIM6)
        RCC->APB1ENR |= RCC_APB1ENR_TIM6EN;
    else if(timer == TIM7)
        RCC->APB1ENR |= RCC_APB1ENR_TIM7EN;
    else if(timer == TIM8)
        RCC->APB2ENR |= RCC_APB2ENR_TIM8EN;
    else if(timer == TIM15)
        RCC->APB2ENR |= RCC_APB2ENR_TIM15EN;
    else if(timer == TIM16)
        RCC->APB2ENR |= RCC_APB2ENR_TIM16EN;
    else if(timer == TIM17)
        RCC->APB2ENR |= RCC_APB2ENR_TIM17EN;
}

float TIM_ClockFrequency(TIM_TypeDef *timer)
{
    uint8_t APB_mul = 1;
    uint8_t APB_pos = RCC_CFGR_PPRE2_Pos;

    if(timer != TIM1 && timer != TIM8 && timer != TIM15
            && timer != TIM16 && timer != TIM17) {
        APB_mul = 2;
        APB_pos = RCC_CFGR_PPRE1_Pos;
    }

    uint8_t AHB_prescale =
        AHBPrescTable[(RCC->CFGR >> RCC_CFGR_HPRE_Pos) & 0b1111];
    uint8_t APB_prescale =
        APBPrescTable[(RCC->CFGR >> APB_pos) & 0b111];

	// uint32_t APB_timer_clk =
    //     ((SystemCoreClock >> AHB_prescale) >> APB_prescale) * APB_mul;
    float AHB_clk = SystemCoreClock / powf(2, AHB_prescale);
    float APB_clk = AHB_clk / powf(2, APB_prescale);
    float APB_timer_clk = APB_clk * APB_mul;

    return APB_timer_clk;
}

void TIM1_UP_TIM16_IRQHandler()
{
    TIM16->SR &= ~TIM_SR_UIF;
    if(_timer16_counter < hout9.value)
        GPIOC->BSRR |= 1 << 13;
    else
        GPIOC->BSRR |= (1 << 29);
    ++_timer16_counter;
}