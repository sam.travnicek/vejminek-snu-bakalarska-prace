#include "StrBuf.h"
#include "utils_mod.h"

void StrBuf_Init(StrBuf *buf, char *array, uint16_t size)
{
    buf->array = array;
    buf->size = size;
    buf->ptr = 0;
}

void StrBuf_Reset(StrBuf *buf)
{
    buf->ptr = 0;
}

uint16_t StrBuf_UsedSpace(StrBuf *buf)
{
    return buf->ptr;
}

uint16_t StrBuf_RemainingSpace(StrBuf *buf)
{
    return buf->size - buf->ptr;
}

uint16_t StrBuf_Capacity(StrBuf *buf)
{
    return buf->size;
}

char* StrBuf_Array(StrBuf *buf)
{
    return buf->array;
}

void StrBuf_UInt32(StrBuf *buf, uint32_t number)
{
    uint8_t nonzero_found = 0;

    if(number == 0) {
        if(buf->ptr < buf->size)
            buf->array[(buf->ptr)++] = '0';
        return;
    }

    for(uint16_t i = 0; i < 10 && buf->ptr < buf->size; ++i) {
        uint8_t digit = (number / cpow10(9-i)) % 10;
        if(digit) {
            nonzero_found = 1;
            buf->array[(buf->ptr)++] = '0' + digit;
        } else {
            if(nonzero_found)
                buf->array[(buf->ptr)++] = '0' + digit;
        }
    }
}

void StrBuf_UInt32Bin(StrBuf *buf, uint32_t number, bool zero_fill)
{
    uint8_t nonzero_found = 0;

    if(number == 0) {
        for(uint8_t i = 0; i < (zero_fill ? 32 : 1) && buf->ptr < buf->size; ++i)
            buf->array[(buf->ptr)++] = '0';
        return;
    }

    for(uint16_t i = 0u; i < 32u && buf->ptr < buf->size; ++i) {
        uint8_t digit = (number >> (31u-i)) & 1u;
        if(digit) {
            nonzero_found = 1;
            buf->array[(buf->ptr)++] = '0' + digit;
        } else {
            if(nonzero_found || zero_fill)
                buf->array[(buf->ptr)++] = '0' + digit;
        }
    }
}

void StrBuf_Float(StrBuf *buf, float number, uint8_t precision)
{
    StrBuf_UInt32(buf, (uint32_t)number);

    if(buf->ptr >= buf->size)
        return;

    buf->array[(buf->ptr)++] = '.';

    char frac_digits[20];
    if(precision > 20)
        precision = 20;

    uint8_t dig0_cnt = 0;
    uint8_t nonzero_found = 0;

    for(uint8_t i = 0; i < precision; ++i) {
        float num = (number - (uint32_t)number) * cpow10(precision-i);
        uint8_t digit = (uint32_t)num % 10;
        frac_digits[i] = '0' + digit;

        if(digit != 0)
            nonzero_found = 1;
        if(!nonzero_found)
            dig0_cnt++;
    }

    if(dig0_cnt == precision) {
        if(buf->ptr < buf->size)
            buf->array[(buf->ptr)++] = '0';
    } else {
        for(uint8_t i = 0; i < precision-dig0_cnt && buf->ptr < buf->size; ++i)
            buf->array[(buf->ptr)++] = frac_digits[precision-1-i];
    }
}

void StrBuf_String(StrBuf *buf, const char *str)
{
    for(uint32_t i = 0; str[i] && buf->ptr < buf->size; ++i)
        buf->array[(buf->ptr)++] = str[i];
}