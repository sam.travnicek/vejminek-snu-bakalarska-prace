#include "module_mod.h"

#define _MODULE_HWID     0x0001
#define _MODULE_SWVER    0x0001

uint8_t _module_id = 0;

void Module_Setup()
{
    _module_id = 1;
}

uint8_t Module_ID()
{
    return _module_id;
}

uint16_t Module_HWID()
{
    return _MODULE_HWID;
}

uint16_t Module_SWVersion()
{
    return _MODULE_SWVER;
}