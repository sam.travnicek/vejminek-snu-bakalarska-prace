#include "com_mod.h"
#include <stdbool.h>
#include "indicator_mod.h"
#include "usbd_cdc_if.h"
#include "gpio_mod.h"
#include "module_mod.h"

#define BROADCAST 0x7F

typedef struct
{
    uint16_t cmd;
    uint16_t mask;
    uint8_t min_length;
    void (*callback)(const Packet *);

} _Cmd_Handler;

void _CmdCb_Info(const Packet *packet);
void _CmdCb_IOModes(const Packet *packet);
void _CmdCb_WriteDigitalOutputs(const Packet *packet);
void _CmdCb_WriteIO(const Packet *packet);
void _CmdCb_ReadDigitalInputs(const Packet *packet);
void _CmdCb_ReadIO(const Packet *packet);

const _Cmd_Handler _cmd_handlers[] =
{
    {
        .cmd = PCMD_INFO,
        .mask = 0xFFF,
        .min_length = 0,
        .callback = _CmdCb_Info
    },
    {
        .cmd = PCMD_IO_MODES,
        .mask = 0xFFF,
        .min_length = 4,
        .callback = _CmdCb_IOModes
    },
    {
        .cmd = PCMD_WR_DOUTS,
        .mask = 0xFFF,
        .min_length = 2,
        .callback = _CmdCb_WriteDigitalOutputs
    },
    {
        .cmd = PCMD_WR_IO,
        .mask = 0xFFF,
        .min_length = 3,
        .callback = _CmdCb_WriteIO
    },
    {
        .cmd = PCMD_RD_DINS,
        .mask = 0xFFF,
        .min_length = 0,
        .callback = _CmdCb_ReadDigitalInputs
    },
    {
        .cmd = PCMD_RD_IO,
        .mask = 0xFFF,
        .min_length = 1,
        .callback = _CmdCb_ReadIO
    }
};

IO_Handle *_OUTPUTS[] = {
    &hout0, &hout1, &hout2, &hout3, &hout4, &hout5, &hout6, &hout7,
    &hout8, &hout9
};

IO_Handle *_INPUTS[] = {
    &hin0, &hin1, &hin2, &hin3, &hin4, &hin5, &hin6, &hin7,
    &hin8, &hin9
};

void Com_PacketReceived(const Packet *packet)
{
    if(packet->dest_id != BROADCAST && packet->dest_id != Module_ID())
        return;
    if(packet->not_request)
        return;

    Indicator_Pulse(0, 40);
    StrBuf_Packet(&USB_TxBuf, packet);
    USB_FlushTx();

    for(uint16_t i = 0; i < sizeof(_cmd_handlers)/sizeof(*_cmd_handlers); ++i) {
        const _Cmd_Handler *handler = _cmd_handlers + i;

        if(handler->cmd == (packet->cmd & handler->mask)) {
            if(packet->length >= handler->min_length) {
                handler->callback(packet);
            }
        }
    }
}

void _CmdCb_Info(const Packet *packet)
{
    Packet tpacket;
    tpacket.src_id = Module_ID();
    tpacket.dest_id = packet->src_id;
    tpacket.cmd = packet->cmd;
    tpacket.priority = PRIORITY_VERY_LOW;
    tpacket.not_request = true;
    tpacket.length = 4;

    tpacket.data[0] = Module_HWID();
    tpacket.data[1] = Module_HWID() >> 8;
    tpacket.data[2] = Module_SWVersion();
    tpacket.data[3] = Module_SWVersion() >> 8;

    BXCAN_Message msg;
    Packet_ToMessage(&tpacket, &msg);
    BXCAN_Send(&hbxcan, &msg);
}

void _CmdCb_IOModes(const Packet *packet)
{
    // data<0:07> - output modes | 0='digital', 1='pwm'
    // data<8:27> - input modes | 00='digital', 01='analog',
    //   10='16-bit counter rising edge', 11='16-bit counter falling edge'

    uint32_t modes = packet->data[0] | ((uint32_t)packet->data[1] << 8)
        | ((uint32_t)packet->data[2] << 16) | ((uint32_t)packet->data[3] << 24);

    uint32_t output_modes = modes & 0x3FF;
    uint32_t input_modes = (modes >> 10) & 0xFFFFF;

    for(uint8_t i = 0; i < 10; ++i)
        IO_Mode(_OUTPUTS[i], (output_modes >> i) & 1);

    for(uint8_t i = 0; i < 10; ++i)
        IO_Mode(_INPUTS[i], (input_modes >> (i << 1)) & 3);
}

void _CmdCb_WriteDigitalOutputs(const Packet *packet)
{
    uint16_t states = packet->data[0] | ((uint16_t)packet->data[1] << 8);

    for(uint8_t i = 0; i < 10; ++i) {
        IO_Handle *hout = _OUTPUTS[i];
        if(hout->mode == IO_MODE_DIGITAL)
            IO_Write(hout, states & (1 << i) ? SET : RESET);
    }
}

void _CmdCb_WriteIO(const Packet *packet)
{
    uint16_t value = packet->data[1] | ((uint16_t)packet->data[2] << 8);
    uint8_t io_id = packet->data[0] & 0xF;
    uint8_t io_dir = (packet->data[0] >> 4) ? IO_DIR_INPUT : IO_DIR_OUTPUT;

    if(io_dir == IO_DIR_OUTPUT) {
        if(io_id < 10)
            IO_Write(_OUTPUTS[io_id], value);
    } else { // writing inputs can serve for resetting counters
        if(io_id < 10)
            IO_Write(_INPUTS[io_id], value);
    }
}

void _CmdCb_ReadDigitalInputs(const Packet *packet)
{
    Packet tpacket;
    tpacket.src_id = Module_ID();
    tpacket.dest_id = packet->src_id;
    tpacket.cmd = packet->cmd;
    tpacket.priority = PRIORITY_VERY_LOW;
    tpacket.not_request = true;
    tpacket.length = 2;

    uint16_t states = 0;

    for(uint8_t i = 0; i < 10; ++i) {
        IO_Handle *hin = _INPUTS[i];
        if(hin->mode != IO_MODE_DIGITAL)
            continue;
        if(IO_Read(hin))
            states |= 1 << i;
    }

    tpacket.data[0] = states;
    tpacket.data[1] = states >> 8;

    BXCAN_Message msg;
    Packet_ToMessage(&tpacket, &msg);
    BXCAN_Send(&hbxcan, &msg);
}

void Com_BroadcastDigitalInputs()
{
    Packet tpacket;
    tpacket.src_id = Module_ID();
    tpacket.dest_id = BROADCAST;
    tpacket.cmd = PCMD_RD_DINS;
    tpacket.priority = PRIORITY_VERY_LOW;
    tpacket.not_request = true;
    tpacket.length = 2;

    uint16_t states = 0;

    for(uint8_t i = 0; i < 10; ++i) {
        IO_Handle *hin = _INPUTS[i];
        if(hin->mode != IO_MODE_DIGITAL)
            continue;
        if(hin->value)
            states |= 1 << i;
    }

    tpacket.data[0] = states;
    tpacket.data[1] = states >> 8;

    BXCAN_Message msg;
    Packet_ToMessage(&tpacket, &msg);
    BXCAN_Send(&hbxcan, &msg);
}

void _CmdCb_ReadIO(const Packet *packet)
{
    Packet tpacket;
    tpacket.src_id = Module_ID();
    tpacket.dest_id = packet->src_id;
    tpacket.cmd = packet->cmd;
    tpacket.priority = PRIORITY_VERY_LOW;
    tpacket.not_request = true;
    tpacket.length = 3;

    uint8_t io_id = packet->data[0] & 0xF;
    uint8_t io_dir = (packet->data[0] >> 4) ? IO_DIR_INPUT : IO_DIR_OUTPUT;
    uint16_t value = 0;

    if(io_dir == IO_DIR_OUTPUT) {
        if(io_id < 10)
            value = IO_Read(_OUTPUTS[io_id]);
    } else {
        if(io_id < 10)
            value = IO_Read(_INPUTS[io_id]);
    }

    tpacket.data[0] = packet->data[0];
    tpacket.data[1] = value;
    tpacket.data[2] = value >> 8;

    BXCAN_Message msg;
    Packet_ToMessage(&tpacket, &msg);
    BXCAN_Send(&hbxcan, &msg);
}