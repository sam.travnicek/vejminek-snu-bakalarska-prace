#include "adc_mod.h"

volatile uint16_t adc1_values[4];
volatile uint16_t adc2_values[4];
volatile uint16_t adc3_values[1];
volatile uint16_t adc4_values[1];

void _ADC_GeneralSetup(ADC_TypeDef *adc)
{
    // Enable DMA, circular conversion, 12-bit mode, right alignment,
    // overwrite old data with new after conversion, continuous conversion mode
    adc->CFGR |= ADC_CFGR_DMACFG;
    adc->CFGR &= ~(0b11 << ADC_CFGR_RES_Pos);
    adc->CFGR &= ~ADC_CFGR_ALIGN;
    adc->CFGR |= ADC_CFGR_OVRMOD;
    adc->CFGR |= ADC_CFGR_CONT;
    adc->CFGR |= ADC_CFGR_DMAEN;
}

void _ADC_Calibration(ADC_TypeDef *adc)
{
    // Voltage regulator enable sequence
    adc->CR &= ~(0b11 << ADC_CR_ADVREGEN_Pos);
    adc->CR |= 0b01 << ADC_CR_ADVREGEN_Pos;
    HAL_Delay(1); // software delay required

    adc->CR &= ~ADC_CR_ADCALDIF; // single-ended input
    adc->CR |= ADC_CR_ADCAL; // start calibration
    while(adc->CR & ADC_CR_ADCAL);

    // Voltage regulator disable sequence
    adc->CR &= ~(0b11 << ADC_CR_ADVREGEN_Pos);
    adc->CR |= 0b10 << ADC_CR_ADVREGEN_Pos;
    HAL_Delay(1); // software delay required
}

void _ADC_Enable(ADC_TypeDef *adc)
{
    adc->CR |= ADC_CR_ADEN;
    while(!(adc->ISR & ADC_ISR_ADRDY));
}

void _ADC_Start(ADC_TypeDef *adc)
{
    adc->CR |= ADC_CR_ADSTART;
}

void _ADC1_CustomSetup()
{
    //--- DMA setup ------------------------------------------------------------

    DMA1_Channel1->CPAR = (uint32_t)(&(ADC1->DR)); // Peripheral address
    DMA1_Channel1->CMAR = (uint32_t)adc1_values; // Memory address
    DMA1_Channel1->CNDTR = sizeof(adc1_values)/sizeof(*adc1_values); // data len
    HAL_Delay(10);
    DMA1_Channel1->CCR |= DMA_CCR_CIRC; // circular mode
    DMA1_Channel1->CCR |= DMA_CCR_MINC; // memory increment mode
    DMA1_Channel1->CCR |= 0b01 << DMA_CCR_PSIZE_Pos; // pheripheral size 16-bits
    DMA1_Channel1->CCR |= 0b01 << DMA_CCR_MSIZE_Pos; // memory size 16-bits
    HAL_Delay(10);
    DMA1_Channel1->CCR |= DMA_CCR_EN; // enable DMA
    DMA1->IFCR |= 0b1111111111111111111111111111111;

    //--- Sampling setup -------------------------------------------------------

    // Sample time 601.5 ADC clock cycles for each channel
    ADC1->SMPR1 = 0;
    ADC1->SMPR2 = 0;

    ADC1->SMPR1 |= 0b111 << ADC_SMPR1_SMP1_Pos;
    ADC1->SMPR1 |= 0b111 << ADC_SMPR1_SMP2_Pos;
    ADC1->SMPR1 |= 0b111 << ADC_SMPR1_SMP3_Pos;
    ADC1->SMPR1 |= 0b111 << ADC_SMPR1_SMP4_Pos;

    //--- Sequence setup -------------------------------------------------------

    // 4 conversions, conversion sequence: CH1, CH2, CH3, CH4
    ADC1->SQR1 = 0;
    ADC1->SQR2 = 0;
    ADC1->SQR3 = 0;
    ADC1->SQR4 = 0;

    ADC1->SQR1 |= 3 << ADC_SQR1_L_Pos; // 4 conversions (write n-1)
    ADC1->SQR1 |= 1 << ADC_SQR1_SQ1_Pos;
    ADC1->SQR1 |= 2 << ADC_SQR1_SQ2_Pos;
    ADC1->SQR1 |= 3 << ADC_SQR1_SQ3_Pos;
    ADC1->SQR1 |= 4 << ADC_SQR1_SQ4_Pos;
}

void _ADC2_CustomSetup()
{
    //--- DMA setup ------------------------------------------------------------

    DMA2_Channel1->CPAR = (uint32_t)(&(ADC2->DR)); // Peripheral address
    DMA2_Channel1->CMAR = (uint32_t)adc2_values; // Memory address
    DMA2_Channel1->CNDTR = sizeof(adc2_values)/sizeof(*adc2_values); // data len
    HAL_Delay(10);
    DMA2_Channel1->CCR |= DMA_CCR_CIRC; // circular mode
    DMA2_Channel1->CCR |= DMA_CCR_MINC; // memory increment mode
    DMA2_Channel1->CCR |= 0b01 << DMA_CCR_PSIZE_Pos; // pheripheral size 16-bits
    DMA2_Channel1->CCR |= 0b01 << DMA_CCR_MSIZE_Pos; // memory size 16-bits
    HAL_Delay(10);
    DMA2_Channel1->CCR |= DMA_CCR_EN; // enable DMA
    DMA2->IFCR |= 0b1111111111111111111111111111111;

    //--- Sampling setup -------------------------------------------------------

    // Sample time 601.5 ADC clock cycles for each channel
    ADC2->SMPR1 = 0;
    ADC2->SMPR2 = 0;

    ADC2->SMPR1 |= 0b111 << ADC_SMPR1_SMP1_Pos;
    ADC2->SMPR1 |= 0b111 << ADC_SMPR1_SMP2_Pos;
    ADC2->SMPR1 |= 0b111 << ADC_SMPR1_SMP3_Pos;
    ADC2->SMPR2 |= 0b111 << ADC_SMPR2_SMP12_Pos;

    //--- Sequence setup -------------------------------------------------------

    // 4 conversions, conversion sequence: CH1, CH2, CH3, CH12
    ADC2->SQR1 = 0;
    ADC2->SQR2 = 0;
    ADC2->SQR3 = 0;
    ADC2->SQR4 = 0;

    ADC2->SQR1 |= 3 << ADC_SQR1_L_Pos; // 4 conversions (write n-1)
    ADC2->SQR1 |= 1 << ADC_SQR1_SQ1_Pos;
    ADC2->SQR1 |= 2 << ADC_SQR1_SQ2_Pos;
    ADC2->SQR1 |= 3 << ADC_SQR1_SQ3_Pos;
    ADC2->SQR1 |= 12 << ADC_SQR1_SQ4_Pos;
}

void _ADC3_CustomSetup()
{
    //--- DMA setup ------------------------------------------------------------

    DMA2_Channel5->CPAR = (uint32_t)(&(ADC3->DR)); // Peripheral address
    DMA2_Channel5->CMAR = (uint32_t)adc3_values; // Memory address
    DMA2_Channel5->CNDTR = sizeof(adc3_values)/sizeof(*adc3_values); // data len
    HAL_Delay(10);
    DMA2_Channel5->CCR |= DMA_CCR_CIRC; // circular mode
    DMA2_Channel5->CCR |= DMA_CCR_MINC; // memory increment mode
    DMA2_Channel5->CCR |= 0b01 << DMA_CCR_PSIZE_Pos; // pheripheral size 16-bits
    DMA2_Channel5->CCR |= 0b01 << DMA_CCR_MSIZE_Pos; // memory size 16-bits
    HAL_Delay(10);
    DMA2_Channel5->CCR |= DMA_CCR_EN; // enable DMA
    DMA2->IFCR |= 0b1111111111111111111111111111111;

    //--- Sampling setup -------------------------------------------------------

    // Sample time 601.5 ADC clock cycles for each channel
    ADC3->SMPR1 = 0;
    ADC3->SMPR2 = 0;

    ADC3->SMPR1 |= 0b111 << ADC_SMPR1_SMP1_Pos;

    //--- Sequence setup -------------------------------------------------------

    // 1 conversion, conversion sequence: CH5
    ADC3->SQR1 = 0;
    ADC3->SQR2 = 0;
    ADC3->SQR3 = 0;
    ADC3->SQR4 = 0;

    ADC3->SQR1 |= 0 << ADC_SQR1_L_Pos; // 1 conversion (write n-1)
    ADC3->SQR1 |= 5 << ADC_SQR1_SQ1_Pos;
}

void _ADC4_CustomSetup()
{
    //--- DMA setup ------------------------------------------------------------

    DMA2_Channel2->CPAR = (uint32_t)(&(ADC4->DR)); // Peripheral address
    DMA2_Channel2->CMAR = (uint32_t)adc4_values; // Memory address
    DMA2_Channel2->CNDTR = sizeof(adc4_values)/sizeof(*adc4_values); // data len
    HAL_Delay(10);
    DMA2_Channel2->CCR |= DMA_CCR_CIRC; // circular mode
    DMA2_Channel2->CCR |= DMA_CCR_MINC; // memory increment mode
    DMA2_Channel2->CCR |= 0b01 << DMA_CCR_PSIZE_Pos; // pheripheral size 16-bits
    DMA2_Channel2->CCR |= 0b01 << DMA_CCR_MSIZE_Pos; // memory size 16-bits
    HAL_Delay(10);
    DMA2_Channel2->CCR |= DMA_CCR_EN; // enable DMA
    DMA2->IFCR |= 0b1111111111111111111111111111111;

    //--- Sampling setup -------------------------------------------------------

    // Sample time 601.5 ADC clock cycles for each channel
    ADC4->SMPR1 = 0;
    ADC4->SMPR2 = 0;

    ADC4->SMPR1 |= 0b111 << ADC_SMPR1_SMP1_Pos;

    //--- Sequence setup -------------------------------------------------------

    // 1 conversion, conversion sequence: CH3
    ADC4->SQR1 = 0;
    ADC4->SQR2 = 0;
    ADC4->SQR3 = 0;
    ADC4->SQR4 = 0;

    ADC4->SQR1 |= 0 << ADC_SQR1_L_Pos; // 1 conversion (write n-1)
    ADC4->SQR1 |= 3 << ADC_SQR1_SQ1_Pos;
}

void ADC_Setup()
{
    // DMA mapping in Reference Manual @ page 270

    // Enable clock for ADC 1, 2, 3, 4
    RCC->AHBENR |= RCC_AHBENR_ADC12EN;
    RCC->AHBENR |= RCC_AHBENR_ADC34EN;

    // Enable clock for DMA
    RCC->AHBENR |= RCC_AHBENR_DMA1EN;
    RCC->AHBENR |= RCC_AHBENR_DMA2EN;

    // Clock divided by 6
    RCC->CFGR2 &= ~(0b1111111111 << RCC_CFGR2_ADCPRE12_Pos);
    RCC->CFGR2 |= 0b10011 << RCC_CFGR2_ADCPRE12_Pos;
    RCC->CFGR2 |= 0b10011 << RCC_CFGR2_ADCPRE34_Pos;

    _ADC_Calibration(ADC1);
    _ADC_Enable(ADC1);

    _ADC_Calibration(ADC2);
    _ADC_Enable(ADC2);

    _ADC_Calibration(ADC3);
    _ADC_Enable(ADC3);

    _ADC_Calibration(ADC4);
    _ADC_Enable(ADC4);

    _ADC_GeneralSetup(ADC1);
    _ADC1_CustomSetup();

    _ADC_GeneralSetup(ADC2);
    _ADC2_CustomSetup();

    _ADC_GeneralSetup(ADC3);
    _ADC3_CustomSetup();

    _ADC_GeneralSetup(ADC4);
    _ADC4_CustomSetup();

    // ADC12 common configuration
    ADC12_COMMON->CCR &= ~(0b11111 << ADC12_CCR_MULTI_Pos); // independent mode
    ADC12_COMMON->CCR |= ADC12_CCR_DMACFG; // DMA circular mode

    // ADC34 common configuration
    ADC34_COMMON->CCR &= ~(0b11111 << ADC34_CCR_MULTI_Pos); // independent mode
    ADC34_COMMON->CCR |= ADC34_CCR_DMACFG; // DMA circular mode

    _ADC_Start(ADC1);
    _ADC_Start(ADC2);
    _ADC_Start(ADC3);
    _ADC_Start(ADC4);
    HAL_Delay(1);
}