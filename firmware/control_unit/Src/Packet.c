#include "Packet.h"

void Packet_ToMessage(const Packet *packet, BXCAN_Message *msg)
{
    msg->identifier = (packet->priority << 27) | (packet->not_request << 26)
        | (packet->src_id << 19) | (packet->dest_id << 12) | packet->cmd;
    msg->extended_identifier = true;
    msg->remote_tx_request = false;
    msg->length = packet->length;

    for(uint8_t i = 0; i < packet->length; ++i)
        msg->data[i] = packet->data[i];
}

void Packet_FromMessage(const BXCAN_Message *msg, Packet *packet)
{
    packet->priority = (msg->identifier >> 27) & 0b11;
    packet->not_request = (msg->identifier >> 26) & 0b1;
    packet->src_id = (msg->identifier >> 19) & 0x7F;
    packet->dest_id = (msg->identifier >> 12) & 0x7F;
    packet->cmd = msg->identifier & 0xFFF;
    packet->length = msg->length;

    for(uint8_t i = 0; i < msg->length; ++i)
        packet->data[i] = msg->data[i];
}

void StrBuf_Packet(StrBuf *buf, const Packet *packet)
{
    StrBuf_String(buf, "Packet {\r\n");

    StrBuf_String(buf, "\t.priority = ");
    StrBuf_UInt32(buf, packet->priority);
    StrBuf_String(buf, "\r\n");

    StrBuf_String(buf, "\t.not_request = ");
    StrBuf_UInt32(buf, packet->not_request);
    StrBuf_String(buf, "\r\n");

    StrBuf_String(buf, "\t.src_id = ");
    StrBuf_UInt32(buf, packet->src_id);
    StrBuf_String(buf, "\r\n");

    StrBuf_String(buf, "\t.dest_id = ");
    StrBuf_UInt32(buf, packet->dest_id);
    StrBuf_String(buf, "\r\n");

    StrBuf_String(buf, "\t.cmd = ");
    StrBuf_UInt32(buf, packet->cmd);
    StrBuf_String(buf, "\r\n");

    StrBuf_String(buf, "\t.length = ");
    StrBuf_UInt32(buf, packet->length);
    StrBuf_String(buf, "\r\n");

    for(uint8_t i = 0; i < packet->length; ++i) {
        StrBuf_String(buf, "\t.data[");
        StrBuf_UInt32(buf, i);
        StrBuf_String(buf, "] = ");
        StrBuf_UInt32(buf, packet->data[i]);
        StrBuf_String(buf, "\r\n");
    }

    StrBuf_String(buf, "}");
}