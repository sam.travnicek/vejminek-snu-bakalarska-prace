#include "bxcan_mod.h"

BXCAN_Handle hbxcan = {
    .bxcan = CAN,
    .loopback_mode = false,
    .silent_mode = false,
    .auto_wakeup = false,
    .auto_retransmission = true,
    .auto_busoff_mgnt = true,
    .recv_fifo_locked = true,
    .chronological_tx = false,
    .prescaler = 44 //17
};

void BXCAN_Setup()
{
    // Enable clock for GPIOB
    RCC->AHBENR |= RCC_AHBENR_GPIOBEN;

    // Enable clock for bxCAN
    RCC->APB1ENR |= RCC_APB1ENR_CANEN;

    // PB9 - bxCAN Tx, PB8 - bxCAN Rx
    GPIOB->AFR[1] &= ~0b11111111; // In stm32f303cc datasheet AF codes
    GPIOB->AFR[1] |= (GPIO_AF9_CAN << 4) | GPIO_AF9_CAN;

    // PB8, PB9 - Alternate function mode
    GPIOB->MODER &= ~(0b1111 << GPIO_MODER_MODER8_Pos);
    GPIOB->MODER |= 0b1010 << GPIO_MODER_MODER8_Pos;

    // PB9 - Push-pull, High speed
    GPIOB->OTYPER &= ~GPIO_OTYPER_OT_9;
    GPIOB->OSPEEDR &= ~GPIO_OSPEEDER_OSPEEDR9;
    GPIOB->OSPEEDR |= 0b11 << GPIO_OSPEEDER_OSPEEDR9_Pos;

    // P8 - Pull-up
    GPIOB->PUPDR &= ~GPIO_PUPDR_PUPDR8;
    GPIOB->PUPDR |= 0b01 << GPIO_PUPDR_PUPDR8_Pos;
}

void BXCAN_Init(BXCAN_Handle *hbxcan)
{
    CAN_TypeDef *can = hbxcan->bxcan;

    // Enter Initilization Mode
    can->MCR |= CAN_MCR_INRQ;
    can->MCR &= ~CAN_MCR_SLEEP;
    while(!(can->MSR & CAN_MSR_INAK));

    can->MCR = 0x00010001u; // Basic configuration
    can->BTR = 0x01230000u;

    if(hbxcan->auto_busoff_mgnt)
        can->MCR |= CAN_MCR_ABOM;

    if(hbxcan->auto_wakeup)
        can->MCR |= CAN_MCR_AWUM; // Enable Automatic wakeup mode

    if(!hbxcan->auto_retransmission)
        can->MCR |= CAN_MCR_NART; // Enable Automatic retransmission on error

    if(hbxcan->recv_fifo_locked)
        can->MCR |= CAN_MCR_RFLM; // Discard incoming messages when FIFO is full

    if(hbxcan->chronological_tx)
        can->MCR |= CAN_MCR_TXFP; // Messages sent in chronological order

    if(hbxcan->loopback_mode)
        can->BTR |= CAN_BTR_LBKM; // Enable Loop back mode

    if(hbxcan->silent_mode)
        can->BTR |= CAN_BTR_SILM; // Enable Silent mode

    can->BTR |= hbxcan->prescaler & 0x3FF;

    // Setup Filters
    can->FMR |= CAN_FMR_FINIT;

    can->FA1R = 0; // Deactivate all filters
    can->FFA1R = 0; // Reset assignments to FIFO buffers
    can->FM1R = 0; // Reset filter mode configurations
    can->FS1R = 0; // Reset filter scale configurations

    // Filter 0 - FIFO 0, Mask mode, 32-bit, Accept all IDs
    can->FS1R |= CAN_FS1R_FSC0;
    can->sFilterRegister[0].FR1 = 0;
    can->sFilterRegister[0].FR2 = 0;
    can->FA1R |= CAN_FA1R_FACT0;

    // Filter 1 - FIFO 1, Mask mode, 32-bit, Accept all IDs
    can->FFA1R |= CAN_FFA1R_FFA1;
    can->FS1R |= CAN_FS1R_FSC1;
    can->sFilterRegister[1].FR1 = 0;
    can->sFilterRegister[1].FR2 = 0;
    can->FA1R |= CAN_FA1R_FACT1;

    can->FMR &= ~CAN_FMR_FINIT;

    // Enter Normal Mode
    can->MCR &= ~CAN_MCR_INRQ;
    while(can->MSR & CAN_MSR_INAK);
}

void BXCAN_Terminate(BXCAN_Handle *hbxcan)
{
}

uint8_t BXCAN_Available(BXCAN_Handle *hbxcan)
{
    CAN_TypeDef *can = hbxcan->bxcan;

    uint8_t avail = can->RF0R & CAN_RF0R_FMP0;
    avail += can->RF1R & CAN_RF1R_FMP1;

    return avail;
}

bool BXCAN_Send(BXCAN_Handle *hbxcan, BXCAN_Message *msg)
{
    if(msg->length > 8u)
        return false;

    CAN_TypeDef *can = hbxcan->bxcan;
    CAN_TxMailBox_TypeDef *mailbox = NULL;

    // Select first empty mailbox
    if(can->TSR & CAN_TSR_TME0)
        mailbox = can->sTxMailBox; // Transmit mailbox 0 empty
    else if(can->TSR & CAN_TSR_TME1)
        mailbox = can->sTxMailBox + 1; // Transmit mailbox 1 empty
    else if(can->TSR & CAN_TSR_TME2)
        mailbox = can->sTxMailBox + 2; // Transmit mailbox 2 empty
    else
        return false; // No mailbox empty;

    if(msg->extended_identifier)
        mailbox->TIR = ((msg->identifier & 0x1FFFFFFF) << 3) | (1 << 2)
            | (msg->remote_tx_request << 1);
    else
        mailbox->TIR = ((msg->identifier & 0x7FF) << 21)
            | (msg->remote_tx_request << 1);

    mailbox->TDTR &= ~0b1111u;
    mailbox->TDTR |= msg->length;

    mailbox->TDLR = 0;
    mailbox->TDHR = 0;

    for(uint8_t i = 0; i < msg->length; ++i) {
        if(i < 4)
            mailbox->TDLR |= ((uint32_t)(msg->data[i])) << (8*i);
        else
            mailbox->TDHR |= ((uint32_t)(msg->data[i])) << (8*(i-4));
    }

    mailbox->TIR |= 1; // Request transmission

    return true;
}

bool BXCAN_Recv(BXCAN_Handle *hbxcan, BXCAN_Message *msg)
{
    CAN_TypeDef *can = hbxcan->bxcan;
    CAN_FIFOMailBox_TypeDef *mailbox = NULL;

    if(can->RF0R & CAN_RF0R_FMP0)
        mailbox = can->sFIFOMailBox;
    else if(can->RF1R & CAN_RF1R_FMP1)
        mailbox = can->sFIFOMailBox + 1;
    else
        return false;

    if(mailbox->RIR & (1 << 2)) {
        msg->extended_identifier = true;
        msg->identifier = (mailbox->RIR >> 3) & 0x1FFFFFFF;
    } else {
        msg->extended_identifier = false;
        msg->identifier = (mailbox->RIR >> 21) & 0x7FF;
    }

    msg->remote_tx_request = (mailbox->RIR >> 1) & 1;
    msg->length = mailbox->RDTR & 0b1111;

    for(uint8_t i = 0; i < msg->length; ++i) {
        if(i < 4)
            msg->data[i] = (mailbox->RDLR >> (8*i)) & 0xFF;
        else
            msg->data[i] = (mailbox->RDHR >> (8*(i-4))) & 0xFF;
    }

    // Release mailbox
    if(mailbox == can->sFIFOMailBox)
        can->RF0R |= CAN_RF0R_RFOM0;
    else
        can->RF1R |= CAN_RF1R_RFOM1;

    return true;
}