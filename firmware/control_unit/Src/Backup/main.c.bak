/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "i2c.h"
#include "spi.h"
#include "tim.h"
#include "usb_device.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdint.h>
#include <math.h>
#include "pwm_mod.h"
#include "io_expander_mod.h"
#include "bxcan_mod.h"
#include "timers_mod.h"
#include "com_mod.h"
#include "indicator_mod.h"
#include "gpio_mod.h"
#include "module_mod.h"
#include "Packet.h"
#include "usbd_cdc_if.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

uint32_t _indicator0_clk = 0;
uint32_t _indicator1_clk = 0;

IOExpander ioexp1;
IOExpander ioexp2;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */


  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C2_Init();
  MX_SPI3_Init();
  MX_TIM3_Init();
  MX_USB_DEVICE_Init();
  /* USER CODE BEGIN 2 */

  //--- MODULE SETUP -----------------------------------------------------------

  Module_Setup();

  //--- IO SETUP ---------------------------------------------------------------

  IO_Setup();
  IO_Init(&hout0);
  IO_Init(&hout1);
  IO_Init(&hout2);
  IO_Init(&hout3);
  IO_Init(&hout4);
  IO_Init(&hout5);
  IO_Init(&hout6);
  IO_Init(&hout7);
  IO_Init(&hout8);
  IO_Init(&hout9);
  IO_Init(&hin0);
  IO_Init(&hin1);
  IO_Init(&hin2);
  IO_Init(&hin3);
  IO_Init(&hin4);
  IO_Init(&hin5);
  IO_Init(&hin6);
  IO_Init(&hin7);
  IO_Init(&hin8);
  IO_Init(&hin9);


  //--- TIMERS SETUP -----------------------------------------------------------

  Timers_Setup();

  Timer_Init(&htimer17);

  Timer_Init(&htim3);
  Timer_EnablePWM(&htim3, 2);
  Timer_PWMDutyCycle(&htim3, 2, (1 << 15) - 1);

  //--- BXCAN SETUP ------------------------------------------------------------

  BXCAN_Setup();
  BXCAN_Init(&hbxcan);

  //--- IO EXPANDER SETUP ------------------------------------------------------

  IOExpander_Init(&ioexp1, IOEXP_LADDR);
  IOExpander_Init(&ioexp2, IOEXP_RADDR);

  // Expander -> 0 = output, 1 = input
  IOExpander_Write8(&ioexp1, 0b10111011);
  IOExpander_Write8(&ioexp2, 0b10000111);
  IOExpander_Write(&ioexp1, 6, SET);

  //--- TJA1055 Reset-Enable ---------------------------------------------------

  IOExpander_Write(&ioexp2, 3, SET);
  HAL_GPIO_WritePin(nSTB_GPIO_Port, nSTB_Pin, SET);
  HAL_Delay(200);
  IOExpander_Write(&ioexp2, 3, RESET);
  HAL_GPIO_WritePin(nSTB_GPIO_Port, nSTB_Pin, RESET);
  HAL_Delay(200);
  IOExpander_Write(&ioexp2, 3, SET);
  HAL_GPIO_WritePin(nSTB_GPIO_Port, nSTB_Pin, SET);

  //--- Main program -----------------------------------------------------------

  IO_Mode(&hout0, IO_MODE_PWM);
  //IO_Write(&hout0, )

  uint32_t cntr = 0;

  /* USER CODE END 2 */



  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while(1) {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    if(!Timer_Updated(&htimer17))
      continue;

    IO_Write(&hout0, IO_Read(&hin0));

    BXCAN_Message rmsg;
    Packet rpacket;

    while(BXCAN_Recv(&hbxcan, &rmsg)) {
      Packet_FromMessage(&rmsg, &rpacket);
      Com_PacketReceived(&rpacket);
    }

    if(_indicator0_clk > 0) {
      if(--_indicator0_clk == 0)
        IOExpander_Write(&ioexp1, 6, SET);
    }

    if(_indicator1_clk > 0) {
      if(--_indicator1_clk == 0)
        IOExpander_Write(&ioexp1, 2, SET);
    }

    if(cntr == 0)
      Indicator_Pulse(1, 40);
    if(++cntr >= 4000)
      cntr = 0;
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB|RCC_PERIPHCLK_I2C2;
  PeriphClkInit.I2c2ClockSelection = RCC_I2C2CLKSOURCE_SYSCLK;
  PeriphClkInit.USBClockSelection = RCC_USBCLKSOURCE_PLL_DIV1_5;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

void Indicator_Pulse(uint8_t num, uint32_t duration_ticks)
{
  if(num == 0) {
    _indicator0_clk = duration_ticks;
    IOExpander_Write(&ioexp1, 6, RESET);
  } else if(num == 1) {
    _indicator1_clk = duration_ticks;
    IOExpander_Write(&ioexp1, 2, RESET);
  }
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
