#include "io_expander_mod.h"
#include "i2c.h"
#include "usbd_cdc_if.h"

void _IOExpander_ReadGPIO(IOExpander *ioexp)
{
    //uint8_t ret =
    HAL_I2C_Master_Receive(&hi2c2, ioexp->addr, &(ioexp->read), 1u, ioexp->timeout);

    // USB_WriteString("HAL_I2C_Master_Receive: ");
    // USB_WriteUInt32(ret);
    // USB_WriteString("\r\n");
    // USB_FlushTx();
}

void _IOExpander_UpdateGPIO(IOExpander *ioexp)
{
    //uint8_t ret =
    HAL_I2C_Master_Transmit(&hi2c2, ioexp->addr, &(ioexp->pins), 1u, ioexp->timeout);

    // USB_WriteString("HAL_I2C_Master_Transmit: ");
    // USB_WriteUInt32(ret);
    // USB_WriteString("\r\n");
    // USB_FlushTx();
}

void IOExpander_Init(IOExpander *ioexp, uint8_t address)
{
    ioexp->addr = address;
    ioexp->timeout = HAL_MAX_DELAY;
    ioexp->read = 0;
    ioexp->pins = 0;
    _IOExpander_ReadGPIO(ioexp);
}

void IOExpander_Write(IOExpander *ioexp, uint8_t pin, uint8_t value)
{
    if(value)
        ioexp->pins |= 1 << pin;
    else
        ioexp->pins &= ~(1 << pin);

    _IOExpander_UpdateGPIO(ioexp);
}

void IOExpander_Write8(IOExpander *ioexp, uint8_t pins)
{
    ioexp->pins = pins;
    _IOExpander_UpdateGPIO(ioexp);
}

uint8_t IOExpander_Read(IOExpander *ioexp, uint8_t pin)
{
    _IOExpander_ReadGPIO(ioexp);
    return (ioexp->read & (1 << pin)) ? SET : RESET;
}

uint8_t IOExpander_Read8(IOExpander *ioexp)
{
    _IOExpander_ReadGPIO(ioexp);
    return ioexp->read;
}

void IOExpander_Toggle(IOExpander *ioexp, uint8_t pin)
{
    ioexp->pins ^= 1 << pin;
    _IOExpander_UpdateGPIO(ioexp);
}