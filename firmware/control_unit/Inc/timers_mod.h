#ifndef TIMERS_MOD_H_
#define TIMERS_MOD_H_

// STM32F303CC - Timers Module

#include "main.h"
#include <math.h>
#include <stdbool.h>

typedef struct
{
    TIM_TypeDef *timer;
    uint16_t prescaler;
    uint16_t reload_value;
    bool update_interrupt;

} Timer_Handle;

extern Timer_Handle htimer1;
extern Timer_Handle htimer2;
extern Timer_Handle htimer3;
extern Timer_Handle htimer4;
extern Timer_Handle htimer8;
extern Timer_Handle htimer15;
extern Timer_Handle htimer16;
extern Timer_Handle htimer17;

void Timers_Setup();

void Timer_Init(Timer_Handle *htimer);

void Timer_EnablePWM(Timer_Handle *htimer, uint8_t channel);

void Timer_PWMDutyCycle(Timer_Handle *htimer, uint8_t channel, uint32_t dc);

bool Timer_Updated(Timer_Handle *htimer);

void TIM_ClockEnable(TIM_TypeDef *timer);

float TIM_ClockFrequency(TIM_TypeDef *timer);


#endif // TIMERS_MOD_H_