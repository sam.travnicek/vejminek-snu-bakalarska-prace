#ifndef IO_EXPANDER_MOD_H_
#define IO_EXPANDER_MOD_H_

#include <stdint.h>

#define IOEXP_LADDR     0x70u
#define IOEXP_RADDR     0x72u

#define IOEXP_INPUT     1
#define IOEXP_OUTPUT    0

typedef struct
{
    uint8_t pins;
    uint8_t read;
    uint8_t addr;       // I2C slave address (shifted left)
    uint32_t timeout;

} IOExpander;

void IOExpander_Init(IOExpander *ioexp, uint8_t address);
void IOExpander_Write(IOExpander *ioexp, uint8_t pin, uint8_t value);
void IOExpander_Write8(IOExpander *ioexp, uint8_t pins);
uint8_t IOExpander_Read(IOExpander *ioexp, uint8_t pin);
uint8_t IOExpander_Read8(IOExpander *ioexp);
void IOExpander_Toggle(IOExpander *ioexp, uint8_t pin);


#endif // IO_EXPANDER_MOD_H_