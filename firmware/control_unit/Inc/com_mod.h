#ifndef COM_MOD_H_
#define COM_MOD_H_

#include "Packet.h"

#define PCMD_RESET          0x000
#define PCMD_INFO           0x001
#define PCMD_IO_MODES       0x010
#define PCMD_WR_DOUTS       0x020
#define PCMD_WR_IO          0x021
#define PCMD_RD_DINS        0x030
#define PCMD_RD_IO          0x031

void Com_PacketReceived(const Packet *packet);

void Com_BroadcastDigitalInputs();

#endif // COM_MOD_H_