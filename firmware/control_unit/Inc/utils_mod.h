#ifndef UTILS_MOD_H_
#define UTILS_MOD_H_

#include <stdint.h>
#include <stdbool.h>

uint32_t cpow10(uint8_t exponent);

bool StartsWith(const char *str, const char *what);

uint16_t ReadUInt32(const char *str, uint32_t *number);

#endif // UTILS_MOD_H_