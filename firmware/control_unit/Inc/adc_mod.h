#ifndef ADC_MOD_H_
#define ADC_MOD_H_

#include "main.h"

extern volatile uint16_t adc1_values[4];
extern volatile uint16_t adc2_values[4];
extern volatile uint16_t adc3_values[1];
extern volatile uint16_t adc4_values[1];

void ADC_Setup();
uint16_t ADC_Read();

#endif // ADC_MOD_H_