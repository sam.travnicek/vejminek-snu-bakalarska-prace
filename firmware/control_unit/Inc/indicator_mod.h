#ifndef INDICATOR_MOD_H_
#define INDICATOR_MOD_H_

#include <stdint.h>

void Indicator_Pulse(uint8_t num, uint32_t duration_ticks);

#endif // INDICATOR_MOD_H_