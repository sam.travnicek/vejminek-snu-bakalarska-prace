#ifndef GPIO_MOD_H_
#define GPIO_MOD_H_

#include <stdint.h>
#include <stdbool.h>
#include "main.h"

#define IO_DIR_OUTPUT   0
#define IO_DIR_INPUT    1

#define IO_MODE_DIGITAL         0b00
#define IO_MODE_PWM             0b01
#define IO_MODE_ANALOG          0b01
#define IO_MODE_COUNTER_RISE    0b10
#define IO_MODE_COUNTER_FALL    0b11

typedef volatile struct
{
    GPIO_TypeDef *gpio;
    uint8_t pin;

    uint8_t direction;
    uint8_t mode;
    uint16_t prev_value;
    uint16_t value;

    volatile uint16_t *adc_value_ptr;

} IO_Handle;

extern IO_Handle hout0;
extern IO_Handle hout1;
extern IO_Handle hout2;
extern IO_Handle hout3;
extern IO_Handle hout4;
extern IO_Handle hout5;
extern IO_Handle hout6;
extern IO_Handle hout7;
extern IO_Handle hout8;
extern IO_Handle hout9;

extern IO_Handle hin0;
extern IO_Handle hin1;
extern IO_Handle hin2;
extern IO_Handle hin3;
extern IO_Handle hin4;
extern IO_Handle hin5;
extern IO_Handle hin6;
extern IO_Handle hin7;
extern IO_Handle hin8;
extern IO_Handle hin9;

void IO_Setup();

void IO_Init(IO_Handle *io);

void IO_Mode(IO_Handle *io, uint8_t mode);

void IO_Write(IO_Handle *io, uint16_t value);

uint16_t IO_Read(IO_Handle *io);

#endif // GPIO_MOD_H_