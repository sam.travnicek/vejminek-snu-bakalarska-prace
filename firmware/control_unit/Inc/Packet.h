#ifndef PACKET_MOD_H_
#define PACKET_MOD_H_

#include <stdint.h>
#include "StrBuf.h"
#include "bxcan_mod.h"

#define PRIORITY_VERY_LOW   0b11
#define PRIORITY_LOW        0b10
#define PRIORITY_HIGH       0b01
#define PRIORITY_VERY_HIGH  0b00

typedef struct
{
    uint32_t not_request : 1;
    uint32_t priority : 2;
    uint32_t src_id : 7;
    uint32_t dest_id : 7;
    uint32_t cmd : 12;
    uint8_t length : 4;
    uint8_t data[8];

} Packet;

void Packet_ToMessage(const Packet *packet, BXCAN_Message *msg);
void Packet_FromMessage(const BXCAN_Message *msg, Packet *packet);

void StrBuf_Packet(StrBuf *buf, const Packet *packet);

#endif // PACKET_MOD_H_