#ifndef MODULE_MOD_H_
#define MODULE_MOD_H_

#include <stdint.h>
#include <stdbool.h>
#include "main.h"

void Module_Setup();

uint8_t Module_ID();

uint16_t Module_HWID();

uint16_t Module_SWVersion();

#endif // MODULE_MOD_H_